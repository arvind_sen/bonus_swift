//
//  BankAccountInfo.swift
//  Bonus
//
//  Created by Arvind Sen on 13/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit

class BankAccountInfo: NSObject {
    // String() created empty string
    var AccountHolderName: String?  = String()
    var BankAccountIFSCCode: String?  = String()
    var BankSwiftCode: String?  = String() // For use in UAE
    var BankAccountNumber: String?  = String()
    var BankName: String?  = String()
    var BranchName: String?  = String()
    var City: String?  = String()
    var CountryID: String?  = String()
    var CreatedDate: String?  = String()
    var ID: String?  = String()
    var TransactionType: String? = String()
    var IsActive: String? = String()
    var UserID: String? = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>) {
        self.AccountHolderName = data["AccountHolderName"] as? String
        self.BankAccountIFSCCode = data["BankAccountIFSCCode"] as? String
        self.BankAccountNumber = data["BankAccountNumber"] as? String
        self.BankSwiftCode = data["SwiftCode"] as? String
        self.BankName = data["BankName"] as? String
        self.BranchName = data["BranchName"] as? String
        self.City = data["City"] as? String
        self.CountryID = data["CountryID"] as? String
        self.CreatedDate = data["CreatedDate"] as? String
        self.ID = data["ID"] as? String
        self.IsActive = data["IsActive"] as? String
        self.UserID = data["UserID"] as? String
    }
}
