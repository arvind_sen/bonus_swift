//
//  Loyalty.swift
//  Bonus
//
//  Created by Arvind Sen on 10/09/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit

class Loyalty: NSObject {

    // String() created empty string
    var AvalDealInNumber: String?  = String()
    var BranchAddress: String?  = String()
    var BranchDescription: String?  = String()
    var BranchID: String?  = String()
    
    var BusinessCategory: String?  = String()
    var BusinessLogoImagePath: String?  = String()
    var BusinessUserStatus: String?  = String()
    var CategoryID: String?  = String()
    
    var CategoryName: String?  = String()
    var CategoryPicture: String?  = String()
    var CurrencyCode: String?  = String()
    var CurrentPunchesRequired: String?  = String()
    
    var DealImage1: String?  = String()
    var DealMasterID: String?  = String()
    var DealPrice: String?  = String()
    var Description: String?  = String()
    
    var Distance : String? = String()
    var EndDate : String? = String()
    var FavoriteID : String? = String()
    var FavoriteStatus : String? = String()
    
    var FilledCategoryPicture : String? = String()
    var FirstName : String? = String()
    var IsImageVerified : String? = String()
    var IsPunchCardLoyality : String? = String()
    
    var Latitude : String? = String()
    var Longitude : String? = String()
    var LoyalityDesciption : String? = String()
    var LoyaltyID : String? = String()
    
    var MapPinCategoryPicture : String? = String()
    var NumberOfBalanceFreeBee : String? = String()
    var PersonType : String? = String()
    var PromotionEndDate : String? = String()
    
    var PromotionStartDate : String? = String()
    var PunchCount : String? = String()
    var PunchRequiredForFreeBee : String? = String()
    var StartDate : String? = String()
    
    var TotalNoOfDeal : String? = String()
    var TypeOfCoupon : String? = String()
    var UserID : String? = String()
    
    var BonusPotLocationID : String? = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>) {
        
        self.AvalDealInNumber = data["AvalDealInNumber"] as? String
        self.BranchAddress = data["BranchAddress"] as? String
        self.BranchDescription = data["BranchDescription"] as? String
        self.BranchID = data["BranchID"] as? String
        
        self.BusinessCategory = data["BusinessCategory"] as? String
        self.BusinessLogoImagePath = data["BusinessLogoImagePath"] as? String
        self.BusinessUserStatus = data["BusinessUserStatus"] as? String
        self.CategoryID = data["CategoryID"] as? String
        
        self.CategoryName = data["CategoryName"] as? String
        self.CategoryPicture = data["CategoryPicture"] as? String
        
        self.CurrencyCode = data["CurrencyCode"] as? String
        self.CurrentPunchesRequired = data["CurrentPunchesRequired"] as? String
        
        self.DealImage1 = data["DealImage1"] as? String
        self.DealMasterID = data["DealMasterID"] as? String
        self.DealPrice = data["DealPrice"] as? String
        self.Description = data["Description"] as? String
        
        self.Distance = data["Distance"] as? String
        self.EndDate = data["EndDate"] as? String
        self.FavoriteID = data["FavoriteID"] as? String
        self.FavoriteStatus = data["FavoriteStatus"] as? String
        
        self.FilledCategoryPicture = data["FilledCategoryPicture"] as? String
        self.FirstName = data["FirstName"] as? String
        
        if(data["IsImageVerified"] != nil){
            self.IsImageVerified = data["IsImageVerified"] as? String
        }
        else if(data["IsBusinessImageVerified"] != nil){
            self.IsImageVerified = data["IsBusinessImageVerified"] as? String
        }
        
        self.IsPunchCardLoyality = data["IsPunchCardLoyality"] as? String
        
        self.Latitude = data["Latitude"] as? String
        self.Longitude = data["Longitude"] as? String
        self.LoyalityDesciption = data["LoyalityDesciption"] as? String
        self.LoyaltyID = data["LoyaltyID"] as? String
        
        self.MapPinCategoryPicture = data["MapPinCategoryPicture"] as? String
        self.NumberOfBalanceFreeBee = data["NumberOfBalanceFreeBee"] as? String
        self.PersonType = data["PersonType"] as? String
        self.PromotionEndDate = data["PromotionEndDate"] as? String
        
        self.PromotionStartDate = data["PromotionStartDate"] as? String
        self.PunchCount = data["PunchCount"] as? String
        self.PunchRequiredForFreeBee = data["PunchRequiredForFreeBee"] as? String
        self.StartDate = data["StartDate"] as? String
        
        self.TotalNoOfDeal = data["TotalNoOfDeal"] as? String
        self.TypeOfCoupon = data["TypeOfCoupon"] as? String
        self.UserID = data["UserID"] as? String
        
        
        if (data["BonusPotLocationID"] != nil){
            self.BonusPotLocationID = data["BonusPotLocationID"] as? String;
        }
        
        // Creating image full path here
        //let iconBasePath = resourcePaths["CategoryIconsPath"] as? String;
        //let BusinessLogoImagePath = resourcePaths["BusinessLogoImagePath"] as? String;
        //let dealImagePath = resourcePaths["DealImage1"] as? String;
        
        //self.CategoryPicture = iconBasePath!+"/"+self.CategoryID!+"/"+self.CategoryPicture!;
        //self.MapPinCategoryPicture = iconBasePath!+"/"+self.CategoryID!+"/"+self.MapPinCategoryPicture!;
        //self.FilledCategoryPicture = iconBasePath!+"/"+self.CategoryID!+"/"+self.FilledCategoryPicture!;
        
        //if(self.BusinessLogoImagePath != nil)
        //{
        //    self.BusinessLogoImagePath = BusinessLogoImagePath!+"/"+self.UserID!+"/"+self.BusinessLogoImagePath!;
        //}
        //self.DealImage1 = dealImagePath!+"/"+self.UserID!+"/"+self.DealImage1!;
    }
    
    init(data:Dictionary<String, AnyObject>, resourcePaths: Dictionary<String, AnyObject>) {
        
        self.AvalDealInNumber = data["AvalDealInNumber"] as? String
        self.BranchAddress = data["BranchAddress"] as? String
        self.BranchDescription = data["BranchDescription"] as? String
        self.BranchID = data["BranchID"] as? String
        
        self.BusinessCategory = data["BusinessCategory"] as? String
        self.BusinessLogoImagePath = data["BusinessLogoImagePath"] as? String
        self.BusinessUserStatus = data["BusinessUserStatus"] as? String
        self.CategoryID = data["CategoryID"] as? String
        
        self.CategoryName = data["CategoryName"] as? String
        self.CategoryPicture = data["CategoryPicture"] as? String
        
        self.CurrencyCode = data["CurrencyCode"] as? String
        self.CurrentPunchesRequired = data["CurrentPunchesRequired"] as? String
        
        self.DealImage1 = data["DealImage1"] as? String
        self.DealMasterID = data["DealMasterID"] as? String
        self.DealPrice = data["DealPrice"] as? String
        self.Description = data["Description"] as? String
        
        self.Distance = data["Distance"] as? String
        self.EndDate = data["EndDate"] as? String
        self.FavoriteID = data["FavoriteID"] as? String
        self.FavoriteStatus = data["FavoriteStatus"] as? String
        
        self.FilledCategoryPicture = data["FilledCategoryPicture"] as? String
        self.FirstName = data["FirstName"] as? String
        self.IsImageVerified = data["IsImageVerified"] as? String
        self.IsPunchCardLoyality = data["IsPunchCardLoyality"] as? String
        
        self.Latitude = data["Latitude"] as? String
        self.Longitude = data["Longitude"] as? String
        self.LoyalityDesciption = data["LoyalityDesciption"] as? String
        self.LoyaltyID = data["LoyaltyID"] as? String
        
        self.MapPinCategoryPicture = data["MapPinCategoryPicture"] as? String
        self.NumberOfBalanceFreeBee = data["NumberOfBalanceFreeBee"] as? String
        self.PersonType = data["PersonType"] as? String
        self.PromotionEndDate = data["PromotionEndDate"] as? String
        
        self.PromotionStartDate = data["PromotionStartDate"] as? String
        self.PunchCount = data["PunchCount"] as? String
        self.PunchRequiredForFreeBee = data["PunchRequiredForFreeBee"] as? String
        self.StartDate = data["StartDate"] as? String
        
        self.TotalNoOfDeal = data["TotalNoOfDeal"] as? String
        self.TypeOfCoupon = data["TypeOfCoupon"] as? String
        self.UserID = data["UserID"] as? String
        
        
        if (data["BonusPotLocationID"] != nil){
            self.BonusPotLocationID = data["BonusPotLocationID"] as? String;
        }
        
        // Creating image full path here
        let iconBasePath = resourcePaths["CategoryIconsPath"] as? String;
        let BusinessLogoImagePath = resourcePaths["BusinessLogoImagePath"] as? String;
        let dealImagePath = resourcePaths["DealImage1"] as? String;
        
        self.CategoryPicture = iconBasePath! + "/" + self.CategoryID! + "/" + self.CategoryPicture!;
        self.MapPinCategoryPicture = iconBasePath! + "/" + self.CategoryID! + "/" + self.MapPinCategoryPicture!;
        self.FilledCategoryPicture = iconBasePath! + "/" + self.CategoryID! + "/" + self.FilledCategoryPicture!;
        
        if(self.BusinessLogoImagePath != nil)
        {
            self.BusinessLogoImagePath = BusinessLogoImagePath!+"/"+self.UserID!+"/"+self.BusinessLogoImagePath!;
        }
        self.DealImage1 = dealImagePath!+"/"+self.UserID!+"/"+self.DealImage1!;
    }
}
