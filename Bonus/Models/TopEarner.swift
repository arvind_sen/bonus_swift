//
//  TopEarner.swift
//  Bonus
//
//  Created by Arvind Sen on 11/07/17.
//  Copyright © 2016 Bhumati. All rights reserved.
//

class TopEarner: NSObject {

    var AmountInBaseCurrency : String?  = String()
    var AmountInHomeCurrency : String?  = String()
    var FirstName : String?  = String()
    var NickName : String?  = String()
    var UserID : String?  = String()
    var sBaseCurrency : String?  = String()
    var sHomeCurrency : String?  = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>) {
        
        self.AmountInBaseCurrency = data["AmountInBaseCurrency"] as? String
        self.AmountInHomeCurrency = data["AmountInHomeCurrency"] as? String
        self.FirstName = data["FirstName"] as? String
        self.NickName = data["NickName"] as? String
        self.UserID = data["UserID"] as? String
        self.sBaseCurrency = data["sBaseCurrency"] as? String
        self.sHomeCurrency = data["sHomeCurrency"] as? String
    }
}
