//
//  ActiveBusiness.swift
//  Bonus
//
//  Created by Arvind Sen on 18/01/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit

class BranchCoupon: NSObject {

    // String() created empty string
    var AboutUs: String? = String()
    var Address: String? = String()
    var AreaName: String? = String()
    var AvalDealInNumber: String?  = String()
    var BonusPotCurrencyCode: String?  = String()
    var BonusPotLocationID: String?  = String()
    var BonusPotMaturityAmount: String?  = String()
    var BonusPotName: String?  = String()
    var BranchDescription: String?  = String()
    var BranchID: String?  = String()
    var BranchSpecific: String?  = String()
    var BusinessCategory: String?  = String()
    var BusinessLogoImagePath: String?  = String()
    //var BuyEndDateExpire: String?  = String()
    
    var CategoryName: String?  = String()
    var CategoryPicture: String?  = String()
    var CurrencyCode: String?  = String()
    var CurrentAmount: String?  = String()
    
    var DealImage1: String?  = String()
    var DealMasterID: String?  = String()
    var DealPerCustomer: String?  = String()
    var DealPrice: String?  = String()
    var Description: String?  = String()
    
    var Distance : String? = String()
    var EndDate : String? = String()
    var FavoriteID : String? = String()
    var FavoriteStatus : String? = String()
    var FavoriteUserID :String? = String()
    var FilledCategoryPicture : String? = String()
    var FirstName : String? = String()
    var IsBusinessImageVerified : String? = String()
    var IsDealImageVerified : String? = String()
    var IsPunchCardLoyality : String? = String()
    
    var Latitude : String? = String()
    var Longitude : String? = String()
    var LoyaltyID : String? = String()
    
    var MapPinCategoryPicture : String? = String()
    var MaxRating : String? = String()
    var NoOfActiveCoupan : String? = String()
    var PersonType : String? = String()
    var PhoneNumber : String? = String()
    var PromotionEndDate : String? = String()
    var PromotionStartDate : String? = String()
    //var PunchCount : String? = String()
    var PunchRequiredForFreeBee : String? = String()
    var RemainingCoupanToBuy : String? = String()
    var StartDate : String? = String()
    
    var TotalNoOfDeal : String? = String()
    var TotalReview : String? = String()
    var TypeOfCoupon : String? = String()
    var UserID : String? = String()
    
    var buyCoupanQuantity : String? = String()
    var customerUserID : String? = String()
    var giftCoupanQuantity : String? = String()
    var giftReceivedCoupanQuantity : String? = String()
    
    var redeemedCoupanQuantity : String? = String()
    var shareCoupanQuantity : String? = String()
    var shareReceivedCoupanQuantity : String? = String()
    
    
    var UTC_EndDate : String? = String()
    var UTC_PromotionEndDate : String? = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>, resourcePaths: Dictionary<String, AnyObject>) {
        
        self.AboutUs = data["AboutUs"] as? String
        self.Address = data["Address"] as? String
        self.AreaName = data["AreaName"] as? String
        self.AvalDealInNumber = data["AvalDealInNumber"] as? String
        self.BonusPotCurrencyCode = data["BonusPotCurrencyCode"] as? String
        self.BonusPotLocationID = data["BonusPotLocationID"] as? String
        self.BonusPotMaturityAmount = data["BonusPotMaturityAmount"] as? String
        self.BonusPotName = data["BonusPotName"] as? String
        
        self.BranchDescription = data["BranchDescription"] as? String
        self.BranchID = data["BranchID"] as? String
        self.BranchSpecific = data["BranchSpecific"] as? String
        
        self.BusinessCategory = data["BusinessCategory"] as? String
        self.BusinessLogoImagePath = data["BusinessLogoImagePath"] as? String
        //self.BuyEndDateExpire = data["BuyEndDateExpire"] as? String
        
        self.CategoryName = data["CategoryName"] as? String
        self.CategoryPicture = data["CategoryPicture"] as? String
        self.CurrencyCode = data["CurrencyCode"] as? String
        self.CurrentAmount = data["CurrentAmount"] as? String
        
        self.DealImage1 = data["DealImage1"] as? String
        self.DealMasterID = data["DealMasterID"] as? String
        self.DealPerCustomer = data["DealPerCustomer"] as? String
        self.DealPrice = data["DealPrice"] as? String
        self.Description = data["Description"] as? String
        
        self.Distance = data["Distance"] as? String
        self.EndDate = data["EndDate"] as? String
        self.FavoriteID = data["FavoriteID"] as? String
        self.FavoriteStatus = data["FavoriteStatus"] as? String
        
        self.FilledCategoryPicture = data["FilledCategoryPicture"] as? String
        self.FirstName = data["FirstName"] as? String
        self.IsBusinessImageVerified = data["IsBusinessImageVerified"] as? String
        self.IsDealImageVerified = data["IsDealImageVerified"] as? String
        
        let punchCardLoyalty = data["IsPunchCardLoyality"]
        self.IsPunchCardLoyality = ("\(punchCardLoyalty!)")
        
        self.Latitude = data["Latitude"] as? String
        self.Longitude = data["Longitude"] as? String
        self.LoyaltyID = data["LoyaltyID"] as? String
        
        self.MapPinCategoryPicture = data["MapPinCategoryPicture"] as? String
        
        self.MaxRating = data["Maxrating"] as? String
        self.NoOfActiveCoupan = data["NoOfActiveCoupan"] as? String
        self.PersonType = data["PersonType"] as? String
        self.PhoneNumber = data["PhoneNumber"] as? String
        self.PromotionEndDate = data["PromotionEndDate"] as? String
        
        self.PromotionStartDate = data["PromotionStartDate"] as? String
        self.PunchRequiredForFreeBee = data["PunchRequiredForFreeBee"] as? String
        self.RemainingCoupanToBuy = data["RemainingCoupanToBuy"] as? String
        self.StartDate = data["StartDate"] as? String
        
        self.TotalNoOfDeal = data["TotalNoOfDeal"] as? String
        self.TotalReview = data["TotalReview"] as? String
        self.TypeOfCoupon = data["TypeOfCoupon"] as? String
        self.UserID = data["UserID"] as? String
        
        self.buyCoupanQuantity = data["buyCoupanQuantity"] as? String
        self.customerUserID = data["customerUserID"] as? String
        self.giftCoupanQuantity = data["giftCoupanQuantity"] as? String
        self.giftReceivedCoupanQuantity = data["giftReceivedCoupanQuantity"] as? String
        
        self.redeemedCoupanQuantity = data["redeemedCoupanQuantity"] as? String
        self.shareCoupanQuantity = data["shareCoupanQuantity"] as? String
        self.shareReceivedCoupanQuantity = data["shareReceivedCoupanQuantity"] as? String
        
        if (data["BonusPotLocationID"] != nil){
            self.BonusPotLocationID = data["BonusPotLocationID"] as? String;
        }
        
        // Creating image full path here
        let iconBasePath = resourcePaths["CategoryIconsPath"] as? String;
        let BusinessLogoImagePath = resourcePaths["BusinessLogoImagePath"] as? String;
        let dealImagePath = resourcePaths["DealImage1"] as? String;
        
        self.CategoryPicture = iconBasePath!+"/"+self.BusinessCategory!+"/"+self.CategoryPicture!;
        self.MapPinCategoryPicture = iconBasePath!+"/"+self.BusinessCategory!+"/"+self.MapPinCategoryPicture!;
        self.FilledCategoryPicture = iconBasePath!+"/"+self.BusinessCategory!+"/"+self.FilledCategoryPicture!;
        
        if(self.BusinessLogoImagePath != nil){
            self.BusinessLogoImagePath = BusinessLogoImagePath! + "/"+self.UserID! + "/"+self.BusinessLogoImagePath!;
        }
        
        if(self.DealImage1 != nil){
            self.DealImage1 = dealImagePath!+"/"+self.DealMasterID!+"/"+self.DealImage1!;
        }
    }
}
