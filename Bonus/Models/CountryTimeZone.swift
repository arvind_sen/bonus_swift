//
//  Country.swift
//  NMS
//
//  Created by Arvind Sen on 09/09/15.
//  Copyright (c) 2015 Arvind Sen. All rights reserved.
//

import UIKit

class CountryTimeZone: NSObject {
   
    var Coordinates : String?  = String()  // String() created empty string
    var CountryCode : String?  = String()
    var TimeZone : String?  = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>) {
        
        self.Coordinates = data["Coordinates"] as? String
        self.CountryCode = data["CountryCode"] as? String
        self.TimeZone = data["TimeZone"] as? String
    }
}
