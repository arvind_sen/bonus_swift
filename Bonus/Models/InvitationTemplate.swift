//
//  Invite.swift
//  Bonus
//
//  Created by Arvind Sen on 01/09/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit

class InvitationTemplate: NSObject {

    // String() created empty string
    var DesignCode: String?  = String()
    var JoiningCode: String?  = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>) {
        
        self.DesignCode = data["DesignCode"] as? String
        self.JoiningCode = data["JoiningCode"] as? String
    }
}
