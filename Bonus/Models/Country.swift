//
//  Country.swift
//  NMS
//
//  Created by Arvind Sen on 09/09/15.
//  Copyright (c) 2015 Arvind Sen. All rights reserved.
//

import UIKit

class Country: NSObject {
   
    var ID : String?  = String()  // String() created empty string
    var CountryCode : String?  = String()
    var CountryName : String?  = String()
    var CouponPrice : String?  = String()
    var CurrencyAbrrevation : String?  = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>) {
        
        self.ID = data["ID"] as? String
        self.CountryCode = data["CountryCode"] as? String
        self.CountryName = data["CountryName"] as? String
        self.CouponPrice = data["CoupanPrice"] as? String
        self.CurrencyAbrrevation = data["CurrencyAbrrevation"] as? String
    }
}
