//
//  ResendInvitation.swift
//  Bonus
//
//  Created by Arvind Sen on 23/06/17.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit

class ResendInvitation: NSObject {

    // String() created empty string
    var CreatedDate: String?  = String()
    var DealCode: String?  = String()
    var DealMasterID: String?  = String()
    var DealReferLinkSend: String?  = String()
    var DealReferLinkSendMedia: String?  = String()
    var DealReferType: String?  = String()
    var ID: String?  = String()
    var InvitationMailID: String?  = String()
    var InvitationType: String?  = String()
    var IsInvitationAccept: String?  = String()
    var JoiningCode: String?  = String()
    var UserID: String?  = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>) {
        
        self.CreatedDate = data["CreatedDate"] as? String
        self.DealCode = data["DealCode"] as? String
        self.DealMasterID = data["DealMasterID"] as? String
        self.DealReferLinkSend = data["DealReferLinkSend"] as? String
        self.DealReferLinkSendMedia = data["DealReferLinkSendMedia"] as? String
        self.DealReferType = data["DealReferType"] as? String
        self.ID = data["ID"] as? String
        self.InvitationMailID = data["InvitationMailID"] as? String
        self.InvitationType = data["InvitationType"] as? String
        self.IsInvitationAccept = data["IsInvitationAccept"] as? String
        self.JoiningCode = data["JoiningCode"] as? String
        self.UserID = data["UserID"] as? String
    }
}
