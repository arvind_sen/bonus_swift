//
//  CustomerNotification.swift
//  Bonus
//
//  Created by Arvind Sen on 27/06/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit

class CustomerNotification: NSObject {
    
    var BusinessLogoImagePath : String?  = String()  // String() created empty string
    var AutoExpiryInDays : String?  = String()
    var BusinessUserID : String?  = String()
    var Description : String?  = String()
    var CreatedDate : String? = String()
    var FirstName : String?  = String()
    var IsImageVerified : String?  = String()
    var LastName : String?  = String()
    var MIDdleName : String? = String()
    var NotificationTransectionID : String?  = String()
    var NotificationType : String?  = String()
    var PersonType : String? = String()
    var TransDate : String? = String()
    var UserID : String? = String()
    var shortMessage : String? = String()
    var AdminLogoPath : String? = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>, resourcePath: Dictionary<String, AnyObject>) {
        
        var data = data;
        //self.BusinessLogoImagePath = data["BusinessLogoImagePath"] as? String
        self.BusinessLogoImagePath = "";
        //data.updateValue("<null>" as AnyObject, forKey: "BusinessLogoImagePath")
        
        if(data["BusinessLogoImagePath"] != nil && (data["BusinessLogoImagePath"]?.isKind(of: NSNull.self))! != true){
            self.BusinessLogoImagePath = data["BusinessLogoImagePath"] as? String
        }
        
        self.AutoExpiryInDays = data["AutoExpiryInDays"] as? String
        self.BusinessUserID = data["BusinessUserID"] as? String
        self.Description = data["Description"] as? String
        self.CreatedDate = data["CreatedDate"] as? String
        self.FirstName = data["FirstName"] as? String
        self.IsImageVerified = data["IsImageVerified"] as? String
        self.LastName = data["LastName"] as? String
        self.MIDdleName = data["MIDdleName"] as? String
        self.NotificationTransectionID = data["NotificationTransectionID"] as? String
        self.NotificationType = data["NotificationType"] as? String;
        self.PersonType = data["PersonType"] as? String;
        self.TransDate = data["TransDate"] as? String;
        self.UserID = data["UserID"] as? String;
        self.shortMessage = data["shortMessage"] as? String;
        
        self.AdminLogoPath = resourcePath["AdminLogoPath"] as? String;
        let businessImgBasePath = resourcePath["BusinessLogoImagePath"] as? String;
        
        if(self.BusinessLogoImagePath != nil)
        {
            self.BusinessLogoImagePath = businessImgBasePath!+"/"+self.BusinessUserID!+"/"+self.BusinessLogoImagePath!;
        }
    }
}
