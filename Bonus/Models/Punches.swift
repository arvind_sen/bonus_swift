//
//  Punches.swift
//  Bonus
//
//  Created by Arvind Sen on 27/09/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit

class Punches: NSObject {

    var NumberOfBalanceFreeBee : String?  = String()  // String() created empty string
    var PunchCount : String?  = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>) {
        
        self.NumberOfBalanceFreeBee = data["NumberOfBalanceFreeBee"] as? String
        self.PunchCount = data["PunchCount"] as? String
    }
}
