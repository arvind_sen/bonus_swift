//
//  Invite.swift
//  Bonus
//
//  Created by Arvind Sen on 01/09/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit

class Invite: NSObject {

    // String() created empty string
    var EmailID: String?  = String()
    var FirstName: String?  = String()
    var ID: String?  = String()
    var UserID: String?  = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>) {
        
        self.EmailID = data["EmailID"] as? String
        self.FirstName = data["FirstName"] as? String
        self.ID = data["ID"] as? String
        self.UserID = data["UserID"] as? String
    }
}
