//
//  WithdrawalHistory.swift
//  Bonus
//
//  Created by Arvind Sen on 13/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit

class WithdrawalHistory: NSObject {
    // String() created empty string
    var AdminApprovedBy: String?  = String()
    var AdminApprovedDate: String?  = String()
    var AdminApprovedStatus: String?  = String()
    var AdminRemark: String?  = String()
    var Amount: String?  = String()
    var ApprovedBy: String?  = String()
    var ApprovedDate: String?  = String()
    var ApprovedStatus: String?  = String()
    var BankAccountInfoID: String?  = String()
    var TransactionType: String? = String()
    var CreatedBy: String? = String()
    var CreatedDate: String? = String()
    
    var CurrencyCode: String?  = String()
    var ID: String?  = String()
    var Remark: String? = String()
    var RequestDate: String? = String()
    var UserID: String? = String()
    var WalletTransactionId: String? = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>) {
        self.AdminApprovedBy = data["AdminApprovedBy"] as? String
        self.AdminApprovedDate = data["AdminApprovedDate"] as? String
        self.AdminApprovedStatus = data["AdminApprovedStatus"] as? String
        self.AdminRemark = data["AdminRemark"] as? String
        self.Amount = data["Amount"] as? String
        self.ApprovedBy = data["ApprovedBy"] as? String
        self.ApprovedDate = data["ApprovedDate"] as? String
        self.ApprovedStatus = data["ApprovedStatus"] as? String
        self.BankAccountInfoID = data["BankAccountInfoID"] as? String
        self.CreatedBy = data["CreatedBy"] as? String
        self.CreatedDate = data["CreatedDate"] as? String
        
        self.CurrencyCode = data["CurrencyCode"] as? String
        self.ID = data["ID"] as? String
        self.Remark = data["Remark"] as? String
        self.RequestDate = data["RequestDate"] as? String
        self.UserID = data["UserID"] as? String
        self.WalletTransactionId = data["WalletTransactionId"] as? String
    }
}
