//
//  Category.swift
//  NMS
//
//  Created by Arvind Sen on 19/09/15.
//  Copyright (c) 2015 Arvind Sen. All rights reserved.
//

import UIKit

class Category: NSObject {
   
    var CategoryName : String?  = String()  // String() created empty string
    var CategoryPicture : String?  = String()
    var FilledCategoryPicture : String?  = String()
    var ID : String?  = String()
    var MapPinCategoryPicture : String? = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>, resourcePath: Dictionary<String, AnyObject>) {
        
        self.CategoryName = data["CategoryName"] as? String
        self.CategoryPicture = data["CategoryPicture"] as? String
        self.FilledCategoryPicture = data["FilledCategoryPicture"] as? String
        self.ID = data["ID"] as? String
        self.MapPinCategoryPicture = data["MapPinCategoryPicture"] as? String
        
        let iconBasePath = resourcePath["CategoryIconsPath"] as? String;
        
        self.CategoryPicture = iconBasePath!+"/"+self.ID!+"/"+self.CategoryPicture!;
        self.MapPinCategoryPicture = iconBasePath!+"/"+self.ID!+"/"+self.MapPinCategoryPicture!;
        self.FilledCategoryPicture = iconBasePath!+"/"+self.ID!+"/"+self.FilledCategoryPicture!;
    }
}
