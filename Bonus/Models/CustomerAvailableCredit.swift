//
//  CustomerAvailableCredit.swift
//  Bonus
//
//  Created by Arvind Sen on 12/07/17.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit

class CustomerAvailableCredit: NSObject {

    // String() created empty string
    var BonusPotWin: String?  = String()
    var BonusPotWinHome: String?  = String()
    var CreditAmount: String?  = String()
    var CurrencyAbrrevation: String?  = String()
    var CurrencyAbrrevationHome: String?  = String()
    var CurrentBalance: String?  = String()
    var DebitAmount: String?  = String()
    var DrCrFlag: String?  = String()
    var Friend: String?  = String()
    var FriendHome: String?  = String()
    var FriendsOfFriends: String?  = String()
    var FriendsOfFriendsHome: String?  = String()
    
    var TotalBalance: String?  = String()
    var TotalBalanceHome: String?  = String()
    var TotalEarnedAmount: String?  = String()
    var TotalEarnedAmountHome: String?  = String()
    var TotalLoadedAmount: String?  = String()
    var UserID: String?  = String()
    var Yourself: String?  = String()
    var YourselfHome: String?  = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>) {
        
        self.BonusPotWin = data["BonusPotWin"] as? String
        self.BonusPotWinHome = data["BonusPotWinHome"] as? String
        self.CreditAmount = data["CreditAmount"] as? String
        self.CurrencyAbrrevation = data["CurrencyAbrrevation"] as? String
        self.CurrencyAbrrevationHome = data["CurrencyAbrrevationHome"] as? String
        self.CurrentBalance = data["CurrentBalance"] as? String
        self.DebitAmount = data["DebitAmount"] as? String
        self.DrCrFlag = data["DrCrFlag"] as? String
        self.Friend = data["Friend"] as? String
        self.FriendHome = data["FriendHome"] as? String
        self.FriendsOfFriends = data["FriendsOfFriends"] as? String
        self.FriendsOfFriendsHome = data["FriendsOfFriendsHome"] as? String
        
        self.TotalBalance = data["TotalBalance"] as? String
        self.TotalBalanceHome = data["TotalBalanceHome"] as? String
        self.TotalEarnedAmount = data["TotalEarnedAmount"] as? String
        self.TotalEarnedAmountHome = data["TotalEarnedAmountHome"] as? String
        self.TotalLoadedAmount = data["TotalLoadedAmount"] as? String
        self.UserID = data["UserID"] as? String
        self.Yourself = data["Yourself"] as? String
        self.YourselfHome = data["YourselfHome"] as? String
    }
}
