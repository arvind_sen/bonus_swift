//
//  User.swift
//  Bonus
//
//  Created by Arvind Sen on 25/08/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit

class User: NSObject {
    
    var AboutUs: String? = String()  // String() created empty string
    var AddressLine1: String?  = String()
    var AddressLine2: String?  = String()
    var BalanceWallet: String?  = String()
    var BranchName: String?  = String()
    var BusinessCategory: String?  = String()
    var BusinessCentreCode: String?  = String()
    var BusinessLogoImagePath: String?  = String()
    var BusinessPhoneNumber: String?  = String()
    var BusinessUserID: String?  = String()
    var Cbpin: String?  = String()
    var City: String?  = String()
    var Company: String?  = String()
    var Country: String?  = String()
    var CountryID: String?  = String()
    var CountryName: String? = String()
    var CouponReedeemptionCode: String?  = String()
    var CreatedDate: String?  = String()
    var CurrencyCode: String?  = String() // User type
    var DateOfBirth: String? = String()
    var DealAdditionalNotificationFlag:String? = String()
    var DealValIDityNotificationFlag: String? = String()
    
    var DealsInWalletFlag: String?  = String()
    var DeviceToken: String?  = String()
    var DeviceType: String?  = String()
    var EmailID: String?  = String()
    var EmailVerificationFlag: String?  = String()
    var FavoriteNotificationFlag: String?  = String()
    var FirstName: String?  = String()
    var Gender: String?  = String()
    var ID: String?  = String()
    var IsActive: String?  = String()
    var IsActiveCoupanCount: String?  = String()
    var IsCouponCount: String?  = String()
    var IsImageVerified: String?  = String()
    var IsLoginFirstTime: String?  = String()
    var IsPunchCardLoyality: String?  = "0"
    var IsSuperAdmin: String?  = String()
    var LastLoginTimestamp: String?  = String()
    var LastLogoutTimestamp: String?  = String()
    var LastName: String?  = String()
    var Latitude: String?  = String()
    var Logitude: String?  = String()
    var MIDdleName: String?  = String()
    var MaxCoupon: String?  = String()
    var Nationality: String?  = String()
    var NickName: String?  = String()
    
    var Password: String?  = String()
    var PersonType: String?  = String()
    var ProfilePicture: String?  = String()
    var ReferalUserFirstName: String?  = String()
    var ReferalUserID: String?  = String()
    var State: String?  = String()
    var Street: String?  = String()
    var TimeZoneName: String?  = String()
    
    var Url: String?  = String()
    var UserLogin: String?  = String()
    var UserLoginStatus: String?  = String()
    var role_id: String?  = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>) {
        
        self.AboutUs = data["AboutUs"] as? String
        self.AddressLine1 = data["AddressLine1"] as? String
        self.AddressLine2 = data["AddressLine2"] as? String
        self.BalanceWallet = data["BalanceWallet"] as? String
        self.BranchName = data["BranchName"] as? String
        self.BusinessCategory = data["BusinessCategory"] as? String
        self.BusinessCentreCode = data["BusinessCentreCode"] as? String
        self.BusinessLogoImagePath = data["BusinessLogoImagePath"] as? String
        self.BusinessPhoneNumber = data["BusinessPhoneNumber"] as? String
        self.BusinessUserID = data["BusinessUserID"] as? String
        self.Cbpin = data["Cbpin"] as? String
        self.City = data["City"] as? String
        self.Company = data["Company"] as? String
        self.Country = data["Country"] as? String
        
        if(data["CountryID"] != nil){
            self.CountryID = data["CountryID"] as? String
        }
        else if(data["CountryId"] != nil){
            self.CountryID = data["CountryId"] as? String
        }
        
        self.CountryName = data["CountryName"] as? String
        self.CouponReedeemptionCode = data["CouponReedeemptionCode"] as? String
        self.CreatedDate = data["CreatedDate"] as? String
        self.CurrencyCode = data["CurrencyCode"] as? String
        self.DateOfBirth = data["DateOfBirth"] as? String
        self.DealAdditionalNotificationFlag = data["DealAdditionalNotificationFlag"]?.stringValue
        self.DealValIDityNotificationFlag = data["DealValIDityNotificationFlag"] as? String
        
        self.DealsInWalletFlag = data["DealsInWalletFlag"] as? String
        self.DeviceToken = data["DeviceToken"] as? String
        self.DeviceType = data["DeviceType"] as? String
        self.EmailID = data["EmailID"] as? String
        self.EmailVerificationFlag = data["EmailVerificationFlag"] as? String
        self.FavoriteNotificationFlag = data["FavoriteNotificationFlag"]?.stringValue
        self.FirstName = data["FirstName"] as? String
        self.Gender = data["Gender"] as? String
        self.ID = data["ID"] as? String
        
        self.IsActive = data["IsActive"]!.stringValue;
        self.IsActiveCoupanCount = data["IsActiveCoupanCount"] as? String
        self.IsCouponCount = data["IsCouponCount"] as? String
        self.IsImageVerified = data["IsImageVerified"] as? String
        self.IsLoginFirstTime = data["IsLoginFirstTime"] as? String
        
        if(data["IsPunchCardLoyality"] != nil)
        {
            if((data["IsPunchCardLoyality"]?.isKind(of: NSNull.self))! != true){
                    self.IsPunchCardLoyality = ("\(data["IsPunchCardLoyality"]!)")
            }
        }
        
        if(data["IsSuperAdmin"] != nil)
        {
            if((data["IsSuperAdmin"]?.isKind(of: NSNull.self))! != true){
                self.IsSuperAdmin = ("\(data["IsSuperAdmin"]!)")
            }
        }
        self.LastLoginTimestamp = data["LastLoginTimestamp"] as? String
        self.LastLogoutTimestamp = data["LastLogoutTimestamp"] as? String
        self.LastName = data["LastName"] as? String
        self.Latitude = data["Latitude"] as? String
        self.Logitude = data["Logitude"] as? String
        self.MIDdleName = data["MIDdleName"] as? String
        self.MaxCoupon = data["MaxCoupon"] as? String
        self.Nationality = data["Nationality"] as? String
        self.NickName = data["NickName"] as? String
        
        self.Password = data["Password"] as? String
        self.PersonType = data["PersonType"] as? String
        self.ProfilePicture = data["ProfilePicture"] as? String
        self.ReferalUserFirstName = data["ReferalUserFirstName"] as? String
        self.ReferalUserID = data["ReferalUserID"] as? String
        self.State = data["State"] as? String
        self.Street = data["Street"] as? String
        
        self.TimeZoneName = data["TimeZoneName"] as? String
        self.Url = data["Url"] as? String
        self.UserLogin = data["UserLogin"] as? String
        self.UserLoginStatus = data["UserLoginStatus"] as? String
        self.role_id = data["role_id"] as? String
    }
    
    
    func encodeWithCoder(_ encoder: NSCoder){
        
        //Encode properties, other class variables, etc
        encoder.encode(self.AboutUs, forKey: "AboutUs")
        encoder.encode(self.AddressLine1, forKey: "AddressLine1")
        encoder.encode(self.AddressLine2, forKey: "AddressLine2")
        encoder.encode(self.BalanceWallet, forKey: "BalanceWallet")
        encoder.encode(self.BranchName, forKey: "BranchName")
        encoder.encode(self.BusinessCategory, forKey: "BusinessCategory")
        encoder.encode(self.BusinessCentreCode, forKey: "BusinessCentreCode")
        encoder.encode(self.BusinessLogoImagePath, forKey: "BusinessLogoImagePath")
        encoder.encode(self.BusinessPhoneNumber, forKey: "BusinessPhoneNumber")
        encoder.encode(self.BusinessUserID, forKey: "BusinessUserID")
        encoder.encode(self.Cbpin, forKey: "Cbpin")
        encoder.encode(self.City, forKey: "City")
        encoder.encode(self.Company, forKey: "Company")
        encoder.encode(self.Country, forKey: "Country")
        encoder.encode(self.CountryID, forKey: "CountryID")
        encoder.encode(self.CountryName, forKey: "CountryName")
        encoder.encode(self.CouponReedeemptionCode, forKey: "CouponReedeemptionCode")
        encoder.encode(self.CreatedDate, forKey: "CreatedDate")
        encoder.encode(self.CurrencyCode, forKey: "CurrencyCode")
        encoder.encode(self.DateOfBirth, forKey: "DateOfBirth")
        encoder.encode(self.DealAdditionalNotificationFlag, forKey: "DealAdditionalNotificationFlag")
        encoder.encode(self.DealValIDityNotificationFlag, forKey: "DealValIDityNotificationFlag")
        
        encoder.encode(self.DealsInWalletFlag, forKey: "DealsInWalletFlag")
        encoder.encode(self.DeviceToken, forKey: "DeviceToken")
        encoder.encode(self.DeviceType, forKey: "DeviceType")
        encoder.encode(self.EmailID, forKey: "EmailID")
        encoder.encode(self.EmailVerificationFlag, forKey: "EmailVerificationFlag")
        encoder.encode(self.FavoriteNotificationFlag, forKey: "FavoriteNotificationFlag")
        encoder.encode(self.FirstName, forKey: "FirstName")
        encoder.encode(self.Gender, forKey: "Gender")
        encoder.encode(self.ID, forKey: "ID")
        encoder.encode(self.IsActive, forKey: "IsActive")
        encoder.encode(self.IsActiveCoupanCount, forKey: "IsActiveCoupanCount")
        encoder.encode(self.IsCouponCount, forKey: "IsCouponCount")
        encoder.encode(self.IsImageVerified, forKey: "IsImageVerified")
        encoder.encode(self.IsLoginFirstTime, forKey: "IsLoginFirstTime")
        encoder.encode(self.IsPunchCardLoyality, forKey: "IsPunchCardLoyality")
        encoder.encode(self.IsSuperAdmin, forKey: "IsSuperAdmin")
        encoder.encode(self.LastLoginTimestamp, forKey: "LastLoginTimestamp")
        encoder.encode(self.LastLogoutTimestamp, forKey: "LastLogoutTimestamp")
        encoder.encode(self.LastName, forKey: "LastName")
        encoder.encode(self.Latitude, forKey: "Latitude")
        encoder.encode(self.Logitude, forKey: "Logitude")
        encoder.encode(self.MIDdleName, forKey: "MIDdleName")
        encoder.encode(self.MaxCoupon, forKey: "MaxCoupon")
        encoder.encode(self.Nationality, forKey: "Nationality")
        encoder.encode(self.NickName, forKey: "NickName")
        
        encoder.encode(self.Password, forKey: "Password")
        encoder.encode(self.PersonType, forKey: "PersonType")
        encoder.encode(self.ProfilePicture, forKey: "ProfilePicture")
        encoder.encode(self.ReferalUserFirstName, forKey: "ReferalUserFirstName")
        encoder.encode(self.ReferalUserID, forKey: "ReferalUserID")
        encoder.encode(self.State, forKey: "State")
        encoder.encode(self.Street, forKey: "Street")
        encoder.encode(self.TimeZoneName, forKey: "TimeZoneName")
        encoder.encode(self.Url, forKey: "Url")
        encoder.encode(self.UserLogin, forKey: "UserLogin")
        encoder.encode(self.UserLoginStatus, forKey: "UserLoginStatus")
        encoder.encode(self.role_id, forKey: "role_id")
    }
    
    func initWithCoder(_ decoder: NSCoder) -> User {
        
        self.AboutUs = decoder.decodeObject(forKey: "AboutUs") as? String
        self.AddressLine1 = decoder.decodeObject(forKey: "AddressLine1") as? String
        self.AddressLine2 = decoder.decodeObject(forKey: "AddressLine2") as? String
        self.BalanceWallet = decoder.decodeObject(forKey: "BalanceWallet") as? String
        self.BranchName = decoder.decodeObject(forKey: "BranchName") as? String
        self.BusinessCategory = decoder.decodeObject(forKey: "BusinessCategory") as? String
        self.BusinessCentreCode = decoder.decodeObject(forKey: "BusinessCentreCode") as? String
        self.BusinessLogoImagePath = decoder.decodeObject(forKey: "BusinessLogoImagePath") as? String
        self.BusinessPhoneNumber = decoder.decodeObject(forKey: "BusinessPhoneNumber") as? String
        self.BusinessUserID = decoder.decodeObject(forKey: "BusinessUserID") as? String
        self.Cbpin = decoder.decodeObject(forKey: "Cbpin") as? String
        self.City = decoder.decodeObject(forKey: "City") as? String
        self.Company = decoder.decodeObject(forKey: "Company") as? String
        self.Country = decoder.decodeObject(forKey: "Country") as? String
        self.CountryID = decoder.decodeObject(forKey: "CountryID") as? String
        self.CountryName = decoder.decodeObject(forKey: "CountryName") as? String
        self.CouponReedeemptionCode = decoder.decodeObject(forKey: "CouponReedeemptionCode") as? String
        self.CreatedDate = decoder.decodeObject(forKey: "CreatedDate") as? String
        self.CurrencyCode = decoder.decodeObject(forKey: "CurrencyCode") as? String
        self.DateOfBirth = decoder.decodeObject(forKey: "DateOfBirth") as? String
        self.DealAdditionalNotificationFlag = decoder.decodeObject(forKey: "DealAdditionalNotificationFlag") as? String
        self.DealValIDityNotificationFlag = decoder.decodeObject(forKey: "DealValIDityNotificationFlag") as? String
        
        self.DealsInWalletFlag = decoder.decodeObject(forKey: "DealsInWalletFlag") as? String
        self.DeviceToken = decoder.decodeObject(forKey: "DeviceToken") as? String
        self.DeviceType = decoder.decodeObject(forKey: "DeviceType") as? String
        self.EmailID = decoder.decodeObject(forKey: "EmailID") as? String
        self.EmailVerificationFlag = decoder.decodeObject(forKey: "EmailVerificationFlag") as? String
        self.FavoriteNotificationFlag = decoder.decodeObject(forKey: "FavoriteNotificationFlag") as? String
        self.FirstName = decoder.decodeObject(forKey: "FirstName") as? String
        self.Gender = decoder.decodeObject(forKey: "Gender") as? String
        self.ID = decoder.decodeObject(forKey: "ID") as? String
        self.IsActive = decoder.decodeObject(forKey: "IsActive") as? String
        self.IsActiveCoupanCount = decoder.decodeObject(forKey: "IsActiveCoupanCount") as? String
        self.IsCouponCount = decoder.decodeObject(forKey: "IsCouponCount") as? String
        self.IsImageVerified = decoder.decodeObject(forKey: "IsImageVerified") as? String
        self.IsLoginFirstTime = decoder.decodeObject(forKey: "IsLoginFirstTime") as? String
        self.IsPunchCardLoyality = decoder.decodeObject(forKey: "IsPunchCardLoyality") as? String
        self.IsSuperAdmin = decoder.decodeObject(forKey: "IsSuperAdmin") as? String
        self.LastLoginTimestamp = decoder.decodeObject(forKey: "LastLoginTimestamp") as? String
        self.LastLogoutTimestamp = decoder.decodeObject(forKey: "LastLogoutTimestamp") as? String
        self.LastName = decoder.decodeObject(forKey: "LastName") as? String
        self.Latitude = decoder.decodeObject(forKey: "Latitude") as? String
        self.Logitude = decoder.decodeObject(forKey: "Logitude") as? String
        self.MIDdleName = decoder.decodeObject(forKey: "MIDdleName") as? String
        self.MaxCoupon = decoder.decodeObject(forKey: "MaxCoupon") as? String
        self.Nationality = decoder.decodeObject(forKey: "Nationality") as? String
        self.NickName = decoder.decodeObject(forKey: "NickName") as? String
        self.Password = decoder.decodeObject(forKey: "Password") as? String
        self.PersonType = decoder.decodeObject(forKey: "PersonType") as? String
        self.ProfilePicture = decoder.decodeObject(forKey: "ProfilePicture") as? String
        self.ReferalUserFirstName = decoder.decodeObject(forKey: "ReferalUserFirstName") as? String
        self.ReferalUserID = decoder.decodeObject(forKey: "ReferalUserID") as? String
        self.State = decoder.decodeObject(forKey: "State") as? String
        self.Street = decoder.decodeObject(forKey: "Street") as? String
        
        self.TimeZoneName = decoder.decodeObject(forKey: "TimeZoneName") as? String
        self.Url = decoder.decodeObject(forKey: "Url") as? String
        self.UserLogin = decoder.decodeObject(forKey: "UserLogin") as? String
        self.UserLoginStatus = decoder.decodeObject(forKey: "UserLoginStatus") as? String
        self.role_id = decoder.decodeObject(forKey: "role_id") as? String
        
        return self;
    }
}
