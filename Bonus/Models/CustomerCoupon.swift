//
//  CustomerCoupon.swift
//  Bonus
//
//  Created by Arvind Sen on 01/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit

class CustomerCoupon: NSObject {

    // String() created empty string
    var AboutUs: String?  = String()
    var Address: String?  = String()
    var AvalDealInNumber: String?  = String()
    
    var BalanceQTY: String?  = String()

    var BranchDescription: String?  = String()
    var BranchID: String?  = String()
    var BranchSpecific: String?  = String()
    var BusinessCategory: String?  = String()
    var BusinessLogoImagePath: String?  = String()
    var BusinessUserID: String?  = String()
    var CBpin: String?  = String()
    var CategoryDescription: String?  = String()
    var CategoryID: String?  = String()
    var CategoryName: String?  = String()
    var CategoryPicture: String?  = String()
    var CouponImagePremium: String?  = String()
    
    var CouponQTY: String? = String();
    
    var CouponReedeemptionCode: String?  = String()
    var CurrencyCode: String?  = String()
    var CustomerUserID: String?  = String()
    var DealImage1: String?  = String()
    var DealImage2: String?  = String()
    var DealMasterID: String?  = String()
    var DealName: String?  = String()
    var DealPrice: String?  = String()
    var DealOffer: String?  = String()
    var DealStatus: String?  = String()
    var Description: String?  = String()
    var Distance : String? = String()
    var EndDate : String? = String()
    var ExpiryFlag: String? = String();
    var FavoriteID : String? = String()
    var FavoriteStatus: String? = String()
    var FavoriteUserID :String? = String()
    var FilledCategoryPicture : String? = String()
    var FirstName : String? = String()
    var IsBusinessImageVerified : String? = String()
    var IsDealImageVerified : String? = String()
    var Latitude : String? = String()
    var Longitude : String? = String()
    var MapPinCategoryPicture : String? = String()
    var MaxRating : String? = String()
    var NoOfActiveCoupan: String?  = String()
    var PersonType : String? = String()
    var PhoneNumber: String?  = String()
    var PromoDate : String? = String()
    var PromotionEndDate : String? = String()
    var PromotionStartDate : String? = String()
    var RemainingCoupanToBuy : String? = String()
    var StartDate : String? = String()
    var StartDate1 : String? = String()
    var TotalNoOfDeal : String? = String()
    var TotalReview : String? = String()
    var TransactionDate : String? = String()
    var TransactionID : String? = String()
    var TypeOfCoupon : String? = String()
    var UrlSlug : String? = String()
    var buyCoupanQuantity : String? = String()
    var dealPerCustomer : String? = String()
    var giftCoupanQuantity : String? = String()
    var giftReceivedCoupanQuantity : String? = String()
    var redeemedCoupanQuantity : String? = String()
    var shareCoupanQuantity : String? = String()
    var shareReceivedCoupanQuantity : String? = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>, resourcePaths: Dictionary<String, AnyObject>) {
        
        self.AvalDealInNumber = data["AvalDealInNumber"] as? String
        self.AboutUs = data["AboutUs"] as? String
        self.Address = data["Address"] as? String
        
        if(data["BalanceQTY"] != nil)
        {
            self.BalanceQTY = data["BalanceQTY"] as? String;
        }
        
        self.BranchDescription = data["BranchDescription"] as? String
        self.BranchID = data["BranchID"] as? String
        self.BranchSpecific = data["BranchSpecific"] as? String
        self.BusinessCategory = data["BusinessCategory"] as? String
        self.BusinessLogoImagePath = data["BusinessLogoImagePath"] as? String
        self.BusinessUserID = data["BusinessUserID"] as? String
        self.CBpin = data["CBpin"] as? String
        //self.CBpin = data["CBpin"] as? String
        self.CategoryName = data["CategoryName"] as? String
        self.CategoryPicture = data["CategoryPicture"] as? String
        self.CurrencyCode = data["CurrencyCode"] as? String
        self.CategoryDescription = data["CategoryDescription"] as? String
        self.CurrencyCode = data["CurrencyCode"] as? String
        self.CustomerUserID = data["CustomerUserID"] as? String
        
        if(data["CouponQTY"] != nil)
        {
            self.CouponQTY = data["CouponQTY"] as? String;
        }
        self.DealImage1 = data["DealImage1"] as? String
        self.DealImage2 = data["DealImage2"] as? String
        self.DealMasterID = data["DealMasterID"] as? String
        self.DealName = data["DealName"] as? String
        self.DealPrice = data["DealPrice"] as? String
        self.DealOffer = data["DealOffer"] as? String
        self.DealStatus = data["DealStatus"] as? String
        self.Description = data["Description"] as? String
        self.Distance = data["Distance"] as? String
        self.EndDate = data["EndDate"] as? String
        
        if(data["ExpiryFlag"] != nil)
        {
            self.ExpiryFlag = data["ExpiryFlag"] as? String
        }
        
        self.FavoriteID = data["FavoriteID"] as? String
        self.FavoriteStatus = ("\(data["FavoriteStatus"]!)");
        self.FavoriteUserID = data["FavoriteUserID"] as? String
        self.FilledCategoryPicture = data["FilledCategoryPicture"] as? String
        self.FirstName = data["FirstName"] as? String
        self.IsBusinessImageVerified = data["IsBusinessImageVerified"] as? String
        self.IsDealImageVerified = data["IsDealImageVerified"] as? String
        self.Latitude = data["Latitude"] as? String
        self.Longitude = data["Longitude"] as? String
        self.MapPinCategoryPicture = data["MapPinCategoryPicture"] as? String
        self.MaxRating = data["Maxrating"] as? String
        self.NoOfActiveCoupan = data["NoOfActiveCoupan"] as? String
        self.PersonType = data["PersonType"] as? String
        self.PromotionEndDate = data["PromotionEndDate"] as? String
        self.PromotionStartDate = data["PromotionStartDate"] as? String
        self.PhoneNumber = data["PhoneNumber"] as? String
        self.PromoDate = data["PromoDate"] as? String
        self.StartDate = data["StartDate"] as? String
        self.StartDate1 = data["StartDate1"] as? String
        self.TotalNoOfDeal = data["TotalNoOfDeal"] as? String
        self.TypeOfCoupon = data["TypeOfCoupon"] as? String
        self.TotalReview = data["TotalReview"] as? String
        self.TransactionDate = data["TransactionDate"] as? String
        self.TransactionID = data["TransactionID"] as? String
        
        self.UrlSlug = data["UrlSlug"] as? String
        self.buyCoupanQuantity = data["buyCoupanQuantity"] as? String
        self.dealPerCustomer = data["dealPerCustomer"] as? String
        self.giftCoupanQuantity = data["giftCoupanQuantity"] as? String
        self.giftReceivedCoupanQuantity = data["giftReceivedCoupanQuantity"] as? String
        self.redeemedCoupanQuantity = data["redeemedCoupanQuantity"] as? String
        self.shareCoupanQuantity = data["shareCoupanQuantity"] as? String;
        self.shareReceivedCoupanQuantity = data["shareReceivedCoupanQuantity"] as? String;
        
        // Creating image full path here
        let iconBasePath = resourcePaths["CategoryIconsPath"] as? String;
        let BusinessLogoImagePath = resourcePaths["BusinessLogoImagePath"] as? String;
        let dealImagePath = resourcePaths["DealImage1"] as? String;
        
        
        if (self.BusinessCategory != nil) {
            self.CategoryPicture = iconBasePath!+"/"+self.BusinessCategory!+"/"+self.CategoryPicture!;
            self.MapPinCategoryPicture = iconBasePath!+"/"+self.BusinessCategory!+"/"+self.MapPinCategoryPicture!;
            self.FilledCategoryPicture = iconBasePath!+"/"+self.BusinessCategory!+"/"+self.FilledCategoryPicture!;
        }
       
        
        
        if(self.BusinessLogoImagePath != nil){
            self.BusinessLogoImagePath = BusinessLogoImagePath! + "/"+self.BusinessUserID! + "/"+self.BusinessLogoImagePath!;
        }
        
        if(self.DealImage1 != nil){
            self.DealImage1 = dealImagePath!+"/"+self.DealMasterID!+"/"+self.DealImage1!;
        }
    }
}
