//
//  Notifications.swift
//  Bonus
//
//  Created by Arvind Sen on 24/04/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit

class Notifications: NSObject {
    
    var BusinessLogoImagePath : String?  = String()  // String() created empty string
    var CreatedBy : String?  = String()
    var CreatedByUser : String?  = String()
    var ID : String?  = String()
    var CreatedDate : String? = String()
    var FirstName : String?  = String()
    var IsImageVerified : String?  = String()
    var NotificationDate : String?  = String()
    var NotificationDescription : String? = String()
    var NotificationShortDescription : String?  = String()
    var Status : String?  = String()
    var UserId : String? = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>, resourcePath: Dictionary<String, AnyObject>) {
        
        if(data["BusinessLogoImagePath"] != nil && (data["BusinessLogoImagePath"]?.isKind(of: NSNull.self))! != true){
            self.BusinessLogoImagePath = data["BusinessLogoImagePath"] as? String
        }
        //self.BusinessLogoImagePath = data["BusinessLogoImagePath"] as? String
        self.CreatedBy = data["CreatedBy"] as? String
        self.CreatedByUser = data["CreatedByUser"] as? String
        self.ID = data["ID"] as? String
        self.CreatedDate = data["CreatedDate"] as? String
        self.FirstName = data["FirstName"] as? String
        self.IsImageVerified = data["IsImageVerified"] as? String
        self.CreatedByUser = data["CreatedByUser"] as? String
        self.NotificationDate = data["NotificationDate"] as? String
        self.NotificationDescription = data["NotificationDescription"] as? String
        self.NotificationShortDescription = data["NotificationShortDescription"] as? String;
        self.Status = data["Status"] as? String;
        self.UserId = data["UserId"] as? String;
        
        let iconBasePath = resourcePath["BusinessLogoImagePath"] as? String;
        
        self.BusinessLogoImagePath = iconBasePath!+"/"+self.CreatedBy!+"/"+self.BusinessLogoImagePath!;
    }
}
