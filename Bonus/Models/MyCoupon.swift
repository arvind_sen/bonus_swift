//
//  MyCoupon.swift
//  Bonus
//
//  Created by Arvind Sen on 30/12/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//


class MyCoupon: NSObject {
    
    var Address : String?  = String()  // String() created empty string
    var AboutUs : String?  = String()
    var AvalDealInNumber : String?  = String()
    var BranchDescription : String?  = String()
    var BranchID : String?  = String()
    var BranchList : String?  = String()
    var BranchSpecific : String?  = String()
    var BranchesList: Array<Branch> = [Branch]()
    var BusinessCategory : String?  = String()
    var BuyExpired : String?  = String()
    var CategoryName : String?  = String()
    var DealMasterID : String?  = String()
    var DealPerCustomer: String?  = String()
    var DealPrice : String?  = String()
    var Description : String? = String()
    var EndDate : String?  = String()
    var EndDate1 : String?  = String()
    
    var FirstName : String?  = String()
    var IsActive: String?  = String()
    var IsActiveCouponCount : String?  = String()
    var IsBusinessImageVerified : String?  = String()
    var IsDealImageVerified : String?  = String()
    var Latitude : String?  = String()
    var Longitude : String?  = String()
    var Maxrating : String?  = String()
    var PhoneNumber : String?  = String()

    var PromotionEndDate : String?  = String()
    var PromotionEndDate1 : String?  = String()
    var PromotionExpired : String?  = String()
    var PromotionStartDate : String?  = String()
    var PromotionStartDate1 : String?  = String()
    var StartDate : String?  = String()
    var StartDate1 : String?  = String()
    var TotalActiveCoupan : String?  = String()
    var TotalNoOfDeal : String?  = String()
    var TotalRedeemed : String?  = String()
    
    var TotalReview : String? = String()
    var TypeOfCoupon : String?  = String()
    var UserID : String?  = String()
    
    var CategoryPicture : String? = String()
    var MapPinCategoryPicture : String?  = String()
    var FilledCategoryPicture : String?  = String()
    
    var BusinessLogoImagePath : String? = String()
    var DealImage1 : String?  = String()

    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>, resourcePaths: Dictionary<String, AnyObject>) {
        
        self.Address = data["Address"] as? String
        self.AboutUs = data["AboutUs"] as? String
        self.AvalDealInNumber = data["AvalDealInNumber"] as? String
        self.BranchDescription = data["BranchDescription"] as? String
        self.BranchID = data["BranchID"] as? String
        self.BranchList = data["BranchList"] as? String
        self.BranchSpecific = data["BranchSpecific"] as? String
        
        let branches = data["BranchesList"] as? NSArray
        for item in branches!  {
            let branch = Branch(data: (item as? Dictionary<String, Any>)! as Dictionary<String, AnyObject>)
            self.BranchesList.append(branch);
        }
        
        self.BuyExpired = data["BuyExpired"] as? String
        self.CategoryName = data["CategoryName"] as? String
        self.DealMasterID = data["DealMasterID"] as? String
        self.DealPerCustomer  = data["DealPerCustomer"] as? String
        self.DealPrice = data["DealPrice"] as? String
        self.Description = data["Description"] as? String
        self.EndDate = data["EndDate"] as? String
        self.EndDate1 = data["PhoneNumber"] as? String
        self.FirstName = data["FirstName"] as? String
        self.IsActive = data["IsActive"] as? String
        self.IsActiveCouponCount = data["IsActiveCouponCount"] as? String
        self.IsBusinessImageVerified  = data["IsBusinessImageVerified"] as? String
        self.IsDealImageVerified = data["IsDealImageVerified"] as? String
        self.Latitude = data["Latitude"] as? String
        self.Longitude = data["Longitude"] as? String
        self.Maxrating = data["Maxrating"] as? String
        self.PhoneNumber = data["PhoneNumber"] as? String
        
        self.PromotionEndDate = data["PromotionEndDate"] as? String
        self.PromotionEndDate1 = data["PromotionEndDate1"] as? String
        self.PromotionExpired = data["PromotionExpired"] as? String
        self.PromotionStartDate  = data["PromotionStartDate"] as? String
        self.PromotionStartDate1 = data["PromotionStartDate1"] as? String
        self.StartDate = data["StartDate"] as? String
        self.StartDate1 = data["StartDate1"] as? String
        self.TotalActiveCoupan = data["TotalActiveCoupan"] as? String
        self.TotalNoOfDeal = data["TotalNoOfDeal"] as? String
        self.TotalRedeemed = data["TotalRedeemed"] as? String
        
        self.TotalReview = data["TotalReview"] as? String
        self.TypeOfCoupon = data["TypeOfCoupon"] as? String
        self.UserID = data["UserID"] as? String
        
        // Creating image full path here
        //let iconBasePath = resourcePaths["CategoryIconsPath"] as? String;
        //let BusinessLogoImagePath = resourcePaths["BusinessLogoImagePath"] as? String;
        //let dealImagePath = resourcePaths["DealImage1"] as? String;
        
        //self.CategoryPicture = iconBasePath!+"/"+self.BusinessCategory!+"/"+self.CategoryPicture!;
        self.CategoryPicture = data["CategoryPicture"] as? String;
        //self.MapPinCategoryPicture = iconBasePath!+"/"+self.BusinessCategory!+"/"+self.MapPinCategoryPicture!;
        self.MapPinCategoryPicture = data["MapPinCategoryPicture"] as? String;
        //self.FilledCategoryPicture = iconBasePath!+"/"+self.BusinessCategory!+"/"+self.FilledCategoryPicture!;
        self.FilledCategoryPicture = data["FilledCategoryPicture"] as? String;
        //self.BusinessLogoImagePath = BusinessLogoImagePath!+"/"+self.UserID!+"/"+self.BusinessLogoImagePath!;
        self.BusinessLogoImagePath = data["BusinessLogoImagePath"] as? String;
        //self.DealImage1 = dealImagePath!+"/"+self.UserID!+"/"+self.DealImage1!;
        self.DealImage1 = data["DealImage1"] as? String;
    }
}
