//
//  CustomerReview.swift
//  Bonus
//
//  Created by Arvind Sen on 17/07/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

class CustomerReview: NSObject {

    // String() created empty string
    
    var BusinessLogoImagePath: String?  = String()
    var CreatedDate: String?  = String()
    var CustomerUserID : String? = String()
    var DealMasterID: String?  = String()
    var DealReviewDate : String? = String()
    var FirstName : String? = String()
    var Id: String? = String()
    var IsImageVerified: String? = String()
    var NickName: String? = String()
    var PersonType : String? = String()
    var Rating: String?  = String()
    var Review: String?  = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>, resourcePaths: Dictionary<String, AnyObject>) {
        
        self.BusinessLogoImagePath = data["BusinessLogoImagePath"] as? String
        self.CreatedDate  = data["CreatedDate"] as? String
        self.CustomerUserID = data["CustomerUserID"] as? String
        self.DealMasterID = data["DealMasterID"] as? String
        self.DealReviewDate = data["DealReviewDate"] as? String
        self.FirstName = data["FirstName"] as? String
        self.Id = data["Id"] as? String
        self.IsImageVerified = data["IsImageVerified"] as? String
        self.NickName = data["NickName"] as? String
        self.PersonType = data["PersonType"] as? String
        self.Rating = data["Rating"] as? String
        self.Review = data["Review"] as? String
        
        let userImagePath = resourcePaths["BusinessLogoImagePath"] as? String;
        
        if(self.BusinessLogoImagePath != nil){
            self.BusinessLogoImagePath = userImagePath! + "/" + self.CustomerUserID! + "/" + self.BusinessLogoImagePath!;
        }
    }
}
