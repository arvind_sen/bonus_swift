//
//  BonusLocalDBHandler.swift
//  Bonus
//
//  Created by Arvind Sen on 15/09/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit

var statement: OpaquePointer? = nil
var db: OpaquePointer? = nil

class BonusLocalDBHandler: NSObject {

    /* This method will call to save all the countries detail in local database */

    class func saveAllCountriesDetail(countriesArray: NSArray)->Bool
    {
        let path = AppUtility.getPath(K_DATABASE_FILE)
        
        print("path----- \(path)")
        
        if sqlite3_open(path, &db) == SQLITE_OK {
            
            // Dropping table if exist
            let dropTableQuerySQL: NSString = "DROP TABLE IF EXISTS countries"
            let sql = dropTableQuerySQL.utf8String;
            sqlite3_prepare_v2(db, sql, -1, &statement, nil)
            
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                sqlite3_reset(statement)
                print("countries dropped");
                
                // Creating table if dropped
                let createTableQuerySQL: NSString = "CREATE TABLE countries ('ID' INTEGER, 'CountryCode' VARCHAR, 'CountryName' VARCHAR, 'CouponPrice' VARCHAR, 'CurrencyAbrrevation' VARCHAR)"
                let sql = createTableQuerySQL.utf8String;
                sqlite3_prepare_v2(db, sql, -1, &statement, nil)
                
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    sqlite3_reset(statement)
                    print("countries table created")
                }
            }
            
            for country in countriesArray {
                
                let countryObject = country as! Country
                let ID = countryObject.ID!
                let CountryCode = countryObject.CountryCode!
                let CountryName = countryObject.CountryName!
                let CouponPrice = countryObject.CouponPrice!
                let CurrencyAbrrevation = countryObject.CurrencyAbrrevation!
                
                let querySQL: NSString = "INSERT INTO countries(ID, CountryCode, CountryName, CouponPrice, CurrencyAbrrevation) VALUES (\(ID), '\(CountryCode)', '\(CountryName)', '\(CouponPrice)', '\(CurrencyAbrrevation)')" as NSString
                
                let sql = querySQL.utf8String;
                
                sqlite3_prepare_v2(db, sql, -1, &statement, nil)
                
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    print("Country Insert...")
                }
                else{
                    print("sqlError = \(sqlite3_errmsg(db))")
                }
                sqlite3_reset(statement)    //Reset the pointer for the statement
            }
        }
        else{
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Local db does not open, Please try again!", onView: self)
        }
        sqlite3_close(db);
        return true;
    }
 
        
    /* This method will call to get all the countries detail from the local db */
    class func getAllCountriesDetail()->NSArray
    {
        let path = AppUtility.getPath(K_DATABASE_FILE)
        print("path----- \(path)")
        
        let allCountries = NSMutableArray()
        
        if sqlite3_open(path, &db) == SQLITE_OK {
            
            let querySQL: NSString = "SELECT * FROM countries"
            let sql = querySQL.utf8String;
            
            if(sqlite3_prepare_v2(db, sql, -1, &statement, nil) == SQLITE_OK)
            {
                while(sqlite3_step(statement) == SQLITE_ROW)
                {
                    let country = Country()
                    country.ID = String(sqlite3_column_int(statement, 0));
                    
                    let code = sqlite3_column_text(statement,1)
                    country.CountryCode = String(cString: code!);
                    
                    let countName = sqlite3_column_text(statement, 2)
                    country.CountryName = String(cString: countName!);

                    let price = sqlite3_column_text(statement, 3)
                    country.CouponPrice = String(cString: price!);
                    
                    let curAbb = sqlite3_column_text(statement, 4)
                    country.CurrencyAbrrevation = String(cString: curAbb!);

                    allCountries.add(country);
                    print("Broom...Country")
                    //sqlite3_reset(statement)
                }
            }
            sqlite3_finalize(statement);
        }
        else{
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Local db does not open, Please try again!", onView: self)
        }
        
        sqlite3_close(db);
        return allCountries;
    }
    
    
    class func saveAllCategoriesDetail(_ array: NSArray)->Bool
    {
        let path = AppUtility.getPath(K_DATABASE_FILE)
        
        print("path----- \(path)")
        
        if sqlite3_open(path, &db) == SQLITE_OK {
            
            // Dropping table if exist
            let dropTableQuerySQL: NSString = "DROP TABLE IF EXISTS Categories"
            let sql = dropTableQuerySQL.utf8String;
            sqlite3_prepare_v2(db, sql, -1, &statement, nil)
            
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                sqlite3_reset(statement)
                print("categories dropped")
                
                // Creating table if dropped
                let createTableQuerySQL: NSString = "CREATE TABLE Categories ('ID' INTEGER, 'CategoryName' VARCHAR, 'CategoryPicture' VARCHAR, 'FilledCategoryPicture' VARCHAR, 'MapPinCategoryPicture' VARCHAR)"
                let sql = createTableQuerySQL.utf8String;
                sqlite3_prepare_v2(db, sql, -1, &statement, nil)
                
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    sqlite3_reset(statement)
                    print("categories table created")
                }
                
            }
            
            // Saved all the server side category locally
            for category in array {
                
                let categoryObj = category as! Category
                
                let id = categoryObj.ID!
                let name = categoryObj.CategoryName!
                let categoryPicture = categoryObj.CategoryPicture!
                let filledPicture = categoryObj.FilledCategoryPicture!
                let mapPinPicture = categoryObj.MapPinCategoryPicture!
                
                let querySQL: NSString = "INSERT INTO Categories('ID', 'CategoryName', 'CategoryPicture', 'FilledCategoryPicture', 'MapPinCategoryPicture') VALUES (\(id), '\(name)', '\(categoryPicture)', '\(filledPicture)', '\(mapPinPicture)')" as NSString
                
                let sql = querySQL.utf8String;
                
                sqlite3_prepare_v2(db, sql, -1, &statement, nil)
                
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    print("Broom...2")
                }
                else{
                    print("sqlError = \(sqlite3_errmsg(db))")
                    
                }
                sqlite3_reset(statement)    //Reset the pointer for the statement
            }
        }
        else{
            
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Local db does not open, Please try again!", onView: self)
        }
        sqlite3_close(db);
        return true;
    }
    
    // Method get call to get all the categories
    class func getAllCategories ()-> NSArray {
        
        let path = AppUtility.getPath(K_DATABASE_FILE)
        print("path----- \(path)")
        
        let allCategories = NSMutableArray()
        
        if sqlite3_open(path, &db) == SQLITE_OK {
            
            let querySQL: NSString = "SELECT * FROM Categories"
            let sql = querySQL.utf8String;
            
            if(sqlite3_prepare_v2(db, sql, -1, &statement, nil) == SQLITE_OK)
            {
                while(sqlite3_step(statement) == SQLITE_ROW)
                {
                    let category = Category()
                    category.ID = String(sqlite3_column_int(statement, 0));
                    
                    
                    
//                    var bytes: (CChar, CChar, CChar, CChar) = (0x61, 0x62, 0x63, 0)
//                    let str : String = withUnsafePointer(to: &bytes) { ptr -> String in
//                        return String(cString: UnsafeRawPointer(ptr).assumingMemoryBound(to: CChar.self))
//                    }
                    
                    
                    /*
                     let catName = sqlite3_column_text(statement,1)
                    let name = String(cString: UnsafePointer<CChar>(catName!))
                    category.CategoryName = name;
                    
                    let picture = sqlite3_column_text(statement, 2)
                    let catPicture = String(cString: UnsafePointer<CChar>(picture!))
                    category.CategoryPicture = catPicture
                    
                    let mapPic = sqlite3_column_text(statement, 3)
                    let mapPicture = String(cString: UnsafePointer<CChar>(mapPic!))
                    category.MapPinCategoryPicture = mapPicture
                    
                    let fillimg = sqlite3_column_text(statement, 4)
                    let filledPicture = String(cString: UnsafePointer<CChar>(fillimg!))
                    
                    */
                    let catName = sqlite3_column_text(statement,1)
                    let name = String(cString: catName!)
                    category.CategoryName = name;
                    
                    let picture = sqlite3_column_text(statement, 2)
                    let catPicture = String(cString: picture!)
                    category.CategoryPicture = catPicture
                    
                    let mapPic = sqlite3_column_text(statement, 3)
                    let mapPicture = String(cString: mapPic!)
                    category.MapPinCategoryPicture = mapPicture
                    
                    let fillimg = sqlite3_column_text(statement, 4)
                    let filledPicture = String(cString: fillimg!)
                    
                    
                    category.FilledCategoryPicture = filledPicture
                    
                    allCategories.add(category);
                }
            }
            sqlite3_finalize(statement);
        }
        else{
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Local db does not open, Please try again!", onView: self)
        }
        
        sqlite3_close(db);
        return allCategories;
    }
}
