//
//  Branch.swift
//  Bonus
//
//  Created by Arvind Sen on 17/11/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

class Branch: NSObject {

    var Address : String?  = String()  // String() created empty string
    var AreaName : String?  = String()
    var BranchDescription : String?  = String()
    var BranchEmployeeUserID : String?  = String()
    var BranchID : String?  = String()
    var Cbpin : String?  = String()
    var CouponAvailable : String?  = String()
    var Email : String?  = String()
    var ID : String?  = String()
    var IsDeleted : String?  = String()
    var IsMainBranch : Int?  = 0
    var Latitude : String?  = String()
    var Longitude : String?  = String()
    var Password : String?  = String()
    var PhoneNumber : String?  = String()
    var userID : String?  = String()
    
    var isSelected : Int = 0; // This vaiable we are using at the time of coupons creation.
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>) {
        
        self.Address = data["Address"] as? String
        self.AreaName = data["AreaName"] as? String
        self.BranchDescription = data["BranchDescription"] as? String
        self.BranchEmployeeUserID = data["BranchEmployeeUserID"] as? String
        self.BranchID = data["BranchID"] as? String
        self.Cbpin = data["Cbpin"] as? String
        self.CouponAvailable = data["CouponAvailable"] as? String
        self.Email = data["Email"] as? String
        self.ID = data["ID"] as? String
        self.IsDeleted = data["IsDeleted"] as? String
        
        if(data["IsMainBranch"] as? Bool == true)
        {
            self.IsMainBranch = 1;
        }
        //self.IsMainBranch  = Int((data["IsMainBranch"] as? Bool)!)!
        self.Latitude = data["Latitude"] as? String
        self.Longitude = data["Longitude"] as? String
        self.Password = data["Password"] as? String
        self.PhoneNumber = data["PhoneNumber"] as? String
        self.userID = data["userID"] as? String
    }
}
