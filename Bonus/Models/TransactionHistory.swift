//
//  TransactionHistory.swift
//  Bonus
//
//  Created by Arvind Sen on 12/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit

class TransactionHistory: NSObject {
    // String() created empty string
    var Amount: String?  = String()
    var AmountHome: String?  = String()
    var CreditAmount: String?  = String()
    var CurrencyAbrrevation: String?  = String()
    var CurrencyAbrrevationHome: String?  = String()
    var DrCrFlag: String?  = String()
    var Narration: String?  = String()
    var TransactionDate: String?  = String()
    var TransactionID: String?  = String()
    var TransactionType: String?  = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>) {
        self.Amount = data["Amount"] as? String
        self.AmountHome = data["AmountHome"] as? String
        self.CreditAmount = data["Address"] as? String
        self.CurrencyAbrrevation = data["CurrencyAbrrevation"] as? String
        self.CurrencyAbrrevationHome = data["CurrencyAbrrevationHome"] as? String
        self.DrCrFlag = data["DrCrFlag"] as? String
        self.Narration = data["Narration"] as? String
        self.TransactionDate = data["TransactionDate"] as? String
        self.TransactionID = data["TransactionID"] as? String
        self.TransactionType = data["TransactionType"] as? String
    }
}
