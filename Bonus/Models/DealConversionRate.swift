//
//  DealConversionRate.swift
//  Bonus
//
//  Created by Arvind Sen on 09/05/17.
//  Copyright © 2016 Bhumati. All rights reserved.
//

class DealConversionRate: NSObject {

    var CustomerCreditBalance : String?  = String()  // String() created empty string
    var CustomerCurrencyCode : String?  = String()
    var ConvertedDealPrice : String?  = String()
    var CustomerCurrencyCodeHome : String?  = String()
    var ConvertedDealPriceHome : String?  = String()
    var DealPrice : String?  = String()
    var BusinessCurrencyCode : String?  = String()
    var ConvertedDealPriceBaseCurrency : String?  = String()
    
    override init(){
        super.init()
    }
    
    init(data:Dictionary<String, AnyObject>) {
        
        self.CustomerCreditBalance = data["CustomerCreditBalance"] as? String
        self.CustomerCurrencyCode = data["CustomerCurrencyCode"] as? String
        self.ConvertedDealPrice = data["ConvertedDealPrice"] as? String
        self.CustomerCurrencyCodeHome = data["CustomerCurrencyCodeHome"] as? String
        self.ConvertedDealPriceHome = data["ConvertedDealPriceHome"] as? String
        self.DealPrice = data["DealPrice"] as? String
        self.BusinessCurrencyCode = data["BusinessCurrencyCode"] as? String
        self.ConvertedDealPriceBaseCurrency = data["ConvertedDealPriceBaseCurrency"] as? String
    }
}
