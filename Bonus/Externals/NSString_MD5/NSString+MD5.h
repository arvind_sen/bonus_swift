//
//  NSString+MD5.h
//  Bonus
//
//  Created by Arvind Sen on 15/03/16.
//  Copyright © 2016 Arvind Sen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>

@interface NSString(MD5)
- (NSString *)MD5String;
@end
