//
//  MyAnnotation.swift
//  Bonus
//
//  Created by Arvind Sen on 17/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import MapKit

class MyAnnotation: NSObject, MKAnnotation {
    public var coordinate: CLLocationCoordinate2D

    var coordinates: CLLocationCoordinate2D?

    
    init(coord: CLLocationCoordinate2D) {
        coordinates = coord;
        self.coordinate = coord;
    }
    
    private func subtitle()->String?{
        return nil;
    }
    
    private func title()->String? {
        return nil;
    }
    
    func setCoordinate(newCoordinate: CLLocationCoordinate2D) {
        coordinates = newCoordinate;
    }
}
