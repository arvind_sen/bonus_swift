//
//  CustomProgressView.swift
//  Bonus
//
//  Created by Arvind Sen on 26/08/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit

class CustomProgressView: UIView {
    //var imageView: UIImageView?
    
    class func showProgressIndicator() {
        
        let appWindow = UIApplication.shared.windows.last
        
       
        print("self.appWindow = \(appWindow?.bounds)");
        
        let customProgressView = CustomProgressView.init(frame: appWindow!.frame)
        
        customProgressView.backgroundColor = UIColor.init(red: 50/255.0, green: 50/255.0, blue: 50/255.0, alpha: 0.5);
        //customProgressView.backgroundColor = UIColor.redColor();
        customProgressView.tag = 200;
        
        appWindow!.addSubview(customProgressView);
        
        let width : CGFloat = 75.0;
        let height : CGFloat = 75.0;
        
        let oX = (customProgressView.frame.width - width)/2
        let oY = (customProgressView.frame.height - height)/2
        
        let imageView = UIImageView.init(frame: CGRect(x: oX, y: oY, width: width, height: height))
        customProgressView.addSubview(imageView);
        
        let image1: UIImage = UIImage(named: "BonusProgress1")!;
//        let image2: UIImage = UIImage(named: "BonusProgress2")!;
//        let image3: UIImage = UIImage(named: "BonusProgress3")!;
//        let image4: UIImage = UIImage(named: "BonusProgress4")!;
        //imageView.animationImages = [image1, image2, image3, image4];
        imageView.image = image1;
        //imageView.animationRepeatCount = 0;
        //imageView.animationDuration = 0.5;
        
        /*
        let transition = CATransition()
        //CATransition *transition = [CATransition animation];
        transition.duration = 0.25;
        //transition.timingFunction =  [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.timingFunction = CAMediaTimingFunction.init(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade;
        transition.delegate = self;
        transition.duration = 0.50
        transition.repeatCount = 200;
        transition.autoreverses = true;
        imageView.layer.addAnimation(transition, forKey: nil);
        //[imageView.layer addAnimation:transition forKey:nil];
         */
        
        let coffeeShakeAnimation = CABasicAnimation(keyPath: "opacity")
        coffeeShakeAnimation.duration = 0.50
        coffeeShakeAnimation.repeatCount = 1000
        coffeeShakeAnimation.autoreverses = true;
        //coffeeShakeAnimation.fillMode = kCAFillModeBoth;
        coffeeShakeAnimation.fromValue = 1.0;
        coffeeShakeAnimation.toValue = 0.5
        imageView.layer.add(coffeeShakeAnimation, forKey: "opacity")
        
        imageView.contentMode = UIViewContentMode.scaleAspectFit;
        //imageView.startAnimating()
        
    }
    
    class func hideProgressIndicator(){
        
        let appWindow = UIApplication.shared.windows.last
        
        if (appWindow != nil) {
            
            for anyObj in (appWindow?.subviews)! {
                print("anyObj = %@", anyObj);
                
                if (appWindow?.viewWithTag(200) !=  nil) {
                    let view = appWindow!.viewWithTag(200)! as UIView;
                    view.removeFromSuperview();
                }
            }
        }
    }
    
    class func showProgressIndicatorOnView(VC: UIViewController) {
        
        let appWindow = VC.navigationController?.view;
        
        if (appWindow != nil)
        {
            print("self.appWindow = \(appWindow?.bounds)");
            
            let customProgressView = CustomProgressView.init(frame: appWindow!.frame)
            
            customProgressView.backgroundColor = UIColor.init(red: 50/255.0, green: 50/255.0, blue: 50/255.0, alpha: 0.5);
            //customProgressView.backgroundColor = UIColor.redColor();
            customProgressView.tag = 45354;
            
            appWindow!.addSubview(customProgressView);
            
            let width : CGFloat = 75.0;
            let height : CGFloat = 75.0;
            
            let oX = (customProgressView.frame.width - width)/2
            let oY = (customProgressView.frame.height - height)/2
            
            let imageView = UIImageView.init(frame: CGRect(x: oX, y: oY, width: width, height: height))
            customProgressView.addSubview(imageView);
            
            let image1: UIImage = UIImage(named: "BonusProgress1")!;
            //        let image2: UIImage = UIImage(named: "BonusProgress2")!;
            //        let image3: UIImage = UIImage(named: "BonusProgress3")!;
            //        let image4: UIImage = UIImage(named: "BonusProgress4")!;
            //imageView.animationImages = [image1, image2, image3, image4];
            imageView.image = image1;
            //imageView.animationRepeatCount = 0;
            //imageView.animationDuration = 0.5;
            
            /*
             let transition = CATransition()
             //CATransition *transition = [CATransition animation];
             transition.duration = 0.25;
             //transition.timingFunction =  [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
             transition.timingFunction = CAMediaTimingFunction.init(name: kCAMediaTimingFunctionEaseInEaseOut)
             transition.type = kCATransitionFade;
             transition.delegate = self;
             transition.duration = 0.50
             transition.repeatCount = 200;
             transition.autoreverses = true;
             imageView.layer.addAnimation(transition, forKey: nil);
             //[imageView.layer addAnimation:transition forKey:nil];
             */
            
            let coffeeShakeAnimation = CABasicAnimation(keyPath: "opacity")
            coffeeShakeAnimation.duration = 0.50
            coffeeShakeAnimation.repeatCount = 1000
            coffeeShakeAnimation.autoreverses = true;
            //coffeeShakeAnimation.fillMode = kCAFillModeBoth;
            coffeeShakeAnimation.fromValue = 1.0;
            coffeeShakeAnimation.toValue = 0.5
            imageView.layer.add(coffeeShakeAnimation, forKey: "opacity")
            
            imageView.contentMode = UIViewContentMode.scaleAspectFit;
            //imageView.startAnimating()
        }
        
    }
    
    class func hideProgressIndicatorOnView(VC: UIViewController){
        
        //let appWindow = UIApplication.shared.windows.last
        let appWindow = VC.navigationController?.view;
        if (appWindow?.viewWithTag(45354) !=  nil) {
            let view = appWindow!.viewWithTag(45354)! as UIView;
            view.removeFromSuperview();
        }
    }
    
}
