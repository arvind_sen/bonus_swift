//
//  CustomPickerView.swift
//  NMS
//
//  Created by Arvind Sen on 21/12/15.
//  Copyright © 2015 Arvind Sen. All rights reserved.
//

let PICKER_VIEW_HEIGHT: Float = 200
let PICKER_TOP_HEADER_HEIGHT : CGFloat = 44.0
let BOTTOM_TAB_BAR_HEIGHT : CGFloat = 20.0
let PICKER_HEIGHT: Float = 176

@objc protocol CustomPickerViewDelegate : NSObjectProtocol {
    
    @objc optional func customPickerViewTitleForRow(row: Int, pickerView: UIView)->String;
    @objc optional func removeCustomPickerView(buttonTitle: String, pickerView: UIView);
    @objc optional func datePickerViewValueChanged(dateStr: String, pickerView: UIView)
}

import UIKit
class CustomPickerView: UIView, UIPickerViewDataSource, UIPickerViewDelegate {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    //var selectedDefaultRow: String!
    var dataArray : NSArray!
    var txtField: UITextField!;
    weak var pickerViewDelegate : CustomPickerViewDelegate? = nil
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.frame = CGRect(x: CGFloat(0.0), y: CGFloat(frame.size.height), width: frame.size.width, height: CGFloat(PICKER_VIEW_HEIGHT))
        print("======\(self.frame)")
        
        self.backgroundColor = UIColor.white;
        
        //let toolBarHeight = CGFloat(44.0);
        let toolBarFrame = CGRect(x: 0.0, y: 0.0, width: frame.size.width, height: PICKER_TOP_HEADER_HEIGHT)
        let toolBar = UIToolbar(frame: toolBarFrame)
        toolBar.tintColor = UIColor.darkGray;
        toolBar.backgroundColor = UIColor.darkGray;
        
        let toolBarItemDoneBtn = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(CustomPickerView.pickerDoneButtonTapped(_:)))
        let toolBarItemCancelBtn = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.done, target: self, action: #selector(CustomPickerView.pickerCancelButtonTapped(_:)))
        
        
        let oX: CGFloat = (frame.width - frame.size.width * 0.63)/2 ;
        self.txtField = UITextField(frame: CGRect(x: oX, y: 10, width: frame.size.width * 0.63, height: PICKER_TOP_HEADER_HEIGHT-10));
        self.txtField.textAlignment = NSTextAlignment.center;
        self.txtField.borderStyle = UITextBorderStyle.roundedRect
        self.txtField.backgroundColor = UIColor.white;
        self.txtField.isUserInteractionEnabled = false
        let textFieldInToolBar = UIBarButtonItem(customView: self.txtField);
        
        //let toolBarFlexibleWidth = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let array = NSArray(array: [toolBarItemCancelBtn, textFieldInToolBar, toolBarItemDoneBtn])
        
        toolBar.items = array as? [UIBarButtonItem];
        self.addSubview(toolBar)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func showCustomPickerViewWithData(_ data: NSArray){
        
        self.dataArray = data;
        
        // Setting picker view
        let width = CGFloat(self.frame.size.width)
        let height = CGFloat(self.frame.size.height - PICKER_TOP_HEADER_HEIGHT);
        let oX = CGFloat(0);
        let oY = PICKER_TOP_HEADER_HEIGHT;

        let picker = UIPickerView(frame: CGRect(x: oX, y: oY, width: width, height: height))
        picker.backgroundColor = UIColor.white;
        picker.tintColor = UIColor.darkGray;
        picker.delegate = self;
        //picker.tag = 900;
        self.addSubview(picker);
        self.backgroundColor = UIColor.clear
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.frame = CGRect(x: CGFloat(0.0), y: CGFloat((self.superview?.frame.size.height)!) - CGFloat(PICKER_HEIGHT) - CGFloat(BOTTOM_TAB_BAR_HEIGHT)+CGFloat(20.0), width: CGFloat(self.frame.size.width), height: CGFloat(PICKER_HEIGHT))
        })
        /*
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.frame = CGRectMake(CGFloat(0.0), CGFloat((self.superview?.frame.size.height)!) - CGFloat(PICKER_HEIGHT) - CGFloat(BOTTOM_TAB_BAR_HEIGHT), CGFloat(self.frame.size.width), CGFloat(PICKER_HEIGHT))
            }) { (Bool) -> Void in
                
                if let row = selectedRow {
                    
                    var couter: Int = 0;
                    for dictionary in self.dataArray{
                        let dict = dictionary as! NSDictionary;
                    
                        for (_, value) in dict {
                            if(value as! String == row){
                                picker.selectRow(couter, inComponent: 0, animated: true)
                                break;
                            }
                        }
                        couter++;
                    }

                }
        }
        */
    }
    
    // MARK:- UIPickerViewDelegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.dataArray.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    
        var titleString = "";
        
        //titleString = dataArray.objectAtIndex(row).value;
        titleString = (self.pickerViewDelegate?.customPickerViewTitleForRow!(row: row, pickerView: self))!;
        self.txtField.text = titleString
        return titleString;
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        var titleString = "";
        
        //titleString = dataArray.objectAtIndex(row).value;
        titleString = (self.pickerViewDelegate?.customPickerViewTitleForRow!(row: row, pickerView: self))!;
        self.txtField.text = titleString
        
    }
    
    // Funtion is using to call date picker view
    func showDatePickerView (_ setMinMax: String?)
    {
        let currentDate = Date();
        
        let width = CGFloat(self.frame.size.width)
        let height = CGFloat(self.frame.size.height - PICKER_TOP_HEADER_HEIGHT);
        let oX = CGFloat(0);
        let oY = PICKER_TOP_HEADER_HEIGHT;
        
        let datePicker = UIDatePicker(frame: CGRect(x: oX, y: oY, width: width, height: height))
        datePicker.backgroundColor = UIColor.white;
        datePicker.tintColor = UIColor.darkGray;
        datePicker.datePickerMode = UIDatePickerMode.date;
        
        if let set = setMinMax{
            
            if(set == "MaxToday"){
                datePicker.maximumDate = Date();
            }
            else if(set == "MinToday"){
                datePicker.minimumDate = Date();
            }
        }
        
        datePicker.tag = 108;
        datePicker.date = currentDate
        datePicker.addTarget(self, action: #selector(CustomPickerView.datePickerChanged(_:)), for: UIControlEvents.valueChanged)
        self.addSubview(datePicker);
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.frame = CGRect(x: CGFloat(0.0), y: CGFloat((self.superview?.frame.size.height)!) - CGFloat(PICKER_HEIGHT) - CGFloat(BOTTOM_TAB_BAR_HEIGHT), width: CGFloat(self.frame.size.width), height: CGFloat(PICKER_HEIGHT))
            
        })
    }
    
    func setPreviousEnteredDate(dateString : String?){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.dateFormat = "dd MMM yyyy";
        let strDate = dateFormatter.date(from: dateString!)
        self.txtField.text = dateString!;

        let picker = self.viewWithTag(108) as! UIDatePicker;
        picker.setDate(strDate!, animated: true);
    }
    
    // Method get called when user start change value
    func datePickerChanged(_ datePicker: UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.dateFormat = "dd MMM yyyy";
        let strDate = dateFormatter.string(from: datePicker.date)
        self.txtField.text = strDate;
        pickerViewDelegate?.datePickerViewValueChanged!(dateStr: strDate, pickerView: self)
    }
    
    // MARK:- Picker button helping methods
    func pickerDoneButtonTapped(_ sender: AnyObject?)
    {
        //pickerFlag = false;
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.frame = CGRect(x: CGFloat(0.0), y: CGFloat(CGFloat((self.superview?.frame.size.height)!) + CGFloat(PICKER_HEIGHT) + CGFloat(BOTTOM_TAB_BAR_HEIGHT)), width: CGFloat(self.frame.size.width), height: CGFloat(PICKER_HEIGHT))
            
            }, completion: { (Bool) -> Void in
                self.removeFromSuperview()
                self.pickerViewDelegate?.removeCustomPickerView!(buttonTitle: "Done", pickerView: self);
        }) 
    }
    
    func pickerCancelButtonTapped(_ sender: AnyObject?)
    {
        //pickerFlag = false;
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.frame = CGRect(x: CGFloat(0.0), y: CGFloat(CGFloat((self.superview?.frame.size.height)!) + CGFloat(PICKER_VIEW_HEIGHT) + CGFloat(BOTTOM_TAB_BAR_HEIGHT)), width: CGFloat(self.frame.size.width), height: CGFloat(PICKER_VIEW_HEIGHT))
            //self.txtFDob.text = "";
            }, completion: { (Bool) -> Void in
                self.removeFromSuperview()
                self.pickerViewDelegate?.removeCustomPickerView!(buttonTitle: "Cancel", pickerView: self);
                
        }) 
    }

}
