//
//  MyCustomAnnotation.swift
//  Bonus
//
//  Created by Arvind Sen on 03/10/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//  MyCustomAnnotation class is basically using to add annotation on map view with different properties.

import UIKit
import MapKit

class MyCustomAnnotation: NSObject, MKAnnotation {
    
    var coordinates : CLLocationCoordinate2D?
    var title : String?
    var subtitle: String?
    var dealCategory : String?
    var indexNumber : NSInteger?
    var annotationNumber : NSInteger?
    var bonusPotLocationID : String?
    var bonusPotCurrentBalance : String?
    var dealCategroyImage : String?
    
    var coordinate: CLLocationCoordinate2D {
        return self.coordinates!
    }
    
//    var subtitle: String {
//        return self.subTitle!
//    }
    
    init(title: String, coordinate: CLLocationCoordinate2D, subtitle: String){
        super.init()
        self.title = title
        self.coordinates = coordinate;
        self.subtitle = subtitle;
    }
}
