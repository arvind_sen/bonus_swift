//
//  CCAvenueWebViewController.m
//  Bonus
//
//  Created by Arvind Sen on 12/07/16.
//  Copyright © 2016 Arvind Sen. All rights reserved.
//

#import "CCAvenueWebViewController.h"
#import "CCTool.h"
#import "MBProgressHUD.h"
#import "TFHpple.h"
#import "TFHppleElement.h"
#import "XPathQuery.h"

@interface CCAvenueWebViewController ()

@end

@implementation CCAvenueWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    
    // Setting user access code and merchant id, these got from CC Avenue account
    // CCAvenue keys
    if([self.currency isEqual: @"INR"])
    {
        self.accessCode = CCAvenue_Access_Code_INR;
        self.merchantId = CCAvenue_Merchant_Id_INR;
        self.redirectUrl = CCAvenue_Redirect_Url_INR;
        self.cancelUrl = CCAvenue_Cancel_Url_INR;
        self.rsaKeyUrl = CCAvenue_RsaKey_Url_INR;
        self.transactionUrl = CCAvenue_Transaction_Url_INR;
    }
    else
    {
        self.accessCode = CCAvenue_Access_Code_AED;
        self.merchantId = CCAvenue_Merchant_Id_AED;
        self.redirectUrl = CCAvenue_Redirect_Url_AED;
        self.cancelUrl = CCAvenue_Cancel_Url_AED;
        self.rsaKeyUrl = CCAvenue_RsaKey_Url_AED;
        self.transactionUrl = CCAvenue_Transaction_Url_AED;
    }
    
    self.orderId = @"";
    // Below are the files downloaded from CC Avenue and uploaded on merchant server also it have other files like .pem etc.
    
    
    NSString *randomString = [self randomStringWithLength:12];
    self.orderId = [NSString stringWithFormat:@"BONUSAPP-%@", [randomString uppercaseString]];
    NSLog(@"---- %@", self.orderId);
    [self showScreenLayout];
    [self showProgressHud];
}

-(NSString *)randomStringWithLength: (int) len {
    
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    return randomString;
}

-(void)viewDidAppear:(BOOL)animated{
    [self callCCAvenueApis];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showScreenLayout{
    
    float topContainerWidth = self.view.frame.size.width;
    float topContainerHeight = self.view.frame.size.height * 0.12;
    
    float oX = 0.0;
    float oY = 0.0;
    
    UIView *topViewContainer = [[UIView alloc] initWithFrame:CGRectMake(oX, oY, topContainerWidth, topContainerHeight)];
    topViewContainer.backgroundColor = [UIColor darkGrayColor];
    [self.view addSubview:topViewContainer];
    
    float labelHeight = 40.0;
    UILabel *topLabel = [[UILabel alloc] initWithFrame:CGRectMake(oX, topContainerHeight - labelHeight, topContainerWidth, labelHeight)];
    topLabel.backgroundColor = [UIColor darkGrayColor];
    topLabel.textAlignment = NSTextAlignmentCenter;
    topLabel.text = @"Bonus Payment";
    topLabel.textColor = [UIColor whiteColor];
    topLabel.font = [UIFont boldSystemFontOfSize:16.0];
    [topViewContainer addSubview:topLabel];
    
    //Setting cancel button
    float btnCanceloX = 5;
    float btnCanceloY = topContainerHeight - labelHeight;
    CGRect cancelBtnSize = CGRectMake(btnCanceloX, btnCanceloY, 60.0, labelHeight);
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = cancelBtnSize;
    [cancelBtn setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [topViewContainer addSubview:cancelBtn];
    cancelBtn = nil;
    
    float webViewHeight = self.view.frame.size.height - topContainerHeight;
    
    self.viewWeb = [[UIWebView alloc] initWithFrame:CGRectMake(0, topContainerHeight, self.view.frame.size.width, webViewHeight)];
    self.viewWeb.delegate = self;
    [self.view addSubview:self.viewWeb];
}

-(void)showProgressHud{
    UIWindow *appWindow = [[[UIApplication sharedApplication] windows] lastObject];
    NSLog(@"self.appWindow = %@", appWindow);
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:appWindow animated:YES];
    hud.dimBackground = YES;
    
}

-(void)hideProgressHud{
    
    UIWindow *appWindow = [[[UIApplication sharedApplication] windows] lastObject];
    NSLog(@"self.appWindow = %@", appWindow);
    [MBProgressHUD hideHUDForView:appWindow animated:YES];
}

-(void)cancelButtonTapped:(id)sender{
    [self.ccAvenueDelegate removeCCAvenueWebViewController:self withResponse:nil];
}


-(void)callCCAvenueApis {
    
    NSString *rsaKeyDataStr = [NSString stringWithFormat:@"access_code=%@&order_id=%@",self.accessCode,self.orderId];
    NSData *requestData = [NSData dataWithBytes: [rsaKeyDataStr UTF8String] length: [rsaKeyDataStr length]];
    NSMutableURLRequest *rsaRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: self.rsaKeyUrl]];
    [rsaRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [rsaRequest setHTTPMethod: @"POST"];
    [rsaRequest setHTTPBody: requestData];

    NSData *rsaKeyData = [NSURLConnection sendSynchronousRequest: rsaRequest returningResponse: nil error: nil];
    NSString *rsaKey = [[NSString alloc] initWithData:rsaKeyData encoding:NSASCIIStringEncoding];
    rsaKey = [rsaKey stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    rsaKey = [NSString stringWithFormat:@"-----BEGIN PUBLIC KEY-----\n%@\n-----END PUBLIC KEY-----\n",rsaKey];
    NSLog(@"%@",rsaKey);
    
    //Encrypting Card Details
    NSString *myRequestString = [NSString stringWithFormat:@"amount=%@&currency=%@", self.amount, self.currency];
    CCTool *ccTool = [[CCTool alloc] init];
    NSString *encVal = [ccTool encryptRSA:myRequestString key:rsaKey];
    encVal = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                   (CFStringRef)encVal,
                                                                                   NULL,
                                                                                   (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8 ));
    
    //Preparing for a webview call
    NSString *urlAsString = [NSString stringWithFormat: @"%@", self.transactionUrl];
    NSString *encryptedStr = [NSString stringWithFormat:@"merchant_id=%@&order_id=%@&redirect_url=%@&cancel_url=%@&enc_val=%@&access_code=%@",self.merchantId, self.orderId, self.redirectUrl,self.cancelUrl, encVal, self.accessCode];
    
    NSData *myRequestData = [NSData dataWithBytes: [encryptedStr UTF8String] length: [encryptedStr length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlAsString]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setValue:urlAsString forHTTPHeaderField:@"Referer"];
    [request setHTTPMethod: @"POST"];
    [request setHTTPBody: myRequestData];
    [self.viewWeb loadRequest:request];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [self hideProgressHud];
    
    NSString *string = webView.request.URL.absoluteString;
    NSLog(@"===== String ===== %@", string);
    
    if ([string rangeOfString:@"/ccavResponseHandler.php"].location != NSNotFound) {
        NSString *html = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
        
        NSString *transStatus = @"Not Known";
        
        if (([html rangeOfString:@"Aborted"].location != NSNotFound) ||
            ([html rangeOfString:@"Cancel"].location != NSNotFound)) {
            
            transStatus = @"Transaction has been cancelled";
            [self cancelButtonTapped:nil];
            
        }else if (([html rangeOfString:@"Success"].location != NSNotFound)) {
            
            transStatus = @"Transaction successful";
            
        }else if (([html rangeOfString:@"Fail"].location != NSNotFound)) {
            
            transStatus = @"Transaction failed";
            [self cancelButtonTapped:nil];
        }
        
        [self parseSuccessfulTransactionDataAndResponseToDelegate:html]; // Parsing of html
        
        // Show status in alert
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:transStatus preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* MyAlert = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:MyAlert];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)parseSuccessfulTransactionDataAndResponseToDelegate :(NSString *)htmlString {
    
    NSData *data = [htmlString dataUsingEncoding:NSUTF8StringEncoding];
    //NSData *data = [NSData dataWithContentsOfFile:htmlString];
    NSData *tutorialsHtmlData = data;
    // 2
    TFHpple *tutorialsParser = [TFHpple hppleWithHTMLData:tutorialsHtmlData];
    // 3
    NSString *tutorialsXpathQueryString = @"//html/body/center/table/tbody/tr";
    NSArray *tutorialsNodes = [tutorialsParser searchWithXPathQuery:tutorialsXpathQueryString];
    
    NSMutableDictionary *addDict = [NSMutableDictionary new];
    
    for (TFHppleElement *element in tutorialsNodes) {
        
        if (element.children.count)
        {
            TFHppleElement *firstElement = element.children.firstObject;
            NSString *dictKey = firstElement.content;
            
            TFHppleElement *lastElement = element.children.lastObject;
            NSString *dictValue = lastElement.content;
            
            [addDict setObject:dictValue forKey:dictKey];
        }
    }
    
    [self.ccAvenueDelegate removeCCAvenueWebViewController:self withResponse:addDict]; // Return response to delegate
}

-(void)refundCcAvenueAmountIfMarchantServerNotUpdateItsDB {
    //ccAvenuePaymentReturnData
    //enc_request=63957FB55DD6E7B968A7588763E08B240878046EF2F520C44BBC63FB9CCE726209A4734877F5904445591304ABB2F5E598B951E39EAFB9A24584B00590ADB077ADE5E8C444EAC5A250B1EA96F68D22E44EA2515401C2CD753DBA91BD0E7DFE7341BE1E7B7550&access_code=8JXENNSSBEZCU8KQ&command=confirmOrder&request_type=XML&response_type=XML&version=1.1
    
    NSMutableDictionary *requestData1 = [[NSMutableDictionary alloc] init];
    [requestData1 setValue:@"105075181516" forKey:@"reference_no"];
    [requestData1 setValue:@"1.0" forKey:@"refund_amount"];
    [requestData1 setValue:@"427795309" forKey:@"refund_ref_no"];
    
    NSError *error = nil;
    NSData *jsonData=[NSJSONSerialization dataWithJSONObject:requestData1 options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString* myRequestString = [[NSString alloc] initWithData:jsonData encoding:NSASCIIStringEncoding];
    NSLog(@"Request JSON Object => %@", myRequestString);
    
    
    
    NSString *rsaKeyDataStr = [NSString stringWithFormat:@"access_code=%@&order_id=%@", self.accessCode, @"BONUSAPP-omGsRzqkfmL"];
    NSData *requestData = [NSData dataWithBytes: [rsaKeyDataStr UTF8String] length: [rsaKeyDataStr length]];
    NSMutableURLRequest *rsaRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: self.rsaKeyUrl]];
    [rsaRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [rsaRequest setHTTPMethod: @"POST"];
    [rsaRequest setHTTPBody: requestData];
    NSData *rsaKeyData = [NSURLConnection sendSynchronousRequest: rsaRequest returningResponse: nil error: nil];
    NSString *rsaKey = [[NSString alloc] initWithData:rsaKeyData encoding:NSASCIIStringEncoding];
    rsaKey = [rsaKey stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    rsaKey = [NSString stringWithFormat:@"-----BEGIN PUBLIC KEY-----\n%@\n-----END PUBLIC KEY-----\n",rsaKey];
    NSLog(@"%@",rsaKey);
    
    
    //Encrypting Card Details
    //NSString *myRequestString = [NSString stringWithFormat:@"amount=%@&currency=%@", self.amount, self.currency];
    CCTool *ccTool = [[CCTool alloc] init];
    NSString *encVal = [ccTool encryptRSA:myRequestString key:rsaKey];
    encVal = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                   (CFStringRef)encVal,
                                                                                   NULL,
                                                                                   (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                   kCFStringEncodingUTF8 ));
    
    //Preparing for a webview call
    //NSString *urlAsString = [NSString stringWithFormat:@"https://secure.ccavenue.com/transaction/initTrans"];
    NSString *urlAsString = @"https://secure.ccavenue.com/transaction/transaction.do";
    NSString *encryptedStr = [NSString stringWithFormat:@"enc_request=%@&access_code=%@&command=%@&request_type=JSON&response_type=JSON&version=1.1", encVal, self.accessCode, @"refundOrder"];
    
    NSData *myRequestData = [NSData dataWithBytes: [encryptedStr UTF8String] length: [encryptedStr length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlAsString]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setValue:urlAsString forHTTPHeaderField:@"Referer"];
    [request setHTTPMethod: @"POST"];
    [request setHTTPBody: myRequestData];
    
    NSURLResponse *responseCode = nil;
    NSData *returData = [NSURLConnection sendSynchronousRequest: request returningResponse:&responseCode error: nil];
    
    //[[[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:[self responseEncoding]] autorelease];
    
    NSLog(@"Return Data = %@", [[NSString alloc] initWithData:returData encoding:NSASCIIStringEncoding]);
    
    //    NSMutableURLRequest *rsaRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: CCAvenue_RsaKey_Url]];
    //    [rsaRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    //    [rsaRequest setHTTPMethod: @"POST"];
    //    [rsaRequest setHTTPBody: requestData];
    //    NSData *rsaKeyData = [NSURLConnection sendSynchronousRequest: rsaRequest returningResponse: nil error: nil];
    
    //[self.viewWeb loadRequest:request];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
