//
//  CCAvenueWebViewController.h
//  Bonus
//
//  Created by Arvind Sen on 12/07/16.
//  Copyright © 2016 Arvind Sen. All rights reserved.
//

#import <UIKit/UIKit.h>

// CCAvenue India(INR) credentials and links
NSString* CCAvenue_Access_Code_INR = @"AVTO65DG95AF59OTFA";
NSString* CCAvenue_Merchant_Id_INR = @"103030";
NSString* CCAvenue_Working_Key_INR = @"";
NSString* CCAvenue_Redirect_Url_INR = @"http://www.bonusapp.net/ccavenue/ccavResponseHandler.php";
NSString* CCAvenue_Cancel_Url_INR = @"http://www.bonusapp.net/ccavenue/ccavResponseHandler.php";
NSString* CCAvenue_RsaKey_Url_INR = @"http://www.bonusapp.net/ccavenue/GetRSA.php";
NSString* CCAvenue_Transaction_Url_INR = @"https://secure.ccavenue.com/transaction/initTrans";

// CCAvenue Dubai(AED) credentials and links
NSString* CCAvenue_Access_Code_AED = @"AVTD02FB72BB80DTBB";
NSString* CCAvenue_Merchant_Id_AED = @"44668";
NSString* CCAvenue_Working_Key_AED = @"";
NSString* CCAvenue_Redirect_Url_AED = @"http://www.bonusapp.net/ccavenuedubai/ccavResponseHandler.php";
NSString* CCAvenue_Cancel_Url_AED = @"http://www.bonusapp.net/ccavenuedubai/ccavResponseHandler.php";
NSString* CCAvenue_RsaKey_Url_AED = @"http://www.bonusapp.net/ccavenuedubai/GetRSA.php";
NSString* CCAvenue_Transaction_Url_AED = @"https://secure.ccavenue.ae/transaction/initTrans";

@class CCAvenueWebViewController;

@protocol CCAvenueWebViewDelegate <NSObject>
    //@optional
-(void)removeCCAvenueWebViewController:(CCAvenueWebViewController *)objCCAvenue withResponse:(NSDictionary *)returnData;
@end

@interface CCAvenueWebViewController : UIViewController <UIWebViewDelegate>

@property UIWebView *viewWeb;
@property (weak, nonatomic) id<CCAvenueWebViewDelegate> ccAvenueDelegate;

@property NSString *accessCode;
@property NSString *merchantId;
@property NSString *orderId;
@property NSString *amount;
@property NSString *currency;
@property NSString *redirectUrl;
@property NSString *cancelUrl;
@property NSString *rsaKeyUrl;
@property NSString *transactionUrl;

-(NSString *)randomStringWithLength: (int) len ;

@end
