//
//  AppUtility.swift
//  Bonus
//
//  Created by Arvind Sen on 25/08/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit
import SystemConfiguration

class AppUtility: NSObject {

    class func topNavigationBackgroundColor (_ VC: UIViewController) {
        
        let rect = CGRect(x: 0, y: 0, width: VC.navigationController!.navigationBar.frame.size.width, height: 64);
        UIGraphicsBeginImageContext(CGSize(width: VC.navigationController!.navigationBar.frame.size.width, height: 64));
        let context = UIGraphicsGetCurrentContext();
        context?.setFillColor(UIColor(red: 10/255.0, green: 10/255.0, blue: 10/255.0, alpha: 0.1).cgColor);
        context?.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        VC.navigationController!.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        //self.navigationController!.navigationBar.shadowImage = image
        VC.navigationController!.navigationBar.isTranslucent = true
        
        VC.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.green]
    }
    
    class func validateEmail(_ emailID: String) -> Bool
    {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailPredicate.evaluate(with: emailID)
    }

    class func showAlertWithTitleAndMessage(_ alertTitle: String!, alertMessage:String!, onView:AnyObject?){
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        onView!.present(alert, animated: true, completion: nil)
        
    }
    
    
    class func imageWithImage(_ image: UIImage, scaledToMaxWidth: CGFloat, scaledToMaxHeight: CGFloat) -> UIImage {
        let oldWidth = image.size.width;
        let oldHeight = image.size.height;
        
        let scaleFactor = (oldWidth > oldHeight) ? scaledToMaxWidth / oldWidth: scaledToMaxHeight/oldHeight;
        let newHeight = oldHeight * scaleFactor;
        let newWidth = oldWidth * scaleFactor;
        let newSize = CGSize(width: newWidth, height: newHeight);
        
        if(UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale))) {
            UIGraphicsBeginImageContextWithOptions(newSize, false, UIScreen.main.scale)
        } else {
            UIGraphicsBeginImageContext(newSize);
        }
        
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height));
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage!;
    }
    
    class func getDateFromString(_ dateString: String, currentFormat: String, desiredFormat: String) -> String {
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = currentFormat;
        let myDate: Date = dateFormatter.date(from: dateString)!
        
        let formatter = DateFormatter();
        formatter.dateFormat = desiredFormat;
        let stringFromDate = formatter.string(from: myDate)
        return stringFromDate;
    }

    
    class func timeLeftSinceDate(_ dateT: Date) -> NSInteger {
        var timeLeft: NSInteger? = 0;
        let today10am = Date()
        
        var seconds = today10am.timeIntervalSince(dateT);
        let days = Int(floor((seconds/(3600*24))))
        if(days > 0){ seconds = seconds - (Double(days) * 3600 * 24)}
        
       let hours = Int((floor(seconds / 3600)));
        if(hours > 0){ seconds = seconds - Double(hours) * 3600;}
        
       let minutes = Int( (floor(seconds / 60)));
        if(minutes > 0) {seconds = seconds - Double(minutes) * 60;}
        
        if(days > 0) {
            timeLeft = days * -1;
        }
        else if(hours > 0) { timeLeft = hours * -1;
        }
        else if(minutes > 0) { timeLeft = minutes * -1;
        }
        else {timeLeft = Int(seconds) * -1;}
        
        return timeLeft!;
        
    }
    
    class func numberOfDaysSinceDate(_ dateTo: String, currentDateFormat: String) -> String {
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = currentDateFormat;
        let myDate = dateFormatter.date(from: dateTo);
        let date1 = Date();
        
        let calender = Calendar.init(identifier: Calendar.Identifier.gregorian);
        let unitFlags: NSCalendar.Unit = [.hour, .day, .minute, .second]
        let components = (calender as NSCalendar?)?.components(unitFlags, from: date1, to: myDate!, options: NSCalendar.Options.init(rawValue: 0));
        
        let remainingDays = "\((components?.day)!) Days"
        return remainingDays
    }
    
    class func daysSinceDateFrom(_ dateFrom: Date, dateTo: Date, currentDateFormat: String) -> String{
        
        let calender = Calendar.init(identifier: Calendar.Identifier.gregorian);
        let unitFlags: NSCalendar.Unit = [.hour, .day, .minute, .second]
        let components = (calender as NSCalendar?)?.components(unitFlags, from: dateFrom, to: dateTo, options: NSCalendar.Options.init(rawValue: 0));
        let remainingDays = "\((components?.day)!)"
        return remainingDays
    }
    
    class func remainingDaysAndTimeForDate(_ dateT: String, currentFormat: String) -> String {
        
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = currentFormat;
        let myDate = dateFormatter.date(from: dateT);
        let date1 = Date();
        
        let calender = NSCalendar.init(identifier: NSCalendar.Identifier.gregorian) //Calendar.init(identifier: Calendar.Identifier.gregorian);
        let unitFlags: NSCalendar.Unit = [.hour, .day, .year, .minute, .second]
        let components = calender?.components(unitFlags, from: date1, to: myDate!, options: NSCalendar.Options.init(rawValue: 0));
        
        var hours = "\((components!.hour)!)";
        var minutes = "\((components!.minute)!)";
        var seconds = "\((components!.second)!)";
        
        if (hours.characters.count < 2) {
            hours = "0\(hours)";
        }
        if (minutes.characters.count < 2) {
            minutes = "0\(minutes)";
        }
        if (seconds.characters.count < 2) {
            seconds = "0\(seconds)";
        }
        
        let remainingDays = "\((components!.day)!) Days \(hours):\(minutes):\(seconds) Remaining";
        
        return remainingDays;
        
    }
    
    /*
     *   Method will call to get any file path from the device document directory
     */
    class func getPath(_ fileName: String) -> String
    {
        let writePath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]).appendingPathComponent(fileName)
        
        return writePath.path;
    }
    
    /*
     *   Method will call to save a sql file inside device document directory
     */
    class func copyFile(_ fileName: NSString)
    {
        let dbPath: String = getPath(fileName as String)
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: dbPath) {
            let fromPath: String? = ((Bundle.main.resourcePath)! as NSString).appendingPathComponent(fileName as String)
            var error : NSError?
            do {
                try fileManager.copyItem(atPath: fromPath!, toPath: dbPath)
            } catch let error1 as NSError {
                error = error1
            }
            let alert: UIAlertView = UIAlertView()
            if (error != nil) {
                alert.message = error?.localizedDescription
                print("Error To copy file:- \(alert.message)")
            } else {
                print("Successfully Copy file")
            }
        }
    }
    
    
    class func isNetworkAvailable() -> Bool
    {
        //TODO : Test once
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }

    class func getIPAddress() -> String? {
        
        let ipData = try? Data(contentsOf: URL(string: "http://ip-api.com/json")!)
        
        let result: AnyObject? = try! JSONSerialization.jsonObject(with: ipData!, options: JSONSerialization.ReadingOptions.mutableLeaves) as AnyObject?
        var ipAddress = ""
        
        if result != nil
        {
            ipAddress = (result!.object(forKey: "query") as? String)!
        }
        
        // Get the external IP Address based on dynsns.org
        //        var error: NSError? = nil
        //        var ipAddress = ""
        //        var ipHtml = NSString(contentsOfURL:NSURL(string: "http://www.dyndns.org/cgi-bin/check_ip.cgi")! , encoding: NSUTF8StringEncoding, error: &error)
        //
        //        if error == nil
        //        {
        //            let regex = NSRegularExpression(pattern: "\\d+", options: NSRegularExpressionOptions.allZeros, error: &error)
        //
        //            if error == nil
        //            {
        //                let matches = regex!.matchesInString(ipHtml! as String, options: NSMatchingOptions.allZeros, range: NSMakeRange(0, ipHtml!.length))
        //
        //                for match in matches
        //                {
        //                    let stringNumber = ipHtml!.substringWithRange(match.range)
        //                    ipAddress = ipAddress.stringByAppendingString(stringNumber)
        //                    ipAddress = ipAddress.stringByAppendingString(".")
        //                }
        //                if ipAddress.hasSuffix(".")
        //                {
        //                    let index: String.Index = advance(ipAddress.startIndex, count(ipAddress)-1)
        //                    ipAddress = ipAddress.substringToIndex(index)
        //                }
        //            }
        //
        //            // Return External IP
        //            return ipAddress
        //        }
        //        else
        //        {
        //
        //        }
        return ipAddress
    }
    
    /* DEVICE_TOKEN */
    class func saveDeviceToken(_ saveDeviceToken: String)
    {
        UserDefaults.standard.set(saveDeviceToken, forKey: K_STR_DEVICE_TOKEN)
    }
    
    class func getDeviceToken() -> String? {
        
        if let token = UserDefaults.standard.object(forKey: K_STR_DEVICE_TOKEN) as? String
        {
            return token
        }
        else
        {
            return ""
        }
    }
    
    // Method will return unique device identifier accross app for single vender
    class func getDeviceNSUUID ()->String {
        let oSUUID = UIDevice.current.identifierForVendor?.uuidString;
        return oSUUID!;
    }
    
//    class func accessoryViewForInputFields(VC: Any)-> UIToolbar {
//        
//        var accessoryDoneButton: UIBarButtonItem!
//        let accessoryToolBar = UIToolbar(frame: CGRect(x : 0, y : 0, width : UIScreen.main.bounds.width, height : 44))
//        //Could also be an IBOutlet, I just happened to have it like this
//        let codeInput = UITextField()
//        
//        //Configured in viewDidLoad()
//        accessoryDoneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: VC, action: #selector((VC as AnyObject).accessoryViewDoneBtnPressed(_:)))
//        let accessroySpace = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil);
//        accessoryDoneButton.tintColor = UIColor.darkGray;
//        accessoryToolBar.items = [accessroySpace, accessoryDoneButton]
//        return accessoryToolBar;
//    }
    
    class func removedBackTitleAndChangeItsColor(VC : UIViewController, backTitle: String, backTintColor: UIColor) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        VC.navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
        VC.navigationController?.navigationBar.tintColor = backTintColor;
    }
    
    class func navigationColorAndItsTextColor(VC: UIViewController) {
        VC.navigationController?.navigationBar.barTintColor = TAB_BAR_BG_COLOR;
        VC.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        VC.navigationController?.isNavigationBarHidden = false;
    }
    
    
    class func userLogoutForCurrentSession(alertMessage: String, VC: UIViewController){
        
        let alert = UIAlertController(title: nil, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.logoutFromTheSessionForAllUsers();
        }))
        VC.present(alert, animated: true, completion: nil)
    }
    
    class func randomStringWithLength(stringLength: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< stringLength {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
        
    }
}
