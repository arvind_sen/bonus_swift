//
//  HelpScrollView.swift
//  Bonus
//
//  Created by Arvind Sen on 14/11/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

class HelpScrollView: UIScrollView {
    
    var callBackOnInitiatedController : ((Bool)->Void)? = nil
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.frame = frame;
        self.isPagingEnabled = true;
        self.contentOffset = CGPoint(x: 0, y: 0);
        self.alpha = 0;
        self.backgroundColor = UIColor.black;
        self.tag = 2986;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func slidingImages() -> Array<String> {
        
        return ["Business_Screen_0_0", "Business_Screen_1_0", "Business_Screen_2", "Business_Screen_3", "Business_Screen_4", "Business_Screen_5", "Business_Screen_6", "Business_Screen_7"];
    }
    
    func showSlidingWindow(pictureArray: Array<String>){
        
        var x : CGFloat = 0;
        let slidingSourceArray = pictureArray;
        let totalCount = slidingSourceArray.count - 1;
        for i in 0...totalCount {
            
            let imgV = UIImageView(frame: CGRect(x: x, y: 0, width: self.frame.size.width, height: self.frame.size.height));
            imgV.image = UIImage(named: slidingSourceArray[i]);
            
            imgV.contentMode = .scaleAspectFit;
            //imgV.backgroundColor = UIColor.init(red: 24/255.0, green: 24/255.0, blue: 24/255.0, alpha: 1);
            self.addSubview(imgV);
            x = x + UIScreen.main.bounds.size.width;
        }
        
        self.contentSize = CGSize(width: x, height: self.frame.size.height);
        
        let btnWidth : CGFloat = 35.0;
        let btnHeight : CGFloat = 25.0;
        
        let closeBtn = UIButton.init(type: UIButtonType.custom);
        closeBtn.frame = CGRect(x: (self.frame.width - btnWidth - 5), y: (self.frame.height - btnHeight - 5), width: btnWidth, height: btnHeight);
        closeBtn.setTitle("X", for: UIControlState());
        closeBtn.alpha = 0;
        closeBtn.layer.borderWidth = 1.0;
        closeBtn.layer.cornerRadius = 3.0;
        closeBtn.layer.borderColor = UIColor.white.cgColor;
        closeBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18.0);
        closeBtn.addTarget(self, action: #selector(HelpScrollView.closeHelpBtnTapped(_:)), for: UIControlEvents.touchUpInside)
        
        let appDel = UIApplication.shared.delegate as! AppDelegate;
        appDel.window?.addSubview(self);
        appDel.window?.addSubview(closeBtn);
        
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 1;
            closeBtn.alpha = 1;
            }, completion: nil);
    }
    
    func closeHelpBtnTapped(_ sender: UIButton) -> Void {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate;
        let scrView = appDel.window?.viewWithTag(2986);
        UIView.animate(withDuration: 0.3, animations: {
            scrView?.alpha = 0;
            sender.alpha = 0;
        }, completion: {(Bool) in
            self.callBackOnInitiatedController!(true);
            scrView?.removeFromSuperview();
            sender.removeFromSuperview();
        }) ;
    }
}
