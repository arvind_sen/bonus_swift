//
//  MyCouponCell.swift
//  Bonus
//
//  Created by Arvind Sen on 30/12/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit

class MyCouponCell: UITableViewCell {
    @IBOutlet weak var businessImgV: UIImageView!

    @IBOutlet weak var couponTitle: UILabel!
    
    @IBOutlet weak var couponStatusImgV: UIImageView!
    @IBOutlet weak var couponDetails: UILabel!
    @IBOutlet weak var expiredTitleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
