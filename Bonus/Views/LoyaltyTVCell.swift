//
//  LoyaltyTVCell.swift
//  Bonus
//
//  Created by Arvind Sen on 09/09/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit

class LoyaltyTVCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var businessImage: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var areaLbl: UILabel!
    
    @IBOutlet weak var redeemptionIcon: UIButton!
    
    @IBOutlet weak var mapPinBtn: UIButton!
    
    @IBOutlet weak var distanceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
