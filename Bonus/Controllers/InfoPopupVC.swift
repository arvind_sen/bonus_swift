//
//  InfoPopupVC.swift
//  Bonus
//
//  Created by Arvind Sen on 18/11/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

class InfoPopupVC: UIViewController {
    
    @IBOutlet weak var infoTitleLbl: UILabel!

    @IBOutlet weak var detailedTxtV: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false;
        self.detailedTxtV.isEditable = false;
        // Do any additional setup after loading the view.
    }
    
    @IBAction func okBtnTapped(_ sender: AnyObject) {
        
        self.view.removeFromSuperview();
        self.removeFromParentViewController();
    }
}
