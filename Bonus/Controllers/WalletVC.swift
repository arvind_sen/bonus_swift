//
//  WalletVC.swift
//  Bonus
//
//  Created by Arvind Sen on 29/06/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class WalletVC: UIViewController, UISearchBarDelegate, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var btnsContainerView: UIView!
    @IBOutlet weak var activeCouponsBtn: UIButton!
    @IBOutlet weak var moneyBtn: UIButton!
    @IBOutlet weak var couponHistoryBtn: UIButton!
    var objPlaceOnMapVC : PlaceOnMapVC? = nil;
    @IBOutlet weak var tblView: UITableView!
    var revealVC : SWRevealViewController? = nil;
    var searchBar: UISearchBar!
    var isWebServiceCalled = true
    var leftMenusSelectedFor : SelectionFor!
    var selectedDict: NSDictionary!
    var lastOffset: String!
    var selectedCategoriesFromLeftMenus : String?
    var selectedIndexPathFromLeftMenus : IndexPath?
    
    var activeCouponsListArray: NSMutableArray!
    var searchDataArray: NSMutableArray!
    var locationManager : CLLocationManager?
    var userCurrentLocationCoordinate : CLLocationCoordinate2D? ;
    var isMapViewComeBack: Bool = false;
    var objMoneyVC: MoneyVC? = nil;
    var objCouponHistoryTVC: CouponHistoryTVC? = nil;
    
    var previouslySelectedBtn: UIButton? = nil;
    var flagToNoDataFound = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // This code is using to hid left menus
        if (self.revealViewController() != nil) {
            //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        self.tblView.backgroundColor = K_LISTING_VIEW_BG_COLOR;
        
        self.showTopBtns();
        self.previouslySelectedBtn = self.activeCouponsBtn;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
        self.navigationController?.navigationBar.barTintColor = UIColor.lightGray;
        //self.navigationController?.isNavigationBarHidden = false;
        
        if (isMapViewComeBack == true && self.activeCouponsListArray != nil) {
            //self.showTableView();
        }
        else
        {
            let objVC = self.navigationController?.presentedViewController;
            print(self.childViewControllers)
            
            if (objVC != nil && objVC!.isKind(of: PlaceOnMapVC.self)) {
                // <#code#>
            }
            else if(self.childViewControllers.count > 0){
                let childVC = self.childViewControllers[0];
                if (childVC.isKind(of: MoneyVC.self)) {
                    // <#code#>
                }
            }
            else{
                self.callLocationManager();
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true);
    }
    
    func showTopBtns(){
        
        // Left toggle button configration
        
        self.revealVC = self.revealViewController()
        //self.navigationController?.navigationBar.addGestureRecognizer(self.revealVC!.panGestureRecognizer())
        
        let revealButtonItem = UIBarButtonItem();
        revealButtonItem.image = UIImage (named: "FilterIcon")
        revealButtonItem.style = UIBarButtonItemStyle.plain;
        revealButtonItem.target = self;
        revealButtonItem.action = #selector(CouponsTVC.revealToggle(_:));
        revealButtonItem.tintColor = UIColor.white;
        self.navigationItem.leftBarButtonItem = revealButtonItem;
        
        
        //Show search bar on top of the view controller
        
        let oY = CGFloat(5.0)
        let oX = CGFloat(50.0)
        
        let height = (self.navigationController?.navigationBar.frame.height)! - CGFloat(2 * oY);
        let width = (self.navigationController?.navigationBar.frame.width)! - CGFloat(2 * oX);
        self.searchBar = UISearchBar.init(frame: CGRect(x: 0.0, y: oY, width: width, height: height));
        self.searchBar.placeholder = "Search"
        self.searchBar.showsCancelButton = false;
        self.searchBar?.delegate = self;
        self.navigationItem.titleView = self.searchBar
        
        // Right Top tab bar button configration
        var rightBtn = UIImage(named: "MapWhitePin")
        rightBtn = rightBtn?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let barRightBtn = UIBarButtonItem(image: rightBtn, style: UIBarButtonItemStyle.plain, target: self, action:#selector(CouponsTVC.rightBtnTapped(_:)));
        self.navigationItem.rightBarButtonItem = barRightBtn;
    }
    
    func revealToggle(_ obj : AnyObject?){
        
        if(self.previouslySelectedBtn != nil && self.previouslySelectedBtn?.tag == 896)
        {
            // Nothing will work
        }
        else
        {
            if (self.objCouponHistoryTVC != nil) {
                
            }
            else{
                self.hideChildView();
            }
            self.searchBar.resignFirstResponder();
            self.revealVC!.revealToggle(nil)
        }
    }
    
    // Method will call to open map view on current view controller
    func rightBtnTapped(_ sender: AnyObject){
        
        if(self.previouslySelectedBtn != nil && self.previouslySelectedBtn?.tag == 896)
        {
            // Nothing will work
        }
        else
        {
            if (self.objCouponHistoryTVC != nil) {
                
            }
            else{
                self.hideChildView();
            }
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "CustomerCouponsMapViewVC") as! CustomerCouponsMapViewVC
            objVC.currentLocationCoordinate = userCurrentLocationCoordinate;
            objVC.leftMenusSelectedFor = self.leftMenusSelectedFor; //Passing same left menu selection data
            objVC.listArray = self.activeCouponsListArray;
            objVC.previouslySelectedBtn = self.previouslySelectedBtn;
            
            let transition = CATransition()
            transition.duration = 0.3
            transition.type = "flip"
            transition.subtype = kCATransitionFromLeft
            self.navigationController?.view.layer.add(transition, forKey: kCATransition)
            self.navigationController?.pushViewController(objVC, animated: false)
            
            
            objVC.callBackOnWalletVC = { listArray in
                print("listArray = %@", listArray);
                
                self.activeCouponsListArray.removeAllObjects();
                self.activeCouponsListArray = listArray;
                self.showTableView();
            }
            
            objVC.callBackWithTopButtons = { selectedBtn in
                
                if (selectedBtn.tag == 895) {
                    self.topNavigationBtnTapped(self.activeCouponsBtn);
                }
                else if (selectedBtn.tag == 896) {
                    self.topNavigationBtnTapped(self.moneyBtn);
                }
                else if (selectedBtn.tag == 897) {
                    self.topNavigationBtnTapped(self.couponHistoryBtn);
                }
            }
        }
    }

    func showTableView(){
        
        if self.searchDataArray != nil{
            self.searchDataArray.removeAllObjects()
        }
        
        self.searchDataArray = NSMutableArray();
        self.searchDataArray.addObjects(from: self.activeCouponsListArray as [AnyObject])
        
        // If displying coupon history view
        if(self.objCouponHistoryTVC != nil) {
            self.objCouponHistoryTVC?.searchDataArray = self.searchDataArray;
            self.objCouponHistoryTVC?.activeCouponsListArray =  self.activeCouponsListArray;
            self.objCouponHistoryTVC?.flagToNoDataFound = self.flagToNoDataFound;
            self.objCouponHistoryTVC?.tableView.reloadData();
        }
        else
        {
            self.tblView.reloadData();
        }
    }
    
    // Method get call to get user current location lat, long
    func callLocationManager() {
        
        if (CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied) {
            
            self.locationManager = CLLocationManager.init();// locationManager need to define at the class variable then only its call delegate methods.
            self.locationManager!.delegate = self;
            
            if self.locationManager!.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
                self.locationManager!.requestWhenInUseAuthorization();
            }
            self.locationManager!.desiredAccuracy = kCLLocationAccuracyBest;
            self.locationManager!.startUpdatingLocation();
        }
        else{
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "To re-enable, please go to Settings and turn on Location Service for this app.", onView: self)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.locationManager!.stopUpdatingLocation();
        let recentLocation = locations.last;
        self.userCurrentLocationCoordinate = recentLocation?.coordinate;
        
        let latString = String(format: "%f", (recentLocation?.coordinate.latitude)!);
        let longString = String(format: "%f", (recentLocation?.coordinate.longitude)!);
        
        let dict = NSDictionary.init(objects: [latString, longString], forKeys: ["Latitude" as NSCopying, "Longitude" as NSCopying]);
        
        if(isWebServiceCalled == true)
        {
            self.isWebServiceCalled = false;
            self.callActiveCouponsWebService(SelectionFor.isSelectedNearBy, withParameters: dict, offSetValue: "0");
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(" failed to get current location");
        
        self.locationManager!.stopUpdatingLocation();
        
        let dict = NSDictionary.init(objects: ["0.0", "0.0"], forKeys: ["Latitude" as NSCopying, "Longitude" as NSCopying]);
        
        if(isWebServiceCalled == true)
        {
            self.isWebServiceCalled = false;
            self.callActiveCouponsWebService(SelectionFor.isSelectedNearBy, withParameters: dict, offSetValue: "0");
        }
    }
    
    func callCouponsTVCFromLeftMenu(_ indexPath: IndexPath, categoriesIds: String = "") -> Void {
        
        print("indexPath = %@", indexPath);
        
        if (indexPath.section == 0 && indexPath.row == 0) {
            self.callActiveCouponsWebService(SelectionFor.isSelectedFavorite, withParameters: nil)
        }
        else if (indexPath.section == 1 && indexPath.row == 0){
            self.callLocationManager();
        }
        else if (indexPath.section == 1 && indexPath.row == 1){
            self.callActiveCouponsWebService(SelectionFor.isSelectedAToZ, withParameters: nil)
        }
        else if (indexPath.section == 2){
            
            let categoriesDict = NSDictionary.init(object: categoriesIds, forKey: "ID" as NSCopying);
            self.callActiveCouponsWebService(SelectionFor.isSelectedProductGroup, withParameters: categoriesDict);
        }
    }
    
    func callActiveCouponsWebService(_ selectedSection: SelectionFor, withParameters: NSDictionary?, offSetValue: String = "0") -> Void {
        
        self.leftMenusSelectedFor = selectedSection;
        self.selectedDict = withParameters;
        self.lastOffset = offSetValue;
        
        var searchFor = "";
        
        if ((self.searchBar.text?.characters.count)! > 0) {
            searchFor = self.searchBar.text!;
        }
        
        let defaults: UserDefaults = UserDefaults.standard

        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID!
        ///let userType = user.PersonType!;

        let uniqueID = AppUtility.getDeviceNSUUID();
        let postDataDict : NSMutableDictionary = ["CustomerUserID" : userId,
                                                  "IsViewHistory" : "0",
                                                  "SortBy" : "Distance",
                                                  "FavoriteStatus" : "0",
                                                  "DealMasterID" : "0",
                                                  "BusinessCategory" : "0",
                                                  "Latitude" : "0",
                                                  "Longitude" : "0",
                                                  "Limit" : "10",
                                                  "Offset" : offSetValue,
                                                  "OrderBy" : "ASC",
                                                  "SearchFor" : searchFor,
                                                  "api_key" : kWEB_SERVICES_API_KEY,
                                                  "DeviceUniqueID" : uniqueID];
        
        if (self.userCurrentLocationCoordinate != nil) {
            let latString = String(format: "%f", self.userCurrentLocationCoordinate!.latitude);
            let longString = String(format: "%f", self.userCurrentLocationCoordinate!.longitude);
            
            postDataDict.setObject(latString, forKey: "Latitude" as NSCopying);
            postDataDict.setObject(longString, forKey: "Longitude" as NSCopying);
        }
        
        switch selectedSection {
        case .isSelectedFavorite:
            postDataDict.setObject("1", forKey: "FavoriteStatus" as NSCopying)
            break;
        case .isSelectedNearBy:
            postDataDict.setObject("ASC", forKey: "OrderBy" as NSCopying)
            break;
        case .isSelectedAToZ:
            postDataDict.setObject("FirstName", forKey: "SortBy" as NSCopying)
            postDataDict.setObject("ASC", forKey: "OrderBy" as NSCopying)
            break;
        default:
            postDataDict.setObject(withParameters!.object(forKey: "ID")! as! String, forKey: "BusinessCategory" as NSCopying)
        }
        
        postDataDict.setObject("0", forKey: "IsShowOnMap" as NSCopying);
        
        if(self.objCouponHistoryTVC != nil) { // If displying coupon history view
            postDataDict.setObject("1", forKey: "IsViewHistory" as NSCopying);
            postDataDict.setObject("DESC", forKey: "OrderBy" as NSCopying);
            postDataDict.setObject("TransactionID", forKey: "SortBy" as NSCopying);
        }
        self.callActiveCoupons(postDataDict);
    }
    
    func callActiveCoupons(_ postData: NSDictionary){
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_CUSTOMER_COUPONS, method: HTTPMethod.post, parameters: postData as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                self.isWebServiceCalled = true// This is using when web service called from location manager.
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    let status = (dataDict?.object(forKey: "Status"))! as? String
                    
                    if(status == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200)
                    {
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        let resourcePath = dataDict?.object(forKey: "ResourcePath") as! Dictionary<String, AnyObject>
                        
                        if(Int(postData.object(forKey: "Offset") as! String)! == 0){
                            self.activeCouponsListArray = nil;
                        }
                        
                        if(self.activeCouponsListArray == nil){
                            self.activeCouponsListArray = NSMutableArray();
                        }
                        
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let item = CustomerCoupon(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>, resourcePaths: resourcePath)
                                
                                self.activeCouponsListArray?.add(item);
                            }
                        }
                        else
                        {
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            if (self.leftMenusSelectedFor == SelectionFor.isSelectedFavorite) {
                               let message = "You do not have any Favourites yet...";
                                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                            }
                            else
                            {
                                self.navigationController?.view.makeToast(message, duration: 2.0, position: .center)
                             }
                        }
                        
                        DispatchQueue.main.async {
                            if(self.activeCouponsListArray != nil && (self.activeCouponsListArray?.count)! > 0){
                                self.flagToNoDataFound = false;
                            }
                            else{
                                self.flagToNoDataFound = true;
                            }
                            self.showTableView();
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        
                    }
                }
                
            }
            )
        }
    }
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        var numOfSections: Int = 0
        if (self.searchDataArray != nil && self.searchDataArray.count > 0)
        {
            //tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else if (self.flagToNoDataFound == true)
        {
            var oX = 0.0;
            var oY = 0.0;
            let widthX = tableView.frame.size.width;
            let heightY = tableView.frame.size.height;
            
            let emptyContainer = UIView.init(frame: CGRect(x: oX, y: oY, width: Double(widthX), height: Double(heightY)));
            emptyContainer.backgroundColor = K_EMPTY_TABLE_BG_COLOR;
            ///tblCell.contentView.addSubview(emptyContainer);
            
            let imgWidth = 100.0;
            let imgHeight = 100.0;
            oX = (Double(widthX) - imgWidth)/2;
            oY = (Double(heightY) - imgHeight)/2;
            
            let imageView = UIImageView.init(frame: CGRect(x: oX, y: oY, width: imgWidth, height: imgHeight));
            imageView.backgroundColor = UIColor.clear;
            imageView.contentMode = .scaleAspectFit;
            imageView.image = UIImage.init(named: "alert");
            emptyContainer.addSubview(imageView);
            
            let titleWidth = emptyContainer.frame.size.width - 20.0;
            let titleHeight = 40.0;
            let OY = imageView.frame.size.height + imageView.frame.origin.y + 10.0;
            
            let titleLbl = UILabel.init(frame: CGRect(x: 10.0, y: Double(OY), width: Double(titleWidth), height: titleHeight));
            titleLbl.textAlignment = .center;
            titleLbl.numberOfLines = 0;
            titleLbl.lineBreakMode = .byWordWrapping;
            titleLbl.text = "No active coupons found"
            titleLbl.textColor = UIColor.darkGray;
            emptyContainer.addSubview(titleLbl);
            //titleLbl.sizeToFit();
            
            tableView.backgroundView  = emptyContainer;
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        var returnValue = 0;
        
        if self.searchDataArray != nil {
            returnValue = self.searchDataArray.count;
        }
        return returnValue;
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //        let cl = tableView.cellForRow(at: indexPath);
        //
        //        if(cl != nil && (cl?.contentView.subviews.count)! > 0)
        //        {
        //            for obj in (cl?.contentView.subviews)! {
        //                obj.removeFromSuperview();
        //            }
        //        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        // Configure the cell...
        self.configureTableViewCell(cell, forRowAtIndexPath: indexPath);
        
        return cell
    }
    
    
    // This below method is setting different values for a row
    func configureTableViewCell(_ cell: UITableViewCell, forRowAtIndexPath:IndexPath){
        
        
        let obj = searchDataArray[forRowAtIndexPath.row] as! CustomerCoupon;
        
        let containerView = cell.contentView.viewWithTag(199);
        let businessImage = containerView?.viewWithTag(1405) as! UIImageView;
        let giftLbl = containerView?.viewWithTag(1413) as! UILabel;
        
        let nameLbl = containerView?.viewWithTag(1406) as! UILabel;
        nameLbl.text = "";
        let areaLbl = containerView?.viewWithTag(1407) as! UILabel;
        areaLbl.text = ""
        let activeCouponLbl = containerView?.viewWithTag(1409) as! UILabel;
        activeCouponLbl.text = ""
        let daysLbl = containerView?.viewWithTag(1410) as! UILabel;
        daysLbl.text = ""
        let mapPinBtn = containerView?.viewWithTag(1411) as! UIButton;
        let distanceLbl = containerView?.viewWithTag(1412) as! UILabel;
        distanceLbl.text = "";
        
        mapPinBtn.addTarget(self, action: #selector(WalletVC.placeOnMapBtnTapped(_:)), for: UIControlEvents.touchUpInside)
        
        if(obj.IsBusinessImageVerified != nil && Int(obj.IsBusinessImageVerified!) == 1 && obj.BusinessLogoImagePath != nil){
            businessImage.sd_setImage(with: URL(string: obj.BusinessLogoImagePath!), placeholderImage: K_PLACE_HOLDER_IMAGE)
        }else{
            businessImage.image = K_PLACE_HOLDER_IMAGE;
        }
        
        giftLbl.isHidden = true;
        
        if(obj.giftReceivedCoupanQuantity != nil && Int(obj.giftReceivedCoupanQuantity!)! >= 1){
            giftLbl.isHidden = false;
            giftLbl.text = "Coupon Gifted";
            if(Int(obj.giftReceivedCoupanQuantity!)! > 1){
                giftLbl.text = "Coupons Gifted";
            }
        }
        
        if (obj.FirstName != nil) {
            let string = obj.FirstName!;
            //nameLbl.text = String(htmlEncodedString: string);
            nameLbl.text = string;
        }
        else if(obj.BranchDescription != nil){
            //nameLbl.text = String(htmlEncodedString: obj.BranchDescription!);
            nameLbl.text = obj.BranchDescription!;
        }
        
        if(obj.BranchDescription != nil){
            areaLbl.text = obj.BranchDescription!;
        }
        
        if(obj.PromotionEndDate != nil){
            let string1 = obj.PromotionEndDate?.replacingOccurrences(of: " PM", with: "");
            let dates = AppUtility.numberOfDaysSinceDate(string1!, currentDateFormat: "dd/MM/yyyy HH:mm:ss")
            daysLbl.text = dates;
        }
        
        
        if(obj.NoOfActiveCoupan != nil){
            activeCouponLbl.text = (obj.NoOfActiveCoupan)! + " Coupon to Redeem"
            if(Int(obj.NoOfActiveCoupan!)! > 1){
                activeCouponLbl.text = (obj.NoOfActiveCoupan)! + " Coupons to Redeem"
            }
        }
        
        //if(obj.Distance)
        // Set distance value
        distanceLbl.text = String(format: "%.02f KM", Float(obj.Distance!)!);
        
        mapPinBtn.setImage(K_BONUS_MAP_PIN_IMAGE, for: UIControlState())
        // Set map/pot/faviourite icons
        if(Int(obj.FavoriteStatus!)! > 0){
            mapPinBtn.setImage(K_FAVIROUTE_IMAGE, for: UIControlState())
        }
        
        for index in 51...55 {
            let starImg = containerView?.viewWithTag(index) as? UIImageView;
            let ratingValue = index - 50;
            if (obj.MaxRating != nil && ratingValue <= Int(obj.MaxRating!)!) {
                starImg?.image = UIImage(named: "starfull")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
            }
            else{
                starImg?.image = UIImage(named: "starempty")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AppUtility.removedBackTitleAndChangeItsColor(VC: self, backTitle: "", backTintColor: UIColor.white);
        let obj = searchDataArray[indexPath.row] as! CustomerCoupon;
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "CouponDetailVC") as! CouponDetailVC
        //objVC.objBranchCoupon = obj;
        objVC.dealMasterId = obj.DealMasterID;
        self.navigationController?.pushViewController(objVC, animated: true);
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!;
        cell.accessoryType = UITableViewCellAccessoryType.none;
    }
    
    // Method will get call when user tap on a map pin on different screens
    func placeOnMapBtnTapped(_ sender: AnyObject) {
        
        let btn = sender as! UIButton;
        let superV = btn.superview;
        let pointInTable = btn.convert((superV?.bounds.origin)!, to:self.tblView)
        let indexPath = self.tblView.indexPathForRow(at: pointInTable);
        let obj = searchDataArray[(indexPath?.row)!] as! CustomerCoupon
        
        if(obj.Latitude != nil && obj.Longitude != nil)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            self.objPlaceOnMapVC = storyboard.instantiateViewController(withIdentifier: "PlaceOnMapVC") as? PlaceOnMapVC
            
            // Initiate values with controller objects
            self.objPlaceOnMapVC?.clLocationCoordinate = CLLocationCoordinate2DMake(Double(obj.Latitude!)!, Double(obj.Longitude!)!);
            self.objPlaceOnMapVC?.locationTitle = obj.FirstName;
            self.objPlaceOnMapVC?.locationSubTitle = obj.BranchDescription
            self.objPlaceOnMapVC?.dealCategoryImage = obj.MapPinCategoryPicture;
            //self.objPlaceOnMapVC?.bonusPotLocationID = objLoyalty.BonusPotLocationID;
            self.objPlaceOnMapVC?.branchID = obj.BranchID;
            
            self.addChildViewController(objPlaceOnMapVC!)
            
            self.objPlaceOnMapVC?.view.frame = self.view.bounds;
            self.view.addSubview((self.objPlaceOnMapVC?.view)!)
            objPlaceOnMapVC?.didMove(toParentViewController: self);
            //self.view.isUserInteractionEnabled = false;
        }
    }
    
    func searchDataWithEnrty(_ str: String?){
        
        if str != nil { //If data array empty
            
            let servicePredicate = NSPredicate(format: "SELF.FirstName CONTAINS[cd] %@", str!)
            
            let result = self.activeCouponsListArray.filtered(using: servicePredicate)
            self.searchDataArray.removeAllObjects()
            self.searchDataArray.addObjects(from: result)
        }
        else{
            self.searchDataArray.removeAllObjects()
            self.searchDataArray.addObjects(from: self.activeCouponsListArray as [AnyObject])
        }
        self.tblView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.callActiveCouponsWebService(self.leftMenusSelectedFor, withParameters: self.selectedDict);
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        //self.searchBar.setShowsCancelButton(true, animated: true)
        //self.hideChildView();
        let chVC = self.childViewControllers;
        for vc in chVC {
            // If child view type is place on map remove it
            if(vc.isKind(of: PlaceOnMapVC.self)){
                vc.view.removeFromSuperview()
                vc.removeFromParentViewController()
                vc.willMove(toParentViewController: nil)
                break;
            }
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        // searchBar.setShowsCancelButton(false, animated: true)
        //self.searchDataWithEnrty(nil);
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.hideChildView();
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating ")
        
        if((scrollView.contentSize.height + 20.0) < (scrollView.contentOffset.y + scrollView.frame.size.height)){
            
            let offset = self.activeCouponsListArray.count;
            // Call more places for the same coordinates
            self.callActiveCouponsWebService(self.leftMenusSelectedFor, withParameters: self.selectedDict, offSetValue: String(offset))
        }
    }
    
    func hideChildView(){
        
        for vc in self.childViewControllers{
            vc.view.removeFromSuperview()
            vc.removeFromParentViewController()
            vc.willMove(toParentViewController: nil)
        }
        self.objMoneyVC = nil;
        self.objCouponHistoryTVC = nil;
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
   
    @IBAction func topNavigationBtnTapped(_ sender: Any) {
        
        let btn = sender as! UIButton;
        self.searchBar.resignFirstResponder();
        
        if(self.previouslySelectedBtn?.tag != btn.tag)
        {
            self.hideChildView(); //First remove any child view if have
            self.searchBar.isUserInteractionEnabled = true;
            self.searchBar.text = "";
            self.navigationController?.navigationBar.isUserInteractionEnabled = true;
            if(btn.tag == 895)
            {
                self.activeCouponsBtn.setImage(kIMAGE_ACTIVE_COUPONS_YELLOW, for: .normal)
                self.moneyBtn.setImage(kIMAGE_MONEY_WHITE, for: .normal)
                self.couponHistoryBtn.setImage(kIMAGE_COUPONS_HISTORY_WHITE, for: .normal)
                
                self.callLocationManager();
            }
            else if(btn.tag == 896){
                self.activeCouponsBtn.setImage(kIMAGE_ACTIVE_COUPONS_WHITE, for: .normal)
                self.moneyBtn.setImage(kIMAGE_MONEY_YELLOW, for: .normal)
                self.couponHistoryBtn.setImage(kIMAGE_COUPONS_HISTORY_WHITE, for: .normal)
                
                if(self.objMoneyVC == nil)
                {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    self.objMoneyVC = storyboard.instantiateViewController(withIdentifier: "MoneyVC") as? MoneyVC
                    //objVC.popUptitle = "About Us";
                    let oY = self.btnsContainerView.frame.origin.y + self.btnsContainerView.frame.size.height
                    let height = self.tblView.frame.height;
                    self.objMoneyVC?.view.frame = CGRect(x: 0.0, y: Double(oY), width: Double(self.tblView.frame.width), height: Double(height));
                    self.view.addSubview((self.objMoneyVC?.view)!)
                    self.addChildViewController((self.objMoneyVC)!)
                    self.objMoneyVC?.didMove(toParentViewController: self);
                    
                    self.searchBar.isUserInteractionEnabled = false;
                    self.navigationController?.navigationBar.isUserInteractionEnabled = false;
                }
            }
            else if(btn.tag == 897){
                self.activeCouponsBtn.setImage(kIMAGE_ACTIVE_COUPONS_WHITE, for: .normal)
                self.moneyBtn.setImage(kIMAGE_MONEY_WHITE, for: .normal)
                self.couponHistoryBtn.setImage(kIMAGE_COUPONS_HISTORY_YELLOW, for: .normal)
                
                if(self.objCouponHistoryTVC == nil)
                {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    self.objCouponHistoryTVC = storyboard.instantiateViewController(withIdentifier: "CouponHistoryTVC") as? CouponHistoryTVC
                    
                    let oY = self.btnsContainerView.frame.origin.y + self.btnsContainerView.frame.size.height
                    let height = self.tblView.frame.height;
                    self.objCouponHistoryTVC?.view.frame = CGRect(x: 0.0, y: Double(oY), width: Double(self.tblView.frame.width), height: Double(height));
                    self.view.addSubview((self.objCouponHistoryTVC?.view)!)
                    self.addChildViewController((self.objCouponHistoryTVC)!)
                    self.objCouponHistoryTVC?.didMove(toParentViewController: self);
                    self.callLocationManager(); // Call web service to display coupons History data
                }
            }
            self.previouslySelectedBtn = btn;
        }
    }
    
}
