//
//  AmountSelectionVC.swift
//  Bonus
//
//  Created by Arvind Sen on 13/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit

class AmountSelectionVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblView: UITableView!
    var amountsArray : Array<String> = [];
    
    var amountCallBack: ((String)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false;
        self.amountsArray = ["20", "50", "100", "500", "1000"];
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.amountsArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        self.configure(cell, forRowAtIndexPath: indexPath)
        
        return cell;
    }
    
    func configure(_ cell: UITableViewCell, forRowAtIndexPath indexPath: IndexPath) {
    
        cell.textLabel?.text = self.amountsArray[indexPath.row] as String;
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let amountValue = self.amountsArray[indexPath.row] as String;
        self.amountCallBack?(amountValue);
        self.closeBtnTapped("" as AnyObject);
    }
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        
        // Removed it from superview controller
        self.view.removeFromSuperview()
        self.removeFromParentViewController();
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
