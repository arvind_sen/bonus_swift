//
//  PunchesVC.swift
//  Bonus
//
//  Created by Arvind Sen on 26/09/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


class PunchesVC: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var businessPin1: UITextField!
    @IBOutlet weak var businessPin2: UITextField!
    @IBOutlet weak var businessPin3: UITextField!
    @IBOutlet weak var businessPin4: UITextField!
    
    var focusedControl : UITextField?
    
    var popupVCDelegate : DismissPopupVCProtocol? = nil
    
    var loyaltyObj : Loyalty!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false;
        // Do any additional setup after loading the view.
        
        self.containerView.backgroundColor = BLUE_BG_COLOR;
        self.containerView.layer.cornerRadius = 7.0;
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.containerView.layer.borderWidth = 1.0;
        self.containerView.clipsToBounds = true;
    }

    override func viewDidAppear(_ animated: Bool) {
        self.businessPin1.becomeFirstResponder();
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func closeBtnTapped(_ sender: AnyObject?) {
        
        self.popupVCDelegate?.dismissPopupViewController(self, responseObj: nil);
        // Removed it from superview controller
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    @IBAction func arrowBtnTapped(_ sender: AnyObject) {
       
        self.businessPin1.resignFirstResponder();
        self.businessPin2.resignFirstResponder();
        self.businessPin3.resignFirstResponder();
        self.businessPin4.resignFirstResponder();
        
        let pin1 : String = self.businessPin1.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let pin2 : String = self.businessPin2.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let pin3 : String = self.businessPin3.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let pin4 : String = self.businessPin4.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        let combinePin = pin1 + "" + pin2 + "" + pin3 + "" + pin4;
        
        var message : String? = nil;
        
        if ((self.businessPin1.text?.characters.count <= 0) || (self.businessPin2.text?.characters.count <= 0) || (self.businessPin3.text?.characters.count <= 0) || (self.businessPin4.text?.characters.count <= 0)){
            message = "Please enter all pin digit correctly";
        }
        
        if message != nil {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
        }
        else {
            
            let loyaltyId = self.loyaltyObj.LoyaltyID;
            let branchId = self.loyaltyObj.BranchID;
            
            let defaults: UserDefaults = UserDefaults.standard
            let nsDataUser = defaults.object(forKey: "UserData") as! Data
            let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
            let userId = user.ID;
            let personType = user.PersonType;
            let deviceUniqueId = AppUtility.getDeviceNSUUID();

            let receiptNumber = "";
            // Created postDataDictonary to post the data on server
            let postDataDict : NSDictionary = ["loyaltyID": loyaltyId!,
                                               "Cbpin": combinePin,
                                               "CustomerID": userId!,
                                               "BranchID": branchId!,
                                               "ReceiptNumber": receiptNumber,
                                               "PersonType": personType!,
                                               "DeviceUniqueID": deviceUniqueId,
                                               "api_key": kWEB_SERVICES_API_KEY];
            
            if !AppUtility.isNetworkAvailable(){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
            }
            else{
                
                CustomProgressView.showProgressIndicator();
                Alamofire.request(kSERVICE_ALLOT_A_PUNCH_CARD, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                    
                    CustomProgressView.hideProgressIndicator();
                    //print(response.request)  // original URL request
                    //print(response.response) // URL response
                    //print(response.data)     // server data
                    //print(response.result)   // result of response serialization
                    
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                    print("value =  \(String(describing: datastring))");
                    
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                        let dataDict = JSON as? NSDictionary;
    
                        if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                            
                             let punch = Punches(data: dataDict?.object(forKey: "Response") as! Dictionary<String, AnyObject>)
                            
                            var message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                            
                            if(message != ""){
                                
                                let totalPunches = Int(self.loyaltyObj.PunchRequiredForFreeBee!)!
                                let punchCount = Int(punch.PunchCount!)!
                                
                                
                                if (punchCount % totalPunches != 0) {
                                   
                                }
                                else{
                                    message = "Congratulations! You earned a Freebie!"
                                }
                                
                                let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                                
                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                    self.popupVCDelegate?.dismissPopupViewController(self, responseObj:punch);
                                    
                                    // Removed it from superview controller
                                    self.view.removeFromSuperview()
                                    self.removeFromParentViewController()
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                        else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                            
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                        }
                        else
                        {
                            self.businessPin1.text = "";
                            self.businessPin2.text = "";
                            self.businessPin3.text = "";
                            self.businessPin4.text = "";
                            
                            let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                            
                        }
                    }
                    }
                )
            }
        }
    }
    
    // MARK:- TextField delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == self.businessPin1 && textField.text?.characters.count >= 1 && string.isEmpty){
            
        }
        else if ((textField == self.businessPin1) && (textField.text?.characters.count == 1 && !string.isEmpty)){
            
            if (self.businessPin2.text?.characters.count < 1) {
                self.businessPin2.becomeFirstResponder();
            }
            else{
                self.businessPin2.resignFirstResponder();
            }
        }
        
        if (textField == self.businessPin2 && textField.text?.characters.count >= 1 && string.isEmpty){
            
        }
        else if ((textField == self.businessPin2) && (textField.text?.characters.count == 1 && !string.isEmpty)){
            
            if (self.businessPin3.text?.characters.count < 1) {
                self.businessPin3.becomeFirstResponder();
            }
            else{
                self.businessPin2.resignFirstResponder();
            }
        }
        
        if (textField == self.businessPin3 && textField.text?.characters.count >= 1 && string.isEmpty){
            
        }
        else if ((textField == self.businessPin3) && (textField.text?.characters.count == 1 && !string.isEmpty)){
            
            if (self.businessPin4.text?.characters.count < 1) {
                self.businessPin4.becomeFirstResponder();
            }
            else{
                self.businessPin3.resignFirstResponder();
            }
        }
        
        
        if (textField == self.businessPin4 && textField.text?.characters.count >= 1 && string.isEmpty){
            
        }
        else if ((textField == self.businessPin4) && (textField.text?.characters.count == 1 && !string.isEmpty)){
            
            self.businessPin4.resignFirstResponder();
        }
        
        return true;
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        self.focusedControl = textField;
        
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField == self.businessPin1)
        {
            self.businessPin2.becomeFirstResponder();
        }
        else if (textField == self.businessPin2)
        {
            self.businessPin3.becomeFirstResponder();
        }
        else if (textField == self.businessPin3)
        {
            self.businessPin4.becomeFirstResponder();
        }
        else if (textField == self.businessPin4)
        {
            businessPin4.resignFirstResponder();
        }
        
        return true;
    }
}
