//
//  CouponHistoryTVC.swift
//  Bonus
//
//  Created by Arvind Sen on 07/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class CouponHistoryTVC: UITableViewController {
    
    var activeCouponsListArray: NSMutableArray!
    var searchDataArray: NSMutableArray!
    var objPlaceOnMapVC : PlaceOnMapVC? = nil;
    var flagToNoDataFound = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        var numOfSections: Int = 0
        if (self.searchDataArray != nil && self.searchDataArray.count > 0)
        {
            //tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else if (self.flagToNoDataFound == true)
        {
            var oX = 0.0;
            var oY = 0.0;
            let widthX = tableView.frame.size.width;
            let heightY = tableView.frame.size.height;
            
            let emptyContainer = UIView.init(frame: CGRect(x: oX, y: oY, width: Double(widthX), height: Double(heightY)));
            emptyContainer.backgroundColor = K_EMPTY_TABLE_BG_COLOR;
            ///tblCell.contentView.addSubview(emptyContainer);
            
            let imgWidth = 100.0;
            let imgHeight = 100.0;
            oX = (Double(widthX) - imgWidth)/2;
            oY = (Double(heightY) - imgHeight)/2;
            
            let imageView = UIImageView.init(frame: CGRect(x: oX, y: oY, width: imgWidth, height: imgHeight));
            imageView.backgroundColor = UIColor.clear;
            imageView.contentMode = .scaleAspectFit;
            imageView.image = UIImage.init(named: "alert");
            emptyContainer.addSubview(imageView);
            
            let titleWidth = emptyContainer.frame.size.width - 20.0;
            let titleHeight = 40.0;
            let OY = imageView.frame.size.height + imageView.frame.origin.y + 10.0;
            
            let titleLbl = UILabel.init(frame: CGRect(x: 10.0, y: Double(OY), width: Double(titleWidth), height: titleHeight));
            titleLbl.textAlignment = .center;
            titleLbl.numberOfLines = 0;
            titleLbl.lineBreakMode = .byWordWrapping;
            titleLbl.text = "No coupon history found"
            titleLbl.textColor = UIColor.darkGray;
            emptyContainer.addSubview(titleLbl);
            //titleLbl.sizeToFit();
            
            tableView.backgroundView  = emptyContainer;
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        var returnValue = 0;
        
        if self.searchDataArray != nil {
            returnValue = self.searchDataArray.count;
        }
        return returnValue;
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //        let cl = tableView.cellForRow(at: indexPath);
        //
        //        if(cl != nil && (cl?.contentView.subviews.count)! > 0)
        //        {
        //            for obj in (cl?.contentView.subviews)! {
        //                obj.removeFromSuperview();
        //            }
        //        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        // Configure the cell...
        self.configureTableViewCell(cell, forRowAtIndexPath: indexPath);
        
        return cell
    }
    
    
    // This below method is setting different values for a row
    func configureTableViewCell(_ cell: UITableViewCell, forRowAtIndexPath:IndexPath){
        
        let obj = searchDataArray[forRowAtIndexPath.row] as! CustomerCoupon;
        
        let containerView = cell.contentView.viewWithTag(214);
        let businessImage = containerView?.viewWithTag(215) as! UIImageView;
        let giftLbl = containerView?.viewWithTag(221) as! UILabel;
        
        let nameLbl = containerView?.viewWithTag(216) as! UILabel;
        nameLbl.text = "";
        let areaLbl = containerView?.viewWithTag(217) as! UILabel;
        areaLbl.text = ""
        let activeCouponLbl = containerView?.viewWithTag(218) as! UILabel;
        activeCouponLbl.text = ""
        let mapPinBtn = containerView?.viewWithTag(219) as! UIButton;
        let distanceLbl = containerView?.viewWithTag(220) as! UILabel;
        distanceLbl.text = "";
        
        if(obj.IsBusinessImageVerified != nil && Int(obj.IsBusinessImageVerified!) == 1 && obj.BusinessLogoImagePath != nil){
            businessImage.sd_setImage(with: URL(string: obj.BusinessLogoImagePath!), placeholderImage: K_PLACE_HOLDER_IMAGE)
        }else{
            businessImage.image = K_PLACE_HOLDER_IMAGE;
        }
        
        giftLbl.text = obj.DealStatus!;
        
        if(obj.DealStatus! == "Gift" || obj.DealStatus! == "gift"){
            giftLbl.text = "gifted";
        }
        
        giftLbl.text = giftLbl.text?.lowercased();
        
        if (obj.FirstName != nil) {
            let string = obj.FirstName!;
            //nameLbl.text = String(htmlEncodedString: string);
            nameLbl.text = string;
        }
        else if(obj.BranchDescription != nil){
            //nameLbl.text = String(htmlEncodedString: obj.BranchDescription!);
            nameLbl.text = obj.BranchDescription!;
        }
        
        if(obj.BranchDescription != nil){
            areaLbl.text = obj.BranchDescription!;
        }
        
        let bought = Int(obj.TotalNoOfDeal!)! - Int(obj.AvalDealInNumber!)!
        
        activeCouponLbl.text = (obj.TotalNoOfDeal)! + " Issued, \(bought) Bought";

        // Set distance value
        distanceLbl.text = String(format: "%.02f KM", Float(obj.Distance!)!);
        
        mapPinBtn.setImage(K_BONUS_MAP_PIN_IMAGE, for: UIControlState())
        
        if(obj.Latitude != nil && obj.Longitude != nil){
            mapPinBtn.addTarget(self, action: #selector(WalletVC.placeOnMapBtnTapped(_:)), for: UIControlEvents.touchUpInside)
        }
        // Set map/pot/faviourite icons
        if(Int(obj.FavoriteStatus!)! > 0){
            mapPinBtn.setImage(K_FAVIROUTE_IMAGE, for: UIControlState())
        }
        
        for index in 51...55 {
            let starImg = containerView?.viewWithTag(index) as? UIImageView;
            let ratingValue = index - 50;
            if (obj.MaxRating != nil && ratingValue <= Int(obj.MaxRating!)!) {
                starImg?.image = UIImage(named: "starfull")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
            }
            else{
                starImg?.image = UIImage(named: "starempty")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AppUtility.removedBackTitleAndChangeItsColor(VC: self.parent!, backTitle: "", backTintColor: UIColor.white);
        let obj = searchDataArray[indexPath.row] as! CustomerCoupon;
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "CouponHistoryDetailVC") as! CouponHistoryDetailVC
        //objVC.objBranchCoupon = obj;
        objVC.dealMasterId = obj.DealMasterID;
        self.navigationController?.pushViewController(objVC, animated: true);
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!;
        cell.accessoryType = UITableViewCellAccessoryType.none;
    }
    
    // Method will get call when user tap on a map pin on different screens
    func placeOnMapBtnTapped(_ sender: AnyObject) {
        
        let btn = sender as! UIButton;
        let superV = btn.superview;
        let pointInTable = btn.convert((superV?.bounds.origin)!, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: pointInTable);
        
        let objLoyalty = searchDataArray[(indexPath?.row)!] as! CustomerCoupon
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.objPlaceOnMapVC = storyboard.instantiateViewController(withIdentifier: "PlaceOnMapVC") as? PlaceOnMapVC
        
        // Initiate values with controller objects
        self.objPlaceOnMapVC?.clLocationCoordinate = CLLocationCoordinate2DMake(Double(objLoyalty.Latitude!)!, Double(objLoyalty.Longitude!)!);
        self.objPlaceOnMapVC?.locationTitle = objLoyalty.FirstName;
        self.objPlaceOnMapVC?.locationSubTitle = objLoyalty.BranchDescription
        self.objPlaceOnMapVC?.dealCategoryImage = objLoyalty.MapPinCategoryPicture;
        //self.objPlaceOnMapVC?.bonusPotLocationID = objLoyalty.BonusPotLocationID;
        self.objPlaceOnMapVC?.branchID = objLoyalty.BranchID;
        
        self.addChildViewController(objPlaceOnMapVC!)
        
        self.objPlaceOnMapVC?.view.frame = self.view.bounds;
        self.view.addSubview((self.objPlaceOnMapVC?.view)!)
        self.objPlaceOnMapVC?.didMove(toParentViewController: self);
        //self.view.isUserInteractionEnabled = false;
    }

    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
