//
//  SecurityAlertVC.swift
//  Bonus
//
//  Created by Arvind Sen on 04/11/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//


class SecurityAlertVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var passwordTxtF: UITextField!
    var popupVCDelegate : DismissPopupVCProtocol? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        super.viewDidLoad()
        view.backgroundColor = K_COLOR_TRANSPARNT;
        view.isOpaque = false;
        
        self.containerView.backgroundColor = UIColor.white;
        self.containerView.layer.cornerRadius = 7.0;
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.containerView.layer.borderWidth = 1.0;
        self.containerView.clipsToBounds = true;
    }

    @IBAction func closeBtnTapped(_ sender: AnyObject?) {
        
        self.popupVCDelegate?.dismissPopupViewController(self, responseObj: nil);
        
        // Removed it from superview controller
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    @IBAction func okBtnTapped(_ sender: AnyObject) {
        
        let pwd = self.passwordTxtF.text;
        
        if((pwd?.characters.count)! <= 0)
        {
             AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Please enter your password", onView: self);
            return;
        }
    
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        let password = pwd!.md5().lowercased()
        
        let concatString = String(format: "%@%@", password, kKEY_PASSWORD_CONCAT_STRING); // returns NSString of the MD5 of test
        let md5Password = (concatString as NSString).md5().lowercased()
        
        if(md5Password == user.Password){
            
            self.popupVCDelegate?.dismissPopupViewController(self, responseObj: "Yes" as AnyObject?);
            // Removed it from superview controller
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        }
        else
        {
            
            self.passwordTxtF.text = "";
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Wrong or Invalid password. Please try again.", onView: self)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
}
