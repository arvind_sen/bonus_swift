//
//  CountriesVC.swift
//  Bonus
//
//  Created by Arvind Sen on 15/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit

class CountriesVC: UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var tblView: UITableView!
    
    var dataArray: NSMutableArray!
    var searchDataArray: NSMutableArray!
    var callBackWithSelectedCountry : ((Country)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false
        // Do any additional setup after loading the view.
        
        self.containerView.layer.cornerRadius = 5.0;
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.containerView.layer.borderWidth = 1.0;
        self.containerView.clipsToBounds = true;
        
        self.searchBar.tintColor = UIColor.darkGray;
        self.searchBar.backgroundColor = UIColor.clear;
        
        self.tblView.register(UITableViewCell.classForKeyedArchiver(), forCellReuseIdentifier: "reuseIdentifier")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showTableView(){
        
        if self.searchDataArray != nil{
            self.searchDataArray.removeAllObjects()
        }
        else
        {
            self.searchDataArray = NSMutableArray();
            self.searchDataArray.addObjects(from: dataArray as [AnyObject])
        }
        
        self.tblView.reloadData();
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var returnValue = 0;
        if searchDataArray != nil {
            returnValue = searchDataArray!.count;
        }
        return returnValue;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let reuseCellId : String = "reuseIdentifier"
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseCellId)
        
        configure(cell, forRowAtIndexPath: indexPath)
        
        return cell;
    }
    
    func configure(_ cell: UITableViewCell, forRowAtIndexPath indexPath: IndexPath) {
        
        let obj = searchDataArray[indexPath.row] as! Country;
        cell.textLabel?.text = obj.CountryName;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let cell : UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!;
        //cell.accessoryType = UITableViewCellAccessoryType.Checkmark;
        //cell.tintColor = K_COLOR_DARK_GREEN_COLOR;
        //[tabularDelegate!.countryTableRowSelection(searchDataArray[indexPath.row])];
        
        let obj = searchDataArray[indexPath.row] as! Country;
        
        self.callBackWithSelectedCountry!(obj);
        // Removed it from superview controller
        self.view.removeFromSuperview()
        self.removeFromParentViewController();
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!;
        cell.accessoryType = UITableViewCellAccessoryType.none;
    }
    
    func searchDataWithEnrty(_ str: String?){
        
        if str != nil { //If data array empty
            
            let servicePredicate = NSPredicate(format: "SELF.CountryName CONTAINS[cd] %@", str!)
            
            let result = self.dataArray.filtered(using: servicePredicate)
            self.searchDataArray.removeAllObjects()
            self.searchDataArray.addObjects(from: result)
            
        }
        else{
            self.searchDataArray.removeAllObjects()
            self.searchDataArray.addObjects(from: self.dataArray as [AnyObject])
        }
        self.tblView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchDataWithEnrty(searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        self.searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        self.searchDataWithEnrty(nil);
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func closeBtnTapped(_ sender: AnyObject) {
        // Removed it from superview controller
        self.view.removeFromSuperview()
        self.removeFromParentViewController();
    }

}
