//
//  LoginVC.swift
//  Bonus
//
//  Created by Arvind Sen on 22/08/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire

class LoginVC: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate, DismissPopupVCProtocol {

    @IBOutlet weak var emailTxtF: UITextField!
    @IBOutlet weak var passwordTxtF: UITextField!
    @IBOutlet weak var rememberMeBtn: UIButton!
    var isRememberMe = true;
    var focusedControl : UITextField?
    var locationManager : CLLocationManager?
    var userCurrentLocationCoordinate : CLLocationCoordinate2D? ;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.displayForgetBtn();
        self.callLocationManager();
        self.callCategoryService();
        
        //self.emailTxtF.text = "itest@yopmail.com";
        //self.emailTxtF.text = "arvind.sen@bhumati.com";
        //self.passwordTxtF.text = "Bhumati@123";
        //        self.emailTxtF.text = "waterpark@yopmail.com";
        //        self.passwordTxtF.text = "qwerty";
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true;
    }
    
    func callCategoryService() {
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["api_key": kWEB_SERVICES_API_KEY];
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{

            //CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_CATEGORIES, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                //CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        let resourcePath = dataDict?.object(forKey: "ResourcePath") as! Dictionary<String, AnyObject>
                        
                        let categoryArray = NSMutableArray();
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let item = Category(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>, resourcePath: resourcePath)
                                
                                categoryArray.add(item);
                            }
                            
                            // Save all the categories in local database
                            BonusLocalDBHandler.saveAllCategoriesDetail(categoryArray);
                        }
                        
                    }
                    
                }
                }
            )
        }
    }
    
    
    
    // Method get call to get user current location lat, long
    func callLocationManager() {
        
        if (CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied) {
            
            self.locationManager = CLLocationManager.init();// locationManager need to define at the class variable then only its call delegate methods.
            self.locationManager!.delegate = self;
            
            if self.locationManager!.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
                    self.locationManager!.requestWhenInUseAuthorization();
                }
            self.locationManager!.desiredAccuracy = kCLLocationAccuracyBest;
            self.locationManager!.startUpdatingLocation();
        }
        else{
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "To re-enable, please go to Settings and turn on Location Service for this app.", onView: self)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let recentLocation = locations.last;
        self.userCurrentLocationCoordinate = recentLocation?.coordinate;
        self.locationManager!.stopUpdatingLocation();
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(" failed to get current location");
        
        self.userCurrentLocationCoordinate = CLLocationCoordinate2D.init(latitude: 0.00, longitude: 0.00);//Define userCurrentLocationCoordinate default values
        self.locationManager!.stopUpdatingLocation();
    }
    
    func displayForgetBtn(){
        
        let forgetBtn = UIButton.init(type: .custom);
        forgetBtn.frame = CGRect(x: 50, y: 0, width: 50, height: self.passwordTxtF.frame.size.height);
        forgetBtn.setTitle("Forgot?", for: UIControlState());
        forgetBtn.titleLabel?.font = UIFont.systemFont(ofSize: 11.0);
        forgetBtn.setTitleColor(UIColor.darkGray, for: UIControlState());
        forgetBtn.addTarget(self, action: #selector(LoginVC.forgotBtnTapped), for: .touchUpInside);
        
        self.passwordTxtF.rightViewMode = UITextFieldViewMode.always;
        self.passwordTxtF.rightView = forgetBtn;
    }

    func forgotBtnTapped() -> Void {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        objVC.popupVCDelegate = self;
        objVC.modalPresentationStyle = .overCurrentContext;
        objVC.modalTransitionStyle = .crossDissolve;
        present(objVC, animated: true) { 
            objVC.emailTxtF.text = self.emailTxtF.text!; // Setting value of email id
        }
    }
    
    func dismissPopupViewController(_ vc: UIViewController, responseObj: AnyObject?) {
        self.dismiss(animated: true, completion: nil);
    }
    
    @IBAction func loginBtnTapped(_ sender: AnyObject) {
        
        let emailId : String = self.emailTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let password : String = self.passwordTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        self.emailTxtF.text = emailId;
        self.passwordTxtF.text = password;
        
        let isValidEmail = AppUtility.validateEmail(emailId);
        
        var message : String? = nil;
        
        if !isValidEmail {
            message = LOGIN_ALERT_MESSAGE_EMAIL_ID;
        }
        else if password.isEmpty {
            message = LOGIN_ALERT_MESSAGE_EMPTY_PASSWORD;
        }
        
        if message != nil {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
        }
        else {
            let deviceToken = AppUtility.getDeviceToken();
            let deviceUniqueId = AppUtility.getDeviceNSUUID();

        
            if(self.userCurrentLocationCoordinate == nil)
            {
                self.userCurrentLocationCoordinate = CLLocationCoordinate2D.init(latitude: 0.00, longitude: 0.00);
            }
            // Created postDataDictonary to post the data on server
            let postDataDict : NSDictionary = ["EmailID": emailId,
                                               "Password": password,
                                               "DeviceToken": deviceToken!,
                                               "DeviceUniqueID": deviceUniqueId,
                                               "DeviceType": "IOS",
                                            "Latitude": (self.userCurrentLocationCoordinate?.latitude)!,
                                            "Longitude": (self.userCurrentLocationCoordinate?.longitude)!,
                                               "api_key": kWEB_SERVICES_API_KEY
                                               ];
            /*
            let postDataDict = ["EmailID": "itest@yopmail.com",
                                "Password": "qwerty",
                                "DeviceToken": "A4A92823AD7D4F7D9DABD9BD8B57901D",
                                "DeviceUniqueID": "A4A92823-AD7D-4F7D-9DAB-D9BD8B57901D",
                                "DeviceType": "IOS",
                                "Latitude": "20.5937",
                                "Longitude": "78.9629",
                                "api_key": "BT4oqNhWcuDysWQmRm@TpkKPH4bIGff"];
            */
            print("postData = %@", postDataDict);
            
            if !AppUtility.isNetworkAvailable(){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
            }
            else{
                
                CustomProgressView.showProgressIndicatorOnView(VC: self);
                Alamofire.request(kSERVICE_LOGIN, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                    CustomProgressView.hideProgressIndicatorOnView(VC: self);
                    //print(response.request)  // original URL request
                    //print(response.response) // URL response
                    //print(response.data)     // server data
                    //print(response.result)   // result of response serialization
                    
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                    print("value =  \(datastring)");
                    
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                        let dataDict = JSON as? NSDictionary;
                        
                        
                        if((dataDict?.object(forKey: "Status"))! as! String == "Pending" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 100) {
                            
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            let pendingVerificationDataDictionary = dataDict?.object(forKey: "Response") as! Dictionary<String, AnyObject>
                            
                            let alert = UIAlertController(title: message, message: "Do you want to resend verification email?", preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                                self.resendVerificationEmailToUser(dataDict: pendingVerificationDataDictionary);
                            }))
                            
                            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil));
                            self.present(alert, animated: true, completion: nil)
                        }
                        else if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                            
                            let user = User(data: dataDict?.object(forKey: "Response") as! Dictionary<String, AnyObject>)
                            
                            self.loginResponseHandler(user);
                        }
                        else
                        {
                            let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                            
                            if (message.range(of: "already").location != NSNotFound){
                                
                                let alert = UIAlertController(title: message as String, message: "Do you want to logout from there?", preferredStyle: UIAlertControllerStyle.alert)
                                
                                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                                    self.logoutFromThePreviousSession();
                                }))
                                
                                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil));
                                self.present(alert, animated: true, completion: nil)
                            }
                            else
                            {
                                //self.navigationController?.view.makeToast(message as String, duration: 2.0, position: .center)
                                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                            }
                        }
                    }
                    }
                )
            }
        }
    }
    
    func resendVerificationEmailToUser(dataDict: Dictionary<String, Any>){
        
        let emailId = dataDict["EmailID"] as? String;
        let firstName = dataDict["FirstName"] as? String;
        let Id = dataDict["ID"] as? String;
        let personType = dataDict["PersonType"] as? String;
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["EmailID": emailId!, "FirstName": firstName!, "ID": Id!, "PersonType": personType!, "api_key":kWEB_SERVICES_API_KEY ];
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_USER_RESEND_ACTIVATION_LINK, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Mailsend" && (dataDict?.object(forKey: "StatusCode"))! as? String == "200") {
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                    }
                    else
                    {
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                    }
                    
                }
            }
            )
        }

    }
    /*
    // Swift 2.0, minor warning on Swift 1.2
    func md5(string string: String) -> String {
        
        var digest = [UInt8](count: Int(CC_MD5_DIGEST_LENGTH), repeatedValue: 0)
        if let data = string.dataUsingEncoding(NSUTF8StringEncoding) {
            CC_MD5(data.bytes, CC_LONG(data.length), &digest)
        }
        
        var digestHex = ""
        for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }
        
        return digestHex
    }
    */
    func logoutFromThePreviousSession(){
        
        let strPwd = self.passwordTxtF.text! as NSString;
        
        let password = strPwd.md5().lowercased()
        
        let concatString = String(format: "%@%@", password, kKEY_PASSWORD_CONCAT_STRING); // returns NSString of the MD5 of test
        let md5Password = (concatString as NSString).md5().lowercased()
    
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["EmailID": self.emailTxtF.text!, "Password": md5Password, "api_key":kWEB_SERVICES_API_KEY ];
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_LOGOUT_PREVIOUS_SESSION, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                    }

                }
                }
            )
        }
    }
    
    func loginResponseHandler(_ result:AnyObject?){
        //println(result)
        let user = result as! User;
        
        let encodedObject = NSKeyedArchiver.archivedData(withRootObject: result!) as Data
        let defaults = UserDefaults.standard
        defaults.set(encodedObject, forKey: "UserData")
        
        defaults.setValue(user.PersonType, forKeyPath: "globalUserType"); // Setting this parameter to identified user type
        
        
        if (isRememberMe == true) {
            
            let emailId : String = self.emailTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            let password : String = self.passwordTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            
            defaults.setValue(emailId, forKeyPath: "rememberUserEmailId");
            defaults.setValue(password, forKeyPath: "rememberUserPassword");
        }
        else{
            defaults.removeObject(forKey: "rememberUserEmailId");
            defaults.removeObject(forKey: "rememberUserPassword");
        }
        
        defaults.synchronize()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.setUpForCustomerAndVisitorTabs();
    }

    
    @IBAction func rememberBtnTapped(_ sender: AnyObject) {
        let btn = sender as? UIButton;
        
        if (isRememberMe == false) {
            btn?.setImage(K_RIGHT_YELLOW_EMPTY, for: UIControlState())
            isRememberMe = true;
        }
        else{
            btn?.setImage(K_RIGHT_YELLOW, for: UIControlState())
            isRememberMe = false;
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        focusedControl = textField;
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.emailTxtF.resignFirstResponder();
        self.passwordTxtF.resignFirstResponder();
        return true;
    }
    
    @IBAction func skipRegistrationBtnTapped(_ sender: Any) {
        
        let defaults = UserDefaults.standard
        defaults.setValue(USER_TYPE_VISITOR, forKeyPath: "globalUserType"); // Setting this parameter to identified user type
        defaults.synchronize();

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.setUpForCustomerAndVisitorTabs();
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
