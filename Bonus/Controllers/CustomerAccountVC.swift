//
//  CustomerAccountVC.swift
//  Bonus
//
//  Created by Arvind Sen on 12/01/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire
import MessageUI


class CustomerAccountVC: UIViewController, UITextFieldDelegate, MFMailComposeViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, DismissSelfViewControllerDelegate, DismissPopupVCProtocol, CustomPickerViewDelegate {

    @IBOutlet weak var emailTxtFContainerView: UIView!
    @IBOutlet weak var pwdTxtFContainerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var topBtnContainer: UIView!
    @IBOutlet weak var notificationBtn: UIButton!
    @IBOutlet weak var personalInfoBtn: UIButton!
    
    @IBOutlet weak var imageVerificationLbl: MarqueeLabel!
    //Controls on view
    @IBOutlet weak var invitedByLbl: UILabel!
    @IBOutlet weak var emailIdTxtF: UITextField!
    @IBOutlet weak var passwordTxtF: UITextField!
    
    @IBOutlet weak var customerPinOne: UITextField!
    @IBOutlet weak var customerPinTwo: UITextField!
    @IBOutlet weak var customerPinThird: UITextField!
    @IBOutlet weak var customerPinFourth: UITextField!
    
    @IBOutlet weak var userImgBtn: UIButton!
    @IBOutlet weak var nameTxtF: UITextField!
    @IBOutlet weak var nickNameTxtF: UITextField!
    @IBOutlet weak var countryNameTxtF: UITextField!
    @IBOutlet weak var timeZoneNameTxtF: UITextField!
    @IBOutlet weak var birthDateTxtF: UITextField!
    @IBOutlet weak var bonusNotificationBtn: UIButton!
    @IBOutlet weak var favoriteNotificaationBtn: UIButton!
    
    var scrollViewInitialEdgeInsets: UIEdgeInsets!
    var customPickerView : CustomPickerView? = nil;
    
    var imageCropperView: UIView!
    var cropper : BFCropInterface!
    var croppedImageToUpload: UIImage!
    
    var categoryListArray: NSArray?
    var selectedCountryId: String? = nil;
    var selectedCountryTimeZone: String? = nil;
    var selectedPencil: UIButton!;
    var isBonusNotificationChecked = false;
    var isFavouriteNotificationChecked = false;
    var focusedControl: AnyObject!
    var selectedCategoryID : String?
    var displayOriginalImageView: UIImageView!
    
    var objSelectedCountry : Country? = nil;
    var objCountriesArray : NSMutableArray? = nil;
    var objSelectedCountryTimeZone : CountryTimeZone? = nil;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.barTintColor = TAB_BAR_BG_COLOR;
        
        self.setUpTopHeaderBtn();
        
        self.emailTxtFContainerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.emailTxtFContainerView.layer.borderWidth = 0.5;
        self.emailTxtFContainerView.clipsToBounds = true;
        self.emailTxtFContainerView.layer.cornerRadius = 5.0;
        
        self.pwdTxtFContainerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.pwdTxtFContainerView.layer.borderWidth = 0.5;
        self.pwdTxtFContainerView.clipsToBounds = true;
        self.pwdTxtFContainerView.layer.cornerRadius = 5.0;
        
        self.configureInputAccesseryViewForInputFields();
    }

    override func viewWillAppear(_ animated: Bool) {
        
        let defaults = UserDefaults.standard
        
        if(defaults.object(forKey: "pushNotificationData") != nil)
        {
            self.topBtnTapped(self.notificationBtn);
        }
        else{
            self.setUpViewControlsAndDetails();
            
            // Added keypad notifications
            NotificationCenter.default.addObserver(self, selector: #selector(BusinessAccountVC.keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(BusinessAccountVC.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Keyboard Appearance delegate
    func keyboardWasShown(_ aNotification: Notification){
        
        let info:NSDictionary = aNotification.userInfo! as NSDictionary;
        //let kbSize = (info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue.size;
        var kbSize = (info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue.size;
        if(kbSize.height <= 0.0){
            kbSize = (info.object(forKey: UIKeyboardFrameEndUserInfoKey) as AnyObject).cgRectValue.size;
        }
        
        let contentInsets = UIEdgeInsetsMake(0, 0.0, kbSize.height, 0.0);
        scrollView.contentInset = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        var aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if(self.focusedControl != nil)
        {
        if (!aRect.contains(self.focusedControl!.frame.origin) ) {
            let scrollPoint = CGPoint(x: 0.0, y: focusedControl!.frame.origin.y-kbSize.height);
            self.scrollView.setContentOffset(scrollPoint, animated: true);
        }
        }
    }
    
    func keyboardWillBeHidden(_ aNotification:Notification){
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            //let contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
            //self.scrollView.contentInset = contentInsets;
            self.scrollView.contentInset = self.scrollViewInitialEdgeInsets;
            //self.scrollView.scrollIndicatorInsets = contentInsets;
            // [scrollView setContentOffset:CGPointZero animated:YES];
        })
    }

    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        scrollViewInitialEdgeInsets = self.scrollView.contentInset;
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width, height: 978.0)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // Method get call to setup
    func setUpTopHeaderBtn() {
        
        let height = self.navigationController?.navigationBar.frame.size.height;
        self.topBtnContainer.frame = CGRect(x: 0, y: 0, width: (self.navigationController?.view.frame.width)!, height: height!);
        self.topBtnContainer.backgroundColor = UIColor.clear;
        self.personalInfoBtn.setImage(UIImage(named: "YellowPersonalinfo"), for: UIControlState())
    }
    
    /*
    func setUpPencilBtns(){
        
        let emailIdBtn = UIButton.init(type: .custom);
        emailIdBtn.frame = CGRect(x: 30, y: 0, width: 30, height: self.passwordTxtF.frame.size.height);
        emailIdBtn.setImage(UIImage(named: "Pencilgry"), for: UIControlState());
        emailIdBtn.titleLabel?.font = UIFont.systemFont(ofSize: 11.0);
        emailIdBtn.setTitleColor(UIColor.darkGray, for: UIControlState());
        emailIdBtn.addTarget(self, action: #selector(CustomerAccountVC.pencilBtnTapped), for: .touchUpInside);
        emailIdBtn.tag = 40;
        self.emailIdTxtF.rightViewMode = UITextFieldViewMode.always;
        self.emailIdTxtF.rightView = emailIdBtn;
        
        let passwordBtn = UIButton.init(type: .custom);
        passwordBtn.frame = CGRect(x: 30, y: 0, width: 30, height: self.passwordTxtF.frame.size.height);
        passwordBtn.setImage(UIImage(named: "Pencilgry"), for: UIControlState());
        passwordBtn.titleLabel?.font = UIFont.systemFont(ofSize: 11.0);
        passwordBtn.setTitleColor(UIColor.darkGray, for: UIControlState());
        passwordBtn.addTarget(self, action:#selector(CustomerAccountVC.pencilBtnTapped(_:)), for: .touchUpInside);
        passwordBtn.tag = 50;
        self.passwordTxtF.rightViewMode = UITextFieldViewMode.always;
        self.passwordTxtF.rightView = passwordBtn;
    }
    */
    func configureInputAccesseryViewForInputFields(){
        //Configured in viewDidLoad()
        var accessoryDoneButton: UIBarButtonItem!
        let accessoryToolBar = UIToolbar(frame: CGRect(x : 0, y : 0, width : UIScreen.main.bounds.width, height : 44))
        accessoryDoneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.accessoryViewDoneBtnPressed(_:)))
        let accessroySpace = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil);
        accessoryDoneButton.tintColor = UIColor.darkGray;
        accessoryToolBar.items = [accessroySpace, accessoryDoneButton]
        
        self.customerPinFourth.inputAccessoryView = accessoryToolBar;
    }
    
    func accessoryViewDoneBtnPressed(_ sender: UIButton){
        
        self.customerPinFourth.resignFirstResponder();
    }
    
    // Method will call to set extranal values for the controls
    func setUpViewControlsAndDetails(){
        
        self.userImgBtn.layer.cornerRadius = 7.0;
        //self.userImgBtn.layer.borderWidth = 1
        self.userImgBtn.clipsToBounds = true;
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        //let userId = user.ID;
        //let personType = user.PersonType;
        
        self.invitedByLbl.text = "Invited By: None";
        
        if(user.ReferalUserFirstName != nil)
        {
            self.invitedByLbl.text = "Invited By: \(user.ReferalUserFirstName!)";
        }
        
        self.emailIdTxtF.text = user.EmailID;
        self.passwordTxtF.text = user.Password;
        self.emailIdTxtF.isUserInteractionEnabled = false;
        self.passwordTxtF.isUserInteractionEnabled = false;
        
        let pinString = user.CouponReedeemptionCode ;
        
        if((pinString?.characters.count)! > 0){
            
            self.customerPinOne.text = (pinString?[(pinString?.startIndex)!...(pinString?.characters.index((pinString?.startIndex)!, offsetBy: 0))!])! as String;
            self.customerPinTwo.text = (pinString?[(pinString?.characters.index((pinString?.startIndex)!, offsetBy: 1))!...(pinString?.characters.index((pinString?.startIndex)!, offsetBy: 1))!])! as String;
            self.customerPinThird.text = (pinString?[(pinString?.characters.index((pinString?.startIndex)!, offsetBy: 2))!...(pinString?.characters.index((pinString?.startIndex)!, offsetBy: 2))!])! as String;
            self.customerPinFourth.text = (pinString?[(pinString?.characters.index((pinString?.startIndex)!, offsetBy: 3))!...(pinString?.characters.index((pinString?.startIndex)!, offsetBy: 3))!])! as String;
            
            self.customerPinOne.isUserInteractionEnabled = false;
            self.customerPinTwo.isUserInteractionEnabled = false;
            self.customerPinThird.isUserInteractionEnabled = false;
            self.customerPinFourth.isUserInteractionEnabled = false;
        }
        
        let imageUrl = user.BusinessLogoImagePath;
    
        self.imageVerificationLbl.isHidden = true;
        self.imageVerificationLbl.backgroundColor = UIColor(red: 10/255.0, green: 10/255.0, blue: 10/255.0, alpha: 0.8);
        
       self.userImgBtn.setImage(K_PLACE_HOLDER_IMAGE, for:UIControlState());
        
        if(user.IsImageVerified != nil && Int(user.IsImageVerified!)! == 1){
            self.userImgBtn.sd_setImage(with: URL(string:imageUrl!), for: UIControlState(), placeholderImage: K_PLACE_HOLDER_IMAGE)
        }
        else{
            self.imageVerificationLbl.isHidden = false;
            self.imageVerificationLbl.text = "Image verification pending";
            if(user.IsImageVerified != nil && Int(user.IsImageVerified!)! == 2){
                self.imageVerificationLbl.text = "Image has been disapproved";
            }
            self.userImgBtn.sd_setImage(with: URL(string:imageUrl!), for: UIControlState(), placeholderImage: K_PLACE_HOLDER_IMAGE)
        }

        /*
        if(imageUrl != nil){
            
            if (imageUrl?.range(of: ".png") == nil) {
                self.userImgBtn.setImage(K_PLACE_HOLDER_IMAGE, for:UIControlState());
            }
            else
            {
                if(Int(user.IsImageVerified!)! == 1){
                    self.userImgBtn.sd_setImage(with: URL(string:imageUrl!), for: UIControlState(), placeholderImage: K_PLACE_HOLDER_IMAGE)
                }
                else{
                    var imgMsg = "Image verification pending";
                    if(Int(user.IsImageVerified!)! == 2){
                        imgMsg = "Image has been disapproved";
                    }
                    self.userImgBtn.sd_setImage(with: URL(string:imageUrl!), for: UIControlState());
                    
                    if (self.userImgBtn.viewWithTag(3098) != nil){
                        let lbl = self.userImgBtn.viewWithTag(3098) as! UILabel;
                        lbl.removeFromSuperview();
                    }
                    
                    let imgMarqueeLbl = MarqueeLabel.init(frame: CGRect(x: 2.0, y: self.userImgBtn.frame.size.height - 15, width: (self.userImgBtn.frame.size.width - 2*3), height: 15));
                    imgMarqueeLbl?.text = imgMsg;
                    imgMarqueeLbl?.marqueeType = .MLContinuous;
                    imgMarqueeLbl?.scrollDuration = 15.0;
                    imgMarqueeLbl?.leadingBuffer = 10.0;
                    imgMarqueeLbl?.trailingBuffer = 5.0;
                    imgMarqueeLbl?.tag = 3098;
                    imgMarqueeLbl?.textColor = UIColor.red;
                    imgMarqueeLbl?.font = UIFont.boldSystemFont(ofSize: 12.0);
                    imgMarqueeLbl?.backgroundColor = UIColor(red: 10/255.0, green: 10/255.0, blue: 10/255.0, alpha: 0.8);
                    self.userImgBtn.addSubview(imgMarqueeLbl!);
                }
            }
        }
        */
        
        if (user.FirstName != nil) {
            self.nameTxtF.text = user.FirstName!
        }
        
        if (user.NickName != nil) {
            self.nickNameTxtF.text = user.NickName!
        }
        
        if (user.CountryName != nil) {
            self.countryNameTxtF.text = user.CountryName;
            self.timeZoneNameTxtF.text = user.TimeZoneName;
            self.selectedCountryId = user.CountryID;
            self.selectedCountryTimeZone = user.TimeZoneName!;
        }
        
        if (user.DateOfBirth != nil && user.DateOfBirth != "0000-00-00") {
            let bDate = AppUtility.getDateFromString((user.DateOfBirth)!, currentFormat: "yyyy-MM-dd", desiredFormat: kDATE_FORMAT);
            self.birthDateTxtF.text = bDate;
        }
        
        self.isBonusNotificationChecked = false;
        self.isFavouriteNotificationChecked = false;
        self.bonusNotificationBtn.setImage(K_RIGHT_YELLOW_EMPTY, for: UIControlState());
        self.favoriteNotificaationBtn.setImage(K_RIGHT_YELLOW_EMPTY, for: UIControlState());
        
        if (user.DealAdditionalNotificationFlag != nil && Int(user.DealAdditionalNotificationFlag!)! == 1) {
            self.isBonusNotificationChecked = true;
            self.bonusNotificationBtn.setImage(K_RIGHT_YELLOW, for: UIControlState());
        }
        
        if (user.FavoriteNotificationFlag != nil && Int(user.FavoriteNotificationFlag!)! == 1) {
            self.isFavouriteNotificationChecked = true;
            self.favoriteNotificaationBtn.setImage(K_RIGHT_YELLOW, for: UIControlState());
        }
    }

    /*
    func pencilBtnTapped(_ sender: AnyObject){
        
        self.selectedPencil = sender as! UIButton;
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "SecurityAlertVC") as! SecurityAlertVC
        objVC.popupVCDelegate = self;
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        self.addChildViewController(objVC)
        objVC.didMove(toParentViewController: self);
    }
    */
    
    func dismissPopupViewController(_ vc: UIViewController, responseObj: AnyObject?) {
        self.dismiss(animated: true, completion: nil);
        
        // If child view return response then only it will excecute
        if (responseObj != nil) {
            
            // If view controller will PunchesVC then excecute it
            if (vc.isKind(of: SecurityAlertVC.self)) {
                //let vcObj = responseObj as! SecurityAlertVC;
                
                if (responseObj != nil) {
                    
                    if (self.selectedPencil.tag == 40) {
                        self.emailIdTxtF.isUserInteractionEnabled = true;
                        //self.emailIdTxtF.text = "";
                        self.emailIdTxtF.becomeFirstResponder();
                    }
                    else if (self.selectedPencil.tag == 50) {
                        self.passwordTxtF.isUserInteractionEnabled = true;
                        self.passwordTxtF.text = "";
                        self.passwordTxtF.becomeFirstResponder();
                    }
                    else if (self.selectedPencil.tag == 63) {
                        self.customerPinOne.isUserInteractionEnabled = true;
                        self.customerPinTwo.isUserInteractionEnabled = true;
                        self.customerPinThird.isUserInteractionEnabled = true;
                        self.customerPinFourth.isUserInteractionEnabled = true;
                        self.customerPinOne.text = "";
                        self.customerPinTwo.text = "";
                        self.customerPinThird.text = "";
                        self.customerPinFourth.text = "";
                        
                        self.customerPinOne.becomeFirstResponder();
                    }
                }
            }
            else  if (vc.isKind(of: AllotFreeBieVC.self)) {
                
            }
        }
    }
    
    @IBAction func notificationsBtnTapped(_ sender: AnyObject) {
        
        let btn = sender as! UIButton;
        
        if(btn.tag == 88){
            if(self.isBonusNotificationChecked == false){
                self.isBonusNotificationChecked = true;
                self.bonusNotificationBtn.setImage(K_RIGHT_YELLOW, for: UIControlState());
            }
            else{
                self.isBonusNotificationChecked = false;
                self.bonusNotificationBtn.setImage(K_RIGHT_YELLOW_EMPTY, for: UIControlState());
            }
        }
        else if(btn.tag == 99){
            if(self.isFavouriteNotificationChecked == false){
                self.isFavouriteNotificationChecked = true;
                self.favoriteNotificaationBtn.setImage(K_RIGHT_YELLOW, for: UIControlState());
            }
            else{
                self.isFavouriteNotificationChecked = false;
                self.favoriteNotificaationBtn.setImage(K_RIGHT_YELLOW_EMPTY, for: UIControlState());
            }
        }
    }

    
    @IBAction func helpBtnTapped(_ sender: AnyObject) {
        
         let customerImagesArray = ["Customer_Screen_0_0", "Customer_Screen_1_0", "Customer_Screen_1_5", "Customer_Screen_2", "Customer_Screen_3", "Customer_Screen_4", "Customer_Screen_5", "Customer_Screen_6", "Customer_Screen_7", "Customer_Screen_8", "Customer_Screen_8_1", "Customer_Screen_9", "Customer_Screen_10", "Customer_Screen_11"];
        let slidingScrollView = HelpScrollView(frame: UIScreen.main.bounds);
        slidingScrollView.callBackOnInitiatedController = {status in
        
        }
        slidingScrollView.showSlidingWindow(pictureArray: customerImagesArray);
    }
    
    
    @IBAction func termsOfUseBtnTapped(_ sender: AnyObject) {
        UIApplication.shared.openURL(URL(string: kTERMS_OF_USE)!)
    }

    @IBAction func logoutBtnTapped(_ sender: AnyObject) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID;
        let personType = user.PersonType;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["UserID": userId!,
                                           "PersonType": personType!,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_LOGOUT, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.logoutFromTheSessionForAllUsers();
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                }
            }
            )
        }
    }
    
    @IBAction func userImgVTapped(_ sender: AnyObject) {
        
        let alertController = UIAlertController(title: "Choose Media Type", message: "", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            //println("you have pressed the Cancel button");
        }
        alertController.addAction(cancelAction)
        
        let photosAction = UIAlertAction(title: "Photos", style: .default) { (action:UIAlertAction!) in
            //println("you have pressed OK button");
            self.imagePickerControllerCallWithSource("Photos")
        }
        alertController.addAction(photosAction)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action:UIAlertAction!) in
            //println("you have pressed OK button");
            self.imagePickerControllerCallWithSource("Camera")
        }
        alertController.addAction(cameraAction)
        
        self.present(alertController, animated: true, completion:nil)
    }
    
    func imagePickerControllerCallWithSource(_ source: String?){
        
        let imagPick = UIImagePickerController()
        //imagPick.allowsEditing = true
        imagPick.delegate = self
        if (source == "Photos"){
            imagPick.sourceType = .photoLibrary
        }
        else{
            imagPick.sourceType = .camera
        }
        self.present(imagPick, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        self.dismiss(animated: true, completion: nil)
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.showCropperOnView(pickedImage);
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showCropperOnView(_ croppingImage : UIImage){
    
        let oY : CGFloat = 64.0; //status and top navigation bar height
        let heights = self.view.frame.size.height - oY - 44.0
        
        //let cropperFrameView = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height);
        let cropperFrameView = CGRect(x: self.view.frame.origin.x, y: CGFloat(oY), width: self.view.frame.size.width, height: heights);
        
        if (self.imageCropperView != nil) {
            self.imageCropperView = nil;
        }
        
        self.imageCropperView = UIView.init(frame: cropperFrameView)
        self.imageCropperView.backgroundColor = UIColor.black;
        self.view.addSubview(self.imageCropperView);
        
        let maxWidth = self.imageCropperView.frame.size.width;
        let maxHeight = maxWidth;
        var scaledImage = AppUtility.imageWithImage(croppingImage, scaledToMaxWidth: maxWidth, scaledToMaxHeight: maxHeight);
        
        var imgWidth = scaledImage.size.width;
        var imgHeight = scaledImage.size.height;
        var imgoX = (cropperFrameView.size.width - imgWidth)/2;
        var imgoY = (cropperFrameView.size.height - imgHeight)/2;
        
        if (croppingImage.size.height > croppingImage.size.width && croppingImage.size.height > (2 * self.imageCropperView.frame.size.height)) {
            //maxHeight = self.imageCropperView.frame.size.height;
            //maxWidth = maxHeight * 3/2;
            imgWidth = cropperFrameView.size.width;
            imgHeight = cropperFrameView.size.height * 0.85;
            imgoX = (cropperFrameView.size.width - imgWidth)/2;
            imgoY = 20;
            scaledImage = croppingImage;
        }
        
        self.displayOriginalImageView = UIImageView.init(frame: CGRect(x: imgoX, y: imgoY, width: imgWidth, height: imgHeight))
        self.displayOriginalImageView.isUserInteractionEnabled = true;
        self.displayOriginalImageView.image = scaledImage;
        self.displayOriginalImageView.contentMode = UIViewContentMode.scaleAspectFit;
        self.imageCropperView.addSubview(self.displayOriginalImageView);
        
        let cancelWidth: CGFloat = 70;
        let cancelHeight: CGFloat = 30;
        let canceloX = (cropperFrameView.size.width/2 - CGFloat(cancelWidth))/2;
        let canceloY = imgoY + imgHeight + 10;
        
        let cropCloseBtn = UIButton(type: UIButtonType.custom)
        cropCloseBtn.frame = CGRect(x: canceloX, y: canceloY, width: cancelWidth, height: cancelHeight);
        cropCloseBtn.setTitle("Cancel", for:UIControlState());
        cropCloseBtn.setTitleColor(UIColor.white, for: UIControlState());
        cropCloseBtn.addTarget(self, action: #selector(BusinessAccountVC.cropperCloseBtnTapped), for: UIControlEvents.touchUpInside);
        self.imageCropperView.addSubview(cropCloseBtn);
        
        let cropBtnWidth = cancelWidth;
        let cropBtnHeight = cancelHeight;
        let cropBtnoX = cropperFrameView.size.width/2 + (cropperFrameView.size.width/2 - cancelWidth)/2;
        let cropBtnoY = canceloY;
        
        let cropBtn = UIButton(type: UIButtonType.custom)
        cropBtn.frame = CGRect(x: cropBtnoX, y: cropBtnoY, width: cropBtnWidth, height: cropBtnHeight);
        cropBtn.setTitle("Crop", for:UIControlState());
        cropBtn.setTitleColor(UIColor.white, for: UIControlState());
        cropBtn.addTarget(self, action: #selector(BusinessAccountVC.cropperCropBtnTapped), for: UIControlEvents.touchUpInside);
        self.imageCropperView.addSubview(cropBtn);
        
        // allocate crop interface with frame and image being cropped
        self.cropper = BFCropInterface.init(frame: self.displayOriginalImageView.bounds, andImage: scaledImage, nodeRadius: 8, cropType: kCroppingTypeSquare);
        self.cropper.isStaticCopWindow = true;
        
        // this is the default color even if you don't set it
        self.cropper.shadowColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.60);
        self.cropper.borderColor = UIColor.white;
        self.cropper.borderWidth = 1.5;
        self.cropper.showNodes = true;
        
        var croperWidth = self.displayOriginalImageView.frame.size.width;
        var croperHeight = self.displayOriginalImageView.frame.size.height;
        
        if (croperWidth < croperHeight) {
            croperHeight = croperWidth;
        }
        else if (croperHeight < croperWidth){
            croperWidth = croperHeight;
        }
        
        let positionX = (self.displayOriginalImageView.frame.size.width - croperWidth)/2;
        let positionY = (croperWidth - croperHeight)/2;
        self.cropper.setCropViewPosition(positionX, y: positionY, width: croperWidth, height: croperHeight);
        // add interface to superview. here we are covering the main image view.
        self.displayOriginalImageView.addSubview(self.cropper);
    }
    
    func cropperCloseBtnTapped(){
        self.imageCropperView.removeFromSuperview();
        self.imageCropperView = nil;
        self.cropper.removeFromSuperview();
        self.cropper = nil;
    }
    
    func cropperCropBtnTapped(){
        // crop image
        let croppedImage = self.cropper.getCroppedImage()
        self.cropperCloseBtnTapped();
        
        let maxWidth = self.view.frame.size.width;
        let maxHeight = maxWidth;
        print("--- %@", croppedImage);
        
        let scaledImage = AppUtility.imageWithImage(croppedImage!, scaledToMaxWidth: maxWidth, scaledToMaxHeight: maxHeight);
        
        self.userImgBtn.setImage(scaledImage, for: UIControlState());
        self.croppedImageToUpload = scaledImage;
    }
    
    // MARK:- TextField delegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        self.focusedControl = textField;
        var returnValue = true
        
        if(textField == self.countryNameTxtF){
            returnValue = false;
            self.countryNameTxtF.resignFirstResponder();
            
            self.showCountriesTableView();
            
        }
        else if (self.birthDateTxtF == textField) {
            returnValue = false;
            textField.resignFirstResponder();
            self.displayDatePickerView(tagValue: 700);
        }
        return returnValue;
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.focusedControl = textField;
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.focusedControl = textField;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField == self.customerPinOne)
        {
            self.customerPinTwo.becomeFirstResponder();
        }
        else if (textField == self.customerPinTwo)
        {
            self.customerPinThird.becomeFirstResponder();
        }
        else if (textField == self.customerPinThird)
        {
            self.customerPinFourth.becomeFirstResponder();
        }
        else if (textField == self.customerPinFourth)
        {
            customerPinFourth.resignFirstResponder();
        }
        else{
            textField.resignFirstResponder();
        }

        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == self.passwordTxtF && ((self.passwordTxtF.text?.characters.count)! > 15 && string.characters.count > 0)) {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Password length can not be more then 15 charactors", onView: nil);
        }
        else if (textField == self.customerPinOne && (textField.text?.characters.count)! >= 1 && string.isEmpty){
            
        }
        else if ((textField == self.customerPinOne) && (textField.text?.characters.count == 1 && !string.isEmpty)){
            
            if ((self.customerPinTwo.text?.characters.count)! < 1) {
                self.customerPinTwo.becomeFirstResponder();
            }
            else{
                self.customerPinTwo.resignFirstResponder();
            }
        }
        
        if (textField == self.customerPinTwo && (textField.text?.characters.count)! >= 1 && string.isEmpty){
            
        }
        else if ((textField == self.customerPinTwo) && (textField.text?.characters.count == 1 && !string.isEmpty)){
            
            if ((self.customerPinThird.text?.characters.count)! < 1) {
                self.customerPinThird.becomeFirstResponder();
            }
            else{
                self.customerPinTwo.resignFirstResponder();
            }
        }
        
        if (textField == self.customerPinThird && (textField.text?.characters.count)! >= 1 && string.isEmpty){
            
        }
        else if ((textField == self.customerPinThird) && (textField.text?.characters.count == 1 && !string.isEmpty)){
            
            if ((self.customerPinFourth.text?.characters.count)! < 1) {
                self.customerPinFourth.becomeFirstResponder();
            }
            else{
                self.customerPinThird.resignFirstResponder();
            }
        }
        
        if (textField == self.customerPinFourth && (textField.text?.characters.count)! >= 1 && string.isEmpty){
            
        }
        else if ((textField == self.customerPinFourth) && (textField.text?.characters.count == 1 && !string.isEmpty)){
            
            self.customerPinFourth.resignFirstResponder();
        }
        
        return true;
    }
    
    func showCountriesTableView(){
        
        let dataArray = BonusLocalDBHandler.getAllCountriesDetail() as! NSMutableArray;
        
        if (dataArray.count > 0) {
            
            self.objCountriesArray = dataArray;
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "CountriesVC") as! CountriesVC
            objVC.dataArray = self.objCountriesArray!;
            objVC.callBackWithSelectedCountry = { county in
                self.countryNameTxtF.text = county.CountryName!;
                self.selectedCountryId = county.ID;
                self.callCountryTimeZones(countryCode: county.CountryCode!);
            }
            
            objVC.view.frame = self.view.bounds;
            self.view.addSubview(objVC.view)
            self.addChildViewController(objVC)
            objVC.didMove(toParentViewController: self);
            objVC.showTableView();
        }
        else
        {
            self.getAllCountries();
        }
    }
    
    func getAllCountries() {
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["ID": "0",
                                           "SortBy" : "CountryName",
                                           "OrderBy": "ASC",
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_COUNTRY_LIST, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        
                        let countryArray = NSMutableArray();
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let item = Country(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>)
                                
                                countryArray.add(item);
                            }
                            
                            // Save all the categories in local database
                            BonusLocalDBHandler.saveAllCountriesDetail(countriesArray: countryArray);
                            
                            self.showCountriesTableView();
                        }
                        
                    }
                    
                }
            }
            )
        }
    }
    
    func callCountryTimeZones(countryCode : String){
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["CountryCode": countryCode,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_SELECT_TIME_ZONE_LIST, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        
                        let countryTimeZonesArray = NSMutableArray();
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let item = CountryTimeZone(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>)
                                
                                countryTimeZonesArray.add(item);
                            }
                            
                            self.showTimeZonesTableView(timeZones: countryTimeZonesArray);
                        }
                        
                    }
                    
                }
            }
            )
        }
    }
    
    func showTimeZonesTableView(timeZones: NSMutableArray){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "CountryTimeZoneVC") as! CountryTimeZoneVC
        objVC.dataArray = timeZones;
        objVC.callBackWithSelectedCountryTimeZone = { countyzones in
            self.objSelectedCountryTimeZone = countyzones
            self.timeZoneNameTxtF.text = countyzones.TimeZone!;
            self.selectedCountryTimeZone = countyzones.TimeZone!;
        }
        
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        self.addChildViewController(objVC)
        objVC.didMove(toParentViewController: self);
        objVC.showTableView();
    }
    
    @IBAction func saveBtnTapped(_ sender: AnyObject) {
        // Disable user interection
        self.emailIdTxtF.isUserInteractionEnabled = false;
        self.passwordTxtF.isUserInteractionEnabled = false;
        self.customerPinOne.isUserInteractionEnabled = false;
        self.customerPinTwo.isUserInteractionEnabled = false;
        self.customerPinThird.isUserInteractionEnabled = false;
        self.customerPinFourth.isUserInteractionEnabled = false;
        
        // Resign from responder
        self.nameTxtF.resignFirstResponder();
        self.nickNameTxtF.resignFirstResponder();
        
        let emailId : String = self.emailIdTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let password : String = self.passwordTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        var nickName : String = self.nickNameTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        var name : String = self.nameTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        var birthDate : String = self.birthDateTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        if (birthDate.characters.count > 0) {
            birthDate = AppUtility.getDateFromString(birthDate, currentFormat: kDATE_FORMAT, desiredFormat: kSTANDARD_DATE_FORMAT);
        }
        
        let pin1 : String = self.customerPinOne.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let pin2 : String = self.customerPinTwo.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let pin3 : String = self.customerPinThird.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let pin4 : String = self.customerPinFourth.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        let combinePin = pin1 + "" + pin2 + "" + pin3 + "" + pin4;
        
        //var category = self.selectedCategoryID;
        
        let isValidEmail = AppUtility.validateEmail(emailId);
        
        var message : String? = nil;
        
        if !isValidEmail {
            message = LOGIN_ALERT_MESSAGE_EMAIL_ID;
        }
        else if password.isEmpty {
            message = "Please enter a valid password";
        }
        else if (password.characters.count < 6){
            message = "Please enter a Password containing at least 6 and max 15 characters";
        }
        else if (combinePin.characters.count < 4){
            message = "Invalid PIN entered. Please try again";
        }
//        else if (category!.characters.count <= 0){
//            message = "Please select business category.";
//        }
        
        if message != nil {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
        }
        else{
            
            let deviceToken = AppUtility.getDeviceToken();
            let deviceUniqueId = AppUtility.getDeviceNSUUID();
            
            var pictureDataString = "";
            // Profile picture uploading
            if (self.croppedImageToUpload != nil)
            {
                let pictureData = UIImagePNGRepresentation(self.croppedImageToUpload);
                pictureDataString = (pictureData?.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))!;
            }
            
            
            if (name.characters.count <= 0)
            {
                name = "";
            }
            
            if (nickName.characters.count <= 0)
            {
                nickName = "";
            }
            
            if (birthDate.characters.count <= 0)
            {
                birthDate = "";
            }
            
            if(self.selectedCountryId == nil){
                self.selectedCountryId = "";
            }
            
            var bonusNotification = "0";
            if(self.isBonusNotificationChecked == true)
            {
                bonusNotification = "1"
            }
            
            var favouriteNotification = "0";
            if(self.isFavouriteNotificationChecked == true)
            {
                favouriteNotification = "1"
            }
            
            //var localTimeZoneName: String { return (NSTimeZone.local as NSTimeZone).name }
            
            let defaults: UserDefaults = UserDefaults.standard
            let nsDataUser = defaults.object(forKey: "UserData") as! Data
            let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
            
            // Created postDataDictonary to post the data on server
            let postDataDict : NSDictionary = ["ID" : user.ID!,
                                               "EmailID" : emailId,
                                               "Password" : password,
                                               "CouponReedeemptionCode": combinePin,
                                               "PersonType" : USER_TYPE_CUSTOMER,
                                               "Name" : name,
                                               "NickName": nickName,
                                               "Country" : self.selectedCountryId!,
                                               "DateOfBirth": birthDate,
                                               "DeviceUniqueID" : deviceUniqueId,
                                               "BusinessLogoImagePath" : pictureDataString,
                                            "DealAdditionalNotificationFlag": bonusNotification,
                                               "FavoriteNotificationFlag": favouriteNotification,
                                               "TimeZoneName": self.selectedCountryTimeZone!,
                                               "api_key" : kWEB_SERVICES_API_KEY
            ];
            
            print("postData = %@", postDataDict);
            
            if !AppUtility.isNetworkAvailable(){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
            }
            else{
                
                CustomProgressView.showProgressIndicator();
                Alamofire.request(kSERVICE_SAVE_USER_PERSONALINFO, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                    
                    CustomProgressView.hideProgressIndicator();
                    
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                    print("value =  \(datastring)");
                    
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                        let dataDict = JSON as? NSDictionary;
                        
                        if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                            
                            let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self);
                            
                            let user = User(data: dataDict?.object(forKey: "Response") as! Dictionary<String, AnyObject>)
                            
                            self.setUserDataInDefaultSession(user);
                        }
                        else if((dataDict?.object(forKey: "Status"))! as! String == "Pending" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 700) {
                            
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            
                            let alert = UIAlertController(title: message, message: nil, preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
                                self.logoutBtnTapped("" as AnyObject);
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                            
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                        }
                        else
                        {
                            let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        }
                    }
                }
                )
            }
        }
    }
    
    func setUserDataInDefaultSession(_ userObj: User){
        
        let encodedObject = NSKeyedArchiver.archivedData(withRootObject: userObj) as Data
        let defaults = UserDefaults.standard
        defaults.set(encodedObject, forKey: "UserData")
        defaults.synchronize()
        
        //self.setUpPencilBtns() // Again setup
    }
    
    @IBAction func topBtnTapped(_ sender: AnyObject) {
        
        let btn = sender as! UIButton;
        
        let viewControllers = self.childViewControllers;
        
        // First check if having childViewControllers
        if (viewControllers.count > 0) {
            for viCont in viewControllers {
                let controller = viCont as UIViewController;
                controller.view.removeFromSuperview();
                controller.removeFromParentViewController();
            }
        }
        
        if(btn == self.personalInfoBtn){
            self.personalInfoBtn.setImage(UIImage(named: "YellowPersonalinfo"), for: UIControlState())
            self.notificationBtn.setImage(UIImage(named: "Notification"), for: UIControlState())
            
            self.setUpViewControlsAndDetails();
        }
        else if(btn == self.notificationBtn){
            self.personalInfoBtn.setImage(UIImage(named: "Personalinfo"), for: UIControlState())
            self.notificationBtn.setImage(UIImage(named: "YellowNotification"), for: UIControlState())
            
            AppUtility.removedBackTitleAndChangeItsColor(VC: self, backTitle: "", backTintColor: UIColor.white);
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "CustomerNotificationTVC") as! CustomerNotificationTVC
            
            let oY = Double((self.navigationController?.navigationBar.frame.height)! + 20.0);
            let height = Double(self.view.bounds.height - (2.0 * (self.navigationController?.navigationBar.frame.height)!) - 20.0)
            
            objVC.view.frame = CGRect(x: 0.0, y: oY, width: Double(self.view.bounds.width), height: height);
            
            self.view.addSubview(objVC.view)
            self.addChildViewController(objVC)
            objVC.didMove(toParentViewController: self);
        }
    }
    
    @IBAction func sendFeedBackBtnTapped(_ sender: AnyObject) {
        
        let mailComposer = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = self;
        mailComposer.setToRecipients([K_CUSTOMER_CARE_EMAIL_ID]);
        mailComposer.setSubject("Bonus : Feedback")
        self.navigationController?.present(mailComposer, animated: true, completion: nil);
    }
    
    
    // MARK :- Mail composer delegate handler
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        // Notifies users about errors associated with the interface
        var message: String?;
        switch (result)
        {
        case MFMailComposeResult.cancelled:
            print("Result: canceled");
            break;
        case MFMailComposeResult.saved:
            print("Result: saved");
            message = "Mail saved successfully";
            break;
        case MFMailComposeResult.sent:
            print("Result: sent");
            message = "Mail sent successfully";
            break;
        case MFMailComposeResult.failed:
            print("Result: failed");
            break;
        default:
            print("Result: not sent");
            message = "Mail could not send";
            break;
        }
        
        controller.dismiss(animated: true, completion: nil);
        
        if (message != nil) {
            AppUtility.showAlertWithTitleAndMessage("Mail Status", alertMessage: message!, onView: self);
        }
    }
    
    func dismissSelfViewControllerFromSuperViewController(_ vc: UIViewController, responseObj: AnyObject?) {
    }
    
    // Custom UI Date picker.
    func displayDatePickerView(tagValue: Int)
    {
        if(self.customPickerView == nil)
        {
            self.customPickerView = CustomPickerView(frame: (self.view.window?.frame)!);
            self.view.window?.addSubview(self.customPickerView!);
            self.customPickerView?.pickerViewDelegate = self;
            self.customPickerView?.tag = tagValue;
            self.customPickerView?.showDatePickerView("MaxToday");
            
            if (self.birthDateTxtF.text?.characters.count != 0) {
                self.customPickerView?.setPreviousEnteredDate(dateString: self.birthDateTxtF.text);
            }

        }
    }
    
    func datePickerViewValueChanged(dateStr: String, pickerView: UIView){
        
        if(pickerView.tag == 700){
            self.birthDateTxtF.text = dateStr;
        }
    }
    
    func removeCustomPickerView(buttonTitle: String, pickerView: UIView) {
        self.customPickerView = nil;
    }
    
    @IBAction func infoBtnTapped(_ sender: Any) {
        let btn = sender as! UIButton
        
        if(btn.tag == 8765){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "MoreDescriptionVC") as! MoreDescriptionVC
            objVC.popUptitle = "About PIN";
            objVC.descriptionTxt = "Please choose a Personal Pin code to use during coupon redemption.";
            objVC.view.frame = self.view.bounds;
            self.view.addSubview(objVC.view)
            self.addChildViewController(objVC)
            objVC.didMove(toParentViewController: self);
        }
    }
    
    @IBAction func pencilBtnTapped(_ sender: Any) {
        
        self.selectedPencil = sender as! UIButton;
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "SecurityAlertVC") as! SecurityAlertVC
        objVC.popupVCDelegate = self;
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        self.addChildViewController(objVC)
        objVC.didMove(toParentViewController: self);
    }
    
}
