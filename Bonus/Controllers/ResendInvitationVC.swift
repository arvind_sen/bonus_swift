//
//  ResendInvitationVC.swift
//  Bonus
//
//  Created by Arvind Sen on 23/06/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class ResendInvitationVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    var resendInvitationsArray : Array<ResendInvitation>? = nil;
    var selectedRowsIndexPaths : Array<IndexPath> = [IndexPath]();
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false
        // Do any additional setup after loading the view.
        
        self.containerView.layer.cornerRadius = 5.0;
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.containerView.layer.borderWidth = 1.0;
        self.containerView.clipsToBounds = true;
        
        //self.getAllResendInvitationUsers();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
        
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.resendInvitationsArray!.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        cell.selectionStyle = .none;
        // Configure the cell...
        self.configureTableViewCell(cell, forRowAtIndexPath: indexPath);
        return cell;
    }

    func configureTableViewCell(_ cell: UITableViewCell, forRowAtIndexPath indexPath: IndexPath) {
        //cell.textLabel!.text = "Row \(indexPath.row)"
        
        let obj = (self.resendInvitationsArray?[indexPath.row])! as ResendInvitation;
        
        let time = AppUtility.getDateFromString((obj.CreatedDate)!, currentFormat: "yyyy-MM-dd HH:mm:ss", desiredFormat: "dd MMM yyyy HH:mm:ss");
        
        //[AppUtility getDateFromString:[dict objectForKey:@"CreatedDate"] withCurrentFormat:@"yyyy-MM-dd HH:mm:ss" andDesiredFormat:@"dd MMM yyyy HH:mm:ss"];
        
        let emailLbl = cell.contentView.viewWithTag(1561) as? UILabel;
        let timeLbl = cell.contentView.viewWithTag(1562) as? UILabel;
        let resendBtn = cell.contentView.viewWithTag(1563) as? UIButton;
        resendBtn?.addTarget(self, action: #selector(resendInvitationEmailButtonTapped(_:)), for: UIControlEvents.touchUpInside);
        
        emailLbl?.text = obj.InvitationMailID;
        timeLbl?.text = time;
    }
    
    @IBAction func closeBtnTapped(_ sender: AnyObject) {
        // Removed it from superview controller
        self.view.removeFromSuperview()
        self.removeFromParentViewController();
    }

    func resendInvitationEmailButtonTapped(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        let pointInTable = btn.convert(btn.bounds.origin, to: self.tblView);
        let indexPath = self.tblView.indexPathForRow(at: pointInTable);
        
        if(selectedRowsIndexPaths.contains(indexPath!) == false){
            selectedRowsIndexPaths.append(indexPath!);
        }
        
        let objResendInvitation = self.resendInvitationsArray?[(indexPath?.row)!];
        
        
        self.resendInvitationToEmailAddress(objResendInvitation!);
    }
    
    func resendInvitationToEmailAddress(_ obj : ResendInvitation){
        
        let alert = UIAlertController(title: "Do you really want to resend invitation", message: (obj.InvitationMailID)!, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            
            self.callServiceToResendInvitation(obj);
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil));
        self.present(alert, animated: true, completion: nil)
    }
    
    func callServiceToResendInvitation(_ obj : ResendInvitation){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID;
        
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["InvitationMailID" : (obj.InvitationMailID)!,
                                           "ID": (obj.ID)!,
                                           "UserID": userId!,
                                           "IsInvitationAccept" : ("\(obj.IsInvitationAccept)!"),
                                           "InvitationType": (obj.InvitationType)!,
                                           "JoiningCode": (obj.JoiningCode)!,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_RESEND_USER_INVITATIONS, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String) == 200) {
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        
                        let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil));
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        
                    }
                }
            }
            )
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
