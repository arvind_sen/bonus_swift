//
//  NotificationTVC.swift
//  Bonus
//
//  Created by Arvind Sen on 13/01/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class NotificationTVC: UITableViewController {
    var notificationDataArray : Array<Notifications> = [Notifications]();
    var flagToNoDataFound = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        AppUtility.navigationColorAndItsTextColor(VC: self);
        
        let defaults: UserDefaults = UserDefaults.standard
        let userType = defaults.object(forKey: "globalUserType") as! String
         if(userType == USER_TYPE_BUSINESS || userType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE) {
            self.showTopBtn()
        }
    }

    func showTopBtn(){
        // Right Top tab bar button configration
        let addBtn = UIButton.init(type: .contactAdd);
        addBtn.addTarget(self, action: #selector(self.topAddBtnTapped), for: UIControlEvents.touchUpInside);
        let barRightBtn = UIBarButtonItem(customView: addBtn);
        self.navigationItem.rightBarButtonItem = barRightBtn;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(true);
        self.getAllNotifications(offset: 0);
    }

    func topAddBtnTapped(){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        if (user.BusinessCategory != nil) { // If business user having category then this condition will excute
            
            if(user.IsActive == "1"){
                
                AppUtility.removedBackTitleAndChangeItsColor(VC: self, backTitle: "", backTintColor: UIColor.white);
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyboard.instantiateViewController(withIdentifier: "AddNotificationVC") as! AddNotificationVC
                self.navigationController?.pushViewController(objVC, animated: true);
            }
            else{
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: AFTER_DEACTIVATE_BUSINESS_ACCOUNT_MESSAGE, onView: self);
            }
        }
        else{
            let alert = UIAlertController(title: nil, message: "Please update mandatory account information.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                
                let tbController = self.revealViewController().frontViewController as? BusinessAndEmployeeTBC;
                tbController?.selectedIndex = 0;
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func getAllNotifications(offset: Int) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        var userId = user.ID;
        
        if(user.PersonType != nil && user.PersonType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE) {
            userId = user.BusinessUserID;
        }
        
        //let personType = user.PersonType;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["UserID": userId!,
                                           "Offset" : String(offset),
                                           "Limit": K_TABLE_ROW_LIMITS,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_BUSINESS_USER_NOTIFICATION_LIST, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        let resourcePath = dataDict?.object(forKey: "ResourcePath") as! Dictionary<String, AnyObject>
                        
                        if(offset == 0){
                            self.notificationDataArray.removeAll();
                        }
                        
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let obj = Notifications(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>, resourcePath: resourcePath)
                                
                                self.notificationDataArray.append(obj);
                            }
                            OperationQueue.main.addOperation {
                                self.flagToNoDataFound = false;
                                self.tableView.reloadData();
                            }
                        }
                        else{
                            OperationQueue.main.addOperation {
                                self.flagToNoDataFound = true;
                                self.tableView.reloadData()
                            }
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as! String == "500"){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 100) {
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        self.navigationController?.view.makeToast(message, duration: 2.0, position: .center)
                        OperationQueue.main.addOperation {
                            self.flagToNoDataFound = true;
                            self.tableView.reloadData()
                        }
                    }
                    else
                    {
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        self.navigationController?.view.makeToast(message, duration: 2.0, position: .center)
                        //AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                        
                        
                    }
                }
            }
            )
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        var numOfSections: Int = 0
        if (self.notificationDataArray.count > 0)
        {
            //tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else if (self.flagToNoDataFound == true)
        {
            var oX = 0.0;
            var oY = 0.0;
            let widthX = tableView.frame.size.width;
            let heightY = tableView.frame.size.height;
            
            let emptyContainer = UIView.init(frame: CGRect(x: oX, y: oY, width: Double(widthX), height: Double(heightY)));
            emptyContainer.backgroundColor = K_EMPTY_TABLE_BG_COLOR;
            ///tblCell.contentView.addSubview(emptyContainer);
            
            let imgWidth = 100.0;
            let imgHeight = 100.0;
            oX = (Double(widthX) - imgWidth)/2;
            oY = (Double(heightY) - imgHeight)/2;
            
            let imageView = UIImageView.init(frame: CGRect(x: oX, y: oY, width: imgWidth, height: imgHeight));
            imageView.backgroundColor = UIColor.clear;
            imageView.contentMode = .scaleAspectFit;
            imageView.image = UIImage.init(named: "alert");
            emptyContainer.addSubview(imageView);
            
            let titleWidth = emptyContainer.frame.size.width - 20.0;
            let titleHeight = 40.0;
            let OY = imageView.frame.size.height + imageView.frame.origin.y + 10.0;
            
            let titleLbl = UILabel.init(frame: CGRect(x: 10.0, y: Double(OY), width: Double(titleWidth), height: titleHeight));
            titleLbl.textAlignment = .center;
            titleLbl.numberOfLines = 0;
            titleLbl.lineBreakMode = .byWordWrapping;
            titleLbl.text = "No notification has been received or added yet"
            titleLbl.textColor = UIColor.darkGray;
            emptyContainer.addSubview(titleLbl);
            titleLbl.sizeToFit();
            
            tableView.backgroundView  = emptyContainer;
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.notificationDataArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...
        configureCell(tblCell: cell, indexPath: indexPath);
        return cell
    }
    
    
    func configureCell(tblCell: UITableViewCell, indexPath: IndexPath){
        
        let obj = notificationDataArray[indexPath.row];
        
        let logoImageV = tblCell.contentView.viewWithTag(100) as! UIImageView;
        let dateLbl = tblCell.contentView.viewWithTag(101) as! UILabel;
        let nameLbl = tblCell.contentView.viewWithTag(102) as! UILabel;
        let detailedLbl = tblCell.contentView.viewWithTag(103) as! UILabel;
        
        let marqueeLbl = tblCell.contentView.viewWithTag(86) as? MarqueeLabel;
        marqueeLbl?.isHidden = true;
        marqueeLbl?.backgroundColor = UIColor(colorLiteralRed: 0.0, green: 0.0, blue: 0.0, alpha: 0.7);
        logoImageV.image = K_PLACE_HOLDER_IMAGE
        
        
        let defaults: UserDefaults = UserDefaults.standard
        let userType = defaults.value(forKey: "globalUserType") as! String;
        
        if(obj.IsImageVerified != nil && Int(obj.IsImageVerified!)! == 1){
            let imageUrl = obj.BusinessLogoImagePath;
            logoImageV.sd_setImage(with: URL(string:imageUrl!), placeholderImage: K_PLACE_HOLDER_IMAGE)
        }
        else{
            
            if(userType == USER_TYPE_BUSINESS || userType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE)
            {
                marqueeLbl?.isHidden = false;
                marqueeLbl?.text = "Image verification pending";
                if(obj.IsImageVerified != nil && Int(obj.IsImageVerified!)! == 2){
                    marqueeLbl?.text = "Image has been disapproved";
                }
                logoImageV.sd_setImage(with: URL(string: obj.BusinessLogoImagePath!), placeholderImage: K_PLACE_HOLDER_IMAGE)
            }
        }
        
        nameLbl.text = obj.FirstName;
        
        let dates = AppUtility.getDateFromString((obj.CreatedDate)!, currentFormat: "yyyy-MM-dd HH:mm:ss", desiredFormat: "dd MMM yyyy HH:mm:ss");
        
        dateLbl.text = dates;
        detailedLbl.text = (obj.NotificationDescription)!;
        detailedLbl.sizeToFit();
    }

    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating ")
        
        if((scrollView.contentSize.height + 20.0) < (scrollView.contentOffset.y + scrollView.frame.size.height)){
            
            let offset = self.notificationDataArray.count;
            self.getAllNotifications(offset: offset)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = self.notificationDataArray[indexPath.row];
        self.displayNotificationDetailView(obj);
    }
    
    func displayNotificationDetailView(_ objNotification : Notifications){
        
        AppUtility.removedBackTitleAndChangeItsColor(VC: self, backTitle: "", backTintColor: UIColor.white);
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "NotificationDetailVC") as! NotificationDetailVC
        
        if(objNotification.FirstName != nil){
            objVC.businessName = objNotification.FirstName!;
        }
        
        if(objNotification.BusinessLogoImagePath != nil){
            objVC.imageUrl = objNotification.BusinessLogoImagePath!;
        }
        
        if(objNotification.CreatedDate != nil){
            objVC.receivedOn = (objNotification.CreatedDate)!
        }
        
        if(objNotification.NotificationDescription != nil){
            objVC.detailDescription = (objNotification.NotificationDescription)!
        }
        
        self.navigationController?.pushViewController(objVC, animated: true);
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
