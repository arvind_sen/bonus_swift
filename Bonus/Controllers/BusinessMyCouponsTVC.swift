//
//  BusinessMyCouponsTVC.swift
//  Bonus
//
//  Created by Arvind Sen on 29/12/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import Alamofire

class BusinessMyCouponsTVC: UITableViewController {

    var myCouponsArray : Array<MyCoupon> = [MyCoupon]()
    var flagToNoDataFound = false;
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        // Do things before loading the view controller
        
        self.navigationController?.navigationBar.barTintColor = TAB_BAR_BG_COLOR;
        self.navigationController?.isNavigationBarHidden = false;
        self.tableView.backgroundColor = K_LISTING_VIEW_BG_COLOR;
        
        self.setTopBarItems();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        if (user.BusinessCategory != nil) {
            self.getAllCoupons(0);
        }
        else{
            let alert = UIAlertController(title: nil, message: "Please update mandatory account information.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                
                let tbController = self.revealViewController().frontViewController as? BusinessAndEmployeeTBC;
                tbController?.selectedIndex = 0;
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func setTopBarItems(){
        let oY = CGFloat(5.0)
        let oX = CGFloat(15.0)
        let height = (self.navigationController?.navigationBar.frame.height)! - CGFloat(2 * oY);
        let width = (self.navigationController?.navigationBar.frame.width)! - CGFloat(2 * oX);
        let titleLbl = UILabel.init(frame: CGRect(x: 0.0, y: oY, width: width, height: height));
        titleLbl.text = "My Coupons"
        titleLbl.textColor = UIColor.white;
        self.navigationItem.titleView = titleLbl
        
        // Right Top tab bar button configration
        let addBtn = UIButton.init(type: .contactAdd);
        addBtn.addTarget(self, action: #selector(BusinessMyCouponsTVC.topAddBtnTapped), for: UIControlEvents.touchUpInside);
        let barRightBtn = UIBarButtonItem(customView: addBtn);
        self.navigationItem.rightBarButtonItem = barRightBtn;
    }
    
    func topAddBtnTapped(){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        if(user.IsActive != nil && user.IsActive! == "1")
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "AddEditCouponVC") as! AddEditCouponVC
            self.navigationController?.pushViewController(objVC, animated: true);
        }
        else
        {
            AppUtility.showAlertWithTitleAndMessage(AFTER_DEACTIVATE_BUSINESS_ACCOUNT_MESSAGE, alertMessage: nil, onView: self);
        }
    }
    
    // Method will call to get all the coupons created by a business user
    func getAllCoupons(_ offset: Int){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        var userId = user.ID;
        
        if(user.PersonType != nil && user.PersonType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE) {
            userId = user.BusinessUserID;
        }
        
        let personType = user.PersonType;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["UserID": userId!,
                                           "PersonType": personType!,
                                           "Limit" : K_TABLE_ROW_LIMITS,
                                           "Offset" : offset,
                                           "SortBy" : "DealMasterID",
                                           "OrderBy" : "DESC",
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_MY_COUPONS_LIST_OF_A_BUSINESS, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                         let resourcePath = dataDict?.object(forKey: "ResourcePath") as! Dictionary<String, AnyObject>
                        
                        if(offset == 0){
                            self.myCouponsArray.removeAll();
                        }
                        
                        if(dictArray.count > 0)
                        {
                            var isActiveCouponCountFlag = 0;
                            
                            for item in dictArray {
                                let obj = MyCoupon(data: item as! Dictionary<String, AnyObject>, resourcePaths: resourcePath)
                                self.myCouponsArray.append(obj);
                                
                                if(isActiveCouponCountFlag == 0 && Int(obj.IsActiveCouponCount!)! > 0) {
                                    isActiveCouponCountFlag = Int(obj.IsActiveCouponCount!)!;
                                }
                            }
                            
                            // If user type business then only set isActiveCoupanCount key
                            if(user.PersonType != USER_TYPE_BUSINESS_BRANCH_EMPLOYEE){
                            
                            if(isActiveCouponCountFlag >= 1){
                                user.IsActiveCoupanCount = ("\(isActiveCouponCountFlag)");
                                let encodedObject = NSKeyedArchiver.archivedData(withRootObject: user) as Data
                                let defaults = UserDefaults.standard
                                defaults.set(encodedObject, forKey: "UserData")
                                defaults.synchronize();
                                }
                            }
                            self.flagToNoDataFound = false;
                        }
                        else{
                            
                            if(self.myCouponsArray.count <= 0)
                            {
                                AppUtility.showAlertWithTitleAndMessage("Please click on + to add a new coupon", alertMessage: nil, onView: self);
                            }
                            else{
                                self.navigationController?.view.makeToast("No record found", duration: 2.0, position: .center)
                            }
                            self.flagToNoDataFound = true;
                        }
                        
                        // Reload table view even though data not found
                        self.tableView.reloadData();
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail") {
                        
                        let message = dataDict?.object(forKey: "Message") as!String
                            
                        let alert = UIAlertController(title: message, message: nil, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                            
                            if(dataDict?.object(forKey: "StatusCode") as! NSNumber == 500){
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.logoutFromTheSessionForAllUsers();
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                }
            )
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        var numOfSections: Int = 0
        if (self.myCouponsArray.count > 0)
        {
            //tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else if (self.flagToNoDataFound == true)
        {
            var oX = 0.0;
            var oY = 0.0;
            let widthX = tableView.frame.size.width;
            let heightY = tableView.frame.size.height;
            
            let emptyContainer = UIView.init(frame: CGRect(x: oX, y: oY, width: Double(widthX), height: Double(heightY)));
            emptyContainer.backgroundColor = K_EMPTY_TABLE_BG_COLOR;
            ///tblCell.contentView.addSubview(emptyContainer);
            
            let imgWidth = 100.0;
            let imgHeight = 100.0;
            oX = (Double(widthX) - imgWidth)/2;
            oY = (Double(heightY) - imgHeight)/2;
            
            let imageView = UIImageView.init(frame: CGRect(x: oX, y: oY, width: imgWidth, height: imgHeight));
            imageView.backgroundColor = UIColor.clear;
            imageView.contentMode = .scaleAspectFit;
            imageView.image = UIImage.init(named: "alert");
            emptyContainer.addSubview(imageView);
            
            let titleWidth = emptyContainer.frame.size.width;
            let titleHeight = 25.0;
            let OY = imageView.frame.size.height + imageView.frame.origin.y + 10.0;
            
            let titleLbl = UILabel.init(frame: CGRect(x: 0.0, y: Double(OY), width: Double(titleWidth), height: titleHeight));
            titleLbl.textAlignment = .center;
            titleLbl.text = "No coupon has been added yet"
            titleLbl.textColor = UIColor.darkGray;
            emptyContainer.addSubview(titleLbl);
            
            tableView.backgroundView  = emptyContainer;
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // #warning Incomplete implementation, return the number of rows
        var returnValue = 0;
        
        if (self.myCouponsArray.count > 0) {
            returnValue = self.myCouponsArray.count;
        }
        return returnValue;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCouponCell", for: indexPath) as! MyCouponCell
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none;
        // Configure the cell...
        self.configureTableViewCell(cell, forRowAtIndexPath: indexPath);
        return cell
    }
    
    // This below method is setting different values for a row
    func configureTableViewCell(_ cell: MyCouponCell, forRowAtIndexPath:IndexPath){
        
        let obj = self.myCouponsArray[forRowAtIndexPath.row] as MyCoupon;
        
        let marqueeLbl = cell.contentView.viewWithTag(40) as? MarqueeLabel;
        marqueeLbl?.isHidden = true;
        marqueeLbl?.backgroundColor = UIColor(colorLiteralRed: 0.0, green: 0.0, blue: 0.0, alpha: 0.7);
        
        cell.businessImgV.image = K_PLACE_HOLDER_IMAGE
        if(obj.IsBusinessImageVerified != nil && Int(obj.IsBusinessImageVerified!)! == 1){
            let imageUrl = obj.BusinessLogoImagePath;
            cell.businessImgV.sd_setImage(with: URL(string:imageUrl!), placeholderImage: K_PLACE_HOLDER_IMAGE)
        }
        else{
            marqueeLbl?.isHidden = false;
            marqueeLbl?.text = "Image verification pending";
            if(obj.IsBusinessImageVerified != nil && Int(obj.IsBusinessImageVerified!)! == 2){
                marqueeLbl?.text = "Image has been disapproved";
            }
            let imageUrl = obj.BusinessLogoImagePath;
            //cell.businessImgV.sd_setImage(with: URL(string:imageUrl!), placeholderImage: K_PLACE_HOLDER_IMAGE)
            cell.businessImgV.sd_setImage(with: URL(string: obj.BusinessLogoImagePath!), placeholderImage: K_PLACE_HOLDER_IMAGE)
        }
        
        var expiredMessage: String? = nil;
        cell.expiredTitleLbl.isHidden = true;
        
        if(obj.PromotionExpired != nil && Int(obj.PromotionExpired!)! == 1){
            expiredMessage = "Promotion Expired";
        }
        else if(obj.BuyExpired != nil && Int(obj.BuyExpired!)! == 1){
            expiredMessage = "Buy Expired";
        }
    
        if (expiredMessage != nil) {
            cell.expiredTitleLbl.isHidden = false;
            cell.expiredTitleLbl.text = expiredMessage;
        }
        
        cell.couponTitle.text = obj.TypeOfCoupon;
        
        cell.couponStatusImgV.image = UIImage(named: "CouponStatusInactive")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        if(obj.IsActive != nil && Int(obj.IsActive!)! == 1){
             cell.couponStatusImgV.image = UIImage(named: "CouponStatusActive")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        }
        
        let totalIssued = Int(obj.TotalNoOfDeal!)!;
        
        var availableDeals = 0;
        if(obj.AvalDealInNumber != nil){
            availableDeals = Int(obj.AvalDealInNumber!)!
        }else{
            availableDeals = totalIssued;
        }
        
        let totalBought = totalIssued - availableDeals;
        let totalRedeemed = Int(obj.TotalRedeemed!)!;
        let totalActive = Int(obj.TotalActiveCoupan!)!;
        
        var couponsStartDate = "Not mentioned"
        if(obj.StartDate != nil){
            couponsStartDate = AppUtility.getDateFromString(obj.StartDate!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kDATE_FORMAT)
        }
        
        var couponsEndDate = "Not mentioned";
        if(obj.EndDate != nil){
            couponsEndDate = AppUtility.getDateFromString(obj.EndDate!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kDATE_FORMAT)
        }
        
        var couponsRedeemedEndDate = "Not mentioned";
        if(obj.PromotionEndDate != nil){
            couponsRedeemedEndDate = AppUtility.getDateFromString(obj.PromotionEndDate!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kDATE_FORMAT)
        }
        
        let lblStr = "Total coupons Issued : \(totalIssued) \nTotal coupons Bought : \(totalBought) \nTotal coupons Redeemed : \(totalRedeemed) \nTotal coupons Active : \(totalActive) \n Offer Start Date: \(couponsStartDate) \n Offer End Date : \(couponsEndDate) \nRedemption End Date : \(couponsRedeemedEndDate)";
        
        cell.couponDetails.text = lblStr;
        
        for index in 51...55 {
            let starImg = cell.contentView.viewWithTag(index) as? UIImageView;
            
            if (index <= Int(obj.Maxrating!)!) {
                starImg?.image = UIImage(named: "starfull")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
            }
            else{
                starImg?.image = UIImage(named: "starempty")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let backItem = UIBarButtonItem()
        backItem.title = "Coupon Management"
        self.navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        //AppUtility.removedBackTitleAndChangeItsColor(VC: self, backTitle: "Coupon Management", backTintColor: UIColor.white);
        let obj = self.myCouponsArray[indexPath.row];
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "CouponDetailVC") as! CouponDetailVC
        objVC.objMyCoupon = obj;
        self.navigationController?.pushViewController(objVC, animated: true);
        /*
        let obj = self.myCouponsArray[indexPath.row] as MyCoupon;
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "AddEditCouponVC") as! AddEditCouponVC
        objVC.myCouponToEdit = obj;
        self.navigationController?.pushViewController(objVC, animated: true);
        */
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating ")
        
        if((scrollView.contentSize.height) < (scrollView.contentOffset.y + scrollView.frame.size.height)){
            let count = self.myCouponsArray.count;
            self.getAllCoupons(count);
        }
    }
    /*
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let offset = scrollView.contentOffset.y + scrollView.frame.size.height
        let height = scrollView.contentSize.height
        
        if(offset >= height)
        {
            let count = self.myCouponsArray.count;
            //if(count % 10 == 0){
                self.getAllCoupons(count);
            //}
        }
    }*/
}
