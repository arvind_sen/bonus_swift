//
//  MoreDescriptionVC.swift
//  Bonus
//
//  Created by Arvind Sen on 17/06/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit

class MoreDescriptionVC: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoTxtView: UITextView!
    
    var popUptitle = "Title";
    var descriptionTxt = "Description";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false;
        
        //self.containerView.backgroundColor = BLUE_BG_COLOR;
        //self.containerView.clipsToBounds = true;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
        self.titleLabel.text = self.popUptitle;
        self.infoTxtView.text = self.descriptionTxt;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func okBtnTapped(_ sender: Any) {
        self.view.removeFromSuperview()
        self.removeFromParentViewController();
    }
}
