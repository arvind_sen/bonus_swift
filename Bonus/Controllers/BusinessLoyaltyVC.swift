//
//  BusinessLoyaltyVC.swift
//  Bonus
//
//  Created by Arvind Sen on 17/11/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import Alamofire
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class BusinessLoyaltyVC: UIViewController, UITextViewDelegate, UITextFieldDelegate {
    
    var isActiveMobilePunchCard = true;
    var delegate: DismissSelfViewControllerDelegate? = nil
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var numberOfPunchTxtF: UITextField!
    @IBOutlet weak var descriptionTxtF: UITextView!
    @IBOutlet weak var checkMarkBtn: UIButton!
    var loyaltyID = "0";
    var previousPunches = "0";
    var focusedControl : AnyObject! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(BusinessLoyaltyVC.keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BusinessLoyaltyVC.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.getLoyaltyInfo();
        self.configureInputAccesseryViewForInputFields();
    }
    
    func configureInputAccesseryViewForInputFields(){
        //Configured in viewDidLoad()
        var accessoryDoneButton: UIBarButtonItem!
        let accessoryToolBar = UIToolbar(frame: CGRect(x : 0, y : 0, width : UIScreen.main.bounds.width, height : 44))
        accessoryDoneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.accessoryViewDoneBtnPressed(_:)))
        let accessroySpace = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil);
        accessoryDoneButton.tintColor = UIColor.darkGray;
        accessoryToolBar.items = [accessroySpace, accessoryDoneButton]
        
        self.numberOfPunchTxtF.inputAccessoryView = accessoryToolBar
        self.descriptionTxtF.inputAccessoryView = accessoryToolBar
    }
    
    func accessoryViewDoneBtnPressed(_ sender: UIButton){
        self.numberOfPunchTxtF.resignFirstResponder();
        self.descriptionTxtF.resignFirstResponder();
    }
    
    func keyboardWasShown(_ aNotification: Notification){
        
        let info:NSDictionary = aNotification.userInfo! as NSDictionary;
        //let kbSize = (info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue.size;
        var kbSize = (info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue.size;
        if(kbSize.height <= 0.0){
            kbSize = (info.object(forKey: UIKeyboardFrameEndUserInfoKey) as AnyObject).cgRectValue.size;
        }
        
        let contentInsets = UIEdgeInsetsMake(0, 0.0, kbSize.height, 0.0);
        scrollView.contentInset = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        var aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!aRect.contains(self.focusedControl!.frame.origin) ) {
            let scrollPoint = CGPoint(x: 0.0, y: focusedControl!.frame.origin.y-kbSize.height);
            self.scrollView.setContentOffset(scrollPoint, animated: true);
        }
    }
    
    func keyboardWillBeHidden(_ aNotification:Notification){
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
            self.scrollView.contentInset = contentInsets;
            self.scrollView.scrollIndicatorInsets = contentInsets;
            // [scrollView setContentOffset:CGPointZero animated:YES]DidDisappear(animated: Bool) {
        }) 
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func getLoyaltyInfo()->Void {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        var userId = user.ID;
        
        if(user.PersonType != nil && user.PersonType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE) {
            userId = user.BusinessUserID;
        }
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["BusinessUserID": userId!,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
//            Alamofire.request(kSERVICE_SELECT_LOYALTY, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
//                //
//            })
            
            Alamofire.request(kSERVICE_SELECT_LOYALTY, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let arry = dataDict?.object(forKey: "Response") as! NSArray;
                        if(arry.count > 0){
                            let firstObj = arry.firstObject as! NSDictionary;
                            let dict = firstObj.object(forKey: "output") as! Dictionary<String, AnyObject>
                            
                            self.setLoyaltyData(dict as NSDictionary);
                        }
                    }
                }
                }
            )
        }
    }
    
    
    func setLoyaltyData(_ dict : NSDictionary) {
        
        self.descriptionTxtF.text =  dict.value(forKey: "LoyalityDescription") as? String;
        self.descriptionTxtF.textColor = UIColor.darkGray;
        
        self.numberOfPunchTxtF.text =  dict.value(forKey: "PunchRequiredForFreeBee") as? String;
        self.loyaltyID = dict.value(forKey: "ID") as! String
        self.previousPunches = dict.value(forKey: "PunchRequiredForFreeBee") as! String
        
        self.checkMarkBtn?.setImage(K_RIGHT_YELLOW_EMPTY, for: UIControlState())
        if(dict.value(forKey: "IsActive") as! Bool == true ){
            self.isActiveMobilePunchCard = true;
            self.checkMarkBtn?.setImage(K_RIGHT_YELLOW, for: UIControlState())
        }
    }
    
    // Method will call to set checkmark button image
    @IBAction func loyaltyActiveBtnTapped(_ sender: AnyObject) {
        let btn = sender as? UIButton;
        
        if (isActiveMobilePunchCard == true) {
            btn?.setImage(K_RIGHT_YELLOW_EMPTY, for: UIControlState())
            isActiveMobilePunchCard = false;
        }
        else{
            btn?.setImage(K_RIGHT_YELLOW, for: UIControlState())
            isActiveMobilePunchCard = true;
        }
    }
    
    @IBAction func saveBtnTapped(_ sender: AnyObject) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        if(Int(user.IsActive!)! == 1){
            
            self.descriptionTxtF.resignFirstResponder();
            self.numberOfPunchTxtF.resignFirstResponder();
            let requiredPunches = self.numberOfPunchTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            let loyaltyDesc = self.descriptionTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            let loyaltyID = self.loyaltyID
            
            let previousNum = self.previousPunches;
            
            var message : String? = nil;
            
            if(loyaltyDesc.characters.count <= 0 || loyaltyDesc == kMESSAGE_EXPLAIN_YOUR_PUNCH_CARD_POLICY) {
                message = "Please describe your punch card policies.";
            }
            else if(requiredPunches.characters.count <= 0 || Int(requiredPunches) == 1){
                message = "Please enter required punches for a free bie";
            }
            else if (Int(requiredPunches) > 10 || Int(requiredPunches) < 2)
            {
                message = "Please enter valid punches per freebie count. Make sure entered count is greater than 2 and less than 10";
            }
            else if (Int(previousNum) > 0 && Int(requiredPunches) < Int(previousNum)){
                message = "Sorry, the required number of punches per freebie cannot be reduced.";
            }
            
            if message != nil {
                AppUtility.showAlertWithTitleAndMessage(message, alertMessage: nil, onView: self)
            }
            else
            {
                let deviceUniqueId = AppUtility.getDeviceNSUUID();
                
                var isActivePunches = "0";
                if(self.isActiveMobilePunchCard == true){
                    isActivePunches = "1";
                }
                
                // Created postDataDictonary to post the data on server
                let postDataDict : NSDictionary = ["loyaltyID" : String(loyaltyID),
                                                   "LoyalityType" : "PUNCHCARD",
                                                   "PunchRequiredForFreeBee" : requiredPunches,
                                                   "LoyalityDescription": loyaltyDesc,
                                                   "BusinessUserID" : user.ID!,
                                                   "IsActive" : isActivePunches,
                                                   "api_key" : kWEB_SERVICES_API_KEY,
                                                   "DeviceUniqueID" : deviceUniqueId];
                
                print("postData = %@", postDataDict);
                
               let alertMe = "Do you want to update your Loyalty section? Please note that you will not be able to reduce the number of punches required per freebie once confirmed."
                
                let alert = UIAlertController(title: "Confirm Loyalty Update...!", message: alertMe as String, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                    self.callSaveLoyaltyDataService(postDataDict);
                }))
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil));
                self.present(alert, animated: true, completion: nil)
            }
        }
        else{
            AppUtility.showAlertWithTitleAndMessage(AFTER_DEACTIVATE_BUSINESS_ACCOUNT_MESSAGE, alertMessage: nil, onView: self)
        }
    }
    
    func callSaveLoyaltyDataService(_ dict : NSDictionary){
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            CustomProgressView.showProgressIndicator();

            Alamofire.request(kSERVICE_ADD_LOYALTY, method: HTTPMethod.post, parameters: dict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        self.getLoyaltyInfo(); // Again call service to update data
                    }
                    else
                    {
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                    }
                }
                }
            )
        }
    }
    
    // MARK:- UITextViewDelegates
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder();
        return true;
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.focusedControl = textView;
        
        if(textView.text == kMESSAGE_EXPLAIN_YOUR_PUNCH_CARD_POLICY){
            textView.text = ""
            textView.textColor = UIColor.darkGray;
        }
        return true;
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // Any new character added is passed in as the "text" parameter
        if(text == "\n"){
            textView.resignFirstResponder();
            return false;
        }
        return true;
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
         self.focusedControl = textField;
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.focusedControl = textField;
        return true;
    }

    // MARK:- TextField delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.focusedControl = textField;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    @IBAction func greyCenterContainerViewTouch(_ sender: AnyObject) {
        
        if(self.focusedControl != nil){
            self.focusedControl.resignFirstResponder();
        }
    }
    
}
