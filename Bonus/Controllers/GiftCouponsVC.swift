//
//  GiftCouponsVC.swift
//  Bonus
//
//  Created by Arvind Sen on 17/06/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class GiftCouponsVC: UIViewController, UITextFieldDelegate {

    var objBranchCoupon: BranchCoupon? = nil;
    var popupVCDelegate : DismissPopupVCProtocol? = nil
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var maxCouponsLabel: UILabel!
    @IBOutlet weak var noOfCouponsTxtF: UITextField!
    @IBOutlet weak var userEmailTxF: UITextField!
    var maxCouponsCanGift = "1";
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false;
        
        //self.containerView.backgroundColor = BLUE_BG_COLOR;
        self.containerView.layer.cornerRadius = 7.0;
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.containerView.layer.borderWidth = 1.0;
        self.containerView.clipsToBounds = true;
        self.setViewContent();
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setViewContent(){
        
        if(self.objBranchCoupon != nil){
        
            self.noOfCouponsTxtF.text = "1";
            self.maxCouponsLabel.text = "You can gift maximum coupons : \(self.maxCouponsCanGift)"
        }
    }
    
    
    @IBAction func minusPlusBtnTapped(_ sender: Any) {
        
        let btn = sender as! UIButton;

        let couponsCanGift = Int(self.maxCouponsCanGift)!;
        var numberOfCoupons = Int(self.noOfCouponsTxtF.text!)!;
        
        if (btn.tag == 74) {
            numberOfCoupons = numberOfCoupons + 1;
            if (numberOfCoupons <= couponsCanGift) {
                self.noOfCouponsTxtF.text = ("\(numberOfCoupons)");
            }
            else {
                numberOfCoupons = numberOfCoupons - 1;
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "You have a maximum of \(numberOfCoupons) coupons which can be gift.", onView: self)
            }
        }
        else if (btn.tag == 73) {
            numberOfCoupons = numberOfCoupons - 1;
            if (numberOfCoupons >= 1) {
                self.noOfCouponsTxtF.text = ("\(numberOfCoupons)");
            }
        }
    }

    @IBAction func sendBtnTapped(_ sender: Any) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID;
        let personType = user.PersonType;
        let loggedUserEmailId = user.EmailID;
        let userSenderName = user.FirstName;
        
        let emailId : String = self.userEmailTxF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let noOfcoupons : String = self.noOfCouponsTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        self.userEmailTxF.text = emailId;
        self.noOfCouponsTxtF.text = noOfcoupons;
        
        let isValidEmail = AppUtility.validateEmail(emailId);
        
        var message : String? = nil;
        
        if !isValidEmail {
            message = LOGIN_ALERT_MESSAGE_EMAIL_ID;
        }
        else if (loggedUserEmailId == emailId) {
            message = "Sorry, you can not gift coupon's to yourself";
        }
        
        if message != nil {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
        }
        else {
            let deviceUniqueId = AppUtility.getDeviceNSUUID();
            
            // Created postDataDictonary to post the data on server
            let postDataDict : NSDictionary = ["DealMasterID" : (self.objBranchCoupon?.DealMasterID)!,
                                               "UserID": userId!,
                                               "FirstName" : userSenderName!,
                                               "PersonType": personType!,
                                               "GiftQuantity": noOfcoupons,
                                               "InvitationMailID" : emailId,
                                               "DealReferLinkSendMedia" : "E-Mail",
                                               "DeviceUniqueID": deviceUniqueId,
                                               "api_key": kWEB_SERVICES_API_KEY];
            
            print("postDataDict = %", postDataDict);
            
            if !AppUtility.isNetworkAvailable(){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
            }
            else{
                
                CustomProgressView.showProgressIndicator();
                Alamofire.request(kSERVICE_GIFT_COUPON_TO_FRIEND, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                    
                    CustomProgressView.hideProgressIndicator();
                    
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                    print("value =  \(datastring)");
                    
                    if let JSON = response.result.value
                    {
                        print("JSON: \(JSON)")
                        let dataDict = JSON as? NSDictionary;
                        
                        if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String) == 200) {
                            
                            let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                            
                            let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
                                
                                self.popupVCDelegate?.dismissPopupViewController(self, responseObj: "Success" as AnyObject?);
                                // Removed it from superview controller
                                self.view.removeFromSuperview()
                                self.removeFromParentViewController();
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                            
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                        }
                        else
                        {
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                            
                        }
                    }
                }
                )
            }
        }
    }
    
    @IBAction func cancelBtnTapped(_ sender: Any) {
        self.popupVCDelegate?.dismissPopupViewController(self, responseObj: nil);
        // Removed it from superview controller
        self.view.removeFromSuperview()
        self.removeFromParentViewController();
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}
