//
//  CategroriesLeftMenuTVC.swift
//  Bonus
//
//  Created by Arvind Sen on 30/08/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class CategroriesLeftMenuTVC: UITableViewController {

    var firstTwoSectionIndexPath: IndexPath?
    var categoryListArray: NSArray?
    var previouslySelectedIndexPathArray:NSMutableArray?
    var productGroupArray : NSMutableArray?
    let sectionHeaderHeight: CGFloat = 40;
    var frontDisplayVC: UIViewController? = nil;
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()

        self.tableView.backgroundColor = TAB_BAR_BG_COLOR;
        self.tableView.separatorStyle = .singleLine;
        self.tableView.separatorColor = GRAY_CUSTOM_BAR_AND_SEPRATOR_BG_COLOR;
        //self.tableView.contentInset = UIEdgeInsetsZero;
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier:"reuseIdentifier");
        
        self.categoryListArray = BonusLocalDBHandler.getAllCategories();
        if (self.categoryListArray!.count > 0) {
            self.tableView.reloadData();
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true);
        
        // Setting for display different view controller appearing
        
        let revealController = self.revealViewController();
        let mainTabVC = revealController?.frontViewController;
        if (mainTabVC?.isKind(of: CustomerAndVisitorTBC.self))! {
            
            let tabBarVC = mainTabVC as! CustomerAndVisitorTBC;
            
            let navcont = tabBarVC.viewControllers![tabBarVC.selectedIndex] as! UINavigationController // Loyalty navigationView Controller
            let objVC = navcont.viewControllers.last;
        
            if(self.frontDisplayVC == nil)
            {
                self.frontDisplayVC = objVC;
            }
            else{
                if(objVC == self.frontDisplayVC){
        
                }
                else{
                    // If tapping on another tab and VC
                    self.productGroupArray?.removeAllObjects();
                    self.previouslySelectedIndexPathArray?.removeAllObjects();
                    self.previouslySelectedIndexPathArray = nil;
                    
                    let tblCell1 = self.tableView.cellForRow(at: IndexPath(row: 1, section: 1))
                    let imgView11 = tblCell1?.contentView.viewWithTag(2098) as! UIImageView;
                    imgView11.image = UIImage(named: "")
                    
                    let tblCell0 = self.tableView.cellForRow(at: IndexPath(row: 0, section: 1))
                    let imgView10 = tblCell0?.contentView.viewWithTag(2098) as! UIImageView;
                    imgView10.image = UIImage(named: "")
                    
                    self.tableView.reloadData();
                }
                self.frontDisplayVC = objVC;
            }
        }
    }
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3;
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        var returnItems = 0;
        
        if (section == 0) {
            returnItems = 1;
        }
        else if (section == 1) {
            returnItems = 2;
        }
        else if (section == 2) {
            if (self.categoryListArray!.count > 0) {
                returnItems = self.categoryListArray!.count;
            }
        }
        return returnItems;
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        var height = self.sectionHeaderHeight;
        if (section == 0)
        {
            height = 20.0;
        }
        return height;
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var sectionView: UIView?;
        
        if (section == 0) {
            //sectionView = UIView();
            //sectionView?.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 20);
            //sectionView?.backgroundColor = TAB_BAR_BG_COLOR;
        }
        else if (section == 1 || section == 2){
            
            let btnHeight = self.sectionHeaderHeight;
            let btnWidth = btnHeight;
            
            var sectionTitle = "Sort"
            
            if (section == 2) {
                sectionTitle = "Business Type"
            }
            
            var nX: CGFloat = 5;
            let nY: CGFloat = 0;
            
            let sepWidth = self.tableView.frame.size.width - btnHeight - 2*nX;
            let sepHeight = sectionHeaderHeight;
            
            sectionView = UIView.init(frame: CGRect(x: nX, y: nY, width: sepWidth, height: sepHeight));
            sectionView?.backgroundColor = GRAY_CUSTOM_BAR_AND_SEPRATOR_BG_COLOR
            let lblHeight = sepHeight - 1;
            
            let sectionLabel = UILabel.init(frame: CGRect(x: nX, y: nY, width: sepWidth, height: lblHeight));
            sectionLabel.textAlignment = NSTextAlignment.left;
            sectionLabel.textColor = UIColor.white;
            sectionLabel.text = sectionTitle;
            sectionLabel.backgroundColor = UIColor.clear;
            sectionView?.addSubview(sectionLabel);
            
            if (section == 2)
            {
                // Group by button
                
                let searchImg = UIImage(named:"ArrowForwardBtn")
                
                nX = self.tableView.frame.size.width - btnWidth - 10;
                let btnSize = CGRect(x: nX, y: nY, width: btnWidth, height: btnHeight)
                
                let productGroupBtn = UIButton.init(type: .custom)
                productGroupBtn.frame = btnSize;
                productGroupBtn.backgroundColor = UIColor.clear;
                productGroupBtn.setImage(searchImg, for: UIControlState());
                productGroupBtn.addTarget(self, action: #selector(CategroriesLeftMenuTVC.productGroupButtonClicked(_:)), for: UIControlEvents.touchUpInside)
                sectionView?.addSubview(productGroupBtn);
            }
        }
        return sectionView;
    }
        
    func productGroupButtonClicked(_ sender: AnyObject){
        
        if (self.productGroupArray != nil && self.productGroupArray?.count > 0) {
            let groupIDs = self.productGroupArray?.componentsJoined(by: ",");
            let indexPath = IndexPath(row: 0, section: 2);
            
            
            let revealController = self.revealViewController();
            revealController?.revealToggle(self);
            let mainTabVC = revealController?.frontViewController;
            
            if (mainTabVC?.isKind(of: CustomerAndVisitorTBC.self))! {
                
                let tabBarVC = mainTabVC as! CustomerAndVisitorTBC;
                
                let navcont = tabBarVC.viewControllers![tabBarVC.selectedIndex] as! UINavigationController // Loyalty navigationView Controller
                let objVC = navcont.viewControllers.last;
                
                if (objVC!.isKind(of: LoyaltyTVC.self)) {
                    let objSelected = objVC as! LoyaltyTVC
                    objSelected.selectedIndexPathFromLeftMenus = indexPath;
                    objSelected.callLoyaltyTVCFromLeftMenu(indexPath, categoriesIds: groupIDs!);
                    
                }
                else if (objVC!.isKind(of: CouponsTVC.self)) {
                    let objSelected = objVC as! CouponsTVC
                    objSelected.selectedIndexPathFromLeftMenus = indexPath;
                    objSelected.callCouponsTVCFromLeftMenu(indexPath, categoriesIds: groupIDs!);
                }
                else if (objVC!.isKind(of: LoyaltyMapLocationVC.self)) {
                    let objSelected = objVC as! LoyaltyMapLocationVC
                    objSelected.selectedIndexPathFromLeftMenus = indexPath;
                    objSelected.callLoyaltyTVCFromLeftMenu(indexPath, categoriesIds: groupIDs!);
                }
                else if (objVC!.isKind(of: CouponsMapLocationVC.self)) {
                    let objSelected = objVC as! CouponsMapLocationVC
                    objSelected.selectedIndexPathFromLeftMenus = indexPath;
                    objSelected.callCouponsTVCFromLeftMenu(indexPath, categoriesIds: groupIDs!);
                }
                else if (objVC!.isKind(of: WalletVC.self)) {
                    let objSelected = objVC as! WalletVC
                    objSelected.selectedIndexPathFromLeftMenus = indexPath;
                    objSelected.callCouponsTVCFromLeftMenu(indexPath, categoriesIds: groupIDs!);
                }
                else if (objVC!.isKind(of: CustomerCouponsMapViewVC.self)) {
                    let objSelected = objVC as! CustomerCouponsMapViewVC
                    objSelected.selectedIndexPathFromLeftMenus = indexPath;
                    objSelected.callCouponsTVCFromLeftMenu(indexPath, categoriesIds: groupIDs!);
                }
            }
            else if (mainTabVC?.isKind(of: BusinessAndEmployeeTBC.self))! {
                
                let tabBarVC = mainTabVC as! BusinessAndEmployeeTBC;
                
                let navcont = tabBarVC.viewControllers![tabBarVC.selectedIndex] as! UINavigationController // Loyalty navigationView Controller
                let objVC = navcont.viewControllers.last;
                
                if (objVC!.isKind(of: CouponsTVC.self)) {
                    let objSelected = objVC as! CouponsTVC
                    objSelected.selectedIndexPathFromLeftMenus = indexPath;
                    objSelected.callCouponsTVCFromLeftMenu(indexPath, categoriesIds: groupIDs!);
                }
                else if (objVC!.isKind(of: CouponsMapLocationVC.self)) {
                    let objSelected = objVC as! CouponsMapLocationVC
                    objSelected.selectedIndexPathFromLeftMenus = indexPath;
                    objSelected.callCouponsTVCFromLeftMenu(indexPath, categoriesIds: groupIDs!);
                }
            }
        }
        else{
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Select at least one category", onView: self);
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        cell.selectionStyle = UITableViewCellSelectionStyle.none;
        
        // Configure the cell...
        configure(cell, forRowAtIndexPath: indexPath)

        return cell
    }

    func configure(_ cell: UITableViewCell, forRowAtIndexPath indexPath: IndexPath) {
        
        cell.contentView.backgroundColor = UIColor.clear;
        cell.backgroundColor = UIColor.clear;
        
        let cate = self.categoryListArray![indexPath.row] as! Category;
        cell.textLabel?.font = UIFont.systemFont(ofSize: 15.0);
        cell.textLabel?.textColor = UIColor.white;
        
        let imgView = cell.contentView.viewWithTag(2098) as? UIImageView;
        if(imgView != nil)
        {
            imgView?.removeFromSuperview();
        }
        
        let iconWidth: CGFloat = 25;
        let iconHeight: CGFloat = 25;
        let oX = cell.frame.size.width - iconWidth - 10;
        let oY = (cell.frame.size.height - iconHeight)/2;
        
        let rectFrame = CGRect(x: oX, y: oY, width: iconWidth, height: iconHeight)
        let iconImgView = UIImageView.init(frame: rectFrame);
        iconImgView.backgroundColor = UIColor.clear;
        iconImgView.tag = 2098;
        iconImgView.isHidden = false;
        cell.contentView.addSubview(iconImgView)
        
        if (indexPath.section == 0) {
            cell.textLabel?.text = "Favorites Only";
            iconImgView.image = UIImage(named: "HeartPink");
        }
        else if (indexPath.section == 1) {
            
            iconImgView.image = UIImage(named: "YellowCircleSmall");
            
            if (indexPath.row == 0) {
                cell.textLabel?.text = "Near By";
                if ((self.previouslySelectedIndexPathArray) == nil){
                    iconImgView.isHidden = false;
                }
                else{
                    iconImgView.isHidden = true;
                }
            }
            else if (indexPath.row == 1) {
                cell.textLabel?.text = "A-Z";
                iconImgView.isHidden = true;
            }
        }
        else if (indexPath.section == 2) {
            
            cell.textLabel?.text = cate.CategoryName;
            iconImgView.sd_setImage(with: URL(string: cate.CategoryPicture!), placeholderImage: K_PLACE_HOLDER_IMAGE);
        }
        
        
        if ((self.previouslySelectedIndexPathArray) != nil)
        {
            if (indexPath.section == 0 && self.previouslySelectedIndexPathArray?.index(of: indexPath) != NSNotFound)
            {
                cell.contentView.backgroundColor = UIColor.black;
            }
            else if (indexPath.section == 1 && self.previouslySelectedIndexPathArray?.index(of: indexPath) != NSNotFound){
               
                cell.contentView.backgroundColor = UIColor.clear;
                iconImgView.isHidden = true;
                
                // Setting its value after reload in did select row please check there
            }
            else if(indexPath.section == 2 && self.previouslySelectedIndexPathArray?.index(of: indexPath) != NSNotFound){
                 cell.contentView.backgroundColor = UIColor.black;
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.backgroundColor = UIColor.black
        let cate = self.categoryListArray![indexPath.row] as! Category;
        
        if (self.previouslySelectedIndexPathArray == nil) {
            self.previouslySelectedIndexPathArray = NSMutableArray();
        }
        
        if (indexPath.section == 0 || indexPath.section == 1) {
            
            self.productGroupArray?.removeAllObjects();
            self.previouslySelectedIndexPathArray?.removeAllObjects();
            self.previouslySelectedIndexPathArray?.add(indexPath);
            
            self.tableView.reloadData();
            
            if (indexPath.section == 1 && indexPath.row == 0) {
                // Set section second row 2nd image empty
                let tblCell1 = tableView.cellForRow(at: IndexPath(row: 1, section: 1))
                let imgView11 = tblCell1?.contentView.viewWithTag(2098) as! UIImageView;
                //imgView11.image = UIImage(named: "")
                imgView11.isHidden = true;
                
                // Set section second row 1st image empty
                let tblCell0 = tableView.cellForRow(at: IndexPath(row: 0, section: 1));
                
                let imgView10 = tblCell0?.contentView.viewWithTag(2098) as! UIImageView;
                tblCell0?.contentView.backgroundColor = UIColor.black;
                //imgView10.image = UIImage(named: "YellowCircleSmall")
                imgView10.isHidden = false;
            }
            else if (indexPath.section == 1 && indexPath.row == 1) {
                // Set section second row 2nd image empty
                let tblCell0 = tableView.cellForRow(at: IndexPath(row: 0, section: 1))
                let imgView10 = tblCell0?.contentView.viewWithTag(2098) as! UIImageView;
                imgView10.isHidden = true;
                
                // Set section second row 1st image empty
                let tblCell1 = tableView.cellForRow(at: IndexPath(row: 1, section: 1));
                
                let imgView11 = tblCell1?.contentView.viewWithTag(2098) as! UIImageView;
                tblCell1?.contentView.backgroundColor = UIColor.black;
                //imgView11.image = UIImage(named: "YellowCircleSmall")
                imgView11.isHidden = false;
            }
            
            let revealController = self.revealViewController();
            revealController?.revealToggle(self);
            
            let mainTabVC = revealController?.frontViewController;

            if (mainTabVC?.isKind(of: CustomerAndVisitorTBC.self))! {
                
                let tabBarVC = mainTabVC as! CustomerAndVisitorTBC;
                
                let navcont = tabBarVC.viewControllers![tabBarVC.selectedIndex] as! UINavigationController // Loyalty navigationView Controller
                let objVC = navcont.viewControllers.last;
                
                if (objVC!.isKind(of: LoyaltyTVC.self)) {
                    let objSelected = objVC as! LoyaltyTVC
                    objSelected.selectedIndexPathFromLeftMenus = indexPath;
                    objSelected.callLoyaltyTVCFromLeftMenu(indexPath, categoriesIds: "");
                }
                else if (objVC!.isKind(of: LoyaltyMapLocationVC.self)) {
                    let objSelected = objVC as! LoyaltyMapLocationVC
                    objSelected.selectedIndexPathFromLeftMenus = indexPath;
                    objSelected.callLoyaltyTVCFromLeftMenu(indexPath, categoriesIds: "");
                }
                else if (objVC!.isKind(of: CouponsTVC.self)) {
                    let objSelected = objVC as! CouponsTVC
                    objSelected.selectedIndexPathFromLeftMenus = indexPath;
                    objSelected.callCouponsTVCFromLeftMenu(indexPath, categoriesIds: "");
                }
                else if (objVC!.isKind(of: CouponsMapLocationVC.self)) {
                    let objSelected = objVC as! CouponsMapLocationVC
                    objSelected.selectedIndexPathFromLeftMenus = indexPath;
                    objSelected.callCouponsTVCFromLeftMenu(indexPath, categoriesIds: "");
                }
                else if (objVC!.isKind(of: WalletVC.self)) {
                    let objSelected = objVC as! WalletVC
                    objSelected.selectedIndexPathFromLeftMenus = indexPath;
                    objSelected.callCouponsTVCFromLeftMenu(indexPath, categoriesIds: "");
                }
                else if (objVC!.isKind(of: CustomerCouponsMapViewVC.self)) {
                    let objSelected = objVC as! CustomerCouponsMapViewVC
                    objSelected.selectedIndexPathFromLeftMenus = indexPath;
                    objSelected.callCouponsTVCFromLeftMenu(indexPath, categoriesIds: "");
                }
            }
            else if (mainTabVC?.isKind(of: BusinessAndEmployeeTBC.self))! {
                
                let tabBarVC = mainTabVC as! BusinessAndEmployeeTBC;
                
                let navcont = tabBarVC.viewControllers![tabBarVC.selectedIndex] as! UINavigationController // Loyalty navigationView Controller
                let objVC = navcont.viewControllers.last;
                
                if (objVC!.isKind(of: CouponsTVC.self)) {
                    let objSelected = objVC as! CouponsTVC
                    objSelected.selectedIndexPathFromLeftMenus = indexPath;
                    objSelected.callCouponsTVCFromLeftMenu(indexPath, categoriesIds: "");
                }
                else if (objVC!.isKind(of: CouponsMapLocationVC.self)) {
                    let objSelected = objVC as! CouponsMapLocationVC
                    objSelected.selectedIndexPathFromLeftMenus = indexPath;
                    objSelected.callCouponsTVCFromLeftMenu(indexPath, categoriesIds: "");
                }
            }
        }
        else if (indexPath.section == 2){
            
            if (self.productGroupArray != nil) {
                
                if((self.productGroupArray?.count)! <= 0){
                    self.previouslySelectedIndexPathArray?.removeAllObjects();
                }
                
                if ((self.productGroupArray?.index(of: cate.ID!))! == NSNotFound){
                    self.productGroupArray?.add(cate.ID!);
                    self.previouslySelectedIndexPathArray?.add(indexPath);
                }
                else{
                    
                    self.productGroupArray?.remove(cate.ID!);
                self.previouslySelectedIndexPathArray?.remove(indexPath)
                    //self.tableView.reloadData()
                }
            }
            else{
                self.productGroupArray = NSMutableArray.init(object: cate.ID!);
                self.previouslySelectedIndexPathArray?.removeAllObjects()
                self.previouslySelectedIndexPathArray?.add(indexPath);
            }
            
            self.tableView.reloadData();
        }
        
        /*
        let dict: NSDictionary? = nil;
        var isCall: Bool = false;
        
        if (indexPath.section == 0 && indexPath.row == 0) {
            isCall = true;
        }
        else if (indexPath.section == 1 && indexPath.row == 0){
            isCall = true;
            
            // Set section second row 2nd image empty
            let tblCell1 = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 1))
            let imgView11 = tblCell1?.contentView.viewWithTag(2098) as! UIImageView;
            imgView11.image = UIImage(named: "")
    
            // Set section second row 1st image empty
            let tblCell0 = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 1));
            
            let imgView10 = tblCell0?.contentView.viewWithTag(2098) as! UIImageView;
            imgView10.image = UIImage(named: "YellowCircleSmall")
        }
        else if (indexPath.section == 1 && indexPath.row == 1){
            isCall = true;
            
            // Set section second row 2nd image empty
            let tblCell0 = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 1))
            let imgView10 = tblCell0?.contentView.viewWithTag(2098) as! UIImageView;
            imgView10.image = UIImage(named: "")
            
            // Set section second row 1st image empty
            let tblCell1 = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 1));
            
            let imgView11 = tblCell1?.contentView.viewWithTag(2098) as! UIImageView;
            imgView11.image = UIImage(named: "YellowCircleSmall")
        }
        else if (indexPath.section == 2) {
            
            if (self.productGroupArray != nil) {
                if ((self.productGroupArray?.indexOfObject(cate.ID!))! == NSNotFound){
                    self.productGroupArray?.addObject(cate.ID!);
                  self.previouslySelectedIndexPathArray?.addObject(indexPath);
                }
                else{
                    
                    self.productGroupArray?.removeObject(cate.ID!);
                    self.previouslySelectedIndexPathArray?.removeObject(indexPath)
                    self.tableView.reloadData()
                }
            }
            else{
                self.productGroupArray = NSMutableArray.init(object: cate.ID!);
                self.previouslySelectedIndexPathArray?.removeAllObjects()
                self.previouslySelectedIndexPathArray?.addObject(indexPath);
            }
        }
        
        if (isCall == true) {
            self.firstTwoSectionIndexPath = indexPath;
            self.productGroupArray = nil;
            self.previouslySelectedIndexPathArray?.removeAllObjects();
            self.previouslySelectedIndexPathArray?.addObject(indexPath);
            //[listDelegate tableViewClickedAtIndexPath:indexPath :dict];
            
            let revealController = self.revealViewController();
            revealController.revealToggle(self);
            
            let mainTabVC = revealController.frontViewController;
            
            if mainTabVC.isKindOfClass(CustomerAndVisitorTBC) {
                
                let tabBarVC = mainTabVC as! CustomerAndVisitorTBC;
                let navcont = tabBarVC.viewControllers![3] as! UINavigationController // Loyalty navigationView Controller
                let objVC = navcont.viewControllers.first as! LoyaltyTVC;
                objVC.selectedIndexPathFromLeftMenus = indexPath;
                objVC.callLoyaltyTVCFromLeftMenu(indexPath, categoriesIds: "");
            }
        }
        else{
            
            if ((self.previouslySelectedIndexPathArray?.indexOfObject(self.firstTwoSectionIndexPath!)) != nil) {
                let tbCell = self.tableView.cellForRowAtIndexPath(self.firstTwoSectionIndexPath!)
                tbCell?.contentView.backgroundColor = TAB_BAR_BG_COLOR
            }
        }
 */
    }
    
    // if tableView is set in attribute inspector with selection to multiple Selection it should work.
    // Just set it back in deselect
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        
        if(indexPath.section == 1){
            
        }
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
