//
//  LoyaltyDetailVC.swift
//  Bonus
//
//  Created by Arvind Sen on 14/09/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit
import CoreLocation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



class LoyaltyDetailVC: UIViewController, DismissPopupVCProtocol {

    @IBOutlet weak var businessImage: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var areaLbl: UILabel!
    
    @IBOutlet weak var redeemptionIcon: UIButton!
    
    @IBOutlet weak var mapPinBtn: UIButton!
    
    @IBOutlet weak var distanceLbl: UILabel!
    
    @IBOutlet weak var punchContainerView: UIView!
    @IBOutlet weak var containerViewLayout: NSLayoutConstraint!
    
    @IBOutlet weak var punchesPolicyTxtV: UITextView!
    
    @IBOutlet weak var punchTermsLbl: UILabel!
    
    var loyaltyObj : Loyalty!

    //var businessLoyaltyObj : Loyalty!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Loyalty Detail"
        self.navigationController!.navigationBar.topItem!.title = ""
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white];
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
       self.showLoyaltyViewDetail();
    }

    func showLoyaltyViewDetail(){
        
        let obj = loyaltyObj;
        
        self.businessImage.image = K_PLACE_HOLDER_IMAGE;
        if(obj?.IsImageVerified != nil && Int((obj?.IsImageVerified!)!)! == 1){
            
            if(obj?.BusinessLogoImagePath != nil)
            {
                self.businessImage.sd_setImage(with: URL(string: (obj?.BusinessLogoImagePath!)!), placeholderImage: K_PLACE_HOLDER_IMAGE)
            }
        }
        
        // Setting business name
        if (obj?.FirstName != nil) {
            self.nameLbl.text = obj?.FirstName!;
        }
        else if(obj?.BranchDescription != nil){
            self.nameLbl.text = obj?.BranchDescription!;
        }
        
        // Setting area lbl value
        if(obj?.BranchDescription != nil){
            self.areaLbl.text = obj?.BranchDescription!;
        }
        
        // Value for freebie
        self.redeemptionIcon.setTitle(obj?.NumberOfBalanceFreeBee, for: UIControlState());
        if (Int((obj?.NumberOfBalanceFreeBee!)!)! > 0) {
            self.redeemptionIcon.setBackgroundImage(UIImage(named:"FreeBie"), for: UIControlState());
        }
        
        // Set distance value
        self.distanceLbl.text = String(format: "%.2f KM", Float((obj?.Distance!)!)!);
        
        // Set map/pot/faviourite icons
        if(obj?.BonusPotLocationID != nil && obj?.BonusPotLocationID != "" && Int((obj?.BonusPotLocationID!)!)! > 0){
            self.mapPinBtn.setImage(K_BONUS_POT_IMAGE, for: UIControlState())
        }else if(obj?.FavoriteStatus != nil && Int((obj?.FavoriteStatus!)!)! > 0){
            self.mapPinBtn.setImage(K_FAVIROUTE_IMAGE, for: UIControlState())
        }
        
        self.nameLbl.sizeToFit();

        // Setting punche on punches container view
       
        // Appearance of circles
        let grayCircleImg = UIImage(named: "WhiteCircle")
        let yellowCircleImg = UIImage(named: "WhiteYellowCircle")
        
        let punchWidth : CGFloat = 35.0;
        let punchHeight = punchWidth;
        
        var totalPunches = 0;
        var allotedPunches = 0;
        
        if ((obj?.PunchRequiredForFreeBee) != nil)
        {
            totalPunches = Int((obj?.PunchRequiredForFreeBee!)!)!;
            allotedPunches = Int((obj?.PunchRequiredForFreeBee!)!)! - Int((obj?.CurrentPunchesRequired!)!)!
            
            if (allotedPunches % totalPunches == 0) {
                allotedPunches = 0;
            }
            
            let heights = self.punchContainerView.frame.size.height;
            var punchContainerHeight = heights;
            if (totalPunches <= 5) {
                punchContainerHeight = heights/2;
                self.containerViewLayout.constant = punchContainerHeight;
            }
            
            let differenceBetweenPunches = CGFloat(10.0);
            
            var inARowPunchase = CGFloat(totalPunches);
            if (totalPunches > 5) {
                inARowPunchase = 5;
            }
            
            let punchSpaceCalculate = (punchWidth * inARowPunchase);
            let betweenSpaceCalculate = (inARowPunchase - 1) * differenceBetweenPunches;
            print("width: = \(self.view.frame.width)")
            
            var nX : CGFloat = (self.view.frame.width - (punchSpaceCalculate + betweenSpaceCalculate))/2;
            var nY = CGFloat(9.0);
            let initialOriginX = nX;
            
            for i in 1...totalPunches {
                
                let rect = CGRect(x: nX, y: nY, width: punchWidth, height: punchHeight)
                
                let punchBtn = UIButton.init(type: .custom);
                punchBtn.frame = rect;
                punchBtn.tag = i;
                punchBtn.backgroundColor = UIColor.clear;
                punchBtn.addTarget(self, action: #selector(LoyaltyDetailVC.aPunchButtonTapped(_:)), for: .touchUpInside);
                
                if (i <= allotedPunches) {
                    punchBtn.setImage(yellowCircleImg, for: UIControlState());
                    punchBtn.isUserInteractionEnabled = false;
                }
                else{
                    punchBtn.setImage(grayCircleImg, for: UIControlState());
                }
                self.punchContainerView.addSubview(punchBtn);
                
                punchBtn.tag = i+100
                
                nX += punchWidth + differenceBetweenPunches;
                if (i%5 == 0) {
                    nY += punchHeight + 10;
                    nX = initialOriginX;
                }
            }
        }
        
        self.punchesPolicyTxtV.text = obj?.LoyalityDesciption;
    }
    
    func aPunchButtonTapped(_ sender: AnyObject?) -> Void {
        
        let defaults = UserDefaults.standard
        let userType = defaults.object(forKey: "globalUserType") as! String;
        
        if (userType == USER_TYPE_VISITOR) {
            // No action would be there
        }
        else if(loyaltyObj.IsPunchCardLoyality != nil && Int(loyaltyObj.IsPunchCardLoyality!) == 0)
        {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "For the time being Business suspended this loyalty program, You can redeem your free bie's now." as String, onView: self)
        }
        else {
        
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "PunchesVC") as! PunchesVC
            objVC.popupVCDelegate = self;
            objVC.loyaltyObj = self.loyaltyObj;
            objVC.view.frame = self.view.bounds;
            self.view.addSubview(objVC.view)
            self.addChildViewController(objVC)
            objVC.didMove(toParentViewController: self);
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    // Method will get call when user tap on a map pin on different screens
    @IBAction func placeOnMapBtnTapped(_ sender: AnyObject) {
        
        let objLoyalty = loyaltyObj;
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "PlaceOnMapVC") as! PlaceOnMapVC

        self.addChildViewController(objVC)

        // Initiate values with controller objects
        objVC.clLocationCoordinate = CLLocationCoordinate2DMake(Double((objLoyalty?.Latitude!)!)!, Double((objLoyalty?.Longitude!)!)!);
        objVC.locationTitle = objLoyalty?.FirstName;
        objVC.locationSubTitle = objLoyalty?.BranchDescription
        objVC.dealCategoryImage = objLoyalty?.MapPinCategoryPicture;
        objVC.bonusPotLocationID = objLoyalty?.BonusPotLocationID;
        objVC.branchID = objLoyalty?.BranchID;
        
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        objVC.didMove(toParentViewController: self);
    }
    
    //Function will call to handle response from popup VC
    func respondOnPreviousController(_ obj: AnyObject?)
    {
        let punch = obj as! Punches;
        
        let balanceFreeBie = Int(punch.NumberOfBalanceFreeBee!);
        
        if (balanceFreeBie > 0) {
            self.redeemptionIcon.setBackgroundImage(UIImage(named: "FreeBie"), for: UIControlState());
        }
        else{
             self.redeemptionIcon.setBackgroundImage(UIImage(named: "FreebieGrey"), for: UIControlState());
        }
        
        self.redeemptionIcon.setTitle(punch.NumberOfBalanceFreeBee!, for: UIControlState());
        
        var totalPunches = 0;
        totalPunches = Int(loyaltyObj.PunchRequiredForFreeBee!)!
        let allotedPunches = Int(punch.PunchCount!)!

        // Appearance of circles
        let grayCircleImg = UIImage(named: "WhiteCircle")
        let yellowCircleImg = UIImage(named: "WhiteYellowCircle")

        for i in 1...totalPunches {
        
            let tagValue = i + 100;
            let btn = self.punchContainerView.viewWithTag(tagValue) as! UIButton;
            
            if (i <= allotedPunches) {
                btn.setImage(yellowCircleImg, for: UIControlState());
                btn.isUserInteractionEnabled = false;
            }
            else{
                btn.setImage(grayCircleImg, for: UIControlState());
                btn.isUserInteractionEnabled = true;
            }
        }
    
        self.dismissPopupViewController(self, responseObj: nil); // Dismiss popup view controller
    }
    
    
    func dismissPopupViewController(_ vc: UIViewController, responseObj: AnyObject?) {
         self.dismiss(animated: true, completion: nil);
        
        // If child view return response then only it will excecute
        if (responseObj != nil) {
            
            // If view controller will PunchesVC then excecute it
            if (vc .isKind(of: PunchesVC.self)) {
                let punch = responseObj as! Punches;
                
                let balanceFreeBie = Int(punch.NumberOfBalanceFreeBee!);
                
                if (balanceFreeBie > 0) {
                    self.redeemptionIcon.setBackgroundImage(UIImage(named: "FreeBie"), for: UIControlState());
                }
                else{
                    self.redeemptionIcon.setBackgroundImage(UIImage(named: "FreebieGrey"), for: UIControlState());
                }
                
                self.redeemptionIcon.setTitle(punch.NumberOfBalanceFreeBee!, for: UIControlState());
                
                var totalPunches = 0;
                totalPunches = Int(loyaltyObj.PunchRequiredForFreeBee!)!
                let allotedPunches = Int(punch.PunchCount!)!
                
                // Appearance of circles
                let grayCircleImg = UIImage(named: "WhiteCircle")
                let yellowCircleImg = UIImage(named: "WhiteYellowCircle")
                
                for i in 1...totalPunches {
                    
                    let tagValue = i + 100;
                    let btn = self.punchContainerView.viewWithTag(tagValue) as! UIButton;
                    
                    if (i <= allotedPunches) {
                        btn.setImage(yellowCircleImg, for: UIControlState());
                        btn.isUserInteractionEnabled = false;
                    }
                    else{
                        btn.setImage(grayCircleImg, for: UIControlState());
                        btn.isUserInteractionEnabled = true;
                    }
                }
                
                //self.dismissPopupViewController(self, responseObj: nil); // Dismiss popup view controller
            }
            else  if (vc .isKind(of: AllotFreeBieVC.self)) {
                
                let dict = responseObj as! NSDictionary;
                let lbl = dict.object(forKey: "NumberOfBalanceFreeBee") as! String;
                if (Int(lbl)! > 0) {
                self.redeemptionIcon.setBackgroundImage(UIImage(named:"FreeBie"), for: UIControlState());
                }
                else{
                self.redeemptionIcon.setBackgroundImage(UIImage(named:"FreebieGrey"), for: UIControlState());
                }
                self.redeemptionIcon.setTitle(lbl, for: UIControlState());
            }
        }
    }
    
    
    @IBAction func redeemptionFreeBieBtnTapped(_ sender: AnyObject) {
        
        if(Int(self.redeemptionIcon.currentTitle!)! > 0)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "AllotFreeBieVC") as! AllotFreeBieVC
            objVC.popupVCDelegate = self;
            objVC.loyaltyObj = self.loyaltyObj;
            objVC.numberOfFreeBieToRedeem = self.redeemptionIcon.currentTitle;
            objVC.view.frame = self.view.bounds;
            self.view.addSubview(objVC.view)
            self.addChildViewController(objVC)
            objVC.didMove(toParentViewController: self);
            
//            objVC.modalPresentationStyle = .OverCurrentContext;
//            objVC.modalTransitionStyle = .CrossDissolve;
//            presentViewController(objVC, animated: true) {
//                objVC.businessPin1.becomeFirstResponder();
//                objVC.maxFreeBieLbl.text = "Max Freebie to Redeem : " + self.redeemptionIcon.currentTitle!;
//            }
        }
        else{
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Sorry! you do not have freebie to redeem." as String, onView: self);
        }
    }
    
//    // Dissmiss view controller
//    func dismissPopupViewController(vc: UIViewController, responseObj:) {
//        self.dismissViewControllerAnimated(true, completion: nil);
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
