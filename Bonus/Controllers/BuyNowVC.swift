//
//  BuyNowVC.swift
//  Bonus
//
//  Created by Arvind Sen on 09/06/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class BuyNowVC: UIViewController {

    var objBranchCoupon: BranchCoupon? = nil;
    var popupVCDelegate : DismissPopupVCProtocol? = nil
    var objDealConversionRate: DealConversionRate? = nil;
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var maxCouponsLabel: UILabel!
    @IBOutlet weak var buyNoOfCouponsTxtF: UITextField!
    @IBOutlet weak var couponsUnitCostLabel: UILabel!
    @IBOutlet weak var couponsCostLabel: UILabel!
    @IBOutlet weak var walletAmountLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false;
        // Do any additional setup after loading the view.
        
        //self.containerView.backgroundColor = BLUE_BG_COLOR;
        self.containerView.layer.cornerRadius = 7.0;
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.containerView.layer.borderWidth = 1.0;
        self.containerView.clipsToBounds = true;
        self.setViewContent();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setViewContent(){
        
        if(self.objBranchCoupon != nil){
           
            var maxCouponsBuy = 0;
            
            if(self.objBranchCoupon?.RemainingCoupanToBuy != nil){
                maxCouponsBuy = Int((self.objBranchCoupon?.RemainingCoupanToBuy)!)!
            }
            else{
                maxCouponsBuy = Int((self.objBranchCoupon?.DealPerCustomer)!)!
            }
            
            //let couponsPrice = Float((self.objBranchCoupon?.DealPrice)!);
            //let dealMasterId = self.objBranchCoupon?.DealMasterID;
            //let currencyCode = self.objBranchCoupon?.CurrencyCode;
            //let currencyCode = user.CurrencyCode;
            
            self.maxCouponsLabel.text = "You can buy maximum coupons : " + String(maxCouponsBuy);
            //self.couponsCostLabel.text = "Coupon Unit Cost :" + currencyCode! + " " + String(format: "%.2f", 0.0);
            //self.walletAmountLabel.text = String(format: "%.2f", 0.0);
            
            self.getDealConvertedRate();
        }
    }
    
    func setUpdatedValuesWithConversionRate(){
        
        var walletAmount = 0.0;
        var currencyConversionRate = 0.0;
        var businessPerCouponPrice = 0.0;
        var businessCurrencyCode = "";
        var customerCurrencyCode = "";
        
        businessPerCouponPrice = Double((self.objDealConversionRate?.DealPrice)!)!
        businessCurrencyCode = (self.objDealConversionRate?.BusinessCurrencyCode)!
        customerCurrencyCode = (self.objDealConversionRate?.CustomerCurrencyCode)!
        currencyConversionRate = Double((self.objDealConversionRate?.ConvertedDealPrice)!)!
        walletAmount = Double((self.objDealConversionRate?.CustomerCreditBalance)!)!
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        if (currencyConversionRate > 0.0) {
            
        }
        else{
            customerCurrencyCode = user.CurrencyCode!;
        }

        let walletAmountString = "Your Wallet Amount : " + customerCurrencyCode + "  " + String(format: "%.2f", walletAmount)
        let walletAmountPart = ": " + customerCurrencyCode + "  " + String(format: "%.2f", walletAmount)
        
        let walletPartRange = (walletAmountString as NSString).range(of: walletAmountPart)
        let walletAmountAttributedString = NSMutableAttributedString(string: walletAmountString);
        walletAmountAttributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 12.0), range: walletPartRange)
        self.walletAmountLabel.attributedText = walletAmountAttributedString
        
        
        if (currencyConversionRate > 0.00) {

            let unitCostString = "Coupon Unit Cost : " + businessCurrencyCode + "  " + String(format: "%.2f", businessPerCouponPrice)
            let unitCostPart = ": " + businessCurrencyCode + "  " + String(format: "%.2f", businessPerCouponPrice)
            
            let unitCostPartRange = (unitCostString as NSString).range(of: unitCostPart)
            let unitCostAttributed = NSMutableAttributedString(string: unitCostString);
            unitCostAttributed.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 12.0), range: unitCostPartRange)
            self.couponsUnitCostLabel.attributedText = unitCostAttributed
            

            let totalCostString = "Total Coupons Cost : " + customerCurrencyCode + " " + String(format: "%.2f", 0.0)
            let totalCostPart = ": " + customerCurrencyCode + " " + String(format: "%.2f", 0.0)
            
            let totalCostPartRange = (totalCostString as NSString).range(of: totalCostPart)
            let totalCostAttributed = NSMutableAttributedString(string: totalCostString);
            totalCostAttributed.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 12.0), range: totalCostPartRange)
            self.couponsCostLabel.attributedText = totalCostAttributed
        }
        else{
            
            let unitCostString = "Coupon Unit Cost : " + businessCurrencyCode + "  " + String(format: "%.2f", businessPerCouponPrice)
            let unitCostPart = ": " + businessCurrencyCode + "  " + String(format: "%.2f", businessPerCouponPrice)
            
            let unitCostPartRange = (unitCostString as NSString).range(of: unitCostPart)
            let unitCostAttributed = NSMutableAttributedString(string: unitCostString);
            unitCostAttributed.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 12.0), range: unitCostPartRange)
            self.couponsUnitCostLabel.attributedText = unitCostAttributed
            
        }
        
    }
    
    func getDealConvertedRate(){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let dealMasterId = self.objBranchCoupon?.DealMasterID;
        
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["UserID": (user.ID)!,
                                           "PersonType": (user.PersonType)!,
                                           "BuyQuantity": "1",
                                           "DealMasterID": (self.objBranchCoupon?.DealMasterID)!,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_GET_DEAL_CONVERTED_RATE, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        
                        if(dictArray.count > 0){
                            let item = dictArray[0];
                             self.objDealConversionRate = DealConversionRate(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>)
                            self.setUpdatedValuesWithConversionRate();
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                }
            }
            )
        }
    }
    
    @IBAction func buyNowMinusPlusBtnTapped(_ sender: Any) {
        
        let btn = sender as! UIButton;
        
        var maxCouponsBuy = 0;
        
        if(self.objBranchCoupon?.RemainingCoupanToBuy != nil){
            maxCouponsBuy = Int((self.objBranchCoupon?.RemainingCoupanToBuy)!)!
        }
        else{
            maxCouponsBuy = Int((self.objBranchCoupon?.DealPerCustomer)!)!
        }
        
        var numberCoupons = Int(self.buyNoOfCouponsTxtF.text!)!;
        
        var walletAmount = 0.0;
        var currencyConversionRate = 0.0;
        var businessPerCouponPrice = 0.0;
        var businessCurrencyCode = "";
        var customerCurrencyCode = "";
        
        if(self.objDealConversionRate != nil){
            
            businessPerCouponPrice = Double((self.objDealConversionRate?.DealPrice)!)!
            businessCurrencyCode = (self.objDealConversionRate?.BusinessCurrencyCode)!;
            customerCurrencyCode = (self.objDealConversionRate?.CustomerCurrencyCode)!;
            //currencyConversionRate = self.objDealConversionRate?.ConvertedDealPrice
            currencyConversionRate = Double((self.objDealConversionRate?.ConvertedDealPriceBaseCurrency)!)!;
            walletAmount = Double((self.objDealConversionRate?.CustomerCreditBalance)!)!;
        }
        
        if(btn.tag == 257) // Plus button tapping
        {
            numberCoupons = numberCoupons + 1;
            
            if(numberCoupons <= maxCouponsBuy)
            {
                var newCalculatedBusinessTotalAmount = businessPerCouponPrice * Double(numberCoupons);
                var newCalculatedAmount = currencyConversionRate * Double(numberCoupons);
                var newCalculatedWalletAmount = walletAmount - newCalculatedAmount;
                
                if (newCalculatedWalletAmount >= 0.0)
                {
                    self.buyNoOfCouponsTxtF.text = ("\(numberCoupons)");
                    
                    let totalCostString = "Total Coupons Cost : " + customerCurrencyCode + " " + String(format: "%.2f", newCalculatedAmount)
                    let totalCostPart = ": " + customerCurrencyCode + " " + String(format: "%.2f", newCalculatedAmount)
                    
                    let totalCostPartRange = (totalCostString as NSString).range(of: totalCostPart)
                    let totalCostAttributed = NSMutableAttributedString(string: totalCostString);
                    totalCostAttributed.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 12.0), range: totalCostPartRange)
                    self.couponsCostLabel.attributedText = totalCostAttributed

                    
                    let walletAmountString = "Your Wallet Amount : " + customerCurrencyCode + " " + String(format: "%.2f", newCalculatedWalletAmount)
                    let walletAmountPart = ": " + customerCurrencyCode + " " + String(format: "%.2f", newCalculatedWalletAmount)
                    
                    let walletPartRange = (walletAmountString as NSString).range(of: walletAmountPart)
                    let walletAmountAttributedString = NSMutableAttributedString(string: walletAmountString);
                    walletAmountAttributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 12.0), range: walletPartRange)
                    self.walletAmountLabel.attributedText = walletAmountAttributedString    
                }
                else
                {
                    AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Sorry, you don't have sufficient credit to buy this coupon.", onView: self)
                }
            }
            else{
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: ("You have a maximum of \(maxCouponsBuy) coupons which can be bought."), onView: self)
            }
        }
        else if(btn.tag == 256) // Minus button tapping
        {
            numberCoupons = numberCoupons - 1;
            
            if (numberCoupons >= 0) {
                
                let newCalculatedBusinessTotalAmount = businessPerCouponPrice * Double(numberCoupons);
                let newCalculatedAmount = currencyConversionRate * Double(numberCoupons);
                let newCalculatedWalletAmount = walletAmount - newCalculatedAmount;
                
                self.buyNoOfCouponsTxtF.text = ("\(numberCoupons)");
                
                let totalCostString = "Total Coupons Cost : " + customerCurrencyCode + " " + String(format: "%.2f", newCalculatedAmount)
                let totalCostPart = ": " + customerCurrencyCode + " " + String(format: "%.2f", newCalculatedAmount)
                
                let totalCostPartRange = (totalCostString as NSString).range(of: totalCostPart)
                let totalCostAttributed = NSMutableAttributedString(string: totalCostString);
                totalCostAttributed.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 12.0), range: totalCostPartRange)
                self.couponsCostLabel.attributedText = totalCostAttributed
                
                
                
                let walletAmountString = "Your Wallet Amount : " + customerCurrencyCode + " " + String(format: "%.2f", newCalculatedWalletAmount)
                let walletAmountPart = ": " + customerCurrencyCode + " " + String(format: "%.2f", newCalculatedWalletAmount)
                
                let walletPartRange = (walletAmountString as NSString).range(of: walletAmountPart)
                let walletAmountAttributedString = NSMutableAttributedString(string: walletAmountString);
                walletAmountAttributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 12.0), range: walletPartRange)
                self.walletAmountLabel.attributedText = walletAmountAttributedString
            }
            
        }
    }
   
    @IBAction func buyNowBtnTapped(_ sender: Any) {
        
        if(Int(self.buyNoOfCouponsTxtF.text!)! <= 0){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Please increase number of coupons.", onView: self);
        }
        else
        {
            var maxCouponsBuy = 0;
            if(self.objBranchCoupon?.RemainingCoupanToBuy != nil){
                maxCouponsBuy = Int((self.objBranchCoupon?.RemainingCoupanToBuy)!)!
            }
            else{
                maxCouponsBuy = Int((self.objBranchCoupon?.DealPerCustomer)!)!
            }
            
            var numberCoupons = Int(self.buyNoOfCouponsTxtF.text!)!;
            
            var availableDeals = 0;
            if(self.objBranchCoupon?.AvalDealInNumber != nil){
                availableDeals = Int((self.objBranchCoupon?.AvalDealInNumber)!)!
            }
            else{
                availableDeals = Int((self.objBranchCoupon?.TotalNoOfDeal)!)!
            }
            
            if(numberCoupons <= availableDeals){
                self.buyCouponWebServiceCall(); // Buy coupon service all
            }
            else{
                if (availableDeals > 0) {
                    
                    var couponString = "Coupon";
                    if (availableDeals > 1) {
                        couponString = "Coupons";
                    }
                    let message = "You can buy maximum \(availableDeals) \(couponString)";
                    AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
                }
                else{
                    AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Sorry, no more coupons available to buy", onView: self);
                }
            }
        }
    }
    
    func buyCouponWebServiceCall()
    {
        let numberOfCoupons = self.buyNoOfCouponsTxtF.text!;
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID;
        let userName = user.FirstName;
        let userType = user.PersonType;
        
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["DealMasterID" : (self.objBranchCoupon?.DealMasterID)!,
                                           "UserID": userId!,
                                           "PersonType": userType,
                                           "BuyQuantity": numberOfCoupons,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_BUY_COUPONS, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String) == 200) {
                        
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
                            
                            self.popupVCDelegate?.dismissPopupViewController(self, responseObj: "Success" as AnyObject?);
                            // Removed it from superview controller
                            self.view.removeFromSuperview()
                            self.removeFromParentViewController();
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        
                    }
                }
            }
            )
        }
    }
    
    @IBAction func cancelBtnTapped(_ sender: Any) {
        self.popupVCDelegate?.dismissPopupViewController(self, responseObj: nil);
        // Removed it from superview controller
        self.view.removeFromSuperview()
        self.removeFromParentViewController();
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
