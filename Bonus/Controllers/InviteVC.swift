//
//  InviteVC.swift
//  Bonus
//
//  Created by Arvind Sen on 21/06/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire
import Contacts
//import AddressBook

class InviteVC: UIViewController, UIWebViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var btnMail: UIButton!
    @IBOutlet weak var emailTxtF: UITextField!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    var objInvitationTemplate: InvitationTemplate? = nil
    var contactStore = CNContactStore();
    var contactListArray = NSMutableArray();
    var multiUsers = 0;
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Invite New Members";
        // Do any additional setup after loading the view.
        AppUtility.navigationColorAndItsTextColor(VC: self);
        
        self.showTopBtn();
        self.arrowBtnSetting();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.callInvitationTemplate();
    }
    
    func arrowBtnSetting(){
        
        self.btnMail.layer.cornerRadius = 7.0;
        self.emailTxtF.rightViewMode = .always
        self.emailTxtF.rightView = self.btnMail;
    }
    
    func showTopBtn(){
        
        // Right Top tab bar button configration
        var rightBtn = UIImage(named: "ResendInvitationYellow")
        rightBtn = rightBtn?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let barRightBtn = UIBarButtonItem(image: rightBtn, style: UIBarButtonItemStyle.plain, target: self, action:#selector(self.topRightBtnTapped));
        self.navigationItem.rightBarButtonItem = barRightBtn;
    }
    
    func callInvitationTemplate(){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID!;
        
        var templateFor = "B";
        if(self.segmentControl.selectedSegmentIndex == 1){
           templateFor = "F";
        }
        
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = [
                                           "UserID": userId,
                                           "UserInvitationType": templateFor,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_INVITATION_TEMPLATE, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dict = dataDict?.object(forKey: "Response") as! NSDictionary
                        self.objInvitationTemplate = InvitationTemplate(data: dict as! Dictionary<String, AnyObject>)
                        self.webView.loadHTMLString((self.objInvitationTemplate?.DesignCode)!, baseURL: nil);
                        
                        //getContactsFromDevice
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                        
                    }
                }
            }
            )
        }
    }
    
    func topRightBtnTapped()
    {
        //print(self.childViewControllers.count);
        
        if(self.childViewControllers.count <= 0)
        {
            let defaults: UserDefaults = UserDefaults.standard
            let nsDataUser = defaults.object(forKey: "UserData") as! Data
            let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
            let userId = user.ID!
            let deviceUniqueId = AppUtility.getDeviceNSUUID();
            
            // Created postDataDictonary to post the data on server
            let postDataDict : NSMutableDictionary = ["UserID": userId,
                                                      "DeviceUniqueID": deviceUniqueId,
                                                      "api_key": kWEB_SERVICES_API_KEY];
            
            print("postDataDict = %", postDataDict);
            
            if !AppUtility.isNetworkAvailable(){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
            }
            else{
                
                CustomProgressView.showProgressIndicator();
                Alamofire.request(kSERVICE_GET_INVITES_LIST_NOT_ACCEPTED, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                    
                    CustomProgressView.hideProgressIndicator();
                    
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                    print("value =  \(datastring)");
                    
                    if let JSON = response.result.value
                    {
                        print("JSON: \(JSON)")
                        let dataDict = JSON as? NSDictionary;
                        
                        if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                            
                            let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                            
                            let message = dataDict?.object(forKey: "Message") as! String
                            if(dictArray.count > 0)
                            {
                                var mutArray = [ResendInvitation]();
                                for item in dictArray {
                                    let obj = ResendInvitation(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>)
                                    
                                    mutArray.append(obj);
                                }
                            self.displayResendInvitationList(mutArray);
                                
                            }
                            else{
                                self.navigationController?.view.makeToast(message, duration: 2.0, position: .center)
                            }
                        }
                        else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                            
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                        }
                    }
                }
                )
            }
        }
    }

    func displayResendInvitationList(_ resendUserList: [ResendInvitation]){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "ResendInvitationVC") as! ResendInvitationVC
        objVC.resendInvitationsArray = resendUserList;
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        self.addChildViewController(objVC)
        objVC.didMove(toParentViewController: self);
        objVC.tblView.reloadData();
    }
    
    // MARK: - WebViewDelegate
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if (self.segmentControl.selectedSegmentIndex == 1) {
            
            let alert = UIAlertController(title: "", message: "Friends", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Manual Entry", style: .default, handler: { action in
                self.emailTxtF.text = "";
                self.emailTxtF.becomeFirstResponder();
            }))
            alert.addAction(UIAlertAction(title: "Select From Phonebook", style: .default, handler: { action in
                OperationQueue.main.addOperation {
                    self.getContactsFromDevice();
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil));
            self.present(alert, animated: true, completion: nil)
            
            
           // self.getContactDetailFromContactsList(); // Method get call to get user device contacts details
        }
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked {
            UIApplication.shared.open(request.url!, options: [:], completionHandler: nil);
            return false
        }
        return true
    }
    
    func requestForAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        
        switch authorizationStatus {
        case .authorized:
            completionHandler(true)
            
        case .denied, .notDetermined:
            self.contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                if access {
                    completionHandler(access)
                }
                else {
                    if authorizationStatus == CNAuthorizationStatus.denied {
                        
                        DispatchQueue.main.async(execute: { () -> Void in
                            let message = "\(accessError!.localizedDescription)\n\nPlease allow the app to access your contacts through the Settings."
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                        })
                    }
                }
            })
            
        default:
            completionHandler(false)
        }
    }
    
    func getContactsFromDevice(){
        
        self.requestForAccess { (accessGranted) -> Void in
            if accessGranted {
                
                let keys = [CNContactGivenNameKey, CNContactEmailAddressesKey, CNContactNamePrefixKey, CNContactNameSuffixKey]
                //let containerId = self.contactStore.defaultContainerIdentifier()
                //let predicate = CNContact.predicateForContacts(withIdentifiers: [containerId])
                self.contactListArray.removeAllObjects();
                try? self.contactStore.enumerateContacts(with: CNContactFetchRequest.init(keysToFetch: keys as [CNKeyDescriptor]), usingBlock: { (contact, stop) in

                    var homeEmailAddress: String? = nil
                    for emailAddress in contact.emailAddresses {
                        if (emailAddress.value.length > 0) {
                            homeEmailAddress = emailAddress.value as String
                            break
                        }
                    }
                    
                    if(homeEmailAddress != nil){
                        var dict: Dictionary<String, Any> = Dictionary();
                        dict["name"] = contact.givenName
                        dict["email"] = homeEmailAddress!
                        dict["isSelect"] = "1"
                        self.contactListArray.add(dict)
                    }
                })
        
                
                if(self.contactListArray.count > 0)
                {
                    OperationQueue.main.addOperation {
                        self.showContactList();
                    }
                }
                else{
                    DispatchQueue.main.async {
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "no Address found", onView: self);
                    }
                }
            }
        }
    }
    
    func showContactList() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "FriendContactsVC") as! FriendContactsVC
        objVC.dataArray = self.contactListArray;
        objVC.searchDataArray = self.contactListArray;
        objVC.callBackOnInviteVC = { contactList in
            
            var mutArray = [String]();
            let contacts = contactList
            
            for dict2 in contacts{
                let dict = dict2 as! NSDictionary;
                if(dict.value(forKey:"isSelect") as! String == "1") {
                    mutArray.append(dict.value(forKey: "email") as! String)
                }
            }
            
            self.emailTxtF.text = mutArray.joined(separator: ", ")
        }
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        self.addChildViewController(objVC)
        objVC.didMove(toParentViewController: self);
    }
    
    @IBAction func segmentControlTapped(_ sender: Any) {
        self.emailTxtF.text = "";
        //isInvitationSendCalled = NO;
        self.callInvitationTemplate();
    }
    
    
    @IBAction func emailArrowBtnTapped(_ sender: Any) {
        
        self.emailTxtF.resignFirstResponder();
        
        let emailId = self.emailTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        self.emailTxtF.text = emailId;
        
        let emailsArray = emailId.components(separatedBy: ",");
        self.multiUsers = 0;
        var callServiceFlag = true;
        
        for email in emailsArray {
            let em = email.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            var message : String? = nil;
        
            let isValidEmail = AppUtility.validateEmail(em);
            
            if(!isValidEmail){
                message = LOGIN_ALERT_MESSAGE_EMAIL_ID;
                callServiceFlag = false;
            }
            
            if(message != nil){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
            }
            self.multiUsers = self.multiUsers + 1;
        }
        
        if(callServiceFlag == true){
            
            let defaults: UserDefaults = UserDefaults.standard
            let nsDataUser = defaults.object(forKey: "UserData") as! Data
            let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
            let userId = user.ID;
            
            var templateFor = "B";
            if(self.segmentControl.selectedSegmentIndex == 1){
                templateFor = "F";
            }
            
            let deviceUniqueId = AppUtility.getDeviceNSUUID();
                
                // Created postDataDictonary to post the data on server
            let postDataDict : NSDictionary = ["ID" : "0",
                                                   "UserID": userId!,
                                                   "InvitationMailID" : emailId,
                                                   "IsInvitationAccept": "0",
                                                   "JoiningCode": (self.objInvitationTemplate?.JoiningCode)!,
                                                   "InvitationType" : "Invite",
                                                   "UserInvitationType" : templateFor,
                                                   "DeviceUniqueID": deviceUniqueId,
                                                   "api_key": kWEB_SERVICES_API_KEY];
                
                print("postDataDict = %", postDataDict);
                
                if !AppUtility.isNetworkAvailable(){
                    AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
                }
                else
                {
                    var serviceName = kSERVICE_USER_INVITATIONS;
                    if(multiUsers > 1){
                        serviceName = kSERVICE_MULTI_USERS_INVITATIONS;
                    }
                    
                    CustomProgressView.showProgressIndicator();
                    Alamofire.request(serviceName, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                        
                        CustomProgressView.hideProgressIndicator();
                        
                        let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                        print("value =  \(datastring)");
                        
                        if let JSON = response.result.value
                        {
                            print("JSON: \(JSON)")
                            let dataDict = JSON as? NSDictionary;
                            
                            if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String) == 200) {
                                
                                self.emailTxtF.text = "";
                                
                                if(self.multiUsers > 1){
                                    //let dict = dataDict?.object(forKey: "Response") as! NSDictionary;
                                    self.displayBulkInvitationResponse(dataDict!);
                                }
                                else
                                {
                                    let message = (dataDict?.object(forKey: "Message"))! as! String;
                                    
                                    let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                                    
                                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
                                        
                                        self.callInvitationTemplate();
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                            else
                            {
                                let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                                
                                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                                
                            }
                        }
                    }
                    )
                }
            }
    }
    
    func displayBulkInvitationResponse(_ dict: NSDictionary){
        
        let alreadySendString = dict.value(forKey: "AlreadyRegistered") as! String;
        let newInvitationString = dict.value(forKey: "NewInvitationMessage") as! String;
        let oldInvitationString = dict.value(forKey: "OldInvitationMessage") as! String;
        
        var inviteMutableArray = [Dictionary<String, Any>]()
        
        if(newInvitationString.characters.count > 1){
            let newInvitationArray = newInvitationString.components(separatedBy: ",");
            let headerTitle = "Invitation sent successfully";
            let inviteDict = ["headerTitle" : headerTitle, "emails" : newInvitationArray] as [String : Any];
            inviteMutableArray.append(inviteDict);
        }
        
        if(oldInvitationString.characters.count > 1){
            let oldInvitationArray = oldInvitationString.components(separatedBy: ",");
            let headerTitle = "Invitation already sent";
            let inviteDict = ["headerTitle" : headerTitle, "emails" : oldInvitationArray] as [String : Any];
            inviteMutableArray.append(inviteDict);
        }
        
        if(alreadySendString.characters.count > 1){
            let alreadySendArray = alreadySendString.components(separatedBy: ",");
            let headerTitle = "Already registered";
            let inviteDict = ["headerTitle" : headerTitle, "emails" : alreadySendArray] as [String : Any];
            inviteMutableArray.append(inviteDict);
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "InvitationStatusVC") as! InvitationStatusVC
        
        objVC.dataArray = inviteMutableArray;
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        self.addChildViewController(objVC)
        objVC.didMove(toParentViewController: self);
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
