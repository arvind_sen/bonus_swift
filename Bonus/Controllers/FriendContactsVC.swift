//
//  FriendContactsVC.swift
//  Bonus
//
//  Created by Arvind Sen on 22/06/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit

class FriendContactsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var selectAllBtn: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblView: UITableView!
    
    var dataArray: NSMutableArray!
    var searchDataArray: NSMutableArray!
    var isAllSelected = true;
    var callBackOnInviteVC : ((NSMutableArray)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false;
        // Do any additional setup after loading the view.
        self.containerView.layer.cornerRadius = 5.0;
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.containerView.layer.borderWidth = 1.0;
        self.containerView.clipsToBounds = true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var returnValue = 0;
        if searchDataArray != nil {
            returnValue = searchDataArray!.count;
        }
        return returnValue;
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        //cell.backgroundColor = UIColor.clear
        //cell.contentView.backgroundColor = UIColor.clear
        cell.selectionStyle = .none;
        // Configure the cell...
        self.configureTableViewCell(cell, forRowAtIndexPath: indexPath);
        return cell;
    }
    
    func configureTableViewCell(_ cell: UITableViewCell, forRowAtIndexPath indexPath: IndexPath) {
        
        let nameLbl = cell.contentView.viewWithTag(1541) as? UILabel;
        let emailLbl = cell.contentView.viewWithTag(1542) as? UILabel;
        let btn = cell.contentView.viewWithTag(1543) as? UIButton;
        
        let obj = searchDataArray[indexPath.row] as! NSDictionary;
        nameLbl?.text = obj.value(forKey: "name") as? String;
        emailLbl?.text = obj.value(forKey: "email") as? String;
        
        // Added delete button when swipe on row
        let imgEmpty = UIImage(named: "RightYellowEmpty");
        let imgFilled = UIImage(named: "RightYellow");
        
        if(obj.value(forKey: "isSelect") as! String == "1"){
            btn?.setImage(imgFilled, for: UIControlState.normal);
        }
        else{
            btn?.setImage(imgEmpty, for: UIControlState.normal);
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let cell = tableView.cellForRow(at: indexPath);
        var dict = self.searchDataArray[indexPath.row] as! Dictionary<String, Any>;
        
        if(dict["isSelect"] as? String == "0"){
            //dict.setValue("1", forKey: "isSelect");
            dict["isSelect"] = "1";
        }
        else{
            //dict.setValue("0", forKey: "isSelect");
            dict["isSelect"] = "0";
        }
        
        self.searchDataArray.replaceObject(at: indexPath.row, with: dict);
        //tableView.reloadSections(IndexSet., with: UITableViewRowAnimation.fade);
        tableView.reloadData()
        
        let totalCount = self.searchDataArray.count;
        var counter = 0;
        
        for dicts in self.searchDataArray {
            if((dicts as! NSDictionary).value(forKey: "isSelect") as? String == "0"){
                self.selectAllBtn.setImage(UIImage(named: "RightYellowEmpty"), for: UIControlState.normal)
                break;
            }
            counter = counter + 1;
        }
        
        if (totalCount == counter) {
            self.selectAllBtn.setImage(UIImage(named: "RightYellow"), for: UIControlState.normal)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!;
        cell.accessoryType = UITableViewCellAccessoryType.none;
    }
    
    func searchDataWithEnrty(_ str: String?){
        
        if str != nil { //If data array empty
            
            let servicePredicate = NSPredicate(format: "SELF.name CONTAINS[cd] %@ || SELF.email CONTAINS[cd] %@", str!, str!);
            
            let result = self.dataArray.filtered(using: servicePredicate)
            self.searchDataArray.removeAllObjects()
            self.searchDataArray.addObjects(from: result)
            
        }
        else{
            self.searchDataArray.removeAllObjects()
            self.searchDataArray.addObjects(from: self.dataArray as [AnyObject])
        }
        self.tblView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchDataWithEnrty(searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        self.searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        self.searchDataWithEnrty(nil);
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        self.view.removeFromSuperview()
        self.removeFromParentViewController();
    }

    @IBAction func doneBtnTapped(_ sender: Any) {
        
        callBackOnInviteVC!(self.searchDataArray);
        
        self.view.removeFromSuperview()
        self.removeFromParentViewController();
    }
    
    @IBAction func selectAllBtnTapped(_ sender: Any) {
        
        var indexNumber = 0;
        
        for dict2 in self.searchDataArray{
            
            var dict = dict2 as! Dictionary<String, Any>;
            
            if(self.isAllSelected == false){
                dict["isSelect"] = "0";
            }
            else{
                dict["isSelect"] = "1";
            }
            
            self.searchDataArray.replaceObject(at: indexNumber, with: dict);
            indexNumber = indexNumber + 1;
        }
        
        let imgEmpty = UIImage(named: "RightYellowEmpty");
        let imgFilled = UIImage(named: "RightYellow");
        
        if(self.isAllSelected == false){
            self.isAllSelected = true;
            self.selectAllBtn.setImage(imgEmpty, for: .normal);
        }
        else{
            self.isAllSelected = false;
            self.selectAllBtn.setImage(imgFilled, for: .normal);
        }
        
        self.tblView.reloadData();
    }
}
