//
//  BranchesVC.swift
//  Bonus
//
//  Created by Arvind Sen on 16/11/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import Alamofire

class BranchesVC: UIViewController {
    
    @IBOutlet weak var branchesTableView: UITableView!
    var branchesListArray = [Branch]();
    var isWebServiceCalled = false
    var delegate: DismissSelfViewControllerDelegate? = nil
    var isHavingMainBranch = false;
     var flagToNoDataFound = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.backgroundColor = K_COLOR_TRANSPARNT;
        self.view.isOpaque = false;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
        
        DispatchQueue.main.async {
            self.isHavingMainBranch = false;
            self.getAndDisplayAllBranchesList(0);
        }
    }
    
    // To add a branch function will call
    @IBAction func addABranchBtnTapped(_ sender: AnyObject) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        if(user.PersonType != nil && user.PersonType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE) {
            AppUtility.showAlertWithTitleAndMessage("You are not the authorize user to add branches.", alertMessage: nil, onView: self);
        }
        else{
            if(Int(user.IsActive!)! == 1) { // If business active then only execute
                AppUtility.removedBackTitleAndChangeItsColor(VC: self.parent!, backTitle: "", backTintColor: UIColor.white);
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyboard.instantiateViewController(withIdentifier: "AddEditABranchVC") as! AddEditABranchVC
                objVC.isHavingMainBranch = self.isHavingMainBranch;
                self.navigationController?.pushViewController(objVC, animated: true);
            }
            else{
                AppUtility.showAlertWithTitleAndMessage(AFTER_DEACTIVATE_BUSINESS_ACCOUNT_MESSAGE, alertMessage: nil, onView: self);
            }
        }
    }
    
    
    // Info btn to show information
    @IBAction func infoBtnTapped(_ sender: AnyObject) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "InfoPopupVC") as! InfoPopupVC
       
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        self.addChildViewController(objVC)
        objVC.didMove(toParentViewController: self);
        
        objVC.infoTitleLbl.text = "How to delete a branch?";
        objVC.detailedTxtV.text = "Swipe from left to right on a branch to Delete.";
    }
    
    
    func getAndDisplayAllBranchesList(_ offset: Int)
    {
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        var userId = user.ID;
        
        if(user.PersonType != nil && user.PersonType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE) {
            userId = user.BusinessUserID;
        }
        
        let personType = user.PersonType;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["UserID": userId!,
                                           "PersonType": personType!,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_BRANCH_LIST_OF_A_BUSINESS, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        
                        if(offset == 0){
                            self.branchesListArray.removeAll();
                        }
                        
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let branch = Branch(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>)
                                
                                self.branchesListArray.append(branch);
                            }
                            
                            OperationQueue.main.addOperation {
                                self.flagToNoDataFound = false;
                                self.branchesTableView.reloadData();
                            }
                        }
                        else{
                            //let message = (dataDict?.object(forKey: "Message"))! as! String;
                            //self.navigationController?.view.makeToast("No record found", duration: 2.0, position: .center)
                            OperationQueue.main.addOperation {
                                self.flagToNoDataFound = true;
                                self.branchesTableView.reloadData()
                            }
                        }
                        
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                }
                }
            )
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        var numOfSections: Int = 0
        if (self.branchesListArray.count > 0)
        {
            //tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else if (self.flagToNoDataFound == true)
        {
            var oX = 0.0;
            var oY = 0.0;
            let widthX = tableView.frame.size.width;
            let heightY = tableView.frame.size.height;
            
            let emptyContainer = UIView.init(frame: CGRect(x: oX, y: oY, width: Double(widthX), height: Double(heightY)));
            emptyContainer.backgroundColor = K_EMPTY_TABLE_BG_COLOR;
            ///tblCell.contentView.addSubview(emptyContainer);
            
            let imgWidth = 100.0;
            let imgHeight = 100.0;
            oX = (Double(widthX) - imgWidth)/2;
            oY = (Double(heightY) - imgHeight)/2;
            
            let imageView = UIImageView.init(frame: CGRect(x: oX, y: oY, width: imgWidth, height: imgHeight));
            imageView.backgroundColor = UIColor.clear;
            imageView.contentMode = .scaleAspectFit;
            imageView.image = UIImage.init(named: "alert");
            emptyContainer.addSubview(imageView);
            
            let titleWidth = emptyContainer.frame.size.width;
            let titleHeight = 25.0;
            let OY = imageView.frame.size.height + imageView.frame.origin.y + 10.0;
            
            let titleLbl = UILabel.init(frame: CGRect(x: 0.0, y: Double(OY), width: Double(titleWidth), height: titleHeight));
            titleLbl.textAlignment = .center;
            titleLbl.text = "No branch has been added yet"
            titleLbl.textColor = UIColor.darkGray;
            emptyContainer.addSubview(titleLbl);
            
            tableView.backgroundView  = emptyContainer;
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // #warning Incomplete implementation, return the number of rows
        var returnValue = 0;
        
        if (self.branchesListArray.count > 0) {
            returnValue = self.branchesListArray.count;
        }
        return returnValue;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reusableCell", for: indexPath) as UITableViewCell
        cell.backgroundColor = UIColor.clear
        // Configure the cell...
        self.configureTableViewCell(cell, forRowAtIndexPath: indexPath);
        return cell
    }
    
    // This below method is setting different values for a row
    func configureTableViewCell(_ cell: UITableViewCell, forRowAtIndexPath:IndexPath){
        
        cell.backgroundColor = UIColor.clear;
        let obj = branchesListArray[forRowAtIndexPath.row];
        
        let title = cell.contentView.viewWithTag(10) as? UILabel;
        title?.text = obj.BranchDescription;
        
        let subTitle = cell.contentView.viewWithTag(11) as? UILabel;
        subTitle?.text = obj.AreaName;
        
        let hoLabel = cell.contentView.viewWithTag(400) as? UILabel;
        hoLabel?.isHidden = true;
        
        
        if((obj.IsMainBranch)! == 1){
            hoLabel?.isHidden = false
            hoLabel?.layer.borderColor = UIColor.white.cgColor;
            cell.backgroundColor = UIColor.init(colorLiteralRed: 225/255, green: 227/255, blue: 229/255, alpha: 1);
            self.isHavingMainBranch = true;
        }
        
        let deleteBtnWidth: CGFloat = 24.0;
        let deleteBtnHeight = deleteBtnWidth;
        let deleteBtnoX = cell.frame.size.width - deleteBtnWidth - 7;
        let deleteBtnoY = (cell.frame.size.height - deleteBtnHeight)/2;
        
        let deleteBtn = UIButton(type: UIButtonType.custom)
        deleteBtn.frame = CGRect(x: deleteBtnoX, y: deleteBtnoY, width: deleteBtnWidth, height: deleteBtnHeight);
        deleteBtn.setImage(UIImage(named: "CircleCloseDelete"), for: UIControlState());
        deleteBtn.addTarget(self, action: #selector(BranchesVC.deleteBranchBtnTapped(_:)), for: UIControlEvents.touchUpInside);
        deleteBtn.alpha = 0;
        deleteBtn.tag = 2986;
        cell.addSubview(deleteBtn);
        
        // Added gestures for swiping
        /**********************************************/
        // Added swipe gesture for left swipe
        
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(BranchesVC.handleSwipeGesture(_:)));
        swipeGesture.direction = UISwipeGestureRecognizerDirection.left;
        cell.addGestureRecognizer(swipeGesture)
        
        let swipeGestureRight = UISwipeGestureRecognizer(target: self, action: #selector(BranchesVC.handleSwipeGesture(_:)));
        swipeGestureRight.direction = UISwipeGestureRecognizerDirection.right;
        cell.addGestureRecognizer(swipeGestureRight)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        
        let obj = self.branchesListArray[indexPath.row];
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        var flagToEdit = false;
        if (user.PersonType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE) {
            if(obj.BranchEmployeeUserID == user.ID){
                flagToEdit = true;
            }
            else{
                AppUtility.showAlertWithTitleAndMessage("Sorry! you can not access this branch detail.", alertMessage: nil, onView: self);
            }
        }
        else{
            flagToEdit = true;
        }
       
        if(flagToEdit == true){
            
            AppUtility.removedBackTitleAndChangeItsColor(VC: self.parent!, backTitle: "", backTintColor: UIColor.white);
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "AddEditABranchVC") as! AddEditABranchVC
            objVC.branchObjToEdit = obj;
            objVC.isHavingMainBranch = self.isHavingMainBranch;
            self.navigationController?.pushViewController(objVC, animated: true);
        }
    }
    
    func deleteBranchBtnTapped(_ sender: UIButton){
        
        let points:CGPoint = sender.convert(sender.bounds.origin, to: self.branchesTableView)
        let indexPath = self.branchesTableView.indexPathForRow(at: points);
        let branchToDelete = self.branchesListArray[(indexPath?.row)!];
        
        if (Int(branchToDelete.CouponAvailable!)! == 1) {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Branch can not be deleted, you still have some coupons active.", onView: self);
        }
        else{
            let alert = UIAlertController(title: nil, message: "Are you really want to delete this branch?" as String, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                self.deleteABranch(branchToDelete.BranchID!);
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil));
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // Call to delete a brach detail from the database
    func deleteABranch(_ branchId : String){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["ID":branchId,
                                           "UserID": userId!,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            //CustomProgressView.showProgressIndicator();
             Alamofire.request(kSERVICE_DELETE_BRANCH_FOR_A_BUSINESS, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                //CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let message = dataDict?.object(forKey: "Message") as! String
                        
                        let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                            self.isHavingMainBranch = false;
                            self.getAndDisplayAllBranchesList(0);
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                }
            )
        }
    }
    
    func handleSwipeGesture(_ gesture: UISwipeGestureRecognizer){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        if(user.PersonType != nil && user.PersonType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE) {
            
        }
        else{
            let swipeLocation:CGPoint = gesture.location(in: self.branchesTableView);
            let swipedIndexPath = self.branchesTableView.indexPathForRow(at: swipeLocation);
            let cell = self.branchesTableView.cellForRow(at: swipedIndexPath!);
            let btn = cell?.viewWithTag(2986);
            
            UIView.animate(withDuration: 0.2, animations: {
                if (gesture.direction == UISwipeGestureRecognizerDirection.right) {
                    btn?.alpha = 1;
                }
                else{
                    btn?.alpha = 0;
                }
            })
        }
    }
    
    func handleSwipeGestureRight(_ gesture: UISwipeGestureRecognizer){
        
    }
//    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
//        let cell : UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!;
//        cell.accessoryType = UITableViewCellAccessoryType.None;
//    }
    
    
    // If user scroll the view
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating ")
        
        if((scrollView.contentSize.height + 20.0) < (scrollView.contentOffset.y + scrollView.frame.size.height)){
            
            let count = self.branchesListArray.count;
            //if(count % 10 == 0){
            //    self.getAndDisplayAllBranchesList(count);
            //}
        }
    }
    
    /*
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y + scrollView.frame.size.height
        let height = scrollView.contentSize.height
        if(offset >= height)
        {
            if(!isWebServiceCalled)
            {
                self.isWebServiceCalled = true;
                let count = self.branchesListArray?.count;
                if(count != nil && count! % 10 == 0){
                    self.getAndDisplayAllBranchesList(count!);
            }
        }
    }
    */
    @IBAction func addBranchBtnTapped(_ sender: AnyObject) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        if(user.IsActive != nil && Int(user.IsActive!)! == 1) {
            if (user.BusinessCategory != nil) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vcObject = storyboard.instantiateViewController(withIdentifier: "AddEditABranchVC") as! AddEditABranchVC
                self.navigationController?.pushViewController(vcObject, animated: true);
            }
            else{
                let alert = UIAlertController(title: nil, message: "Please update mandatory account information.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    
                    self.delegate?.dismissSelfViewControllerFromSuperViewController(self, responseObj: nil);
                    
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
        else {
               AppUtility.showAlertWithTitleAndMessage("Sorry! you can not perform this action, because your account has been deactivated", alertMessage: nil, onView: self)
        }
    }
    
    
}
