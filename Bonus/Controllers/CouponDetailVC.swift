//
//  CouponDetailVC.swift
//  Bonus
//
//  Created by Arvind Sen on 13/05/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class CouponDetailVC: UIViewController, CLLocationManagerDelegate, DismissPopupVCProtocol {

    var objBranchCoupon: BranchCoupon? = nil;
    var objMyCoupon: MyCoupon? = nil;
    var dealMasterId : String? = nil;

    var isFavorite = false;
    var isLoyaltyActive = false;
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewContainer: UIView!
    @IBOutlet weak var dealImgView: UIImageView!
    @IBOutlet weak var imageVarificationLbl: MarqueeLabel!
    @IBOutlet weak var favoriteBtn: UIButton!
    
    @IBOutlet weak var businessLogoImgView: UIImageView!
    @IBOutlet weak var businessNameLbl: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var businessBranchAddressLbl: UILabel!
    @IBOutlet weak var loyalityBtn: UIButton!
    @IBOutlet weak var starContainerView: UIView!
    @IBOutlet weak var businessBranchNumberLbl: UIButton!
    @IBOutlet weak var businessBranchDistanceLbl: UILabel!
    @IBOutlet weak var mapPinViewBtn: UIButton!
    
    @IBOutlet weak var businessDescriptionLbl: UILabel!
    
    @IBOutlet weak var dealTypeLbl: UILabel!
    @IBOutlet weak var dealDescriptionLbl: UILabel!
    @IBOutlet weak var redeemedBeforeLbl: UILabel!
    @IBOutlet weak var totalIssuedLbl: UILabel!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var giftBtn: UIButton!
    
    @IBOutlet weak var couponsCountLbl: UILabel!
    @IBOutlet weak var couponsCanBoughtLbl: UILabel!
    @IBOutlet weak var couponsBoughtRemainingDaysLbl: UILabel!
    @IBOutlet weak var addCardBtn: UIButton!
    
    @IBOutlet weak var redeemedCouponsCountLbl: UILabel!
    @IBOutlet weak var redeemedCouponToBuyLbl: UILabel!
    @IBOutlet weak var redeemedRemainingDaysLbl: UILabel!
    @IBOutlet weak var barCodeBtn: UIButton!
    
    var locationManager : CLLocationManager?
    var userCurrentLocationCoordinate : CLLocationCoordinate2D? ;
    
    @IBOutlet weak var darkGrayYellowView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        AppUtility.navigationColorAndItsTextColor(VC: self);
        self.couponsCountLbl.layer.borderColor = UIColor.white.cgColor;
        self.redeemedCouponsCountLbl.layer.borderColor = UIColor.white.cgColor;
        
        //self.showADealDetail();
        
        if(self.objMyCoupon != nil){
            self.showTopBtns();
        }
        //self.callLocationManager();
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
        self.navigationController?.isNavigationBarHidden = false;
        self.callLocationManager();
        /*
        if(self.objBranchCoupon?.BranchDescription != nil){
            self.navigationItem.title = self.objBranchCoupon?.BranchDescription;
        }
        else if (self.objBranchCoupon?.FirstName != nil) {
            self.navigationItem.title = self.objBranchCoupon?.FirstName;
        }
        */
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.scrollView.contentSize = CGSize(width: Double(self.view.frame.size.width), height: 710.0);
    }

    func showTopBtns(){
        
        // Right Top tab bar button configration
        var deleteBtnImg = UIImage(named: "DeleteWhite")
        deleteBtnImg = deleteBtnImg?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        var editBtnImg = UIImage(named: "EditPencilWhite")
        editBtnImg = editBtnImg?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        let btnContainersView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 70.0, height: 30.0))
        btnContainersView.backgroundColor = UIColor.clear;
        
        let deleteBtn = UIButton(frame: CGRect(x: 5.0, y: 0.0, width: 30.0, height: 30.0));
        deleteBtn.setImage(deleteBtnImg, for: UIControlState.normal);
        deleteBtn.addTarget(self, action: #selector(self.deleteBusinessDeal), for: UIControlEvents.touchUpInside);
        btnContainersView.addSubview(deleteBtn);
        
        let editBtn = UIButton(frame: CGRect(x: 40.0, y: 0.0, width: 30.0, height: 30.0));
        editBtn.setImage(editBtnImg, for: UIControlState.normal);
        editBtn.addTarget(self, action: #selector(self.editBusinessDeal), for: UIControlEvents.touchUpInside);
        btnContainersView.addSubview(editBtn);
        
        let btnV = UIBarButtonItem(customView: btnContainersView)
        self.navigationItem.rightBarButtonItem = btnV;
        
        let backArrowImg = UIImage(named: "ArrowBackBtn")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
       
        let btnContainersView1 = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 220.0, height: 30.0))
        btnContainersView1.backgroundColor = UIColor.clear;
        
        let backBtn = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: 220.0, height: 30.0));
        backBtn.setImage(backArrowImg, for: UIControlState.normal);
        backBtn.setTitle("Coupon Management", for: .normal)
        backBtn.contentHorizontalAlignment = .left;
        backBtn.addTarget(self, action: #selector(self.couponBackBtnTapped), for: UIControlEvents.touchUpInside);
        btnContainersView1.addSubview(backBtn);
        backBtn.backgroundColor = UIColor.clear;
        
        let btnV1 = UIBarButtonItem(customView: btnContainersView1)
        self.navigationItem.leftBarButtonItem = btnV1;
    }
    
    func couponBackBtnTapped(){
        self.navigationController?.popViewController(animated: true);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func deleteBusinessDeal(){
        
        if(self.objMyCoupon != nil){
            
            if(Int((self.objMyCoupon?.TotalActiveCoupan!)!)! > 0){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Coupon cannot be deleted as you still have some coupons active.", onView: self);
            }
            else{
                let alert = UIAlertController(title: nil, message: "Are you really want to delete this coupon?" as String, preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                    self.deleteABusinessDeal();
                }))
                
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil));
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func editBusinessDeal(){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        if(user.IsActive != nil && user.IsActive! == "1")
        {
            if(Int((self.objMyCoupon?.BuyExpired)!)! == 1)
            {
                AppUtility.showAlertWithTitleAndMessage("Sorry! you can not edit expired coupons.", alertMessage: nil, onView: self);
            }
            else
            {
                AppUtility.removedBackTitleAndChangeItsColor(VC: self, backTitle: "", backTintColor: UIColor.white);
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyboard.instantiateViewController(withIdentifier: "AddEditCouponVC") as! AddEditCouponVC
                objVC.myCouponToEdit = self.objMyCoupon;
                self.navigationController?.pushViewController(objVC, animated: true);
            }
        }
        else
        {
        AppUtility.showAlertWithTitleAndMessage(AFTER_DEACTIVATE_BUSINESS_ACCOUNT_MESSAGE, alertMessage: nil, onView: self);
        }
    }
    
    func deleteABusinessDeal(){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["ID": (self.objMyCoupon?.DealMasterID)!,
                                           "UserID": userId!,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicatorOnView(VC: self);
            Alamofire.request(kSERVICE_DELETE_A_COUPON_DETAIL, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicatorOnView(VC: self);
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let message = dataDict?.object(forKey: "Message") as! String
                        
                        let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                            self.navigationController?.popViewController(animated: true);
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    
                }
            }
            )
        }
        /*
         NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
         [dataDict setObject:idToDelete forKey:ID_KEY];
         [dataDict setObject:userId forKey:USER_ID_KEY];
         [dataDict setObject:kWEB_SERVICES_API_KEY forKey:kAPI_KEY];
         [dataDict setObject:[AppUtility getDeviceNSUUID] forKey:DEVICE_UNIQUE_ID];
         [self setDataModelWithData:dataDict andServiceName:kSERVICE_DELETE_A_COUPON_DETAIL];
         */
    }
    
    // Method get call to get user current location lat, long
    func callLocationManager() {
        
        if (CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied) {
            
            self.locationManager = CLLocationManager.init();// locationManager need to define at the class variable then only its call delegate methods.
            self.locationManager!.delegate = self;
            
            if self.locationManager!.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
                self.locationManager!.requestWhenInUseAuthorization();
            }
            self.locationManager!.desiredAccuracy = kCLLocationAccuracyBest;
            self.locationManager!.startUpdatingLocation();
        }
        else{
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "To re-enable, please go to Settings and turn on Location Service for this app.", onView: self)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.locationManager!.stopUpdatingLocation();
        let recentLocation = locations.last;
        self.userCurrentLocationCoordinate = recentLocation?.coordinate;
        
        let latString = String(format: "%f", (recentLocation?.coordinate.latitude)!);
        let longString = String(format: "%f", (recentLocation?.coordinate.longitude)!);
        
        let dict = NSDictionary.init(objects: [latString, longString], forKeys: ["Latitude" as NSCopying, "Longitude" as NSCopying]);
        
        //self.callActiveBusinessWebService(SelectionFor.isSelectedNearBy, withParameters: dict, offSetValue: "0");
        
        if(self.isLoyaltyActive == true){
            self.isLoyaltyActive = false;
            self.getUserPunchCardDetailWithDict(dict)
        }
        else{
            self.callADealSpecificDetail(dict)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(" failed to get current location");
        
        self.locationManager!.stopUpdatingLocation();
        
        let dict = NSDictionary.init(objects: ["0.0", "0.0"], forKeys: ["Latitude" as NSCopying, "Longitude" as NSCopying]);
        
        if(isLoyaltyActive == true){
            self.getUserPunchCardDetailWithDict(dict)
        }
        else{
            self.callADealSpecificDetail(dict)
        }
    }
    
    func moveOnLoyaltyDetailScreen(loyalty: Loyalty){

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "LoyaltyDetailVC") as! LoyaltyDetailVC
        objVC.loyaltyObj = loyalty;
        self.navigationController?.pushViewController(objVC, animated: true);
    }
    
    func getUserPunchCardDetailWithDict(_ dict : NSDictionary){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID;
        let personType = user.PersonType;
        
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["DealId" : (self.objBranchCoupon?.DealMasterID)!,
                                           "BranchID": (self.objBranchCoupon?.BranchID)!,
                                           "UserID": userId!,
                                           "PersonType": personType!,
                                           "Latitude": dict.value(forKey: "Latitude") as! String,
                                           "Longitude": dict.value(forKey: "Longitude") as! String,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicatorOnView(VC: self);
            Alamofire.request(kSERVICE_SELECT_USER_PUNCH_CARD_DETAILS, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicatorOnView(VC: self);
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        
                        //self.loyaltyListArray = NSMutableArray();
                        if(dictArray.count > 0)
                        {
                            let item = dictArray[0]
                            let objLoyalty = Loyalty(data: (item as! Dictionary<String, AnyObject>))
                            self.moveOnLoyaltyDetailScreen(loyalty: objLoyalty);
                        }
                        else
                        {
                            let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        }
                        //let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        //let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        //AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        
                    }
                }
            }
            )
        }
    }
    
    func callADealSpecificDetail(_ dict : NSDictionary?) {
        
        var userId = ""
        var userType = "";
        let defaults: UserDefaults = UserDefaults.standard
        let globalUserType = defaults.object(forKey: "globalUserType") as! String;
        
        if(globalUserType == USER_TYPE_VISITOR){
            userId = "0";
            userType = USER_TYPE_VISITOR;
        }
        else{
            
            let nsDataUser = defaults.object(forKey: "UserData") as! Data
            let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
            
            userId = user.ID!
            userType = user.PersonType!;
        }
        
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        var latitude = "0";
        var longitude = "0";
        if(dict != nil){
            latitude = ("\((dict?["Latitude"])!)");
            longitude = ("\((dict?["Longitude"])!)");
        }
        
        var dealId = "";
        var branchId = "0";
        if(self.objMyCoupon != nil){
            dealId = (self.objMyCoupon?.DealMasterID!)!
        }
        else
        {
            //dealId = (self.objBranchCoupon?.DealMasterID!)!
            if(self.self.objBranchCoupon != nil && self.objBranchCoupon?.BranchID != nil){
                branchId = (self.objBranchCoupon?.BranchID)!;
            }
            dealId = self.dealMasterId!
        }
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSMutableDictionary = ["UserID": userId,
                                                  "PersonType": userType,
                                                  "BranchID": branchId,
                                                  "DealId": dealId,
                                                  "Latitude": latitude,
                                                  "Longitude": longitude,
                                                  "BusinessCategory": "0",
                                                  "Limit": "1",
                                                  "Offset": "0",
                                                  "OrderBy": "ASC",
                                                  "SortBy": "Distance",
                                                  "DeviceUniqueID": deviceUniqueId,
                                                  "api_key": kWEB_SERVICES_API_KEY];

        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicatorOnView(VC: self);
            Alamofire.request(kSERVICE_BRANCH_DEALS, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicatorOnView(VC: self);
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        let resourcePath = dataDict?.object(forKey: "ResourcePath") as! Dictionary<String, AnyObject>
                        
                        if(dictArray.count > 0)
                        {
                            self.objBranchCoupon = BranchCoupon(data: (dictArray[0] as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>, resourcePaths: resourcePath)
                            
                            self.showADealDetail()
                        }
                        else
                        {
                            if(self.objMyCoupon != nil)
                            { // This condition will excecute only if business coupon expired and business tring to view it.
                                self.updatedObjectBranchCouponWithMyCoupon()
                            }
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                }
            }
            )
        }
    }
    
    func updatedObjectBranchCouponWithMyCoupon(){
        
        self.objBranchCoupon = BranchCoupon();
        
        self.objBranchCoupon?.AboutUs = self.objMyCoupon?.AboutUs
        self.objBranchCoupon?.Address = self.objMyCoupon?.Address
        //self.AreaName = self.objMyCoupon.AreaName
        self.objBranchCoupon?.AvalDealInNumber = self.objMyCoupon?.AvalDealInNumber
        //self.BonusPotCurrencyCode = self.objMyCoupon?.
        //self.BonusPotLocationID = self.objMyCoupon.Bonu
        //self.BonusPotMaturityAmount = self.objMyCoupon.Bonus
        //self.BonusPotName = self.objMyCoupon.Bonu
        
        self.objBranchCoupon?.BranchDescription = self.objMyCoupon?.BranchDescription;
        self.objBranchCoupon?.BranchID = self.objMyCoupon?.BranchID;
        self.objBranchCoupon?.BranchSpecific = self.objMyCoupon?.BranchSpecific;
        
        self.objBranchCoupon?.BusinessCategory = self.objMyCoupon?.BusinessCategory
        self.objBranchCoupon?.BusinessLogoImagePath = self.objMyCoupon?.BusinessLogoImagePath
        //self.objBranchCoupon?.PromotionEndDate
        //self.objBranchCoupon?.BuyEndDateExpire = self.objMyCoupon?.BuyExpired
        
        self.objBranchCoupon?.CategoryName = self.objMyCoupon?.CategoryName
        self.objBranchCoupon?.CategoryPicture = self.objMyCoupon?.CategoryPicture
        //self.CurrencyCode = self.objMyCoupon.CurrencyCode
        //self.CurrentAmount = self.objMyCoupon.Curre
        
        self.objBranchCoupon?.DealImage1 = self.objMyCoupon?.DealImage1
        self.objBranchCoupon?.DealMasterID = self.objMyCoupon?.DealMasterID
        self.objBranchCoupon?.DealPerCustomer = self.objMyCoupon?.DealPerCustomer
        self.objBranchCoupon?.DealPrice = self.objMyCoupon?.DealPrice
        self.objBranchCoupon?.Description = self.objMyCoupon?.Description
        
        self.objBranchCoupon?.Distance = "0.0"
        self.objBranchCoupon?.EndDate = self.objMyCoupon?.EndDate
        //self.FavoriteID = self.objMyCoupon.fav
        self.objBranchCoupon?.FavoriteStatus = "0";
        
        self.objBranchCoupon?.FilledCategoryPicture = self.objMyCoupon?.FilledCategoryPicture
        self.objBranchCoupon?.FirstName = self.objMyCoupon?.FirstName
        self.objBranchCoupon?.IsBusinessImageVerified = self.objMyCoupon?.IsBusinessImageVerified;
        self.objBranchCoupon?.IsDealImageVerified = self.objMyCoupon?.IsDealImageVerified
        
        //let punchCardLoyalty = self.objMyCoupon.pun
        self.objBranchCoupon?.IsPunchCardLoyality = "0"
        
        self.objBranchCoupon?.Latitude = self.objMyCoupon?.Latitude;
        self.objBranchCoupon?.Longitude = self.objMyCoupon?.Longitude;
        self.objBranchCoupon?.LoyaltyID = "0"
        
        self.objBranchCoupon?.MapPinCategoryPicture = self.objMyCoupon?.MapPinCategoryPicture;
        
        self.objBranchCoupon?.MaxRating = self.objMyCoupon?.Maxrating
        self.objBranchCoupon?.NoOfActiveCoupan = "0"
        //self.PersonType = self.objMyCoupon.
        self.objBranchCoupon?.PhoneNumber = self.objMyCoupon?.PhoneNumber;
        self.objBranchCoupon?.PromotionEndDate = self.objMyCoupon?.PromotionEndDate
        
        self.objBranchCoupon?.PromotionStartDate = self.objMyCoupon?.PromotionStartDate
        self.objBranchCoupon?.PunchRequiredForFreeBee = "0"
        self.objBranchCoupon?.RemainingCoupanToBuy = "0"
        self.objBranchCoupon?.StartDate = self.objMyCoupon?.StartDate;
        
        self.objBranchCoupon?.TotalNoOfDeal = self.objMyCoupon?.TotalNoOfDeal
        self.objBranchCoupon?.TotalReview = self.objMyCoupon?.TotalReview
        self.objBranchCoupon?.TypeOfCoupon = self.objMyCoupon?.TypeOfCoupon
        self.objBranchCoupon?.UserID = self.objMyCoupon?.UserID
        
        self.objBranchCoupon?.buyCoupanQuantity = "0"
        self.objBranchCoupon?.BonusPotLocationID = "0"
        //self.customerUserID = self.objMyCoupon?.custo
        //self.giftCoupanQuantity = self.objMyCoupon?.gift
        //self.giftReceivedCoupanQuantity = self.objMyCoupon?.rec
        
        //self.redeemedCoupanQuantity = self.objMyCoupon?.red
        //self.shareCoupanQuantity = self.objMyCoupon?.share
        //self.shareReceivedCoupanQuantity = data["shareReceivedCoupanQuantity"] as? String
        
//        if (data["BonusPotLocationID"] != nil){
//            self.BonusPotLocationID = data["BonusPotLocationID"] as? String;
//        }
        
        // Creating image full path her
        self.objBranchCoupon?.CategoryPicture = self.objMyCoupon?.CategoryPicture;
        self.objBranchCoupon?.MapPinCategoryPicture = self.objMyCoupon?.MapPinCategoryPicture
        self.objBranchCoupon?.FilledCategoryPicture = self.objMyCoupon?.FilledCategoryPicture;
        self.objBranchCoupon?.BusinessLogoImagePath = self.objMyCoupon?.BusinessLogoImagePath;
        self.objBranchCoupon?.DealImage1 = self.objMyCoupon?.DealImage1;
        
        self.showADealDetail() // Now update screen data
        
    }

    func showADealDetail() {
        
        if(self.objBranchCoupon != nil){
            
            let obj = self.objBranchCoupon;
            
            let defaults: UserDefaults = UserDefaults.standard
            let userType = defaults.value(forKey: "globalUserType") as! String;
            
            if(self.objMyCoupon != nil){
                
            }
            else{
                if(obj?.BranchDescription != nil){
                    self.title = obj?.BranchDescription;
                }
                else if (obj?.FirstName != nil) {
                    self.title = obj?.FirstName;
                }
            }
            
            if(obj?.IsDealImageVerified != nil && Int((obj?.IsDealImageVerified)!)! == 1 && obj?.DealImage1 != nil){
                self.dealImgView.sd_setImage(with: URL(string: (obj?.DealImage1)!), placeholderImage: K_DEAL_PLACE_HOLDER_IMAGE)
            }else{
                dealImgView.image = K_DEAL_PLACE_HOLDER_IMAGE;
                self.imageVarificationLbl.isHidden = false;
                
                self.imageVarificationLbl.text = "Image verification pending";
                if(obj?.IsDealImageVerified != nil && Int((obj?.IsDealImageVerified!)!)! == 2){
                    self.imageVarificationLbl.text = "Image has been disapproved";
                }
                self.dealImgView.sd_setImage(with: URL(string: (obj?.DealImage1)!), placeholderImage: K_DEAL_PLACE_HOLDER_IMAGE)
            }
            
            self.favoriteBtn.isHidden = false;
            if(userType == USER_TYPE_BUSINESS || userType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE){
                self.favoriteBtn.isHidden = true;
            }
            
            if(obj?.FavoriteStatus != nil){
                if(Int((obj?.FavoriteStatus)!)! == 1 && userType == USER_TYPE_CUSTOMER){
                    self.favoriteBtn.setImage(UIImage(named:"HeartPink"), for: .normal);
                    self.isFavorite = true;
                }
            }
            if(obj?.IsBusinessImageVerified != nil && Int((obj?.IsBusinessImageVerified!)!)! == 1 && obj?.BusinessLogoImagePath != nil){
                self.businessLogoImgView.sd_setImage(with: URL(string: (obj?.BusinessLogoImagePath!)!), placeholderImage: K_PLACE_HOLDER_IMAGE)
            }else{
                self.businessLogoImgView.image = K_PLACE_HOLDER_IMAGE;
            }
            
            if (obj?.FirstName != nil) {
                let string = String(htmlEncodedString: (obj?.FirstName!)!);
                let string1 = String(htmlEncodedString: string);
                self.businessNameLbl.text = string1;
                self.businessNameLbl.sizeToFit();
            }
            else if(obj?.BranchDescription != nil){
                let string = String(htmlEncodedString: (obj?.BranchDescription!)!);
                let string1 = String(htmlEncodedString: string);
                self.businessNameLbl.text = string1;
                self.businessNameLbl.sizeToFit();
            }
            
            if(obj?.BranchDescription != nil){
                let string = String(htmlEncodedString: (obj?.BranchDescription!)!);
                let string1 = String(htmlEncodedString: string);
                self.addressLabel.text = string1;
                self.addressLabel.sizeToFit();
            }
            
            if(obj?.Address != nil){
                let string = String(htmlEncodedString: (obj?.Address!)!);
                let string1 = String(htmlEncodedString: string);
                self.businessBranchAddressLbl.text = string1;
                self.businessBranchAddressLbl.sizeToFit();
            }
            
            self.loyalityBtn.isUserInteractionEnabled = false;
            
            print("obj, \(obj)")
            if(obj?.IsPunchCardLoyality != nil && obj?.LoyaltyID != nil){
                if(Int((obj?.IsPunchCardLoyality)!)! > 0 && Int((obj?.LoyaltyID)!)! > 0){
                    
                    if(userType == USER_TYPE_CUSTOMER){
                        self.loyalityBtn.isUserInteractionEnabled = true;
                        self.loyalityBtn.setImage(UIImage(named: "LoyaltyIconGreen"), for: .normal);
                    }
                }
            }
            
            for index in 51...55 {
                let starImg = self.starContainerView.viewWithTag(index) as? UIImageView;
                let ratingValue = index - 50;
                if (obj?.MaxRating != nil && ratingValue <= Int((obj?.MaxRating!)!)!) {
                    starImg?.image = UIImage(named: "starfull")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
                }
                else{
                    starImg?.image = UIImage(named: "starempty")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
                }
            }
            
            if (obj?.PhoneNumber != nil) {
                self.businessBranchNumberLbl.setTitle(" " + (obj?.PhoneNumber)!, for: .normal)
            }
            
            if (obj?.Distance != nil && (obj?.Distance?.characters.count)! > 0) {
                self.businessBranchDistanceLbl.text = String(format: "%.02f KM", Float((obj?.Distance!)!)!);
            }
            
            self.mapPinViewBtn.setImage(K_BONUS_MAP_PIN_IMAGE, for: .normal)
            
            if(obj?.BonusPotLocationID != nil && Int((obj?.BonusPotLocationID)!)! > 0){
                self.mapPinViewBtn.setImage(K_BONUS_POT_IMAGE, for: .normal)
            }
            else if(obj?.FavoriteStatus != nil && Int((obj?.FavoriteStatus!)!)! > 0){
                self.mapPinViewBtn.setImage(K_FAVIROUTE_IMAGE, for: .normal)
            }
            
            if(obj?.AboutUs != nil){
                let string = String(htmlEncodedString: (obj?.AboutUs!)!);
                let string1 = String(htmlEncodedString: string);
                self.businessDescriptionLbl.text = string1;
                
                print("length = \(string1.characters.count)");
                
                if(string1.characters.count > 150) {
                    
                    self.businessDescriptionLbl.isUserInteractionEnabled = true;
                    
                    let moreText = "...MORE";
                    let index = string1.index(string1.startIndex, offsetBy: 140)
                    
                    let breakedString = string1.substring(to: index)
                    let newString = breakedString + "" + moreText;
                    
                    self.businessDescriptionLbl.text = newString;
                    let range = (newString as NSString).range(of: moreText)
                    let attributedString = NSMutableAttributedString.init(string: newString);
                    
                    attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightText, range: range)
                    attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 10.0), range: range)
                    self.businessDescriptionLbl.attributedText = attributedString;
                    
                    let singleTap = UITapGestureRecognizer.init(target: self, action: #selector(aboutUsMoreOptionTapped))
                    singleTap.numberOfTapsRequired = 1;
                    singleTap.numberOfTouchesRequired = 1;
                    self.businessDescriptionLbl.addGestureRecognizer(singleTap);
                    self.businessDescriptionLbl.sizeToFit();
                }
            }
            
            if(obj?.TypeOfCoupon != nil){
                self.dealTypeLbl.text = obj?.TypeOfCoupon!;
            }
            
            
            if(obj?.Description != nil){
                let string = String(htmlEncodedString: (obj?.Description!)!);
                let string1 = String(htmlEncodedString: string);
                self.dealDescriptionLbl.text = string1;
                
                print("length = \(string1.characters.count)");
                
                if(string1.characters.count > 80) {
                    
                    self.dealDescriptionLbl.isUserInteractionEnabled = true;
                    
                    let moreText = "...MORE";
                    let index = string1.index(string1.startIndex, offsetBy: 55)
                    
                    let breakedString = string1.substring(to: index)
                    let newString = breakedString + "" + moreText;
                    
                    self.dealDescriptionLbl.text = newString;
                    let range = (newString as NSString).range(of: moreText)
                    let attributedString = NSMutableAttributedString.init(string: newString);
                    
                    attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightText, range: range)
                    attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 10.0), range: range)
                    self.dealDescriptionLbl.attributedText = attributedString;
                    
                    let singleTap = UITapGestureRecognizer.init(target: self, action: #selector(readMoreOptionTapped))
                    singleTap.numberOfTapsRequired = 1;
                    singleTap.numberOfTouchesRequired = 1;
                    self.dealDescriptionLbl.addGestureRecognizer(singleTap);
                    self.dealDescriptionLbl.sizeToFit();
                }
                
                var availableDeals = "0";
                
                if(obj?.TotalNoOfDeal != nil){
                    self.totalIssuedLbl.text = (obj?.TotalNoOfDeal)! + " Issued " + (obj?.TotalNoOfDeal)! + " Available";
                }
                
                if(obj?.AvalDealInNumber != nil){
                    availableDeals = (obj?.AvalDealInNumber)! ;
                    self.totalIssuedLbl.text = (obj?.TotalNoOfDeal)! + " Issued " + availableDeals + " Available";
                }
                
                if(obj?.PromotionEndDate != nil){
                    let redeemtionDate = AppUtility.getDateFromString((obj?.PromotionEndDate)!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kDATE_FORMAT)
                    self.redeemedBeforeLbl.text = "Redeem before " + redeemtionDate;
                }
                
                if(obj?.RemainingCoupanToBuy != nil && userType == USER_TYPE_CUSTOMER){
                    self.couponsCountLbl.text = obj?.RemainingCoupanToBuy;
                    //self.remainingCouponsToBuy.text = obj?.RemainingCoupanToBuy;
                }
                else{
                    self.couponsCountLbl.text = obj?.DealPerCustomer;
                    //remainingCouponsToBuy = obj?.DealPerCustomer;
                }
                
                if(obj?.EndDate != nil){
                    let endDate = AppUtility.getDateFromString((obj?.EndDate)!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kDATE_FORMAT)
                    self.couponsCanBoughtLbl.text = self.couponsCountLbl.text! + " coupons can be bought untill " + endDate
                }
                
                if(obj?.EndDate != nil && obj?.EndDate?.isEmpty == false){
                    let stringEndDate = obj?.EndDate;
                    let newString1 = stringEndDate?.replacingOccurrences(of: "PM", with: "")
                    
                    let dateFormatter = DateFormatter();
                    dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
                    let myDate = dateFormatter.date(from: newString1!);
                    
                    let timeLeft = AppUtility.timeLeftSinceDate(myDate!);
                    
                    if(timeLeft <= 0){
                        self.couponsBoughtRemainingDaysLbl.text = "";
                        self.couponsCanBoughtLbl.text = "Offer end date expired";
                    }
                    else{
                        let remainDays = AppUtility.remainingDaysAndTimeForDate(newString1!, currentFormat: "dd/MM/yyyy HH:mm:ss");
                        self.couponsBoughtRemainingDaysLbl.text = remainDays;
                    }
                }
                
                if(obj?.NoOfActiveCoupan != nil){
                    
                    self.redeemedCouponsCountLbl.text = obj?.NoOfActiveCoupan;
                    
                    if(Int((obj?.NoOfActiveCoupan)!)! > 0){
                        self.barCodeBtn.isUserInteractionEnabled = true;
                        self.darkGrayYellowView.backgroundColor = YELLOW_BG_COLOR;
                    }
                    else{
                        self.barCodeBtn.isUserInteractionEnabled = false;
                        self.darkGrayYellowView.backgroundColor = UIColor.darkGray;
                    }
                }
                
                self.redeemedRemainingDaysLbl.text = "";
                self.redeemedCouponToBuyLbl.text = "No Coupons available to Redeem"
                
                if(obj?.NoOfActiveCoupan != nil && Int((obj?.NoOfActiveCoupan)!)! > 0) {
                    let redemptionEndDate = obj?.PromotionEndDate;
                    let newString1 = redemptionEndDate?.replacingOccurrences(of: " PM", with: "")
                    
                    self.redeemedRemainingDaysLbl.text = AppUtility.remainingDaysAndTimeForDate( newString1!, currentFormat: "dd/MM/yyyy HH:mm:ss");
                    var str = "Coupon";
                    if(Int((obj?.NoOfActiveCoupan)!)! > 1) {
                        str = "Coupons"
                    }
                    
                    self.redeemedCouponToBuyLbl.text = (obj?.NoOfActiveCoupan)! + " " + str + " available to Redeem"
                }
            }
            
            // UserInterection disabling for business and its employee.
            if(userType == USER_TYPE_BUSINESS || userType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE){
                self.barCodeBtn.isUserInteractionEnabled = false;
                self.shareBtn.isUserInteractionEnabled = false;
                self.giftBtn.isUserInteractionEnabled = false;
                self.addCardBtn.isUserInteractionEnabled = false;
            }
        }
    
    }
    
    func logoutFromSession() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.logoutFromTheSessionForAllUsers();
    }
    
    func showAlertForVisitor(){
        
        let alert = UIAlertController(title: nil, message: "Please login as customer to continue.", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            self.logoutFromSession();
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil));
        self.present(alert, animated: true, completion: nil)
    }
    
    func readMoreOptionTapped(){
        
        let string = String(htmlEncodedString: (self.objBranchCoupon?.Description!)!);
        let string1 = String(htmlEncodedString: string);
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "MoreDescriptionVC") as! MoreDescriptionVC
        objVC.popUptitle = "Coupon Description";
        objVC.descriptionTxt = string1;
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        self.addChildViewController(objVC)
        objVC.didMove(toParentViewController: self);
    }
    
    func aboutUsMoreOptionTapped(){
        
        let string = String(htmlEncodedString: (self.objBranchCoupon?.AboutUs!)!);
        let string1 = String(htmlEncodedString: string);
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "MoreDescriptionVC") as! MoreDescriptionVC
        objVC.popUptitle = "About Us";
        objVC.descriptionTxt = string1;
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        self.addChildViewController(objVC)
        objVC.didMove(toParentViewController: self);
    }
    
    @IBAction func favoriteButtonClicked(_ sender: Any) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let userType = defaults.object(forKey: "globalUserType") as! String

        if(userType == USER_TYPE_VISITOR){
            self.showAlertForVisitor()
        }
        else if(userType == USER_TYPE_CUSTOMER){
            let btn = sender as! UIButton;
            
            let nsDataUser = defaults.object(forKey: "UserData") as! Data
            let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
            let userId = user.ID;
            let personType = user.PersonType;
            
            var favorite = "0";
            
            let pinkHeartImage = UIImage(named: "HeartPink")
            let greyHeartImage = UIImage(named: "HeartGrey")
            
            if(self.isFavorite == false){
                self.isFavorite = true;
                favorite = "1";
                btn.setImage(pinkHeartImage, for: .normal)
            }
            else{
                self.isFavorite = false;
                btn.setImage(greyHeartImage, for: .normal)
            }
            
            let deviceUniqueId = AppUtility.getDeviceNSUUID();
            
            // Created postDataDictonary to post the data on server
            let postDataDict : NSDictionary = ["UserID": userId!,
                                               "PersonType": personType!,
                                               "DealMasterID": (self.objBranchCoupon?.DealMasterID)!,
                                               "FavoriteStatus": favorite,
                                               "DeviceUniqueID": deviceUniqueId,
                                               "api_key": kWEB_SERVICES_API_KEY];

            print("postDataDict = %", postDataDict);
            
            if !AppUtility.isNetworkAvailable(){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
            }
            else{
                
                CustomProgressView.showProgressIndicatorOnView(VC: self);
                Alamofire.request(kSERVICE_FAVOURITE_A_DEAL, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                    
                    CustomProgressView.hideProgressIndicatorOnView(VC: self);
                    
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                    //print("value =  \(datastring)");
                    
                    if let JSON = response.result.value
                    {
                        print("JSON: \(JSON)")
                        let dataDict = JSON as? NSDictionary;
                        
                        if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                            
                            //let message = dataDict?.object(forKey: "Message") as! String
                            //self.navigationController?.view.makeToast(message, duration: 2.0, position: .center)
                            OperationQueue.main.addOperation {
                                self.callLocationManager();
                            }
                            
                            /*
                            let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                             */
//                            let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
//                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        }
                        else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                            
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                        }
                    }
                }
                )
            }
        }
    }
   
    @IBAction func loyaltyButtonClicked(_ sender: Any) {
        
        self.isLoyaltyActive = true;
        self.callLocationManager();
    }
    
    private func callNumber(phoneNumber:String) {
        
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func phoneNumberTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: "Dial this number now?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            
            self.callNumber(phoneNumber: (self.objBranchCoupon?.PhoneNumber)!);
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil));
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func openMapView(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objPlaceOnMapVC = storyboard.instantiateViewController(withIdentifier: "PlaceOnMapVC") as! PlaceOnMapVC
        
        // Initiate values with controller objects
        objPlaceOnMapVC.clLocationCoordinate = CLLocationCoordinate2DMake(Double((self.objBranchCoupon?.Latitude!)!)!, Double((self.objBranchCoupon?.Longitude!)!)!);
        objPlaceOnMapVC.locationTitle = self.objBranchCoupon?.FirstName;
        objPlaceOnMapVC.locationSubTitle = self.objBranchCoupon?.BranchDescription
        objPlaceOnMapVC.dealCategoryImage = self.objBranchCoupon?.MapPinCategoryPicture;
        objPlaceOnMapVC.bonusPotLocationID = self.objBranchCoupon?.BonusPotLocationID;
        objPlaceOnMapVC.branchID = self.objBranchCoupon?.BranchID;
        
        self.addChildViewController(objPlaceOnMapVC)
        objPlaceOnMapVC.view.frame = self.view.bounds;
        self.view.addSubview((objPlaceOnMapVC.view)!)
        objPlaceOnMapVC.didMove(toParentViewController: self);
    }
    
    @IBAction func shareButtonClicked(_ sender: Any) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let userType = defaults.object(forKey: "globalUserType") as! String
        
        if(userType == USER_TYPE_VISITOR){
            self.showAlertForVisitor()
        }
        else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "SharingVC") as! SharingVC
            objVC.popupVCDelegate = self;
            objVC.objBranchCoupon = self.objBranchCoupon;
            objVC.view.frame = self.view.bounds;
            self.view.addSubview(objVC.view)
            self.addChildViewController(objVC)
            objVC.didMove(toParentViewController: self);
        }
    }

    @IBAction func giftButtonTapped(_ sender: Any) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let userType = defaults.object(forKey: "globalUserType") as! String
        
        if(userType == USER_TYPE_VISITOR){
            self.showAlertForVisitor()
        }
        else
        {
            if(Int(self.redeemedCouponsCountLbl.text!)! <= 0){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "You don't have enough coupons to gift. Please first tap on cart and buy coupons.", onView: self)
            }
            else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyboard.instantiateViewController(withIdentifier: "GiftCouponsVC") as! GiftCouponsVC
                objVC.popupVCDelegate = self;
                objVC.objBranchCoupon = self.objBranchCoupon;
                objVC.maxCouponsCanGift = self.redeemedCouponsCountLbl.text!;
                objVC.view.frame = self.view.bounds;
                self.view.addSubview(objVC.view)
                self.addChildViewController(objVC)
                objVC.didMove(toParentViewController: self);
            }
        }
    }

    @IBAction func cartButtonTapped(_ sender: Any) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let userType = defaults.object(forKey: "globalUserType") as! String
        
        if(userType == USER_TYPE_VISITOR){
            self.showAlertForVisitor()
        }
        else
        {
            if(Int(self.couponsCountLbl.text!)! <= 0){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "You reached maximum number of buy limit, now you can not buy more coupons for this deal.", onView: self)
            }
            else{
                if(self.couponsCanBoughtLbl.text == "Offer end date expired"){
                    AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Sorry! You can not buy this coupon becasue it's offer end date has been expired", onView: self)
                }
                else{
                    var availableDeals = 0;
                    if(self.objBranchCoupon?.AvalDealInNumber != nil ){
                        availableDeals = Int((self.objBranchCoupon?.AvalDealInNumber)!)!;
                    }
                    else if(self.objBranchCoupon?.TotalNoOfDeal != nil ){
                        availableDeals = Int((self.objBranchCoupon?.TotalNoOfDeal)!)!;
                    }
                    
                    if (availableDeals > 0) {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let objVC = storyboard.instantiateViewController(withIdentifier: "BuyNowVC") as! BuyNowVC
                        objVC.popupVCDelegate = self;
                        objVC.objBranchCoupon = self.objBranchCoupon;
                        objVC.view.frame = self.view.bounds;
                        self.view.addSubview(objVC.view)
                        self.addChildViewController(objVC)
                        objVC.didMove(toParentViewController: self);
                    }
                    else{
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Sorry, no more coupons available to buy", onView: self)
                    }
                }
            }
        }
    }
    
    @IBAction func barButtonClicked(_ sender: Any) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let userType = defaults.object(forKey: "globalUserType") as! String
        
        if(userType == USER_TYPE_VISITOR){
            self.showAlertForVisitor()
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "RedeemCouponsVC") as! RedeemCouponsVC
            objVC.popupVCDelegate = self;
            objVC.objBranchCoupon = self.objBranchCoupon;
            objVC.numberOfCouponsToRedeem = self.redeemedCouponsCountLbl.text!;
            objVC.view.frame = self.view.bounds;
            self.view.addSubview(objVC.view)
            self.addChildViewController(objVC)
            objVC.didMove(toParentViewController: self);
        }
    }
    
    @IBAction func reviewButtonClicked(_ sender: Any) {
        
        AppUtility.removedBackTitleAndChangeItsColor(VC: self, backTitle: "", backTintColor: UIColor.white);
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "CustomerReviewsTVC") as! CustomerReviewsTVC
        objVC.objBranchCoupon = self.objBranchCoupon;
        self.navigationController?.pushViewController(objVC, animated: true);
    }
    
    func dismissPopupViewController(_ vc: UIViewController, responseObj: AnyObject?) {
        self.dismiss(animated: true, completion: nil);
        
        // If child view return response then only it will excecute
        if (responseObj != nil) {
            
            // If view controller will PunchesVC then excecute it
            if (vc .isKind(of: SharingVC.self)) {
                
                //self.dismissPopupViewController(self, responseObj: nil); // Dismiss popup view controller
            }
            else  if (vc .isKind(of: BuyNowVC.self) || vc .isKind(of: GiftCouponsVC.self)) {
                if(responseObj != nil && responseObj as! String == "Success"){
                    self.callLocationManager(); // Calling this to get updated location and its updated data for a deal
                }
            }
            else  if (vc .isKind(of: RedeemCouponsVC.self)) {
                if(responseObj != nil){
                    self.callLocationManager(); 
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
