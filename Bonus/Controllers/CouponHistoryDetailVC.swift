//
//  CouponHistoryDetailVC.swift
//  Bonus
//
//  Created by Arvind Sen on 08/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class CouponHistoryDetailVC: UIViewController, CLLocationManagerDelegate, DismissPopupVCProtocol {

    @IBOutlet weak var dealStatusBtn: UIButton!
    var objCustomerCoupon: CustomerCoupon? = nil;
    var dealMasterId : String? = nil;
    var isLoyaltyActive = false;
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewContainer: UIView!
    @IBOutlet weak var dealImgView: UIImageView!
    @IBOutlet weak var imageVarificationLbl: MarqueeLabel!
    
    @IBOutlet weak var businessLogoImgView: UIImageView!
    @IBOutlet weak var businessNameLbl: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var businessBranchAddressLbl: UILabel!
    @IBOutlet weak var loyalityBtn: UIButton!
    @IBOutlet weak var starContainerView: UIView!
    @IBOutlet weak var businessBranchNumberLbl: UIButton!
    @IBOutlet weak var businessBranchDistanceLbl: UILabel!
    @IBOutlet weak var mapPinViewBtn: UIButton!
    
    @IBOutlet weak var businessDescriptionLbl: UILabel!
    @IBOutlet weak var dealTypeLbl: UILabel!
    @IBOutlet weak var dealDescriptionLbl: UILabel!
    @IBOutlet weak var shareBtn: UIButton!
    
    @IBOutlet weak var redemptionOrGiftDateLbl: UILabel!
    @IBOutlet weak var referenceNumberLbl: UILabel!
    var locationManager : CLLocationManager?
    var userCurrentLocationCoordinate : CLLocationCoordinate2D? ;

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        AppUtility.navigationColorAndItsTextColor(VC: self);
        self.callLocationManager();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
        self.navigationController?.isNavigationBarHidden = false;
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.scrollView.contentSize = CGSize(width: Double(self.view.frame.size.width), height: 720.0);
    }
    
    // Method get call to get user current location lat, long
    func callLocationManager() {
        
        if (CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied) {
            
            self.locationManager = CLLocationManager.init();// locationManager need to define at the class variable then only its call delegate methods.
            self.locationManager!.delegate = self;
            
            if self.locationManager!.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
                self.locationManager!.requestWhenInUseAuthorization();
            }
            self.locationManager!.desiredAccuracy = kCLLocationAccuracyBest;
            self.locationManager!.startUpdatingLocation();
        }
        else{
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "To re-enable, please go to Settings and turn on Location Service for this app.", onView: self)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.locationManager!.stopUpdatingLocation();
        let recentLocation = locations.last;
        self.userCurrentLocationCoordinate = recentLocation?.coordinate;
        
        let latString = String(format: "%f", (recentLocation?.coordinate.latitude)!);
        let longString = String(format: "%f", (recentLocation?.coordinate.longitude)!);
        
        let dict = NSDictionary.init(objects: [latString, longString], forKeys: ["Latitude" as NSCopying, "Longitude" as NSCopying]);
        
        if(self.isLoyaltyActive == true){
            self.isLoyaltyActive = false;
            self.getUserPunchCardDetailWithDict(dict)
        }
        else{
            self.callADealSpecificDetail(dict)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(" failed to get current location");
        
        self.locationManager!.stopUpdatingLocation();
        
        let dict = NSDictionary.init(objects: ["0.0", "0.0"], forKeys: ["Latitude" as NSCopying, "Longitude" as NSCopying]);
        
        if(isLoyaltyActive == true){
            self.getUserPunchCardDetailWithDict(dict)
        }
        else{
            self.callADealSpecificDetail(dict)
        }
    }
    
    func moveOnLoyaltyDetailScreen(loyalty: Loyalty){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "LoyaltyDetailVC") as! LoyaltyDetailVC
        objVC.loyaltyObj = loyalty;
        self.navigationController?.pushViewController(objVC, animated: true);
    }
    
    func getUserPunchCardDetailWithDict(_ dict : NSDictionary){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID;
        let personType = user.PersonType;
        
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["DealId" : (self.objCustomerCoupon?.DealMasterID)!,
                                           "BranchID": (self.objCustomerCoupon?.BranchID)!,
                                           "UserID": userId!,
                                           "PersonType": personType!,
                                           "Latitude": dict.value(forKey: "Latitude") as! String,
                                           "Longitude": dict.value(forKey: "Longitude") as! String,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_SELECT_USER_PUNCH_CARD_DETAILS, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        
                        //self.loyaltyListArray = NSMutableArray();
                        if(dictArray.count > 0)
                        {
                            let item = dictArray[0]
                            let objLoyalty = Loyalty(data: (item as! Dictionary<String, AnyObject>))
                            self.moveOnLoyaltyDetailScreen(loyalty: objLoyalty);
                        }
                        else
                        {
                            let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        
                    }
                }
            }
            )
        }
    }
    
    func callADealSpecificDetail(_ dict : NSDictionary?) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID!
        
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        var latitude = "0";
        var longitude = "0";
        if(dict != nil){
            latitude = ("\((dict?["Latitude"])!)");
            longitude = ("\((dict?["Longitude"])!)");
        }
        
        let dealId = self.dealMasterId!
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSMutableDictionary = ["CustomerUserID" : userId,
                                                  "IsViewHistory" : "1",
                                                  "SortBy" : "Distance",
                                                  "FavoriteStatus" : "0",
                                                  "DealMasterID" : dealId,
                                                  "BusinessCategory" : "0",
                                                  "Latitude" : latitude,
                                                  "Longitude" : longitude,
                                                  "Limit" : "1",
                                                  "Offset" : "0",
                                                  "OrderBy" : "ASC",
                                                  "SearchFor" : "",
                                                  "api_key" : kWEB_SERVICES_API_KEY,
                                                  "DeviceUniqueID" : deviceUniqueId];
        
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_CUSTOMER_COUPONS, method: HTTPMethod.post, parameters: postDataDict as! [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        let resourcePath = dataDict?.object(forKey: "ResourcePath") as! Dictionary<String, AnyObject>
                        
                        if(dictArray.count > 0)
                        {
                            self.objCustomerCoupon = CustomerCoupon(data: (dictArray[0] as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>, resourcePaths: resourcePath)
                            
                            self.showADealDetail()
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else{
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                    }
                }
            }
            )
        }
    }
    
    
    func showADealDetail() {
        
        if(self.objCustomerCoupon != nil){
            
            let obj = self.objCustomerCoupon;
            
            let defaults: UserDefaults = UserDefaults.standard
            let userType = defaults.value(forKey: "globalUserType") as! String;
            
            if(obj?.BranchDescription != nil){
                    self.title = obj?.BranchDescription;
            }
            else if (obj?.FirstName != nil) {
                self.title = obj?.FirstName;
            }
            
            dealImgView.image = K_DEAL_PLACE_HOLDER_IMAGE;
            if(obj?.IsDealImageVerified != nil && Int((obj?.IsDealImageVerified)!)! == 1 && obj?.DealImage1 != nil){
                self.dealImgView.sd_setImage(with: URL(string: (obj?.DealImage1)!), placeholderImage: K_DEAL_PLACE_HOLDER_IMAGE)
            }            
            
            if(obj?.IsBusinessImageVerified != nil && Int((obj?.IsBusinessImageVerified!)!)! == 1 && obj?.BusinessLogoImagePath != nil){
                self.businessLogoImgView.sd_setImage(with: URL(string: (obj?.BusinessLogoImagePath!)!), placeholderImage: K_PLACE_HOLDER_IMAGE)
            }else{
                self.businessLogoImgView.image = K_PLACE_HOLDER_IMAGE;
            }
            
            self.businessNameLbl.text = "";
            if (obj?.FirstName != nil) {
                let string = String(htmlEncodedString: (obj?.FirstName!)!);
                let string1 = String(htmlEncodedString: string);
                self.businessNameLbl.text = string1;
                self.businessNameLbl.sizeToFit();
            }
            else if(obj?.BranchDescription != nil){
                let string = String(htmlEncodedString: (obj?.BranchDescription!)!);
                let string1 = String(htmlEncodedString: string);
                self.businessNameLbl.text = string1;
                self.businessNameLbl.sizeToFit();
            }
            
            self.addressLabel.text = "";
            if(obj?.BranchDescription != nil){
                let string = String(htmlEncodedString: (obj?.BranchDescription!)!);
                let string1 = String(htmlEncodedString: string);
                self.addressLabel.text = string1;
                self.addressLabel.sizeToFit();
            }
            
            self.businessBranchAddressLbl.text = "";
            if(obj?.Address != nil){
                let string = String(htmlEncodedString: (obj?.Address!)!);
                let string1 = String(htmlEncodedString: string);
                self.businessBranchAddressLbl.text = string1;
                self.businessBranchAddressLbl.sizeToFit();
            }
            
            self.loyalityBtn.isUserInteractionEnabled = false;
            self.loyalityBtn.isHidden = true;
            /*
            if(obj?.IsPunchCardLoyality != nil && obj?.LoyaltyID != nil){
                if(Int((obj?.IsPunchCardLoyality)!)! > 0 && Int((obj?.LoyaltyID)!)! > 0){
                    
                    if(userType == USER_TYPE_CUSTOMER){
                        self.loyalityBtn.isUserInteractionEnabled = true;
                        self.loyalityBtn.setImage(UIImage(named: "LoyaltyIconGreen"), for: .normal);
                    }
                }
            }
            */
            for index in 51...55 {
                let starImg = self.starContainerView.viewWithTag(index) as? UIImageView;
                let ratingValue = index - 50;
                if (obj?.MaxRating != nil && ratingValue <= Int((obj?.MaxRating!)!)!) {
                    starImg?.image = UIImage(named: "starfull")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
                }
                else{
                    starImg?.image = UIImage(named: "starempty")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
                }
            }
            
            if (obj?.PhoneNumber != nil) {
                self.businessBranchNumberLbl.setTitle(" " + (obj?.PhoneNumber)!, for: .normal)
            }
            
            if (obj?.Distance != nil) {
                self.businessBranchDistanceLbl.text = String(format: "%.02f KM", Float((obj?.Distance!)!)!);
            }
            
            self.mapPinViewBtn.setImage(K_BONUS_MAP_PIN_IMAGE, for: .normal)
            
            if(obj?.FavoriteStatus != nil && Int((obj?.FavoriteStatus!)!)! > 0){
                self.mapPinViewBtn.setImage(K_FAVIROUTE_IMAGE, for: .normal)
            }
            
            self.businessDescriptionLbl.text = "";
            if(obj?.AboutUs != nil){
                let string = String(htmlEncodedString: (obj?.AboutUs!)!);
                let string1 = String(htmlEncodedString: string);
                self.businessDescriptionLbl.text = string1;
                
                print("length = \(string1.characters.count)");
                
                if(string1.characters.count > 150) {
                    
                    self.businessDescriptionLbl.isUserInteractionEnabled = true;
                    
                    let moreText = "...MORE";
                    let index = string1.index(string1.startIndex, offsetBy: 140)
                    
                    let breakedString = string1.substring(to: index)
                    let newString = breakedString + "" + moreText;
                    
                    self.businessDescriptionLbl.text = newString;
                    let range = (newString as NSString).range(of: moreText)
                    let attributedString = NSMutableAttributedString.init(string: newString);
                    
                    attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightText, range: range)
                    attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 10.0), range: range)
                    self.businessDescriptionLbl.attributedText = attributedString;
                    
                    let singleTap = UITapGestureRecognizer.init(target: self, action: #selector(aboutUsMoreOptionTapped))
                    singleTap.numberOfTapsRequired = 1;
                    singleTap.numberOfTouchesRequired = 1;
                    self.businessDescriptionLbl.addGestureRecognizer(singleTap);
                    self.businessDescriptionLbl.sizeToFit();
                }
            }
            
            if(obj?.TypeOfCoupon != nil){
                self.dealTypeLbl.text = obj?.TypeOfCoupon!;
            }
            
            if(obj?.Description != nil){
                let string = String(htmlEncodedString: (obj?.Description!)!);
                let string1 = String(htmlEncodedString: string);
                self.dealDescriptionLbl.text = string1;
                
                print("length = \(string1.characters.count)");
                
                if(string1.characters.count > 80) {
                    
                    self.dealDescriptionLbl.isUserInteractionEnabled = true;
                    
                    let moreText = "...MORE";
                    let index = string1.index(string1.startIndex, offsetBy: 55)
                    
                    let breakedString = string1.substring(to: index)
                    let newString = breakedString + "" + moreText;
                    
                    self.dealDescriptionLbl.text = newString;
                    let range = (newString as NSString).range(of: moreText)
                    let attributedString = NSMutableAttributedString.init(string: newString);
                    
                    attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightText, range: range)
                    attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 10.0), range: range)
                    self.dealDescriptionLbl.attributedText = attributedString;
                    
                    let singleTap = UITapGestureRecognizer.init(target: self, action: #selector(readMoreOptionTapped))
                    singleTap.numberOfTapsRequired = 1;
                    singleTap.numberOfTouchesRequired = 1;
                    self.dealDescriptionLbl.addGestureRecognizer(singleTap);
                    self.dealDescriptionLbl.sizeToFit();
                }
                
                let dealStat = ((obj?.DealStatus!)!).lowercased();
                if(obj?.DealStatus! != nil && dealStat == "expired"){
                    self.dealStatusBtn.setImage(kIMAGE_EXPIRED_COUPON, for: .normal)
                }
                else{
                    self.dealStatusBtn.setImage(kIMAGE_RIGHT_CHECK_MARK, for: .normal)
                }
                
                self.dealStatusBtn.setTitle("  " + dealStat, for: .normal);
                
                if(obj?.TransactionDate != nil){
                    
                    let transectionDate = AppUtility.getDateFromString((obj?.TransactionDate!)!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: "dd MMM yyyy");
                    
                    let arr = obj?.TransactionDate?.components(separatedBy: " ");
                    var timeString = transectionDate;
                    if((arr?.count)! > 0){
                        timeString = timeString + " " + (arr?[1])! + " " + (arr?[2])!;
                    }
                    
                    var dealMessage = "Redeemed";
                    
                    if(obj?.DealStatus != nil && (obj?.DealStatus! == "gift" || obj?.DealStatus! == "Gift" || obj?.DealStatus! == "Gifted")){
                        dealMessage = "Gifted";
                    }
                    else if(obj?.DealStatus != nil && (obj?.DealStatus! == "expire" || obj?.DealStatus! == "expired" || obj?.DealStatus! == "Expired")){
                        dealMessage = "Expired";
                    }
                    
                    var coupon = "Coupon";
                    if(obj?.CouponQTY != nil && (Int((obj?.CouponQTY!)!)! > 1)){
                        coupon = "Coupons";
                    }
                    
                    self.redemptionOrGiftDateLbl.text = ("\((obj?.CouponQTY!)!) coupon \(dealMessage) on \(timeString)");
                    
                    self.referenceNumberLbl.text = "";
                    if(obj?.TransactionID != nil){
                        self.referenceNumberLbl.text = ("Redemption/Gift Code : \((obj?.TransactionID!)!)")
                    }
                }
            }
        }
    }
    
    func logoutFromSession() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.logoutFromTheSessionForAllUsers();
    }

    func readMoreOptionTapped(){
        
        let string = String(htmlEncodedString: (self.objCustomerCoupon?.Description!)!);
        let string1 = String(htmlEncodedString: string);
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "MoreDescriptionVC") as! MoreDescriptionVC
        objVC.popUptitle = "Coupon Description";
        objVC.descriptionTxt = string1;
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        self.addChildViewController(objVC)
        objVC.didMove(toParentViewController: self);
    }
    
    func aboutUsMoreOptionTapped(){
        
        let string = String(htmlEncodedString: (self.objCustomerCoupon?.AboutUs!)!);
        let string1 = String(htmlEncodedString: string);
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "MoreDescriptionVC") as! MoreDescriptionVC
        objVC.popUptitle = "About Us";
        objVC.descriptionTxt = string1;
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        self.addChildViewController(objVC)
        objVC.didMove(toParentViewController: self);
    }

    @IBAction func loyaltyButtonClicked(_ sender: Any) {
        
        self.isLoyaltyActive = true;
        self.callLocationManager();
    }
    
    private func callNumber(phoneNumber:String) {
        
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func phoneNumberTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: "Dial this number now?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            
            self.callNumber(phoneNumber: (self.objCustomerCoupon?.PhoneNumber)!);
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil));
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func reviewButtonClicked(_ sender: Any) {
        
        AppUtility.removedBackTitleAndChangeItsColor(VC: self, backTitle: "", backTintColor: UIColor.white);
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "CustomerReviewsTVC") as! CustomerReviewsTVC
        objVC.objCustomerCoupon = self.objCustomerCoupon;
        self.navigationController?.pushViewController(objVC, animated: true);
    }
    
    @IBAction func openMapView(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objPlaceOnMapVC = storyboard.instantiateViewController(withIdentifier: "PlaceOnMapVC") as! PlaceOnMapVC
        
        if(self.objCustomerCoupon?.Latitude != nil)
        {
            // Initiate values with controller objects
            objPlaceOnMapVC.clLocationCoordinate = CLLocationCoordinate2DMake(Double((self.objCustomerCoupon?.Latitude!)!)!, Double((self.objCustomerCoupon?.Longitude!)!)!);
            objPlaceOnMapVC.locationTitle = self.objCustomerCoupon?.FirstName;
            objPlaceOnMapVC.locationSubTitle = self.objCustomerCoupon?.BranchDescription
            objPlaceOnMapVC.dealCategoryImage = self.objCustomerCoupon?.MapPinCategoryPicture;
            //objPlaceOnMapVC.bonusPotLocationID = self.objCustomerCoupon?.BonusPotLocationID;
            objPlaceOnMapVC.branchID = self.objCustomerCoupon?.BranchID;
            
            self.addChildViewController(objPlaceOnMapVC)
            objPlaceOnMapVC.view.frame = self.view.bounds;
            self.view.addSubview((objPlaceOnMapVC.view)!)
            objPlaceOnMapVC.didMove(toParentViewController: self);
        }
    }
    
    @IBAction func shareButtonClicked(_ sender: Any) {
       
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "SharingVC") as! SharingVC
        objVC.popupVCDelegate = self;
        objVC.objCustomerCoupon = self.objCustomerCoupon;
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        self.addChildViewController(objVC)
        objVC.didMove(toParentViewController: self);
        
    }

    func dismissPopupViewController(_ vc: UIViewController, responseObj: AnyObject?) {
        self.dismiss(animated: true, completion: nil);
        
        // If child view return response then only it will excecute
        if (responseObj != nil) {
            
            // If view controller will PunchesVC then excecute it
            if (vc .isKind(of: SharingVC.self)) {
                
                //self.dismissPopupViewController(self, responseObj: nil); // Dismiss popup view controller
            }
            else  if (vc .isKind(of: BuyNowVC.self) || vc .isKind(of: GiftCouponsVC.self)) {
                if(responseObj != nil && responseObj as! String == "Success"){
                    self.callLocationManager(); // Calling this to get updated location and its updated data for a deal
                }
            }
            else  if (vc .isKind(of: RedeemCouponsVC.self)) {
                if(responseObj != nil){
                    self.callLocationManager();
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
