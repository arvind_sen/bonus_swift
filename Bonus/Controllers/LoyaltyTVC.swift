//
//  LoyaltyTVC.swift
//  Bonus
//
//  Created by Arvind Sen on 08/09/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class LoyaltyTVC: UITableViewController, UISearchBarDelegate, CLLocationManagerDelegate {

    var revealVC : SWRevealViewController? = nil;
    var searchBar: UISearchBar!
    var isWebServiceCalled = false
    var leftMenusSelectedFor : SelectionFor!
    var selectedDict: NSDictionary!
    var lastOffset: String!
    var selectedCategoriesFromLeftMenus : String?
    var selectedIndexPathFromLeftMenus : IndexPath?
    
    var didSelectedRowObj: Loyalty!;
    
    var loyaltyListArray: NSMutableArray!
    var searchDataArray: NSMutableArray!
    var locationManager : CLLocationManager?
    var userCurrentLocationCoordinate : CLLocationCoordinate2D? ;
    var isMapViewComeBack: Bool = false;
    
    var flagToNoDataFound = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // This code is using to hid left menus
        if (self.revealViewController() != nil) {
            //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
        self.tableView.backgroundColor = K_LISTING_VIEW_BG_COLOR;
        //self.tableView.separatorInset = UIEdgeInsetsZero;
        //self.tableView.contentInset = UIEdgeInsetsMake(1, 4, 1, 4);
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.showTopBtns();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
        
        if (isMapViewComeBack == true && self.loyaltyListArray != nil) {
            self.showTableView();
        }
        else
        {
            let objVC = self.navigationController?.presentedViewController;
            
            if (objVC != nil && objVC!.isKind(of: PlaceOnMapVC.self)) {
               // <#code#>
            }
            else{
                self.callLocationManager();
            }
        }
    }
    
    // Method get call to get user current location lat, long
    func callLocationManager() {
        
        if (CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied) {
            
            self.locationManager = CLLocationManager.init();// locationManager need to define at the class variable then only its call delegate methods.
            self.locationManager!.delegate = self;
            
            if self.locationManager!.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
                self.locationManager!.requestWhenInUseAuthorization();
            }
            self.locationManager!.desiredAccuracy = kCLLocationAccuracyBest;
            self.locationManager!.startUpdatingLocation();
        }
        else{
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "To re-enable, please go to Settings and turn on Location Service for this app.", onView: self)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.locationManager!.stopUpdatingLocation();
        let recentLocation = locations.last;
        self.userCurrentLocationCoordinate = recentLocation?.coordinate;
        
        let latString = String(format: "%f", (recentLocation?.coordinate.latitude)!);
        let longString = String(format: "%f", (recentLocation?.coordinate.longitude)!);
        
        let dict = NSDictionary.init(objects: [latString, longString], forKeys: ["Latitude" as NSCopying, "Longitude" as NSCopying]);
        
        self.loyaltyCheckInServiceCall(SelectionFor.isSelectedNearBy, withParameters: dict, offSetValue: "0");
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(" failed to get current location");
        
        self.locationManager!.stopUpdatingLocation();
        
        let dict = NSDictionary.init(objects: ["0.0", "0.0"], forKeys: ["Latitude" as NSCopying, "Longitude" as NSCopying]);
        
        self.loyaltyCheckInServiceCall(SelectionFor.isSelectedNearBy, withParameters: dict, offSetValue: "0");
    }

    func loyaltyCheckInServiceCall(_ selectedSection: SelectionFor, withParameters: NSDictionary?, offSetValue: String = "0") -> Void {
        
        self.leftMenusSelectedFor = selectedSection;
        self.selectedDict = withParameters;
        self.lastOffset = offSetValue;
        
        var searchFor = "";
        
        if self.searchBar.text?.characters.count > 0 {
            searchFor = self.searchBar.text!;
        }
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        let uniqueID = AppUtility.getDeviceNSUUID();
        let postDataDict : NSMutableDictionary = ["UserID" : user.ID!,
                                           "FavoriteStatus" : "0",
                                           "Latitude" : "0",
                                           "Longitude" : "0",
                                           "Limit" : K_TABLE_ROW_LIMITS,
                                           "Offset" : offSetValue,
                                           "SortBy" : "Distance",
                                           "BusinessCategory" : 0,
                                           "OrderBy" : "ASC",
                                           "SearchFor" : searchFor,
                                           "api_key" : kWEB_SERVICES_API_KEY,
                                           "DeviceUniqueID" : uniqueID];

        if (self.userCurrentLocationCoordinate != nil) {
            let latString = String(format: "%f", self.userCurrentLocationCoordinate!.latitude);
            let longString = String(format: "%f", self.userCurrentLocationCoordinate!.longitude);
            
            postDataDict.setObject(latString, forKey: "Latitude" as NSCopying);
            postDataDict.setObject(longString, forKey: "Longitude" as NSCopying);
        }

        switch selectedSection {
        case .isSelectedFavorite:
            postDataDict.setObject("1", forKey: "FavoriteStatus" as NSCopying)
            break;
        case .isSelectedNearBy:
            postDataDict.setObject("ASC", forKey: "OrderBy" as NSCopying)
            break;
        case .isSelectedAToZ:
            postDataDict.setObject("BranchDescription", forKey: "SortBy" as NSCopying)
            postDataDict.setObject("ASC", forKey: "OrderBy" as NSCopying)
            break;
        default:
            postDataDict.setObject(withParameters!.object(forKey: "ID")! as! String, forKey: "BusinessCategory" as NSCopying)
        }

        postDataDict.setObject("0", forKey: "IsShowOnMap" as NSCopying);
        postDataDict.setObject(user.PersonType!, forKey: "PersonType" as NSCopying);
        
        self.callLoyaltyService(postDataDict);
    }
    
    func callLoyaltyService(_ postData: NSDictionary){
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_NEAREST_PUNCH_CARD_BUSINESS_LIST, method: HTTPMethod.post, parameters: postData as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                self.isWebServiceCalled = false;
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        let resourcePath = dataDict?.object(forKey: "ResourcePath") as! Dictionary<String, AnyObject>
                        
                        if(Int(postData.object(forKey: "Offset") as! String)! == 0){
                            self.loyaltyListArray = nil;
                        }
                        
                        if(self.loyaltyListArray == nil){
                            self.loyaltyListArray = NSMutableArray();
                        }
                        
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let item = Loyalty(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>, resourcePaths: resourcePath)
                                
                                self.loyaltyListArray?.add(item);
                            }
                        }
                        else
                        {
                            self.navigationController?.view.makeToast("No record found", duration: 2.0, position: .center)
                        }
                        
                        DispatchQueue.main.async {
                            if(self.loyaltyListArray != nil && (self.loyaltyListArray?.count)! > 0){
                                self.flagToNoDataFound = false;
                            }
                            else{
                                self.flagToNoDataFound = true;
                            }
                            self.showTableView();
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        
                    }
                }
                }
            )
        }
    }
    
    func showTopBtns(){
        
        // Left toggle button configration
        self.navigationController?.navigationBar.barTintColor = TAB_BAR_BG_COLOR;
        self.navigationController?.isNavigationBarHidden = false;
        self.revealVC = self.revealViewController()
        //self.navigationController?.navigationBar.addGestureRecognizer(self.revealVC!.panGestureRecognizer())
        
        let revealButtonItem = UIBarButtonItem();
        revealButtonItem.image = UIImage (named: "FilterIcon")
        revealButtonItem.style = UIBarButtonItemStyle.plain;
        revealButtonItem.target = self;
        revealButtonItem.action = #selector(LoyaltyTVC.revealToggle(_:));
        revealButtonItem.tintColor = UIColor.white;
        self.navigationItem.leftBarButtonItem = revealButtonItem;
        
        
        //Show search bar on top of the view controller
        
        let oY = CGFloat(5.0)
        let oX = CGFloat(50.0)
        
        let height = (self.navigationController?.navigationBar.frame.height)! - CGFloat(2 * oY);
        let width = (self.navigationController?.navigationBar.frame.width)! - CGFloat(2 * oX);
        self.searchBar = UISearchBar.init(frame: CGRect(x: 0.0, y: oY, width: width, height: height));
        self.searchBar.placeholder = "Search"
        self.searchBar.showsCancelButton = false;
        self.searchBar?.delegate = self;
        self.navigationItem.titleView = self.searchBar
        
        // Right Top tab bar button configration
        var rightBtn = UIImage(named: "MapWhitePin")
        rightBtn = rightBtn?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let barRightBtn = UIBarButtonItem(image: rightBtn, style: UIBarButtonItemStyle.plain, target: self, action:#selector(LoyaltyTVC.rightBtnTapped(_:)));
        self.navigationItem.rightBarButtonItem = barRightBtn;
    }
    
    
    override func viewDidLayoutSubviews() {
         super.viewDidLayoutSubviews()
        /*
        if let rect = self.navigationController?.navigationBar.frame {
            let y = rect.size.height + rect.origin.y
            self.tableView.contentInset = UIEdgeInsetsMake( y, 0, 0, 0)
        }*/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Method will call to open map view on current view controller
    func rightBtnTapped(_ sender: AnyObject){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "LoyaltyMapLocationVC") as! LoyaltyMapLocationVC
        objVC.currentLocationCoordinate = userCurrentLocationCoordinate;
        objVC.leftMenusSelectedFor = self.leftMenusSelectedFor; //Passing same left menu selection data
        objVC.listArray = self.loyaltyListArray;
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = "flip"
        transition.subtype = kCATransitionFromLeft
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(objVC, animated: false)
    }

    func revealToggle(_ obj : AnyObject?){
        
//        if self.focusedControl != nil {
//            self.focusedControl.resignFirstResponder();
//        }
//        self.removeAddressSearchView();
        
        self.searchBar.resignFirstResponder();
        self.revealVC!.revealToggle(nil)
    }
    
    func showTableView(){
        
        if self.searchDataArray != nil{
            self.searchDataArray.removeAllObjects()
        }
        
        self.searchDataArray = NSMutableArray();
        self.searchDataArray.addObjects(from: self.loyaltyListArray as [AnyObject])
        
        self.tableView.reloadData();
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        var numOfSections: Int = 0
        if (self.searchDataArray != nil && self.searchDataArray.count > 0)
        {
            //tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else if (self.flagToNoDataFound == true)
        {
            var oX = 0.0;
            var oY = 0.0;
            let widthX = tableView.frame.size.width;
            let heightY = tableView.frame.size.height;
            
            let emptyContainer = UIView.init(frame: CGRect(x: oX, y: oY, width: Double(widthX), height: Double(heightY)));
            emptyContainer.backgroundColor = K_EMPTY_TABLE_BG_COLOR;
            ///tblCell.contentView.addSubview(emptyContainer);
            
            let imgWidth = 100.0;
            let imgHeight = 100.0;
            oX = (Double(widthX) - imgWidth)/2;
            oY = (Double(heightY) - imgHeight)/2;
            
            let imageView = UIImageView.init(frame: CGRect(x: oX, y: oY, width: imgWidth, height: imgHeight));
            imageView.backgroundColor = UIColor.clear;
            imageView.contentMode = .scaleAspectFit;
            imageView.image = UIImage.init(named: "alert");
            emptyContainer.addSubview(imageView);
            
            let titleWidth = emptyContainer.frame.size.width - 20.0;
            let titleHeight = 50.0;
            let OY = imageView.frame.size.height + imageView.frame.origin.y + 10.0;
            
            let titleLbl = UILabel.init(frame: CGRect(x: 10.0, y: Double(OY), width: Double(titleWidth), height: titleHeight));
            titleLbl.textAlignment = .center;
            titleLbl.numberOfLines = 0;
            titleLbl.lineBreakMode = .byWordWrapping;
            titleLbl.text = "No business loyalty has been added yet"
            titleLbl.textColor = UIColor.darkGray;
            emptyContainer.addSubview(titleLbl);
            //titleLbl.sizeToFit();
            
            tableView.backgroundView  = emptyContainer;
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // #warning Incomplete implementation, return the number of rows
        var returnValue = 0;
        
        if self.searchDataArray != nil {
            returnValue = self.searchDataArray.count;
        }
        return returnValue;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoyaltyTVCell", for: indexPath) as! LoyaltyTVCell
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        // Configure the cell...
        self.configureTableViewCell(cell, forRowAtIndexPath: indexPath);

        return cell
    }
    
    // This below method is setting different values for a row
    func configureTableViewCell(_ cell: LoyaltyTVCell, forRowAtIndexPath:IndexPath){
        let obj = searchDataArray[forRowAtIndexPath.row] as! Loyalty;
        
        if(obj.IsImageVerified != nil && Int(obj.IsImageVerified!) == 1 && obj.BusinessLogoImagePath != nil){
            cell.businessImage.sd_setImage(with: URL(string: obj.BusinessLogoImagePath!), placeholderImage: K_PLACE_HOLDER_IMAGE)
        }else{
            cell.businessImage.image = K_PLACE_HOLDER_IMAGE;
        }
        
        if (obj.FirstName != nil) {
            cell.nameLbl.text = obj.FirstName!;
        }
        else if(obj.BranchDescription != nil){
            cell.nameLbl.text = obj.BranchDescription!;
        }
        
        // Setting area lbl value
        if(obj.BranchDescription != nil){
            cell.areaLbl.text = obj.BranchDescription!;
            cell.areaLbl.sizeToFit();
        }
        
        // Value for freebie
        cell.redeemptionIcon.setTitle(obj.NumberOfBalanceFreeBee, for: UIControlState());
        if (Int(obj.NumberOfBalanceFreeBee!) > 0) {
            cell.redeemptionIcon.setBackgroundImage(UIImage(named:"FreeBie"), for: UIControlState());
        }
        else
        {
           cell.redeemptionIcon.setBackgroundImage(UIImage(named:"FreebieGrey"), for: UIControlState());
        }
        
        // Set distance value
        cell.distanceLbl.text = String(format: "%.02f KM", Float(obj.Distance!)!);
        
        // Set map/pot/faviourite icons
        if(obj.BonusPotLocationID != nil && Int(obj.BonusPotLocationID!) > 0){
            cell.mapPinBtn.setImage(K_BONUS_POT_IMAGE, for: UIControlState())
        }else if(obj.FavoriteStatus != nil && Int(obj.FavoriteStatus!) > 0){
            cell.mapPinBtn.setImage(K_FAVIROUTE_IMAGE, for: UIControlState())
        }
        
        // Appearance of circles
        let grayCircleImg = UIImage(named: "WhiteCircle")
        let yellowCircleImg = UIImage(named: "WhiteYellowCircle")
        
        let circleImgHeight = CGFloat(20);
        let circleImgWidth = CGFloat(20);
        var oX = cell.businessImage.frame.size.width + 10.0 as CGFloat;
        let nX = oX;
        var oY = cell.contentView.frame.size.height/2 - 5;
        var totalPunches = 0;
        var allotedPunches = 0;
        
        if ((obj.PunchRequiredForFreeBee) != nil) {
            totalPunches = Int(obj.PunchRequiredForFreeBee!)!;
            allotedPunches = Int(obj.PunchRequiredForFreeBee!)! - Int(obj.CurrentPunchesRequired!)!
            if (allotedPunches % totalPunches == 0) {
                allotedPunches = 0;
            }
        }
        
        for i in 1...totalPunches {
            let rect = CGRect(x: oX, y: oY, width: circleImgWidth, height: circleImgWidth)
            
            let imageView = UIImageView.init(frame: rect);
            imageView.backgroundColor = UIColor.clear;
            
            if (i <= allotedPunches) {
                imageView.image = yellowCircleImg;
            }
            else{
                imageView.image = grayCircleImg;
            }
            cell.containerView.addSubview(imageView);
            
            oX += circleImgWidth + 8;
            
            if (i%5 == 0) {
                oY += circleImgHeight+3;
                oX = nX;
            }
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = searchDataArray[indexPath.row] as! Loyalty;
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "LoyaltyDetailVC") as! LoyaltyDetailVC
        objVC.loyaltyObj = obj;
        self.navigationController?.pushViewController(objVC, animated: true);
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!;
        cell.accessoryType = UITableViewCellAccessoryType.none;
    }
    
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating ")
        
        if((scrollView.contentSize.height - 45.0) < (scrollView.contentOffset.y + scrollView.frame.size.height)){
            
            let count = self.loyaltyListArray?.count;
            if(count != nil){
                self.loyaltyCheckInServiceCall(self.leftMenusSelectedFor, withParameters: selectedDict, offSetValue: String(describing: count!));
            }
        }
    }
    

    /*
    // If user scroll the view
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y + scrollView.frame.size.height
        let height = scrollView.contentSize.height
        
        if(offset >= height)
        {
            if(!isWebServiceCalled)
            {
                self.isWebServiceCalled = true;
                let count = self.loyaltyListArray?.count;
                
                
                if(count != nil){
                    self.loyaltyCheckInServiceCall(self.leftMenusSelectedFor, withParameters: selectedDict, offSetValue: String(describing: count!));
                }
                
            }
        }
    }
    */
    
    // Method will get call when user tap on a map pin on different screens
    @IBAction func placeOnMapBtnTapped(_ sender: AnyObject) {
        
        let btn = sender as! UIButton;
        let pointInTable = btn.convert(btn.bounds.origin, to: self.view)
        let indexPath = self.tableView.indexPathForRow(at: pointInTable);
        
        let objLoyalty = self.loyaltyListArray.object(at: (indexPath?.row)!) as! Loyalty;
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "PlaceOnMapVC") as! PlaceOnMapVC
        
        // Initiate values with controller objects
        objVC.clLocationCoordinate = CLLocationCoordinate2DMake(Double(objLoyalty.Latitude!)!, Double(objLoyalty.Longitude!)!);
        objVC.locationTitle = objLoyalty.FirstName;
        objVC.locationSubTitle = objLoyalty.BranchDescription
        objVC.dealCategoryImage = objLoyalty.MapPinCategoryPicture;
        objVC.bonusPotLocationID = objLoyalty.BonusPotLocationID;
        objVC.branchID = objLoyalty.BranchID;

        self.addChildViewController(objVC)
        objVC.view.frame = self.view.bounds;
        self.tableView.addSubview(objVC.view)
        objVC.didMove(toParentViewController: self);
    }
    
    
    func searchDataWithEnrty(_ str: String?){
        
        if str != nil { //If data array empty
            
            let servicePredicate = NSPredicate(format: "SELF.FirstName CONTAINS[cd] %@", str!)
            
            let result = self.loyaltyListArray.filtered(using: servicePredicate)
            self.searchDataArray.removeAllObjects()
            self.searchDataArray.addObjects(from: result)
        }
        else{
            self.searchDataArray.removeAllObjects()
            self.searchDataArray.addObjects(from: self.loyaltyListArray as [AnyObject])
        }
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.loyaltyCheckInServiceCall(self.leftMenusSelectedFor, withParameters: self.selectedDict);
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        //self.searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        // searchBar.setShowsCancelButton(false, animated: true)
        //self.searchDataWithEnrty(nil);
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func callLoyaltyTVCFromLeftMenu(_ indexPath: IndexPath, categoriesIds: String = "") -> Void {
        
        print("indexPath = %@", indexPath);
        
        if (indexPath.section == 0 && indexPath.row == 0) {
            self.loyaltyCheckInServiceCall(SelectionFor.isSelectedFavorite, withParameters: nil)
        }
        else if (indexPath.section == 1 && indexPath.row == 0){
            self.callLocationManager();
        }
        else if (indexPath.section == 1 && indexPath.row == 1){
            self.loyaltyCheckInServiceCall(SelectionFor.isSelectedAToZ, withParameters: nil)
        }
        else if (indexPath.section == 2){
            
            let categoriesDict = NSDictionary.init(object: categoriesIds, forKey: "ID" as NSCopying);
            self.loyaltyCheckInServiceCall(SelectionFor.isSelectedProductGroup, withParameters: categoriesDict);
        }
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        self.navigationItem.backBarButtonItem?.title = "";
    }
    

}
