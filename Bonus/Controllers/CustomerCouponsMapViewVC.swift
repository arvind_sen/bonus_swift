//
//  CustomerCouponsMapViewVC.swift
//  Bonus
//
//  Created by Arvind Sen on 05/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import MapKit
import Alamofire

class CustomerCouponsMapViewVC: UIViewController , MKMapViewDelegate, UISearchBarDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var btnsContainerView: UIView!
    @IBOutlet weak var activeCouponsBtn: UIButton!
    @IBOutlet weak var moneyBtn: UIButton!
    @IBOutlet weak var couponHistoryBtn: UIButton!
    
    var revealVC : SWRevealViewController? = nil;
    var searchBar: UISearchBar!
    var listArray: NSMutableArray!
    var currentLocationCoordinate : CLLocationCoordinate2D? ;
    var selectedCategoriesFromLeftMenus : String?
    var selectedIndexPathFromLeftMenus : IndexPath?
    
    var isWebServiceCalled = false
    var leftMenusSelectedFor : SelectionFor!
    var selectedDict: NSDictionary!
    var lastOffset: String!
    var locationManager : CLLocationManager?
    var didSelectedRowObj: Loyalty!;
    var searchDataArray: NSMutableArray!
    var isMapViewRegionSet = false;
    @IBOutlet weak var mapView: MKMapView!
    
    var categoriesDict: NSDictionary? = nil;
    var callBackOnWalletVC : ((NSMutableArray)->Void)?
    var callBackWithTopButtons : ((UIButton)->Void)?
    var objMoneyVC: MoneyVC? = nil;
    var objCouponHistoryTVC: CouponHistoryTVC? = nil;
    var previouslySelectedBtn: UIButton? = nil;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // This code is using to hid left menus
        if (self.revealViewController() != nil) {
            //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        // Do any additional setup after loading the view.
        self.showTopBtns();
        self.showMapWithSpecifiedArea();
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.lightGray;
        self.navigationController?.isNavigationBarHidden = false;
        self.showDataOnMapView();
        self.showSelectedBtn();
    }
    
    func showTopBtns(){
        
        // Left toggle button configration
        self.revealVC = self.revealViewController()
        self.navigationController?.navigationBar.addGestureRecognizer(self.revealVC!.panGestureRecognizer())
        
        let revealButtonItem = UIBarButtonItem();
        revealButtonItem.image = UIImage (named: "FilterIcon")
        revealButtonItem.style = UIBarButtonItemStyle.plain;
        revealButtonItem.target = self;
        revealButtonItem.action = #selector(CustomerCouponsMapViewVC.revealToggle(_:));
        revealButtonItem.tintColor = UIColor.white;
        self.navigationItem.leftBarButtonItem = revealButtonItem;
        
        //Show search bar on top of the view controller
        
        let oY = CGFloat(5.0)
        let oX = CGFloat(50.0)
        
        let height = (self.navigationController?.navigationBar.frame.height)! - CGFloat(2 * oY);
        let width = (self.navigationController?.navigationBar.frame.width)! - CGFloat(2 * oX);
        //let height = CGFloat (44);
        //let width = self.view.frame.width;
        
        let frameRect = CGRect(x: 0.0, y: oY, width: width, height: height);
        self.searchBar = UISearchBar.init(frame: frameRect);
        self.searchBar.placeholder = "Search"
        self.searchBar.showsCancelButton = false;
        self.searchBar?.delegate = self;
        self.navigationItem.titleView = self.searchBar
        self.navigationItem.titleView?.frame = frameRect;
        //self.navigationItem.titleView?.backgroundColor = UIColor.redColor()
        
        // Right Top tab bar button configration
        var rightBtn = UIImage(named: "MenuIcon")
        rightBtn = rightBtn?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let barRightBtn = UIBarButtonItem(image: rightBtn, style: UIBarButtonItemStyle.plain, target: self, action:#selector(CustomerCouponsMapViewVC.rightBtnTapped(_:)));
        self.navigationItem.rightBarButtonItem = barRightBtn;
    }
    
    
    // Method will display map view with specified area
    func showMapWithSpecifiedArea(){
        self.isMapViewRegionSet = true;
        if(currentLocationCoordinate != nil){
            let location = CLLocationCoordinate2DMake((currentLocationCoordinate?.latitude)!, (currentLocationCoordinate?.longitude)!);
            let region = MKCoordinateRegionMakeWithDistance(location, 12500, 12500);
            // 12500 Meter = 12.5 KM
            self.mapView.setRegion(region, animated: true);
            
        }
        else{
            let location = CLLocationCoordinate2DMake(20.5937, 78.9629);
            let region = MKCoordinateRegionMakeWithDistance(location, 10000, 10000);
            // 12500 Meter = 12.5 KM
            self.mapView.setRegion(region, animated: true);
        }
    }
    
    // Method get call to get user current location lat, long
    func callLocationManager() {
        
        if (CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied) {
            
            self.locationManager = CLLocationManager.init();// locationManager need to define at the class variable then only its call delegate methods.
            self.locationManager!.delegate = self;
            
            if self.locationManager!.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
                self.locationManager!.requestWhenInUseAuthorization();
            }
            self.locationManager!.desiredAccuracy = kCLLocationAccuracyBest;
            self.locationManager!.startUpdatingLocation();
        }
        else{
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "To re-enable, please go to Settings and turn on Location Service for this app.", onView: self)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.locationManager!.stopUpdatingLocation();
        let recentLocation = locations.last;
        self.currentLocationCoordinate = recentLocation?.coordinate;
        
        let latString = String(format: "%f", (recentLocation?.coordinate.latitude)!);
        let longString = String(format: "%f", (recentLocation?.coordinate.longitude)!);
        
        let dict = NSDictionary.init(objects: [latString, longString], forKeys: ["Latitude" as NSCopying, "Longitude" as NSCopying]);
        
        self.callActiveCouponsWebService(SelectionFor.isSelectedNearBy, withParameters: dict, offSetValue: "0");
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(" failed to get current location");
        
        self.locationManager!.stopUpdatingLocation();
        let dict = NSDictionary.init(objects: ["0.0", "0.0"], forKeys: ["Latitude" as NSCopying, "Longitude" as NSCopying]);
        self.callActiveCouponsWebService(SelectionFor.isSelectedNearBy, withParameters: dict, offSetValue: "0");
    }
    
    func showDataOnMapView(){
        
        if (self.mapView.annotations.count > 0) {
            self.mapView.removeAnnotations(self.mapView.annotations);
        }
        self.mapView.showsUserLocation = true;
        
        var mutArray :Array = [MyCustomAnnotation]()
        var newUpdatedListArray = NSMutableArray();
        var index = 0;
        
        for obj in self.listArray  {
            
            let business = obj as! CustomerCoupon;
            if (business.Longitude != nil && business.Latitude != nil)
            {
                let latitude = business.Latitude;
                let longitude = business.Longitude;
                let location = CLLocation.init(latitude: Double(latitude!)!, longitude: Double(longitude!)!)
                
                let annotation = MyCustomAnnotation.init(title: business.FirstName!, coordinate:location.coordinate, subtitle:business.BranchDescription!);
                annotation.dealCategroyImage = business.MapPinCategoryPicture;
                //annotation.bonusPotLocationID = business.BonusPotLocationID;
                annotation.indexNumber = index;
                annotation.annotationNumber = NSInteger(business.BranchID!)!;
                mutArray.append(annotation);
                newUpdatedListArray.add(obj);
            }
            index += 1;
        }
        
        self.listArray.removeAllObjects();
        self.listArray = newUpdatedListArray;
        if (mutArray.count > 0) {
            self.mapView.addAnnotations(mutArray);
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if (annotation .isKind(of: MKUserLocation.self)) {
            return nil
        }
        
        if (annotation.isKind(of: MyCustomAnnotation.self)) {
            var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: "CustomPinAnnotation")
            pinView = nil;
            pinView?.isUserInteractionEnabled = true;
            
            if (pinView == nil) {
                pinView = MKAnnotationView.init(annotation: annotation, reuseIdentifier: "CustomPinAnnotation")
            }
            else{
                pinView!.annotation = annotation;
            }
            
            pinView?.canShowCallout = true;
            
            let otherPin = UIImage(named: "OthersPin")
            pinView?.centerOffset = CGPoint(x: 0, y: -((otherPin?.size.height)!/2))
            pinView?.image = otherPin;
            
            let myAnnotation = annotation as! MyCustomAnnotation
            
            let manager = SDWebImageManager.shared()
            
            _ = manager?.downloadImage(with: URL(string: myAnnotation.dealCategroyImage!), options: SDWebImageOptions.cacheMemoryOnly, progress: nil, completed: { (img, error, SDImageCacheType, bool, NSURL) in
                
                pinView!.image = img; // Setting pin image when downloaded from the server
                if (myAnnotation.bonusPotLocationID != nil && Int(myAnnotation.bonusPotLocationID!)! > 0) {
                    pinView!.image = UIImage(named:"MapIconYellow");
                }
            })
            
            // Because this is an iOS app, add the detail disclosure button to display details about the annotation in another view.
            let arrowImg = UIImage(named: "Arrow")
            let arrowFram = CGRect(x: 0, y: 0, width: 40.0, height: 40.0);
            let arrowBtn = UIButton.init(type: .custom);
            arrowBtn.frame = arrowFram;
            arrowBtn.setImage(arrowImg, for: UIControlState())
            arrowBtn.backgroundColor = UIColor.clear;
            pinView?.rightCalloutAccessoryView = arrowBtn;
            
            return pinView;
        }
        
        return nil;
    }
    
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        if (isMapViewRegionSet == true) {
            isMapViewRegionSet = false;
        }
        else{
            
            print("--- lat = \(mapView.centerCoordinate.latitude), long = \(mapView.centerCoordinate.longitude)");
            self.currentLocationCoordinate = mapView.centerCoordinate;
            
            let latString = String(format: "%f", (mapView.centerCoordinate.latitude));
            let longString = String(format: "%f", (mapView.centerCoordinate.longitude));
            
            let dict = NSDictionary.init(objects: [latString, longString], forKeys: ["Latitude" as NSCopying, "Longitude" as NSCopying]);
            
            self.callActiveCouponsWebService(leftMenusSelectedFor, withParameters: dict, offSetValue: "0");
            //isMapViewRegionSet = false;
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        let myCustAnn = view.annotation as! MyCustomAnnotation;
        let obj = self.listArray[myCustAnn.indexNumber!] as! CustomerCoupon;
        
        AppUtility.removedBackTitleAndChangeItsColor(VC: self, backTitle: "", backTintColor: UIColor.white);
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "CouponDetailVC") as! CouponDetailVC
        objVC.dealMasterId = obj.DealMasterID;
        self.navigationController?.pushViewController(objVC, animated: true);
    }
    
    func revealToggle(_ obj : AnyObject?){
        
        self.searchBar.resignFirstResponder();
        self.revealVC!.revealToggle(nil)
    }
    
    func callCouponsTVCFromLeftMenu(_ indexPath: IndexPath, categoriesIds: String = "") -> Void {
        
        print("indexPath = %@", indexPath);
        
        if (indexPath.section == 0 && indexPath.row == 0) {
            self.callActiveCouponsWebService(SelectionFor.isSelectedFavorite, withParameters: nil)
        }
        else if (indexPath.section == 1 && indexPath.row == 0){
            self.callLocationManager();
        }
        else if (indexPath.section == 1 && indexPath.row == 1){
            self.callActiveCouponsWebService(SelectionFor.isSelectedAToZ, withParameters: nil)
        }
        else if (indexPath.section == 2){
            
            self.categoriesDict = NSDictionary.init(object: categoriesIds, forKey: "ID" as NSCopying);
            self.callActiveCouponsWebService(SelectionFor.isSelectedProductGroup, withParameters: categoriesDict);
        }
    }
    
    func callActiveCouponsWebService(_ selectedSection: SelectionFor, withParameters: NSDictionary?, offSetValue: String = "0") -> Void {
        
        self.leftMenusSelectedFor = selectedSection;
        self.selectedDict = withParameters;
        self.lastOffset = offSetValue;
        
        var searchFor = "";
        
        if ((self.searchBar.text?.characters.count)! > 0) {
            searchFor = self.searchBar.text!;
        }
        
        let defaults: UserDefaults = UserDefaults.standard
        
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID!
        ///let userType = user.PersonType!;
        
        let uniqueID = AppUtility.getDeviceNSUUID();
        let postDataDict : NSMutableDictionary = ["CustomerUserID" : userId,
                                                  "IsViewHistory" : "0",
                                                  "SortBy" : "Distance",
                                                  "FavoriteStatus" : "0",
                                                  "DealMasterID" : "0",
                                                  "BusinessCategory" : "0",
                                                  "Latitude" : "0",
                                                  "Longitude" : "0",
                                                  "Limit" : "10",
                                                  "Offset" : offSetValue,
                                                  "OrderBy" : "ASC",
                                                  "SearchFor" : searchFor,
                                                  "api_key" : kWEB_SERVICES_API_KEY,
                                                  "DeviceUniqueID" : uniqueID];
        
        if (self.currentLocationCoordinate != nil) {
            let latString = String(format: "%f", self.currentLocationCoordinate!.latitude);
            let longString = String(format: "%f", self.currentLocationCoordinate!.longitude);
            
            postDataDict.setObject(latString, forKey: "Latitude" as NSCopying);
            postDataDict.setObject(longString, forKey: "Longitude" as NSCopying);
        }
        
        switch selectedSection {
        case .isSelectedFavorite:
            postDataDict.setObject("1", forKey: "FavoriteStatus" as NSCopying)
            break;
        case .isSelectedNearBy:
            postDataDict.setObject("ASC", forKey: "OrderBy" as NSCopying)
            break;
        case .isSelectedAToZ:
            postDataDict.setObject("FirstName", forKey: "SortBy" as NSCopying)
            postDataDict.setObject("ASC", forKey: "OrderBy" as NSCopying)
            break;
        default:
            if(self.categoriesDict != nil)
            {
                postDataDict.setObject(self.categoriesDict?.object(forKey: "ID")! as! String, forKey: "BusinessCategory" as NSCopying)
            }
        }
        
        postDataDict.setObject("1", forKey: "IsShowOnMap" as NSCopying);
        //postDataDict.setObject(user.PersonType!, forKey: "PersonType" as NSCopying);
        self.callActiveCoupons(postDataDict);
    }
    
    
    func callActiveCoupons(_ postData: NSDictionary){
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_CUSTOMER_COUPONS, method: HTTPMethod.post, parameters: postData as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        let resourcePath = dataDict?.object(forKey: "ResourcePath") as! Dictionary<String, AnyObject>
                        
                        if(Int(postData.object(forKey: "Offset") as! String)! == 0){
                            self.listArray = nil;
                        }
                        
                        if(self.listArray == nil){
                            self.listArray = NSMutableArray();
                        }
                        
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let item = CustomerCoupon(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>, resourcePaths: resourcePath)
                                
                                self.listArray?.add(item);
                            }
                        }
                        else
                        {
                            var message = (dataDict?.object(forKey: "Message"))! as! String;
                            
                            if (self.leftMenusSelectedFor == SelectionFor.isSelectedFavorite) {
                                message = "You do not have any Favourites yet...";
                            }
                            /*
                             else{
                             [AppUtility showAlertMessage:@"No data found for selected item"];
                             }
                             */
                            self.navigationController?.view.makeToast(message, duration: 2.0, position: .center)
                            //AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                        }
                        
                        DispatchQueue.main.async {
                            self.showDataOnMapView();
                            self.isMapViewRegionSet = false;
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        
                    }
                }
            }
            )
        }
    }
    
    // Method will call to open map view on current view controller
    func rightBtnTapped(_ sender: AnyObject){
        
        //print( "controllers = %@", self.navigationController?.viewControllers)
        
        let objVC = self.navigationController?.viewControllers.first as! WalletVC;
        //objVC.activeCouponsListArray = self.listArray;
        objVC.isMapViewComeBack = true;
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = "flip"
        transition.subtype = kCATransitionFromLeft
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        
        callBackOnWalletVC?(self.listArray)
        self.navigationController?.popViewController(animated: true);
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.callActiveCouponsWebService(self.leftMenusSelectedFor, withParameters: self.selectedDict);
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        //self.searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        // searchBar.setShowsCancelButton(false, animated: true)
        //self.searchDataWithEnrty(nil);
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func hideChildView(){
        
        for vc in self.childViewControllers{
            vc.view.removeFromSuperview()
            vc.removeFromParentViewController()
            vc.willMove(toParentViewController: nil)
        }
        self.objMoneyVC = nil;
        self.objCouponHistoryTVC = nil;
    }
    
    func showSelectedBtn(){
        
        if(self.previouslySelectedBtn?.tag == 895){
            self.activeCouponsBtn.setImage(kIMAGE_ACTIVE_COUPONS_YELLOW, for: .normal)
            self.moneyBtn.setImage(kIMAGE_MONEY_WHITE, for: .normal)
            self.couponHistoryBtn.setImage(kIMAGE_COUPONS_HISTORY_WHITE, for: .normal)
        }
        else if(self.previouslySelectedBtn?.tag == 896){
            self.activeCouponsBtn.setImage(kIMAGE_ACTIVE_COUPONS_WHITE, for: .normal)
            self.moneyBtn.setImage(kIMAGE_MONEY_YELLOW, for: .normal)
            self.couponHistoryBtn.setImage(kIMAGE_COUPONS_HISTORY_WHITE, for: .normal)
        }
        else if(self.previouslySelectedBtn?.tag == 897){
            self.activeCouponsBtn.setImage(kIMAGE_ACTIVE_COUPONS_WHITE, for: .normal)
            self.moneyBtn.setImage(kIMAGE_MONEY_WHITE, for: .normal)
            self.couponHistoryBtn.setImage(kIMAGE_COUPONS_HISTORY_YELLOW, for: .normal)
        }
    }
 
    @IBAction func topNavigationBtnTapped(_ sender: Any) {
        
        let btn = sender as! UIButton;
        
        if(self.previouslySelectedBtn?.tag != btn.tag)
        {
            self.callBackWithTopButtons!(btn);
            self.navigationController?.popViewController(animated: false);
            /*
            self.hideChildView(); //First remove any child view if have
            self.searchBar.isUserInteractionEnabled = true;
            self.navigationController?.navigationBar.isUserInteractionEnabled = true;
            if(btn.tag == 895)
            {
                self.activeCouponsBtn.setImage(kIMAGE_ACTIVE_COUPONS_YELLOW, for: .normal)
                self.moneyBtn.setImage(kIMAGE_MONEY_WHITE, for: .normal)
                self.couponHistoryBtn.setImage(kIMAGE_COUPONS_HISTORY_WHITE, for: .normal)
                
                
            }
            else if(btn.tag == 896){
                self.activeCouponsBtn.setImage(kIMAGE_ACTIVE_COUPONS_WHITE, for: .normal)
                self.moneyBtn.setImage(kIMAGE_MONEY_YELLOW, for: .normal)
                self.couponHistoryBtn.setImage(kIMAGE_COUPONS_HISTORY_WHITE, for: .normal)
                
                if(self.objMoneyVC == nil)
                {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    self.objMoneyVC = storyboard.instantiateViewController(withIdentifier: "MoneyVC") as? MoneyVC
                    
                    let oY = self.btnsContainerView.frame.origin.y + self.btnsContainerView.frame.size.height
                    let height = self.mapView.frame.height;
                    self.objMoneyVC?.view.frame = CGRect(x: 0.0, y: Double(oY), width: Double(self.mapView.frame.width), height: Double(height));
                    self.view.addSubview((self.objMoneyVC?.view)!)
                    self.addChildViewController((self.objMoneyVC)!)
                    self.objMoneyVC?.didMove(toParentViewController: self);
                    
                    self.searchBar.isUserInteractionEnabled = false;
                    self.navigationController?.navigationBar.isUserInteractionEnabled = false;
                }
            }
            else if(btn.tag == 897){
                self.activeCouponsBtn.setImage(kIMAGE_ACTIVE_COUPONS_WHITE, for: .normal)
                self.moneyBtn.setImage(kIMAGE_MONEY_WHITE, for: .normal)
                self.couponHistoryBtn.setImage(kIMAGE_COUPONS_HISTORY_YELLOW, for: .normal)
                
                if(self.objCouponHistoryTVC == nil)
                {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    self.objCouponHistoryTVC = storyboard.instantiateViewController(withIdentifier: "CouponHistoryTVC") as? CouponHistoryTVC
                    
                    let oY = self.btnsContainerView.frame.origin.y + self.btnsContainerView.frame.size.height
                    let height = self.mapView.frame.height;
                    self.objCouponHistoryTVC?.view.frame = CGRect(x: 0.0, y: Double(oY), width: Double(self.mapView.frame.width), height: Double(height));
                    self.view.addSubview((self.objCouponHistoryTVC?.view)!)
                    self.addChildViewController((self.objCouponHistoryTVC)!)
                    self.objCouponHistoryTVC?.didMove(toParentViewController: self);
                    self.callLocationManager(); // Call web service to display coupons History data
                }
            }
            self.previouslySelectedBtn = btn;
            */
        }
    }
    
}
