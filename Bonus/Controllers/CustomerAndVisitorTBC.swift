//
//  CustomerAndVisitorTBC.swift
//  Bonus
//
//  Created by Arvind Sen on 30/08/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit

class CustomerAndVisitorTBC: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tabBar.barTintColor = TAB_BAR_BG_COLOR;
        self.delegate = self;
        
        // Account image setup
        let item1 = self.tabBar.items![0]
        item1.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_WHITE], for: UIControlState())
        item1.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_YELLOW], for: .selected)
        item1.selectedImage = UIImage(named: "PersonAccountYellow")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        item1.image = UIImage(named: "PersonAccount")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
   
        // Wallet image setup
        let item2 = self.tabBar.items![1]
        item2.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_WHITE], for: UIControlState())
        item2.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_YELLOW], for: .selected)
        item2.selectedImage = UIImage(named: "Walletyellow")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        item2.image = UIImage(named: "Wallet")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        // Invite image setup
        let item3 = self.tabBar.items![2]
        item3.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_WHITE], for: UIControlState())
        item3.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_YELLOW], for: .selected)
        item3.selectedImage = UIImage(named: "YellowInvite")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        item3.image = UIImage(named: "invite")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        // Loyalty image setup
        let item4 = self.tabBar.items![3]
        item4.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_WHITE], for: UIControlState())
        item4.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_YELLOW], for: .selected)
        item4.selectedImage = UIImage(named: "YellowLoyalty")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        item4.image = UIImage(named: "Loyalty")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        // Coupons image setup
        let item5 = self.tabBar.items![4]
        item5.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_WHITE], for: UIControlState())
        item5.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_YELLOW], for: .selected)
        item5.selectedImage = UIImage(named: "YellowCoupons")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        item5.image = UIImage(named: "coupons")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        
        
        let defaults = UserDefaults.standard
        
        if(defaults.value(forKey: "globalUserType") as! String == USER_TYPE_VISITOR){
            item1.isEnabled = false;
            item2.isEnabled = false;
            item3.isEnabled = false;
            item4.isEnabled = false;
            
            item5.selectedImage = UIImage(named: "ReturnBack")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            //item5.image = UIImage(named: "coupons")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        }
        //self.tabBarController?. = 4;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        let defaults = UserDefaults.standard;
        let userType = defaults.value(forKey: "globalUserType") as! String
        // This code is using to hid left menus
        if (tabBarController.selectedIndex == 0 && self.revealViewController() != nil) {
            
            let value = self.revealViewController().frontViewPosition
            if(value == FrontViewPosition.right){
                self.revealViewController().revealToggle(animated: true);
            }
        }
        else if (tabBarController.selectedIndex == 1) {
            
            let navCont = tabBarController.selectedViewController as! UINavigationController;
            
        }
        else if (tabBarController.selectedIndex == 4 && userType == USER_TYPE_VISITOR){
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.logoutFromTheSessionForAllUsers();
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
