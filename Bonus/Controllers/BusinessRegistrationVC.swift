//
//  BusinessRegistrationVC.swift
//  Bonus
//
//  Created by Arvind Sen on 23/08/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit
import Alamofire
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class BusinessRegistrationVC: UIViewController, InvitedFromDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var termsAndConditionBtn: UIButton!
    @IBOutlet weak var checkMarkBtn: UIButton!
    @IBOutlet weak var nameTxtF: UITextField!
    @IBOutlet weak var emailAddressTxtF: UITextField!
    @IBOutlet weak var passwordTxtF: UITextField!
    @IBOutlet weak var countryCodeTxtF: UITextField!
    @IBOutlet weak var timeZoneTxtF: UITextField!
    @IBOutlet weak var referalCodeTxtF: UITextField!
    
    var focusedControl : UITextField?
    var isAgreedOnTermsAndConditions = false;
    var invitesListArray = NSMutableArray()
    var invitedByUser: Invite?
    
    @IBOutlet weak var registrationBtn: UIButton!
    
    var objSelectedCountry : Country? = nil;
    var objCountriesArray : NSMutableArray? = nil;
    var objSelectedCountryTimeZone : CountryTimeZone? = nil;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // Added keypad notifications
        NotificationCenter.default.addObserver(self, selector: #selector(CustomerRegistrationVC.keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CustomerRegistrationVC.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.getAllCountries();
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if (CURRENT_DEVICE_SIZE.height <= 568){
            self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height + 80);
        }
    }
    
    func keyboardWasShown(_ aNotification: Notification){
        
        let info:NSDictionary = aNotification.userInfo! as NSDictionary;
        //let kbSize = (info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue.size;
        var kbSize = (info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue.size;
        if(kbSize.height <= 0.0){
            kbSize = (info.object(forKey: UIKeyboardFrameEndUserInfoKey) as AnyObject).cgRectValue.size;
        }
        
        let contentInsets = UIEdgeInsetsMake(0, 0.0, kbSize.height, 0.0);
        scrollView.contentInset = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        var aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!aRect.contains(self.focusedControl!.frame.origin) ) {
            let scrollPoint = CGPoint(x: 0.0, y: focusedControl!.frame.origin.y-kbSize.height);
            self.scrollView.setContentOffset(scrollPoint, animated: true);
        }
    }
    
    func keyboardWillBeHidden(_ aNotification:Notification){
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
            self.scrollView.contentInset = contentInsets;
            self.scrollView.scrollIndicatorInsets = contentInsets;
            // [scrollView setContentOffset:CGPointZero animated:YES];
        }) 
    }

    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        //NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardDidHideNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getAllCountries() {
        
        let dataArray = BonusLocalDBHandler.getAllCountriesDetail() as! NSMutableArray;
        
        if(dataArray.count > 0)
        {
            self.objCountriesArray = dataArray;
        }
        else{
            // Created postDataDictonary to post the data on server
            let postDataDict : NSDictionary = ["ID": "0",
                                               "SortBy" : "CountryName",
                                               "OrderBy": "ASC",
                                               "api_key": kWEB_SERVICES_API_KEY];
            
            if !AppUtility.isNetworkAvailable(){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
            }
            else{
                
                CustomProgressView.showProgressIndicator();
                Alamofire.request(kSERVICE_COUNTRY_LIST, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                    CustomProgressView.hideProgressIndicator();
                    
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                    print("value =  \(datastring)");
                    
                    if let JSON = response.result.value
                    {
                        print("JSON: \(JSON)")
                        let dataDict = JSON as? NSDictionary;
                        
                        if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                            
                            let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                            
                            let countryArray = NSMutableArray();
                            if(dictArray.count > 0)
                            {
                                for item in dictArray {
                                    let item = Country(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>)
                                    
                                    countryArray.add(item);
                                }
                                
                                // Save all the categories in local database
                                BonusLocalDBHandler.saveAllCountriesDetail(countriesArray: countryArray);
                            }
                            
                        }
                        
                    }
                }
                )
            }
        }
    }
    
    @IBAction func registrationBtnTapped(_ sender: AnyObject) {
        
        let name : String = self.nameTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let emailId : String = self.emailAddressTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let password : String = self.passwordTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        var refCodeString : String = self.referalCodeTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        self.nameTxtF.text = name;
        self.emailAddressTxtF.text = emailId;
        self.passwordTxtF.text = password;
        
        let isValidEmail = AppUtility.validateEmail(emailId);
        
        var message : String? = nil;
        
        if name.isEmpty {
            message = "Please enter your name";
        }
        else if !isValidEmail {
            message = LOGIN_ALERT_MESSAGE_EMAIL_ID;
        }
        else if password.isEmpty {
            message = LOGIN_ALERT_MESSAGE_EMPTY_PASSWORD;
        }
        else if (password.characters.count < 6 || password.characters.count > 15) {
            message = PLEASE_ENTER_ATLEAST_SIX_CHARACTORS;
        }
        else if (self.objSelectedCountry == nil) {
            
            message = "Please select your country";
        }
        else if ((self.invitesListArray.count > 0) && (self.invitedByUser == nil)) {
            message = "Please select an Invited by";
        }
        else if (isAgreedOnTermsAndConditions == false) {
            message = "Please accept terms and conditions.";
        }
        
        if message != nil {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
        }
        else {
            if refCodeString.isEmpty {
                refCodeString = "";
            }
            
            var referalUserId = "0"
            var invitationId = "0"
            
            if (self.invitedByUser != nil) {
                referalUserId = (self.invitedByUser?.UserID)!;
                invitationId = (self.invitedByUser?.ID)!;
            }
            
            // Created postDataDictonary to post the data on server
            let postDataDict : NSDictionary = ["Name": name,
                                               "EmailID": emailId,
                                               "Password": password,
                                               "PersonType": USER_TYPE_BUSINESS,
                                               "CountryId": (self.objSelectedCountry?.ID)!,
                                               "ReferalUserID": referalUserId,
                                               "InvitationID": invitationId,
                                               "JoiningCode": refCodeString,
                                               "TimeZoneName": (self.objSelectedCountryTimeZone?.TimeZone)!,
                                               "api_key": kWEB_SERVICES_API_KEY];
            
            print(postDataDict);
            
            let alertMessage = "Please confirm your Country of Operation. Once confirmed, your Country cannot be changed. \n '\(self.countryCodeTxtF.text!)'"
            let alert = UIAlertController(title: alertMessage, message: nil, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                self.registerBusinessUserServiceCall(postDataDict: postDataDict);
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil));
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func registerBusinessUserServiceCall(postDataDict: NSDictionary){
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_REGISTRATION, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                            self.navigationController?.popToRootViewController(animated: true);
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Pending") {
                        
                        let message = dataDict?.object(forKey: "Message")! as! String;
                        
                        let alert = UIAlertController(title: message, message: "Do you want resend verification mail?" as String, preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                            self.resendVerificationEmailToUser(dataDict?.object(forKey: "Response")! as! NSDictionary);
                        }))
                        
                        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil));
                        self.present(alert, animated: true, completion: nil)
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        if (message.range(of: "already").location != NSNotFound){
                            let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil));
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        }
                    }
                }
            }
            )
        }
    }
    
    // Method get called to resend registration varification email
    func resendVerificationEmailToUser(_ dataDict: NSDictionary)
    {
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["FirstName": dataDict.object(forKey: "FirstName")!, "EmailID": dataDict.object(forKey: "EmailID")!, "ID": dataDict.object(forKey: "ID")!,                                               "api_key": kWEB_SERVICES_API_KEY];
        
        print(postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_USER_RESEND_ACTIVATION_LINK, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                            self.navigationController?.popToRootViewController(animated: true);
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Pending") {
                        
                        let message = dataDict?.object(forKey: "Message")! as! String;
                        
                        let alert = UIAlertController(title: message, message: "Do you want resend verification mail?" as String, preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                            
                            self.resendVerificationEmailToUser((dataDict?.object(forKey: "Status"))! as! NSDictionary);
                        }))
                        
                        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil));
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Mailsend") {
                        
                        let message = dataDict?.object(forKey: "Message")! as! String;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        if (message.range(of: "already").location != NSNotFound){
                            
                            let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                                //self.logoutFromThePreviousSession();
                            }))
                            
                            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil));
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        }
                    }
                }
                }
            )
        }
    }
    
    // Method will call to get all the invities
    func getInvitesListForEmailAddress() -> Void {
        
        let emailId : String = self.emailAddressTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        self.emailAddressTxtF.text = emailId;
        
        let isValidEmail = AppUtility.validateEmail(emailId);
        
        var message : String? = nil;
        
        if !isValidEmail {
            message = LOGIN_ALERT_MESSAGE_EMAIL_ID;
        }
        
        if message != nil {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
        }
        else {
            
            // Created postDataDictonary to post the data on server
            let postDataDict : NSDictionary = ["EmailID": emailId,
                                               "api_key": kWEB_SERVICES_API_KEY];
            
            if !AppUtility.isNetworkAvailable(){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
            }
            else{
                
                CustomProgressView.showProgressIndicator();
                Alamofire.request(kSERVICE_INVITES_LIST, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                    CustomProgressView.hideProgressIndicator();
                    
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                    print("value =  \(datastring)");
                    
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                        let dataDict = JSON as? NSDictionary;
                        
                        if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                            
                            let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                            
                            self.invitesListArray.removeAllObjects();
                            if(dictArray.count > 0)
                            {
                                for item in dictArray {
                                    let item = Invite(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>)
                                    self.invitesListArray.add(item);
                                }
                            }
                            
                            self.showInvitiesTextField();
                        }
                        else
                        {
                            let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                            
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                            
                        }
                    }
                    }
                )
            }
        }
    }
    
    // Method get call to show invities list
    func showInvitiesTextField() {
        
        if (self.invitesListArray.count > 0) {
            
            if (self.invitesListArray.count > 0) {
                
                if self.referalCodeTxtF.alpha == 1 {
                }
                else{
                    self.referalCodeTxtF.alpha = 1;
                    self.updateFramsForRegisterBtnAndTermsConditions();
                }
            }
            else{
                if (referalCodeTxtF.alpha == 1) {// If alpha is already one then call only call otherwise no need to call
                    referalCodeTxtF.alpha = 0;
                    self.updateFramsForRegisterBtnAndTermsConditions();
                }
                referalCodeTxtF.alpha = 0;
            }
        }
    }
    
    func updateFramsForRegisterBtnAndTermsConditions(){
        
        let difference : CGFloat = 35.0;
        
        if (referalCodeTxtF.alpha == 1) {
            
            UIView.animate(withDuration: ANIMATION_DURATION_VIEW_SLIDING, animations: {
                
                self.checkMarkBtn.frame = CGRect(x: self.checkMarkBtn.frame.origin.x, y: self.checkMarkBtn.frame.origin.y + difference, width: self.checkMarkBtn.frame.size.width, height: self.checkMarkBtn.frame.size.height);
                
                self.termsAndConditionBtn.frame = CGRect(x: self.termsAndConditionBtn.frame.origin.x, y: self.termsAndConditionBtn.frame.origin.y + difference, width: self.termsAndConditionBtn.frame.size.width, height: self.termsAndConditionBtn.frame.size.height);
                
                self.registrationBtn.frame = CGRect(x: self.registrationBtn.frame.origin.x, y: self.registrationBtn.frame.origin.y + difference, width: self.registrationBtn.frame.size.width, height: self.registrationBtn.frame.size.height);
            })
        }
        else{
            UIView.animate(withDuration: ANIMATION_DURATION_VIEW_SLIDING, animations: {
                
                self.checkMarkBtn.frame = CGRect(x: self.checkMarkBtn.frame.origin.x, y: self.checkMarkBtn.frame.origin.y - difference, width: self.checkMarkBtn.frame.size.width, height: self.checkMarkBtn.frame.size.height);
                
                self.termsAndConditionBtn.frame = CGRect(x: self.termsAndConditionBtn.frame.origin.x, y: self.termsAndConditionBtn.frame.origin.y - difference, width: self.termsAndConditionBtn.frame.size.width, height: self.termsAndConditionBtn.frame.size.height);
                
                self.registrationBtn.frame = CGRect(x: self.registrationBtn.frame.origin.x, y: self.registrationBtn.frame.origin.y - difference, width: self.registrationBtn.frame.size.width, height: self.registrationBtn.frame.size.height);
                
            })
        }
    }
    
    // MARK:- TextField delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if (textField == emailAddressTxtF) {
            self.getInvitesListForEmailAddress();
        }
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        self.focusedControl = textField;
        var returnValue = true
        
        if(textField == self.countryCodeTxtF){
            returnValue = false;
            self.countryCodeTxtF.resignFirstResponder();
            self.objCountriesArray = BonusLocalDBHandler.getAllCountriesDetail() as? NSMutableArray;
            if (self.objCountriesArray != nil && self.objCountriesArray?.count > 0) {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyboard.instantiateViewController(withIdentifier: "CountriesVC") as! CountriesVC
                objVC.dataArray = self.objCountriesArray!;
                objVC.callBackWithSelectedCountry = { county in
                    self.objSelectedCountry = county
                    self.countryCodeTxtF.text = county.CountryName!;
                    self.callCountryTimeZones(countryCode: county.CountryCode!);
                }
                
                objVC.view.frame = self.view.bounds;
                self.view.addSubview(objVC.view)
                self.addChildViewController(objVC)
                objVC.didMove(toParentViewController: self);
                objVC.showTableView();
            }
        }
        else if(textField == self.referalCodeTxtF)
        {
            returnValue = false;
            self.referalCodeTxtF.resignFirstResponder();
            
            if (self.invitesListArray.count > 0)
            {
                // Display table code will be here
                
                self.showInvitedFromPopup();
            }
        }
        
        if (returnValue == false)
        {
            emailAddressTxtF.resignFirstResponder();
            passwordTxtF.resignFirstResponder();
            referalCodeTxtF.resignFirstResponder();
        }
        return returnValue;
    }
    
    func callCountryTimeZones(countryCode : String){
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["CountryCode": countryCode,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_SELECT_TIME_ZONE_LIST, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        
                        let countryTimeZonesArray = NSMutableArray();
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let item = CountryTimeZone(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>)
                                
                                countryTimeZonesArray.add(item);
                            }
                            
                            self.showTimeZonesTableView(timeZones: countryTimeZonesArray);
                        }
                        
                    }
                    
                }
            }
            )
        }
    }
    
    func showTimeZonesTableView(timeZones: NSMutableArray){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "CountryTimeZoneVC") as! CountryTimeZoneVC
        objVC.dataArray = timeZones;
        objVC.callBackWithSelectedCountryTimeZone = { countyzones in
            self.objSelectedCountryTimeZone = countyzones
            self.timeZoneTxtF.text = countyzones.TimeZone!;
        }
        
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        self.addChildViewController(objVC)
        objVC.didMove(toParentViewController: self);
        objVC.showTableView();
    }
    
    func showInvitedFromPopup(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "InvitedFromVC") as! InvitedFromVC
        objVC.invitedDelegate = self;
        objVC.dataArray = self.invitesListArray;
        objVC.modalPresentationStyle = .overCurrentContext;
        objVC.modalTransitionStyle = .crossDissolve;
        present(objVC, animated: true) {
            //objVC
            objVC.showTableView();
        }
    }
    
    func dismissInvitedFromVC(_ vc: UIViewController, selectedObj: AnyObject?) {
        
        if (selectedObj == nil) {
            
        }
        else{
            invitedByUser = selectedObj as? Invite;
            self.referalCodeTxtF.text = invitedByUser?.FirstName;
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField == self.nameTxtF)
        {
            self.emailAddressTxtF.becomeFirstResponder();
        }
        else if (textField == self.emailAddressTxtF)
        {
            self.passwordTxtF.becomeFirstResponder();
        }
        else if (textField == self.passwordTxtF)
        {
            self.passwordTxtF.resignFirstResponder();
        }
        
        return true;
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func checkMarkBtnTapped(_ sender: AnyObject) {
        let btn = sender as? UIButton;
        
        if (isAgreedOnTermsAndConditions == false) {
            btn?.setImage(K_RIGHT_YELLOW, for: UIControlState())
            isAgreedOnTermsAndConditions = true;
        }
        else{
            btn?.setImage(K_RIGHT_YELLOW_EMPTY, for: UIControlState())
            isAgreedOnTermsAndConditions = false;
        }
    }
    
    @IBAction func termsAndConditionBtnTapped(_ sender: AnyObject) {
        //UIApplication.shared.open(URL(string: kTERMS_OF_USE)!, options: nil, completionHandler: nil)
        
        UIApplication.shared.openURL(URL(string: kTERMS_OF_USE)!)
    }
    
    @IBAction func backBtnTapped(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true);
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
