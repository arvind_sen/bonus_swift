//
//  AddEditABranchVC.swift
//  Bonus
//
//  Created by Arvind Sen on 18/11/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//
import MapKit
import Alamofire
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


class AddEditABranchVC: UIViewController, UIScrollViewDelegate, MKMapViewDelegate, CLLocationManagerDelegate, DismissPopupVCProtocol {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mainViewContainer: UIView!
    @IBOutlet weak internal var emailTxtFContainerView: UIView!
    
    @IBOutlet weak internal var pwdTxtFContainerView: UIView!
    @IBOutlet weak var passwordTxtF: UITextField!
    @IBOutlet weak var emailTxtF: UITextField!
    @IBOutlet weak var checkMarkBtn: UIButton!
    @IBOutlet weak var businessPin1: UITextField!
    @IBOutlet weak var businessPin2: UITextField!
    @IBOutlet weak var businessPin3: UITextField!
    @IBOutlet weak var businessPin4: UITextField!
    @IBOutlet weak var branchNameTxtF: UITextField!
    @IBOutlet weak var areaNameTxtF: UITextField!
    @IBOutlet weak var telephoneNoTxtF: UITextField!
    @IBOutlet weak var addressTxtF: UITextField!
    @IBOutlet weak var addressMapView: MKMapView!
    @IBOutlet weak var mapViewHeightConstraint: NSLayoutConstraint!
    var locationCoordinate : CLLocationCoordinate2D? ;
    var pinAnnotationView:MKPinAnnotationView!
    var isMapViewSizeIncreased = false;
    var isAddAsMainBranch = false;
    var isHavingMainBranch = false;
    var branchObjToEdit : Branch!
    var hud : MBProgressHUD!
    var locationManager : CLLocationManager?
    var focusedControl : UITextField?
    
    var scrollViewInitialEdgeInsets: UIEdgeInsets!
    var scrollViewInitialContentOffset: CGPoint!
    var titleOfScreen = "Add Branch"
    var saveBtnTitle = "Save"
    var selectedPencil: UIButton!;
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.setAddressGoBtn();
        AppUtility.navigationColorAndItsTextColor(VC: self);
        
        NotificationCenter.default.addObserver(self, selector: #selector(BusinessAccountVC.keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BusinessAccountVC.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        //self.navigationController?.navigationBar.barTintColor = TAB_BAR_BG_COLOR;
        self.title = titleOfScreen;
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white];
        self.setRightTopBarBtn();
        
        self.configureInputAccesseryViewForInputFields();
        
        if (self.isHavingMainBranch == true) { // Already having main branch then gray the checkmark
            self.checkMarkBtn.isUserInteractionEnabled = false;
            self.checkMarkBtn.setImage(K_GRAY_RIGHT_EMPTY, for: UIControlState())
        }
        
        self.emailTxtFContainerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.emailTxtFContainerView.layer.borderWidth = 0.5;
        self.emailTxtFContainerView.clipsToBounds = true;
        self.emailTxtFContainerView.layer.cornerRadius = 5.0;
        
        self.pwdTxtFContainerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.pwdTxtFContainerView.layer.borderWidth = 0.5;
        self.pwdTxtFContainerView.clipsToBounds = true;
        self.pwdTxtFContainerView.layer.cornerRadius = 5.0;
    }
    
    func configureInputAccesseryViewForInputFields(){
        //Configured in viewDidLoad()
        var accessoryDoneButton: UIBarButtonItem!
        let accessoryToolBar = UIToolbar(frame: CGRect(x : 0, y : 0, width : UIScreen.main.bounds.width, height : 44))
        accessoryDoneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.accessoryViewDoneBtnPressed(_:)))
        let accessroySpace = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil);
        accessoryDoneButton.tintColor = UIColor.darkGray;
        accessoryToolBar.items = [accessroySpace, accessoryDoneButton]
        
        self.businessPin1.inputAccessoryView = accessoryToolBar
        self.businessPin2.inputAccessoryView = accessoryToolBar
        self.businessPin3.inputAccessoryView = accessoryToolBar
        self.businessPin4.inputAccessoryView = accessoryToolBar
        self.telephoneNoTxtF.inputAccessoryView = accessoryToolBar
    }
    
    func accessoryViewDoneBtnPressed(_ sender: UIButton){
        self.businessPin1.resignFirstResponder();
        self.businessPin2.resignFirstResponder();
        self.businessPin3.resignFirstResponder();
        self.businessPin4.resignFirstResponder();
        self.telephoneNoTxtF.resignFirstResponder();
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true);
        //let contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height: 800);
        //self.scrollViewInitialEdgeInsets
        scrollViewInitialEdgeInsets = self.scrollView.contentInset;
        self.scrollViewInitialContentOffset = self.scrollView.contentOffset;
        
        self.emailTxtF.isUserInteractionEnabled = true;
        self.passwordTxtF.isUserInteractionEnabled = true;
        self.businessPin1.isUserInteractionEnabled = true;
        self.businessPin2.isUserInteractionEnabled = true;
        self.businessPin3.isUserInteractionEnabled = true;
        self.businessPin4.isUserInteractionEnabled = true;
        
        if (self.branchObjToEdit != nil) {
            let lat = Double(self.branchObjToEdit.Latitude!);
            let long = Double(self.branchObjToEdit.Longitude!);
            let coordinate = CLLocationCoordinate2DMake(lat!, long!)
            self.locationCoordinate = coordinate;
            
            self.emailTxtF.isUserInteractionEnabled = false;
            self.passwordTxtF.isUserInteractionEnabled = false;
            self.businessPin1.isUserInteractionEnabled = false;
            self.businessPin2.isUserInteractionEnabled = false;
            self.businessPin3.isUserInteractionEnabled = false;
            self.businessPin4.isUserInteractionEnabled = false;
            
            self.displayLargeMapView(nil);
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
        
        if (self.branchObjToEdit != nil) {
            self.showDataInFieldsToUpdate();
        }
    }
    
    func showDataInFieldsToUpdate() -> Void {
        
        self.title = "Edit Branch"
        self.checkMarkBtn.isUserInteractionEnabled = false;
        if(self.branchObjToEdit.IsMainBranch == 1){
            self.checkMarkBtn.isUserInteractionEnabled = true;
            self.checkMarkBtn.setImage(K_RIGHT_YELLOW, for: UIControlState())
            self.isAddAsMainBranch = true
        }
        else{
            if (self.isHavingMainBranch == true) { // Already having main branch then gray the checkmark
                self.checkMarkBtn.isUserInteractionEnabled = false;
                self.checkMarkBtn.setImage(K_GRAY_RIGHT_EMPTY, for: UIControlState())
            }
            else{
                 self.checkMarkBtn.isUserInteractionEnabled = true;
            }
        }
        
        self.addressTxtF.text = self.branchObjToEdit.Address;
        self.areaNameTxtF.text = self.branchObjToEdit.AreaName;
        self.telephoneNoTxtF.text = self.branchObjToEdit.PhoneNumber ;
        self.emailTxtF.text = self.branchObjToEdit.Email;
        self.branchNameTxtF.text = self.branchObjToEdit.BranchDescription;
        
        let pinString = self.branchObjToEdit.Cbpin;
        
        if(pinString?.characters.count > 0){
            
            self.businessPin1.text = (pinString?[(pinString?.startIndex)!...(pinString?.characters.index((pinString?.startIndex)!, offsetBy: 0))!])! as String;
            self.businessPin2.text = (pinString?[(pinString?.characters.index((pinString?.startIndex)!, offsetBy: 1))!...(pinString?.characters.index((pinString?.startIndex)!, offsetBy: 1))!])! as String;
            self.businessPin3.text = (pinString?[(pinString?.characters.index((pinString?.startIndex)!, offsetBy: 2))!...(pinString?.characters.index((pinString?.startIndex)!, offsetBy: 2))!])! as String;
            self.businessPin4.text = (pinString?[(pinString?.characters.index((pinString?.startIndex)!, offsetBy: 3))!...(pinString?.characters.index((pinString?.startIndex)!, offsetBy: 3))!])! as String;
        }
        
        if (self.branchObjToEdit.Password != nil) {
            self.passwordTxtF.text = self.branchObjToEdit.Password;
        }
    }
    
    func setRightTopBarBtn(){
        
        let rightBtn = UIBarButtonItem.init(title: "Save", style: UIBarButtonItemStyle.done, target: self, action: #selector(AddEditABranchVC.saveBtnTapped))
        self.navigationItem.rightBarButtonItem = rightBtn;
    }
    
    func keyboardWasShown(_ aNotification: Notification){
        
        let info:NSDictionary = aNotification.userInfo! as NSDictionary;
        //let kbSize = (info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue.size;
        var kbSize = (info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue.size;
        if(kbSize.height <= 0.0){
            kbSize = (info.object(forKey: UIKeyboardFrameEndUserInfoKey) as AnyObject).cgRectValue.size;
        }
        
        let contentInsets = UIEdgeInsetsMake(0, 0.0, kbSize.height, 0.0);
        scrollView.contentInset = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        var aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        
        if(self.focusedControl != nil){
            if (!aRect.contains(self.focusedControl!.frame.origin) ) {
                let scrollPoint = CGPoint(x: 0.0, y: focusedControl!.frame.origin.y-kbSize.height);
                self.scrollView.setContentOffset(scrollPoint, animated: true);
            }
        }
    }
    
    func keyboardWillBeHidden(_ aNotification:Notification){
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            //let contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
            if(self.scrollViewInitialEdgeInsets != nil)
            {
                self.scrollView.contentInset = self.scrollViewInitialEdgeInsets;
            }
            //self.scrollView.contentOffset = self.scrollViewInitialContentOffset;
            //self.scrollView.scrollIndicatorInsets = contentInsets;
            // [scrollView setContentOffset:CGPointZero animated:YES];
        }) 
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func setAddressGoBtn(){
        
        let pinH: CGFloat = 30;
        let pinW: CGFloat = 35;
        
        let goBtn = UIButton.init(type: .custom);
        goBtn.frame = CGRect(x: 0, y: 0, width: pinW, height: pinH);
        goBtn.setTitle("Go", for: UIControlState())
        goBtn.setTitleColor(TAB_BAR_BG_COLOR, for: UIControlState())
        goBtn.titleLabel?.font = UIFont.systemFont(ofSize: 15.0);
        goBtn.backgroundColor = TEXT_FIELD_BORDER_COLOR;
        goBtn.addTarget(self, action: #selector(AddEditABranchVC.goBtnTapped), for: UIControlEvents.touchUpInside);
        self.addressTxtF.rightViewMode = UITextFieldViewMode.always;
        self.addressTxtF.rightView = goBtn;
        self.addressMapView.layer.borderColor =  GRAY_CUSTOM_BAR_AND_SEPRATOR_BG_COLOR.cgColor;
        
        if (CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied) {
            
            self.locationManager = CLLocationManager.init();// locationManager need to define at the class variable then only its call delegate methods.
            self.locationManager!.delegate = self;
            if self.locationManager!.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
                self.locationManager!.requestWhenInUseAuthorization();
            }
           self.locationManager!.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
            self.locationManager!.startUpdatingLocation();
        }
        else{
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "To re-enable, please go to Settings and turn on Location Service for this app.", onView: self)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.locationManager!.stopUpdatingLocation();
        let recentLocation = locations.last;
        self.locationCoordinate = recentLocation?.coordinate;
        
        let region = MKCoordinateRegionMakeWithDistance(self.locationCoordinate!, 800, 800);
        self.addressMapView.setRegion(region, animated: true);
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        self.locationCoordinate = CLLocationCoordinate2DMake(20.5937, 78.9629);
        print(" failed to get current location");
        self.locationManager!.stopUpdatingLocation();
    }
    
    func goBtnTapped(){
        
        self.addressTxtF.resignFirstResponder();
        self.addressTxtF.text = self.addressTxtF.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        self.areaNameTxtF.text = self.areaNameTxtF.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        var alertMessage = "";
        var searchAddress = "";
        
        if (self.addressTxtF.text?.characters.count == 0){
            if (self.areaNameTxtF.text?.characters.count == 0) {
                alertMessage = "Please enter pin address";
            }
            else{
                searchAddress = self.areaNameTxtF.text!;
            }
        }
        else{
            searchAddress = addressTxtF.text!;
        }
        
        // Show alert if not entered area name or address
        if (alertMessage.characters.count > 0) {
           AppUtility.showAlertWithTitleAndMessage(alertMessage, alertMessage:nil, onView: self)
        }
        else{
            
            //let location = searchAddress;
            let geocoder = CLGeocoder();
            geocoder.geocodeAddressString(searchAddress, completionHandler: { (placeMarks, error) in
                
                if((placeMarks) != nil && placeMarks?.count > 0){
                    let topPlace = placeMarks![0];
                    let placeMark = MKPlacemark.init(placemark: topPlace);
                    self.locationCoordinate = placeMark.coordinate;
                    self.isMapViewSizeIncreased = false;
                    self.displayLargeMapView(nil);
                }
                else{
                    AppUtility.showAlertWithTitleAndMessage("Address not found", alertMessage:nil, onView: self)
                }
            })
        }
    }
    
    // Info btn to show information
    @IBAction func infoBtnTapped(_ sender: AnyObject) {
        
        if(self.focusedControl != nil){
            self.focusedControl?.resignFirstResponder();
        }
        let btn = sender as! UIButton;
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "InfoPopupVC") as! InfoPopupVC
        
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        self.addChildViewController(objVC)
        objVC.didMove(toParentViewController: self);
        
        if (btn.tag == 4657) {
            objVC.infoTitleLbl.text = "About PIN";
            objVC.detailedTxtV.text = "Please choose a Business Pin code to use during coupon redemption.";
        }
        else if(btn.tag == 4756)
        {
            objVC.infoTitleLbl.text = "Account Managing Branch";
            objVC.detailedTxtV.text = "Check option if this branch will be the account from which all branches will be managed. If the Bonus Management Account is not linked to a specific branch, then this option should not be checked.";
        }
    }
    
    
    @IBAction func displayLargeMapView(_ sender: UITapGestureRecognizer?) {

        if(self.isMapViewSizeIncreased == false)
        {
            UIView.animate(withDuration: 0.3, animations: {
                
                 let scrollPoint = CGPoint(x: 0.0, y: self.scrollView.contentSize.height - self.scrollView.frame.size.height);
                
                if (self.branchObjToEdit != nil) {
                    self.scrollView.contentOffset = scrollPoint;
                }
                
                let oX = self.addressMapView.frame.origin.x
                let oY = self.addressMapView.frame.origin.y
                
                let mapViewWidth = self.addressMapView.frame.size.width;
                self.mapViewHeightConstraint.constant = self.mainViewContainer.frame.size.height - self.addressMapView.frame.origin.y - self.addressMapView.frame.size.height - CGFloat(20) + self.addressMapView.frame.size.height;
                
                self.addressMapView.frame = CGRect(x: oX, y: oY, width: mapViewWidth, height: self.mapViewHeightConstraint.constant);
                
                // Set map region
                let region = MKCoordinateRegionMakeWithDistance(self.locationCoordinate!, 2000, 2000);
                self.addressMapView.setRegion(region, animated: true);
                
                }, completion: { (Bool) in
                    
                    self.isMapViewSizeIncreased = true;
                    if(self.addressMapView.annotations.count > 0){
                        self.addressMapView.removeAnnotations(self.addressMapView.annotations);
                    }
                    
                    //let annotationPin = MKAnnotation.;
                    //annotationPin.
                    let myPin = MKPointAnnotation();
                    myPin.title = "";
                    myPin.coordinate = self.locationCoordinate!
                    self.pinAnnotationView = MKPinAnnotationView(annotation: myPin, reuseIdentifier: nil)
                    
                    //let myPin = MyCustomAnnotation.init(title: "h", coordinate: self.locationCoordinate!, subtitle: "hh");
                    self.addressMapView.addAnnotation(myPin);
            }) 
            
        }
    }
    
    @IBAction func handleLongPressOnMapView(_ sender: UILongPressGestureRecognizer) {
        
        if (sender.state != UIGestureRecognizerState.began){
        return;
        }
        
        let touchPoint = sender.location(in: self.addressMapView);
        let touchMapCoordinate = self.addressMapView.convert(touchPoint, toCoordinateFrom: self.addressMapView)
        if (self.addressMapView.annotations.count > 0) {
            self.addressMapView.removeAnnotations(self.addressMapView.annotations)
        }
        self.locationCoordinate = touchMapCoordinate;
       
    
        let myPin = MKPointAnnotation();
        myPin.title = "";
        myPin.coordinate = self.locationCoordinate!
        self.pinAnnotationView = MKPinAnnotationView(annotation: myPin, reuseIdentifier: nil)
        
        //let myPin = MyAnnotation.init(coord: self.locationCoordinate!)
        //let myPin = MyCustomAnnotation(title: "", coordinate: self.locationCoordinate!, subtitle: "");
        //myPin.title = "";
        //myPin.coordinate = self.locationCoordinate!
        self.addressMapView.addAnnotation(myPin);
        
        let pinView = self.mapView(self.addressMapView, viewFor: myPin)
        self.mapView(self.addressMapView, annotationView: pinView!, didChange: MKAnnotationViewDragState.ending, fromOldState: MKAnnotationViewDragState.none)
    }
    
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        
        
        if (newState == MKAnnotationViewDragState.ending) {
            
            let droppedAt = view.annotation!.coordinate;
            NSLog("Pin dropped at %f,%f", droppedAt.latitude, droppedAt.longitude);
            
            self.locationCoordinate = view.annotation!.coordinate;
            
            let location = CLLocation.init(latitude: droppedAt.latitude, longitude: droppedAt.longitude);
            let geocoder = CLGeocoder();
            geocoder.reverseGeocodeLocation(location, completionHandler: { (placeMarks, error) in
                
                var placemark:CLPlacemark!
                
                if error == nil && placeMarks!.count > 0 {
                    placemark = placeMarks![0] as CLPlacemark
                    
                    var addressString : String = ""
                    
                    if (placemark.subThoroughfare?.characters.count > 0){
                        addressString = placemark.subThoroughfare!;
                    }
                    
                    if (placemark.thoroughfare?.characters.count > 0){
                        if (placemark.subThoroughfare?.characters.count > 0){
                            addressString = addressString + ", " + placemark.thoroughfare!;
                        }
                        else {
                            addressString = placemark.thoroughfare!;
                        }
                    }

                    if (placemark.locality?.characters.count > 0){
                        if (placemark.thoroughfare?.characters.count > 0){
                            addressString = addressString + ", " + placemark.locality!;
                        }
                        else {
                            addressString = placemark.locality!;
                        }
                    }
                    
                    if (placemark.country?.characters.count > 0){
                        
                        if (placemark.locality?.characters.count > 0){
                            addressString = addressString + ", " + placemark.country!;
                        }
                        else {
                            addressString = placemark.country!;
                        }
                    }
                    
                    if (placemark.postalCode?.characters.count > 0){
                        
                        if (placemark.country?.characters.count > 0){
                            addressString = addressString + ", " + placemark.postalCode!;
                        }
                        else {
                            addressString = placemark.postalCode!;
                        }
                    }
                    
                    self.addressTxtF.text = addressString;
                }
            })
        }
    }
    
    /*
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            //return nil so map view draws "blue dot" for standard user location
            return nil
        }
        var pin : MKPinAnnotationView! = nil
        pin = self.addressMapView.dequeueReusableAnnotationViewWithIdentifier("myPin") as? MKPinAnnotationView
        if (pin == nil) {
            pin = MKPinAnnotationView.init(annotation: annotation, reuseIdentifier: "myPin");
            //pin?.canShowCallout = true;
            
        }
        else{
            pin.annotation = annotation;
        }
        
        pin.draggable = true;
        pin.animatesDrop = true;
        pin.canShowCallout = true;
        
//        if(pin!.annotation!.coordinate.longitude == mapView.userLocation.coordinate.longitude ||  pin!.annotation!.coordinate.latitude == mapView.userLocation.coordinate.latitude)
//        {
//            if (annotation.isKindOfClass(MKUserLocation))
//            {
//                //user location view is being requested,
//                //return nil so it uses the default which is a blue dot...
//                return nil;
//            }
//        }
        
        //let pinView = pin as! MKPinAnnotationView;
        //pinView.draggable = true;
        //pinView.animatesDrop = true;
        
        return pin;
    }
 */
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        
        if (self.pinAnnotationView != nil)
        {
            self.pinAnnotationView.animatesDrop = true;
            self.pinAnnotationView.isDraggable = true
            //self.pinAnnotationView.canShowCallout = true
            return self.pinAnnotationView
        }
        
        return nil;
//        var v : MKAnnotationView! = nil
//        let ident = "bike"
//        v = mapView.dequeueReusableAnnotationViewWithIdentifier(ident)
//        if (v == nil) {
//             //v = MKPinAnnotationView.init(annotation: annotation, reuseIdentifier: ident);
//            v = MKAnnotationView(annotation:annotation, reuseIdentifier:ident)
//        }
//        v.annotation = annotation
//        v.draggable = true
//        //v.animatesDrop = true
//        return v
    }
 
    
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        //self.mapView.centerCoordinate;
        
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Set and display toast text when screen is in add branch mode
        
        if (self.branchObjToEdit == nil) {
            
           let contentOffsetY = self.scrollView.contentOffset.y + self.scrollView.frame.size.height;
            let contentSizeHeight = self.scrollView.contentSize.height;
            
            if ((contentOffsetY > (contentSizeHeight - 50)) || (contentOffsetY > (contentSizeHeight + 50))){
                if (self.hud == nil) {
                    self.hud = MBProgressHUD.showAdded(to: self.addressMapView, animated: true);
            self.hud.mode = MBProgressHUDModeText;
            self.hud.labelText = "Tap to enlarge map!"
            self.hud.margin = 10.0;
            self.hud.removeFromSuperViewOnHide = true;
            self.hud.hide(true, afterDelay: 3.0);
                }
            }
            
        }
        
    }
    
    @IBAction func checkMarkBtnTapped(_ sender: AnyObject) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        if(user.PersonType != nil && user.PersonType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE) {
            AppUtility.showAlertWithTitleAndMessage("Sorry! you are not authorized to access this option.", alertMessage: nil, onView:self)
        }
        
        if(self.isAddAsMainBranch == false){
            self.isAddAsMainBranch = true;
            self.checkMarkBtn.setImage(K_RIGHT_YELLOW, for: UIControlState());
            self.emailTxtF.text = user.EmailID;
            self.passwordTxtF.text = user.Password;
            self.emailTxtF.isUserInteractionEnabled = false;
            self.passwordTxtF.isUserInteractionEnabled = false;
        }
        else{
            self.isAddAsMainBranch = false;
            self.checkMarkBtn.setImage(K_RIGHT_YELLOW_EMPTY, for: UIControlState());
            self.emailTxtF.text = "";
            self.passwordTxtF.text = "";
            self.emailTxtF.isUserInteractionEnabled = true;
            self.passwordTxtF.isUserInteractionEnabled = true;
        }
    }
    
    // MARK:- TextFieldDelegate methods
    
    func textField(_ textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == self.passwordTxtF) {
            if (self.passwordTxtF.text?.characters.count > 15 && string.characters.count > 0) {
                AppUtility.showAlertWithTitleAndMessage("Password length can not be more then 15 charactors", alertMessage: nil, onView: self)
            }
        }
        else if (textField == self.telephoneNoTxtF) {
            
            if (textField.text?.characters.count > 15 && string.characters.count > 0) {
                textField.resignFirstResponder();
                AppUtility.showAlertWithTitleAndMessage("Phone number length can not be more than 15.", alertMessage: nil, onView: self)
            }
        }
        else if (textField == self.businessPin1 && textField.text?.characters.count >= 1 && string.isEmpty){
            
        }
        else if ((textField == self.businessPin1) && (textField.text?.characters.count == 1 && !string.isEmpty)){
            
            if (self.businessPin2.text?.characters.count < 1) {
                self.businessPin2.becomeFirstResponder();
            }
            else{
                self.businessPin2.resignFirstResponder();
            }
        }
        
        if (textField == self.businessPin2 && textField.text?.characters.count >= 1 && string.isEmpty){
            
        }
        else if ((textField == self.businessPin2) && (textField.text?.characters.count == 1 && !string.isEmpty)){
            
            if (self.businessPin3.text?.characters.count < 1) {
                self.businessPin3.becomeFirstResponder();
            }
            else{
                self.businessPin2.resignFirstResponder();
            }
        }
        
        if (textField == self.businessPin3 && textField.text?.characters.count >= 1 && string.isEmpty){
            
        }
        else if ((textField == self.businessPin3) && (textField.text?.characters.count == 1 && !string.isEmpty)){
            
            if (self.businessPin4.text?.characters.count < 1) {
                self.businessPin4.becomeFirstResponder();
            }
            else{
                self.businessPin3.resignFirstResponder();
            }
        }
        
        
        if (textField == self.businessPin4 && textField.text?.characters.count >= 1 && string.isEmpty){
            
        }
        else if ((textField == self.businessPin4) && (textField.text?.characters.count == 1 && !string.isEmpty)){
            
            self.businessPin4.resignFirstResponder();
        }
        
        return true;
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        self.focusedControl = textField;
        
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField == self.businessPin1)
        {
            self.businessPin2.becomeFirstResponder();
        }
        else if (textField == self.businessPin2)
        {
            self.businessPin3.becomeFirstResponder();
        }
        else if (textField == self.businessPin3)
        {
            self.businessPin4.becomeFirstResponder();
        }
        else if (textField == self.businessPin4)
        {
            businessPin4.resignFirstResponder();
        }
        else{
            textField.resignFirstResponder();
        }
        
        return true;
    }

    @IBAction func pencilBtnTapped(_ sender: Any) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        if(user.PersonType != nil && user.PersonType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE) {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "You are not authorized user to edit login detail" as String, onView: self)
        }
        else
        {
            self.selectedPencil = sender as! UIButton;
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "SecurityAlertVC") as! SecurityAlertVC
            objVC.popupVCDelegate = self;
            objVC.view.frame = self.view.bounds;
            self.view.addSubview(objVC.view)
            self.addChildViewController(objVC)
            objVC.didMove(toParentViewController: self);
        }
    }
    
    func dismissPopupViewController(_ vc: UIViewController, responseObj: AnyObject?) {
        self.dismiss(animated: true, completion: nil);
        
        // If child view return response then only it will excecute
        if (responseObj != nil) {
            
            // If view controller will PunchesVC then excecute it
            if (vc.isKind(of: SecurityAlertVC.self)) {
                //let vcObj = responseObj as! SecurityAlertVC;
                
                if (responseObj != nil) {
                    
                    if (self.selectedPencil.tag == 40) {
                        self.emailTxtF.isUserInteractionEnabled = true;
                        //self.emailIdTxtF.text = "";
                        self.emailTxtF.becomeFirstResponder();
                    }
                    else if (self.selectedPencil.tag == 50) {
                        self.passwordTxtF.isUserInteractionEnabled = true;
                        self.passwordTxtF.text = "";
                        self.passwordTxtF.becomeFirstResponder();
                    }
                    else if (self.selectedPencil.tag == 61) {
                        self.businessPin1.isUserInteractionEnabled = true;
                        self.businessPin2.isUserInteractionEnabled = true;
                        self.businessPin3.isUserInteractionEnabled = true;
                        self.businessPin4.isUserInteractionEnabled = true;
                        self.businessPin1.text = "";
                        self.businessPin2.text = "";
                        self.businessPin3.text = "";
                        self.businessPin4.text = "";
                        
                        self.businessPin1.becomeFirstResponder();
                    }
                }
            }
        }
    }
    
    func saveBtnTapped() {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        if(Int(user.IsActive!)! == 1) { // If business active then only execute
            
            if (self.focusedControl != nil) {
                self.focusedControl?.resignFirstResponder();
            }
            
            let emailId : String = self.emailTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            let password : String = self.passwordTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            
            let pin1 : String = self.businessPin1.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            let pin2 : String = self.businessPin2.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            let pin3 : String = self.businessPin3.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            let pin4 : String = self.businessPin4.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            
            let combinePin = pin1 + "" + pin2 + "" + pin3 + "" + pin4;
            
            let branchName : String = self.branchNameTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            let areaName : String = self.areaNameTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            let telephone : String = self.telephoneNoTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            let address : String = self.addressTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            
            var latitude = "0.0";
            var longitude = "0.0";
            if let lat = self.locationCoordinate?.latitude{
                latitude = String(lat);
            }
            
            if let lon = self.locationCoordinate?.longitude{
                longitude = String(lon);
            }
            
            let isValidEmail = AppUtility.validateEmail(emailId);
            
            var message : String? = nil;
            
            if !isValidEmail {
                message = LOGIN_ALERT_MESSAGE_EMAIL_ID;
            }
            else if password.isEmpty {
                message = "Please enter a valid password";
            }
            else if (password.characters.count < 6){
                message = PLEASE_ENTER_ATLEAST_SIX_CHARACTORS;
            }
            else if (combinePin.characters.count < 4){
                message = "Please enter a valid pin number for this branch";
            }
            else if (branchName.characters.count <= 0){
                message = "Please enter a valid branch name.";
            }
            else if (areaName.characters.count <= 0){
                message = "Please enter a valid branch area name.";
            }
            else if (telephone.characters.count <= 6 || telephone.characters.count > 15){
                message = "Please enter a valid Telephone Number. Ensure that the entered number is at least 6 character long and less than 15 characters.";
            }
            else if (address.characters.count <= 0){
                message = "Please enter branch address";
            }
            
            if message != nil {
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
            }
            else{
        
                let deviceUniqueId = AppUtility.getDeviceNSUUID();
                var branchId = "0";
                
                if(self.branchObjToEdit != nil){
                    branchId = self.branchObjToEdit.BranchID!;
                }
                
                if(self.locationCoordinate == nil){
                    latitude = "" ;
                    longitude = "" ;
                }
            
                var mainBranchCheck = "0";
                if (self.isAddAsMainBranch == true) {
                    mainBranchCheck = "1";
                }
                // businessEmployeeDictToUpdate = [[NSDictionary alloc] initWithDictionary:dataDict];

                // Created postDataDictonary to post the data on server
                let postDataDict : NSDictionary = ["UserID" : user.ID!,
                                                   "Email" : emailId,
                                                   "Password" : password,
                                                   "Cbpin": combinePin,
                                                   "PersonType" : USER_TYPE_BUSINESS_BRANCH_EMPLOYEE,
                                                   "BranchName" : branchName,
                                                   "AreaName":areaName,
                                                   "PhoneNumber" : telephone,
                                                   "Address" : address,
                                                   "Latitude" : latitude,
                                                   "Longitude" : longitude,
                                                   "ID" : branchId, // Branch Id
                                                    "IsMainBranch" : mainBranchCheck,
                                                   "api_key" : kWEB_SERVICES_API_KEY,
                                                    "DeviceUniqueID" : deviceUniqueId
                ];
                
                print("postData = %@", postDataDict);
                
                if !AppUtility.isNetworkAvailable(){
                    AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
                }
                else{
                    
                CustomProgressView.showProgressIndicatorOnView(VC: self);
                Alamofire.request(kSERVICE_SAVE_BRANCH_FOR_A_BUSINESS, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                        
                        CustomProgressView.hideProgressIndicatorOnView(VC: self);
                        
                        let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                        print("value =  \(datastring)");
                        
                        if let JSON = response.result.value {
                            print("JSON: \(JSON)")
                            let dataDict = JSON as? NSDictionary;
                            
                            if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                                
                                let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                                
                                if(user.PersonType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE) {
                                    
                                    user.EmailID = self.emailTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
                                    
                                    let pwd = self.passwordTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
                                    
                                    // Convert password in md5 as on server side.
                                    let password = pwd.md5().lowercased()
                                    let concatString = String(format: "%@%@", password, kKEY_PASSWORD_CONCAT_STRING); // returns NSString of the MD5 of test
                                    let md5Password = (concatString as NSString).md5().lowercased()
                                   
                                    user.Password = md5Password;
                                    
                                    let encodedObject = NSKeyedArchiver.archivedData(withRootObject: user) as Data
                                    let defaults = UserDefaults.standard
                                    defaults.set(encodedObject, forKey: "UserData")
                                    defaults.synchronize();
                                }
                                
                                let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                                
                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                            else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                                
                                let message = (dataDict?.object(forKey: "Message"))! as! String;
                                AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                            }
                            else
                            {
                                let message = (dataDict?.object(forKey: "Message"))! as! String;
                                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                            }
                        }
                        }
                    )
                }
            }
            
        }
        else{
            AppUtility.showAlertWithTitleAndMessage(AFTER_DEACTIVATE_BUSINESS_ACCOUNT_MESSAGE, alertMessage: nil, onView: self);
        }
    }
}
