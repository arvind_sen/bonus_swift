//
//  TotalEarningVC.swift
//  Bonus
//
//  Created by Arvind Sen on 14/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class TotalEarningVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    var objCustomerMoney: CustomerMoney? = nil;
    var earningFilterdDataDictionary = NSMutableDictionary();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtility.navigationColorAndItsTextColor(VC: self);
        self.title = "Earning"
        // Do any additional setup after loading the view.
        self.getTotalEarningDetails();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getTotalEarningDetails(){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID!;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["UserID": userId,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_USER_TOTAL_EARNING_DETAIL, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("TopEarnersJSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    let status = (dataDict?.object(forKey: "Status"))! as? String
                    
                    if(status == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200)
                    {
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        self.filterDataAndUpdateTableView(dictArray);
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                    }
                }
            }
            )
        }
    }
    
    func filterDataAndUpdateTableView(_ dataArray : NSArray){
        
        // Add filter for bonus pot win
        let servicePredicate0 = NSPredicate(format: "ReferalPerson CONTAINS[cd] %@", "BonusPot");
        let result0 = dataArray.filtered(using: servicePredicate0)
        self.earningFilterdDataDictionary.setValue(result0, forKey: "Bonus");
        
        // Add filter for yourself earning
        let servicePredicate1 = NSPredicate(format: "ReferalPerson CONTAINS[cd] %@", "You");
        let result1 = dataArray.filtered(using: servicePredicate1)
        self.earningFilterdDataDictionary.setValue(result1, forKey: "You");

        // Add filter for earning through Friends
        let servicePredicate2 = NSPredicate(format: "ReferalPerson CONTAINS[cd] %@", "Fiends");
        let result2 = dataArray.filtered(using: servicePredicate2)
        self.earningFilterdDataDictionary.setValue(result2, forKey: "YourFriends");
        
        // Add filter for earning through Friends of Friend
        let servicePredicate3 = NSPredicate(format: "ReferalPerson CONTAINS[cd] %@", "Friends of Friend");
        let result3 = dataArray.filtered(using: servicePredicate3)
        self.earningFilterdDataDictionary.setValue(result3, forKey: "YourFriendsOfFriend");
        
        self.tblView.reloadData();
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var numberOfRows = 1;
        
        if (section == 0) {
            numberOfRows = 1;
        }
        else{
            
            if(section == 1 && self.earningFilterdDataDictionary.value(forKey: "Bonus") != nil){
                let array = self.earningFilterdDataDictionary.value(forKey: "Bonus") as! NSArray;
                if(array.count > 0){
                    numberOfRows = array.count;
                }
            }
            else if(section == 2 && self.earningFilterdDataDictionary.value(forKey: "You") != nil){
                let array = self.earningFilterdDataDictionary.value(forKey: "You") as! NSArray;
                if(array.count > 0){
                    numberOfRows = array.count;
                }
            }
            else if(section == 3 && self.earningFilterdDataDictionary.value(forKey: "YourFriends") != nil){
                let array = self.earningFilterdDataDictionary.value(forKey: "YourFriends") as! NSArray;
                if(array.count > 0){
                    numberOfRows = array.count;
                }
            }
            else if(section == 4 && self.earningFilterdDataDictionary.value(forKey: "YourFriendsOfFriend") != nil){
                let array = self.earningFilterdDataDictionary.value(forKey: "YourFriendsOfFriend") as! NSArray;
                if(array.count > 0){
                    numberOfRows = array.count;
                }
            }
        }
        return numberOfRows;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var headerHeight: CGFloat = 30.0;
        if (section == 0) {
            headerHeight = 45.0;
        }
        return headerHeight;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height = 30;
        if(indexPath.section == 0){
            height = 2;
        }
        return CGFloat(height);
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        var headerViewHight : CGFloat = 30.0;
        if (section == 0) {
            headerViewHight = 44.0;
        }
        
        let headerView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: Double(self.tblView.frame.size.width), height: Double(headerViewHight)))
        headerView.backgroundColor = UIColor.gray;
        var nX = 10.0;
        let nY = 0.0;
        let lblHeight1 = 30.0;
        let lblWidth1 = Double(self.tblView.frame.size.width)/2.0 - nX;
        
        let lblLabel1 = UILabel(frame: CGRect(x: nX, y: nY, width: lblWidth1, height: lblHeight1));
        lblLabel1.font = UIFont.systemFont(ofSize: 12.0);
        lblLabel1.textColor = UIColor.white;
        lblLabel1.numberOfLines = 1;
        headerView.addSubview(lblLabel1);

        let lblWidth2 = Double(self.tblView.frame.size.width) - lblWidth1 - 2*nX;
        nX += lblWidth1;
        
        let lblLabel2 = UILabel(frame: CGRect(x: nX, y: nY, width: lblWidth2, height: lblHeight1));
        lblLabel2.font = UIFont.systemFont(ofSize: 12.0);
        lblLabel2.textColor = UIColor.white;
        lblLabel2.numberOfLines = 1;
        lblLabel2.textAlignment = .right;
        headerView.addSubview(lblLabel2);
        
        var lblString = String();
        var lbl2AmountStr = String();
        
        let currencyAbbr = (self.objCustomerMoney?.CurrencyAbrrevation!)!;
        switch section {
        case 1:
            lblString = "BONUS POT"
            lbl2AmountStr = ("\(currencyAbbr) 0.0");
            if(self.objCustomerMoney?.BonusPotWin != nil){
                lbl2AmountStr = String(format: "%@ %.2f", currencyAbbr, Double((self.objCustomerMoney?.BonusPotWin)!)!)
            }
            break;
        case 2:
            lblString = "YOU"
            lbl2AmountStr = ("\(currencyAbbr) 0.0");
            if(self.objCustomerMoney?.Yourself != nil){
                lbl2AmountStr = String(format: "%@ %.2f", currencyAbbr, Double((self.objCustomerMoney?.Yourself)!)!)
            }
            break;
        case 3:
            lblString = "FRIEND"
            lbl2AmountStr = ("\(currencyAbbr) 0.0");
            if(self.objCustomerMoney?.Friend != nil){
                lbl2AmountStr = String(format: "%@ %.2f", currencyAbbr, Double((self.objCustomerMoney?.Friend)!)!)
            }
            break;
        case 4:
            lblString = "FRIENDS OF FRIEND"
            lbl2AmountStr = ("\(currencyAbbr) 0.0");
            if(self.objCustomerMoney?.FriendsOfFriends != nil){
                lbl2AmountStr = String(format: "%@ %.2f", currencyAbbr, Double((self.objCustomerMoney?.FriendsOfFriends)!)!)
            }
            break;
        default:
            lblString = "TOTAL EARNED"
            lbl2AmountStr = ("\(currencyAbbr) 0.0");
            if(self.objCustomerMoney?.TotalEarnedAmount != nil){
                lbl2AmountStr = String(format: "%@ %.2f", currencyAbbr, Double((self.objCustomerMoney?.TotalEarnedAmount)!)!)
            }
        }
        
        lblLabel1.text = lblString;
        lblLabel2.text = lbl2AmountStr;
        
        if (section == 0)
        {
            var yourAmount = currencyAbbr + " 0.0";
            var friendsAmount = currencyAbbr + " 0.0";
            var friendsOfFriendAmount = currencyAbbr + " 0.0";
            
            if(self.objCustomerMoney?.Yourself != nil){
                
                yourAmount = ("\(currencyAbbr) 0.0");
                if(self.objCustomerMoney?.Yourself != nil){
                    yourAmount = String(format: "%@ %.2f", currencyAbbr, Double((self.objCustomerMoney?.Yourself)!)!)
                }
                //yourAmount = String(format: "%@ %.2f", currencyAbbr, Double((self.objCustomerMoney?.Yourself)!)!);
            }
            
            if(self.objCustomerMoney?.Friend != nil){
                friendsAmount = ("\(currencyAbbr) 0.0");
                if(self.objCustomerMoney?.Friend != nil){
                    friendsAmount = String(format: "%@ %.2f", currencyAbbr, Double((self.objCustomerMoney?.Friend)!)!)
                }
                //friendsAmount = String(format: "%@ %.2f", currencyAbbr, Double((self.objCustomerMoney?.Friend)!)!);
            }
            
            if(self.objCustomerMoney?.FriendsOfFriends != nil){
                friendsOfFriendAmount = ("\(currencyAbbr) 0.0");
                if(self.objCustomerMoney?.FriendsOfFriends != nil){
                    friendsOfFriendAmount = String(format: "%@ %.2f", currencyAbbr, Double((self.objCustomerMoney?.FriendsOfFriends)!)!)
                }
                //friendsOfFriendAmount = String(format: "%@ %.2f", currencyAbbr, Double((self.objCustomerMoney?.FriendsOfFriends)!)!);
            }
            
            let earningViewHeight = 15.0;
            
            let earningLbl = UILabel(frame: CGRect(x: 0, y: lblHeight1, width: Double(self.tblView.frame.size.width), height: earningViewHeight));
            earningLbl.font = UIFont.systemFont(ofSize: 10.0);
            earningLbl.textColor = UIColor.white;
            earningLbl.backgroundColor = UIColor.darkGray;
            earningLbl.numberOfLines = 1;
            earningLbl.textAlignment = .center;
            earningLbl.text = ("You:\(yourAmount)   Friend:\(friendsAmount)   Friends of Friend:\(friendsOfFriendAmount)")
            headerView.addSubview(earningLbl);
        }
        return headerView;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        cell.selectionStyle = .none;
        
        // Configure the cell...
        self.configureTableViewCell(cell, forRowAtIndexPath: indexPath);
        return cell
    }
    
    // This below method is setting different values for a row
    func configureTableViewCell(_ cell: UITableViewCell, forRowAtIndexPath:IndexPath){
        
        cell.backgroundColor = UIColor.white;
        cell.accessoryType = .none;
        
        var dict: NSDictionary? = nil;
        
        var rowDisplayFlag = false;
        
        if(forRowAtIndexPath.section == 1 && self.earningFilterdDataDictionary.value(forKey: "Bonus") != nil){
            let array = self.earningFilterdDataDictionary.value(forKey: "Bonus") as! NSArray;
            if(array.count > 0)
            {
                dict = array.object(at: forRowAtIndexPath.row) as? NSDictionary;
                rowDisplayFlag = true
            }
        }
        else if(forRowAtIndexPath.section == 2 && self.earningFilterdDataDictionary.value(forKey: "You") != nil){
            let array = self.earningFilterdDataDictionary.value(forKey: "You") as! NSArray;
            if(array.count > 0)
            {
                dict = array.object(at: forRowAtIndexPath.row) as? NSDictionary;
                rowDisplayFlag = true
            }
        }
        else if(forRowAtIndexPath.section == 3 && self.earningFilterdDataDictionary.value(forKey: "YourFriends") != nil){
            let array = self.earningFilterdDataDictionary.value(forKey: "YourFriends") as! NSArray;
            if(array.count > 0)
            {
                dict = array.object(at: forRowAtIndexPath.row) as? NSDictionary;
                rowDisplayFlag = true
            }
        }
        else if(forRowAtIndexPath.section == 4 && self.earningFilterdDataDictionary.value(forKey: "YourFriendsOfFriend") != nil){
            let array = self.earningFilterdDataDictionary.value(forKey: "YourFriendsOfFriend") as! NSArray;
            if(array.count > 0)
            {
                dict = array.object(at: forRowAtIndexPath.row) as? NSDictionary;
                rowDisplayFlag = true
            }
        }

        
        if(rowDisplayFlag == false){
            cell.backgroundColor = UIColor.clear;
            
            let lblHeight1 = 30.0;
            let lblWidth1 = cell.frame.size.width;
            
            let lblLabel1 = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: Double(lblWidth1), height: lblHeight1));
            lblLabel1.font = UIFont.systemFont(ofSize: 14.0);
            lblLabel1.backgroundColor = UIColor.clear;
            lblLabel1.textColor = UIColor.black;
            lblLabel1.numberOfLines = 1;
            lblLabel1.textAlignment = .center;
            lblLabel1.text = "No Record found";
            cell.addSubview(lblLabel1);
        }
        else
        {
            let cellContentViewWidth = cell.frame.size.width - 2*10;
            let cellContentViewHeight = 30.0;
            
            var nX = 10.0;
            let lblHeight1 = cellContentViewHeight;
            let lblWidth1 = Double(cellContentViewWidth/2.0) - nX;
            var nY = 0.0;
            
            let lblLabel1 = UILabel(frame: CGRect(x: nX, y: nY, width: Double(lblWidth1), height: lblHeight1));
            lblLabel1.font = UIFont.systemFont(ofSize: 12.0);
            lblLabel1.backgroundColor = UIColor.clear;
            lblLabel1.textColor = UIColor.black;
            lblLabel1.numberOfLines = 1;
            lblLabel1.text = "";
            
            if(dict?.object(forKey: "FirstName") != nil)
            {
                lblLabel1.text = dict?.object(forKey: "FirstName") as? String;
            }
            cell.addSubview(lblLabel1);

            let lblHeight2 = cellContentViewHeight;
            let lblWidth2 = Double(cellContentViewWidth) - lblWidth1 - nX;
            nX = Double(cellContentViewWidth) - lblWidth2 + nX;
            
            
            let lblLabel2 = UILabel(frame: CGRect(x: nX, y: nY, width: Double(lblWidth2), height: lblHeight2));
            lblLabel2.font = UIFont.systemFont(ofSize: 12.0);
            lblLabel2.backgroundColor = UIColor.clear;
            lblLabel2.textColor = UIColor.black;
            lblLabel2.numberOfLines = 1;
            lblLabel2.textAlignment = .right;
            
            let UserCurrencyCode = dict?.object(forKey: "UserCurrencyCode") as? String;
             let AmountUserCurrency = dict?.object(forKey: "AmountUserCurrency") as? String;
            
            lblLabel2.text = String(format: "%@ %.2f", UserCurrencyCode!, Double(AmountUserCurrency!)!);
            cell.addSubview(lblLabel2);

        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
