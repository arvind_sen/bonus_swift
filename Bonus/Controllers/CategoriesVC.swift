//
//  CategoriesVC.swift
//  Bonus
//
//  Created by Arvind Sen on 11/11/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit

protocol CategoryVCDelegate {
    func dismissCategoryVC(_ vc: UIViewController, selectedObj: AnyObject?);
    //func signUpSuccessful(username: String, password: String)
    //func signUpUnsuccessful(error: NSError)
}

class CategoriesVC: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var tblView: UITableView!
    
    var dataArray: NSMutableArray!
    var searchDataArray: NSMutableArray!
    var delegate : CategoryVCDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false
        // Do any additional setup after loading the view.
        
        self.containerView.layer.cornerRadius = 5.0;
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.containerView.layer.borderWidth = 1.0;
        self.containerView.clipsToBounds = true;
        
        self.searchBar.tintColor = UIColor.darkGray;
        self.searchBar.backgroundColor = UIColor.clear;
        
        self.tblView.register(UITableViewCell.classForKeyedArchiver(), forCellReuseIdentifier: "reuseIdentifier")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showTableView(_ dataSourceArray: NSMutableArray) {
        
        self.dataArray = dataSourceArray;
        
        if self.searchDataArray != nil{
            self.searchDataArray.removeAllObjects()
        }
        else
        {
            self.searchDataArray = NSMutableArray();
            self.searchDataArray.addObjects(from: dataArray as [AnyObject])
        }
        
        self.tblView.reloadData();
    }
    
    
    // MARK: - Table view data source
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var returnValue = 0;
        if searchDataArray != nil {
            returnValue = searchDataArray!.count;
        }
        return returnValue;
    }
    
    func configure(_ cell: UITableViewCell, forRowAtIndexPath indexPath: IndexPath) {
        //cell.textLabel!.text = "Row \(indexPath.row)"
        
        let obj = searchDataArray[indexPath.row] as! Category;
        cell.textLabel?.text = obj.CategoryName;
        //cell.detailTextLabel?.text = obj.EmailID;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        let reuseCellId : String = "reuseIdentifier"
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseCellId)
        
        print(searchDataArray[indexPath.row]);
        configure(cell, forRowAtIndexPath: indexPath)
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        
        //let cell : UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!;
        //cell.accessoryType = UITableViewCellAccessoryType.Checkmark;
        //cell.tintColor = K_COLOR_DARK_GREEN_COLOR;
        //[tabularDelegate!.countryTableRowSelection(searchDataArray[indexPath.row])];
        let obj = searchDataArray[indexPath.row] as! Category;
        self.delegate?.dismissCategoryVC(self, selectedObj: obj)
        self.closeBtnTapped("" as AnyObject);
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAtIndexPath indexPath: IndexPath) {
        let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!;
        cell.accessoryType = UITableViewCellAccessoryType.none;
    }
    
    func searchDataWithEnrty(_ str: String?){
        
        if str != nil { //If data array empty
            
            let servicePredicate = NSPredicate(format: "SELF.CategoryName CONTAINS[cd] %@", str!)
            
            let result = self.dataArray.filtered(using: servicePredicate)
            self.searchDataArray.removeAllObjects()
            self.searchDataArray.addObjects(from: result)
            
        }
        else{
            self.searchDataArray.removeAllObjects()
            self.searchDataArray.addObjects(from: self.dataArray as [AnyObject])
        }
        self.tblView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchDataWithEnrty(searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        self.searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        self.searchDataWithEnrty(nil);
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    @IBAction func closeBtnTapped(_ sender: AnyObject) {
        self.delegate?.dismissCategoryVC(self, selectedObj: nil);
        // Removed it from superview controller
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
}
