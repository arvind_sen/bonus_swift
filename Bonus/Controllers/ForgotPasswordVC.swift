//
//  ForgotPasswordVC.swift
//  Bonus
//
//  Created by Arvind Sen on 31/08/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailTxtF: UITextField!
    @IBOutlet weak var containerView: UIView!
    var popupVCDelegate : DismissPopupVCProtocol? = nil
    var emailId:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false
        // Do any additional setup after loading the view.
        
        self.containerView.layer.cornerRadius = 5.0;
        self.containerView.clipsToBounds = true;
        self.emailTxtF.delegate = self;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true
    }
    
    @IBAction func sendBtnTapped(_ sender: AnyObject) {
        
        let emailId : String = self.emailTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        self.emailTxtF.text = emailId;
        let isValidEmail = AppUtility.validateEmail(emailId);
        
        var message : String? = nil;
        
        if !isValidEmail {
            message = LOGIN_ALERT_MESSAGE_EMAIL_ID;
        }
        
        if message != nil {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
        }
        else {
            // Created postDataDictonary to post the data on server
            let postDataDict : NSDictionary = ["EmailID": emailId,
                                               "api_key": kWEB_SERVICES_API_KEY];
            
            if !AppUtility.isNetworkAvailable(){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
            }
            else{
                
                CustomProgressView.showProgressIndicator();
                Alamofire.request(kSERVICE_FORGOT_PASSWORD, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                    CustomProgressView.hideProgressIndicator();
                    
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                    print("value =  \(datastring)");
                    
                    if let JSON = response.result.value
                    {
                        print("JSON: \(JSON)")
                        let dataDict = JSON as? NSDictionary;
                        
                        if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                            
                            let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                            
                            let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                self.cancelBtnTapped(nil);
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else{
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
                        }
                        
                    }
                    }
                )
            }
        }
        
    }
    
    @IBAction func cancelBtnTapped(_ sender: AnyObject?) {
        popupVCDelegate?.dismissPopupViewController(self, responseObj: nil);
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
