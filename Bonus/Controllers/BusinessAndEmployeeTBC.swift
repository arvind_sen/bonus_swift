//
//  BusinessAndEmployeeTBC.swift
//  Bonus
//
//  Created by Arvind Sen on 30/08/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit

class BusinessAndEmployeeTBC: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Do any additional setup after loading the view.
        
        self.tabBar.barTintColor = TAB_BAR_BG_COLOR;
        self.delegate = self;
        
        // Account image setup
        let item1 = self.tabBar.items![0]
        item1.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_WHITE], for: UIControlState())
        item1.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_YELLOW], for: .selected)
        item1.selectedImage = UIImage(named: "PersonAccountYellow")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        item1.image = UIImage(named: "PersonAccount")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        // Wallet image setup
        let item2 = self.tabBar.items![1]
        item2.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_WHITE], for: UIControlState())
        item2.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_YELLOW], for: .selected)
        item2.selectedImage = UIImage(named: "YellowNotificationWOTxt")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        item2.image = UIImage(named: "NotificationWOTxt")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        // Invite image setup
        let item3 = self.tabBar.items![2]
        item3.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_WHITE], for: UIControlState())
        item3.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_YELLOW], for: .selected)
        item3.selectedImage = UIImage(named: "YellowMycoupons")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        item3.image = UIImage(named:"Mycoupons")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        // Loyalty image setup
        let item4 = self.tabBar.items![3]
        item4.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_WHITE], for: UIControlState())
        item4.setTitleTextAttributes([NSForegroundColorAttributeName : ICONS_TINT_COLOR_YELLOW], for: .selected)
        item4.selectedImage = UIImage(named: "YellowCoupons")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        item4.image = UIImage(named: "coupons")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        self.selectedIndex = 5;
        
        let defaults = UserDefaults.standard
        if(defaults.value(forKey: "globalUserType") as! String == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE){
            item2.isEnabled = false;
            item3.isEnabled = false;
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
