//
//  CountryTimeZoneVC.swift
//  Bonus
//
//  Created by Arvind Sen on 15/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit

class CountryTimeZoneVC: UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var tblView: UITableView!
    
    var dataArray: NSMutableArray!
    var searchDataArray: NSMutableArray!
    var callBackWithSelectedCountryTimeZone : ((CountryTimeZone)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false
        // Do any additional setup after loading the view.
        
        self.containerView.layer.cornerRadius = 5.0;
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.containerView.layer.borderWidth = 1.0;
        self.containerView.clipsToBounds = true;
        
        self.tblView.register(UITableViewCell.classForKeyedArchiver(), forCellReuseIdentifier: "reuseIdentifier")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showTableView(){
        
        if self.searchDataArray != nil{
            self.searchDataArray.removeAllObjects()
        }
        else
        {
            self.searchDataArray = NSMutableArray();
            self.searchDataArray.addObjects(from: dataArray as [AnyObject])
        }
        
        self.tblView.reloadData();
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var returnValue = 0;
        if searchDataArray != nil {
            returnValue = searchDataArray!.count;
        }
        return returnValue;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let reuseCellId : String = "reuseIdentifier"
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseCellId)
        
        configure(cell, forRowAtIndexPath: indexPath)
        
        return cell;
    }
    
    func configure(_ cell: UITableViewCell, forRowAtIndexPath indexPath: IndexPath) {
        
        let obj = searchDataArray[indexPath.row] as! CountryTimeZone;
        cell.textLabel?.text = obj.TimeZone;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = searchDataArray[indexPath.row] as! CountryTimeZone;
        
        self.callBackWithSelectedCountryTimeZone!(obj);
        // Removed it from superview controller
        self.view.removeFromSuperview()
        self.removeFromParentViewController();
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!;
        cell.accessoryType = UITableViewCellAccessoryType.none;
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func closeBtnTapped(_ sender: AnyObject) {
        // Removed it from superview controller
        self.view.removeFromSuperview()
        self.removeFromParentViewController();
    }
}
