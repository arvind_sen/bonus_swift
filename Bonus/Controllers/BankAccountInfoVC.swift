//
//  BankAccountInfoVC.swift
//  Bonus
//
//  Created by Arvind Sen on 13/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class BankAccountInfoVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var accountHolderNameTxtF: UITextField!
    @IBOutlet weak var bankNameTxtF: UITextField!
    @IBOutlet weak var branchNameTxtF: UITextField!
    @IBOutlet weak var accountNumberTxtF: UITextField!
    @IBOutlet weak var ifscCodeTxtF: UITextField!
    @IBOutlet weak var swiftCodeTxtF: UITextField!
    @IBOutlet weak var updateAccountInfoBtn: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    var focusedControl : UITextField?
    
    var withdrawalAmount: String? = nil;
    var currencyCode: String? = nil;
    var updateBtnFlag = false;
    var objBankAccountInfo: BankAccountInfo? = nil;
    var callBackNotToReloadView: ((Bool)->Void)? ;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // Do any additional setup after loading the view.
        self.amountLbl.layer.borderColor = UIColor.gray.cgColor;
        self.amountLbl.text = String(format: "Withdrawal Amount : %.2f %@", Double(self.withdrawalAmount!)!, currencyCode!)
        self.getUserStoredAccountInfo();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func keyboardWasShown(_ aNotification: Notification){
        
        let info:NSDictionary = aNotification.userInfo! as NSDictionary;
        //let kbSize = (info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue.size;
        var kbSize = (info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue.size;
        if(kbSize.height <= 0.0){
            kbSize = (info.object(forKey: UIKeyboardFrameEndUserInfoKey) as AnyObject).cgRectValue.size;
        }
        
        let contentInsets = UIEdgeInsetsMake(0, 0.0, kbSize.height, 0.0);
        scrollView.contentInset = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        var aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!aRect.contains(self.focusedControl!.frame.origin) ) {
            let scrollPoint = CGPoint(x: 0.0, y: focusedControl!.frame.origin.y-kbSize.height);
            self.scrollView.setContentOffset(scrollPoint, animated: true);
        }
    }
    
    func keyboardWillBeHidden(_ aNotification:Notification){
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            //let contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
            //self.scrollView.contentInset = self.scrollViewInitialEdgeInsets;
            //self.scrollView.contentOffset = self.scrollViewInitialContentOffset;
            //self.scrollView.scrollIndicatorInsets = contentInsets;
            // [scrollView setContentOffset:CGPointZero animated:YES];
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func getUserStoredAccountInfo(){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID!;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["UserID": userId,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_GET_USER_BANK_DETAIL, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("BankInfoJSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    let status = (dataDict?.object(forKey: "Status"))! as? String
                    
                    if(status == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200)
                    {
                        let dict = dataDict?.object(forKey: "Response") as! NSDictionary
                        
                        self.objBankAccountInfo = BankAccountInfo(data: dict.object(forKey: "BankAccountInfo") as! Dictionary<String, AnyObject>)
                        
                        DispatchQueue.main.async {
                            self.updateScreenFields();
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail"){
                        
                        if((dataDict?.object(forKey: "StatusCode"))! as! String == "100"){
                            // No data found
                        }
                        else if((dataDict?.object(forKey: "StatusCode"))! as! Int == 500){
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                        }
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                    }
                }
            }
            )
        }
    }
    
    func updateScreenFields(){
        self.accountHolderNameTxtF.text = self.objBankAccountInfo?.AccountHolderName;
        self.bankNameTxtF.text = self.objBankAccountInfo?.BankName;
        self.branchNameTxtF.text = self.objBankAccountInfo?.BranchName;
        self.accountNumberTxtF.text = self.objBankAccountInfo?.BankAccountNumber;
        self.ifscCodeTxtF.text = self.objBankAccountInfo?.BankAccountIFSCCode;
        self.swiftCodeTxtF.text = self.objBankAccountInfo?.BankSwiftCode;
        self.updateAccountInfoBtn.isHidden = false;
    }

    @IBAction func updateAccountInfoBtnTapped(_ sender: Any) {
        
        if (updateBtnFlag == true) {
            self.updateAccountInfoBtn.setImage(K_RIGHT_YELLOW_EMPTY, for: UIControlState())
            updateBtnFlag = false;
        }
        else{
            self.updateAccountInfoBtn.setImage(K_RIGHT_YELLOW, for: UIControlState())
            updateBtnFlag = true;
        }
    }
    
    @IBAction func submitBtnTapped(_ sender: Any) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID;
        
        let accountHolderName : String = self.accountHolderNameTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let bankName : String = self.bankNameTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let accountNumber : String = self.accountNumberTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let branchName : String = self.branchNameTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let ifscCode : String = self.ifscCodeTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let swiftCode : String = self.swiftCodeTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        self.accountHolderNameTxtF.text = accountHolderName;
        self.bankNameTxtF.text = bankName;
        self.accountNumberTxtF.text = accountNumber;
        self.branchNameTxtF.text = branchName;
        self.ifscCodeTxtF.text = ifscCode;
        self.swiftCodeTxtF.text = swiftCode;
        
        var message : String? = nil;
        
        if (accountHolderName.characters.count <= 0) {
            message = "Please enter account holder name";
        }
        else if (bankName.characters.count <= 0) {
            message = "Please enter bank name";
        }
        else if (accountNumber.characters.count <= 0) {
            message = "Please enter account number";
        }
        else if (branchName.characters.count <= 0) {
            message = "Please enter branch name";
        }
        else if (ifscCode.characters.count <= 0) {
            message = "Please enter IFSC code";
        }
        else if (self.currencyCode != nil && self.currencyCode == "AED") {
            if (swiftCode.characters.count <= 0) {
                message = "Please enter Swift Code";
            }
        }
        
        if message != nil {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
        }
        else {
            
            var addUpdate = "0";
            var accountSavedId = "0";
            
            if(self.objBankAccountInfo != nil && self.updateBtnFlag == true){
                // if Checked mark checked then update account info
                addUpdate = "1";
                accountSavedId = (self.objBankAccountInfo?.ID)!;
            }
            else if(self.objBankAccountInfo != nil && self.updateBtnFlag == false){
                // if Checked mark checked then update account info
                addUpdate = "";
                accountSavedId = "";
            }
 
            let deviceUniqueId = AppUtility.getDeviceNSUUID();
            // Created postDataDictonary to post the data on server
            let postDataDict : NSDictionary = ["addupdate" : addUpdate,
                                               "UserID": userId!,
                                               "ID" : accountSavedId,
                                               "AccountHolderName": accountHolderName,
                                               "BankName": bankName,
                                               "BranchName": branchName,
                                               "BankAccountNumber" : accountNumber,
                                               "BankAccountIFSCCode" : ifscCode,
                                               "SwiftCode" : swiftCode,
                                               "City" : "",
                                               "CountryID": "",
                                               "Amount": withdrawalAmount!,
                                               "CurrencyCode" : currencyCode!,
                                               "DeviceUniqueID": deviceUniqueId,
                                               "api_key": kWEB_SERVICES_API_KEY];
            
            print("postDataDict = %", postDataDict);
            
            if !AppUtility.isNetworkAvailable(){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
            }
            else{
                
                CustomProgressView.showProgressIndicator();
                Alamofire.request(kSERVICE_USER_SAVE_WITHDRWAL_REQUEST, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                    
                    CustomProgressView.hideProgressIndicator();
                    
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                    print("value =  \(datastring)");
                    
                    if let JSON = response.result.value
                    {
                        print("JSON: \(JSON)")
                        let dataDict = JSON as? NSDictionary;
                        
                        if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String) == 200) {
                            
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            self.showAlertWithMessage(message: message);
                        }
                        else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? String == "500"){
                            
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                        }
                        else
                        {
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            self.showAlertWithMessage(message: message);
                        }
                    }
                }
                )
            }
        }
    }
    
    func showAlertWithMessage(message : String)
    {
        //let message = (dataDict?.object(forKey: "Message"))! as! String;
        
        let alert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
            self.callBackNotToReloadView!(false);
            self.dismiss(animated: true, completion: nil);
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func cancelBtnTapped(_ sender: Any) {
        self.callBackNotToReloadView!(false);
        self.dismiss(animated: true, completion: nil);
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        focusedControl = textField;
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if((textField.text?.characters.count)! > 0){
            textField.resignFirstResponder();
        }
        else
        {
            if (textField == self.accountHolderNameTxtF)
            {
                self.bankNameTxtF.becomeFirstResponder();
            }
            else if (textField == self.bankNameTxtF)
            {
                self.branchNameTxtF.becomeFirstResponder();
            }
            else if (textField == self.branchNameTxtF)
            {
                self.accountNumberTxtF.becomeFirstResponder();
            }
            else if (textField == self.accountNumberTxtF)
            {
                self.ifscCodeTxtF.becomeFirstResponder();
            }
            else{
                textField.resignFirstResponder();
            }
        }
        return true;
    }

}
