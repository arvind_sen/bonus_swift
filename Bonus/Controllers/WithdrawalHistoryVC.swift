//
//  WithdrawalHistoryVC.swift
//  Bonus
//
//  Created by Arvind Sen on 14/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class WithdrawalHistoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblView: UITableView!
    var withdrawalHistoryArray : Array<WithdrawalHistory> = [WithdrawalHistory]();
    
    var callBackNotToReloadView: ((Bool)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getUserWithdrawalHistory();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getUserWithdrawalHistory(){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID!;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["UserID": userId,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_GET_USER_WITHDRWAL_REQUEST_LIST, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("WithdrawalHistoryJSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    let status = (dataDict?.object(forKey: "Status"))! as? String
                    
                    if(status == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200)
                    {
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        
                        self.withdrawalHistoryArray.removeAll();
                        
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let obj = WithdrawalHistory(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>)
                                self.withdrawalHistoryArray.append(obj);
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.tblView.reloadData();
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Success" && (dataDict?.object(forKey: "StatusCode"))! as! String == "100"){
                        
                        DispatchQueue.main.async {
                            self.tblView.reloadData();
                        }
                        //let message = (dataDict?.object(forKey: "Message"))! as! String;
                        //AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                    }
                }
            }
            )
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.withdrawalHistoryArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        // Configure the cell...
        configureCell(tblCell: cell, indexPath: indexPath);
        return cell
    }
    
    
    func configureCell(tblCell: UITableViewCell, indexPath: IndexPath){

        let obj = self.withdrawalHistoryArray[indexPath.row];
        let withdrawalLbl = tblCell.contentView.viewWithTag(1635) as! UILabel;
        let statusLbl = tblCell.contentView.viewWithTag(1636) as! UILabel;
        let remarkLbl = tblCell.contentView.viewWithTag(1637) as! UILabel;
        let dateLbl = tblCell.contentView.viewWithTag(1638) as! UILabel;
        
        
        withdrawalLbl.text = String(format: "Withdrawal Amount : %.2f %@", Double(obj.Amount!)!, obj.CurrencyCode!)
        
        statusLbl.text = "Pending";
        if(Int(obj.ApprovedStatus!)! == 1){
            statusLbl.text = "Approved";
            statusLbl.textColor = UIColor.green;
        }
        else if(Int(obj.ApprovedStatus!)! == 2){
            statusLbl.text = "Cancelled";
        }

        remarkLbl.text = "Remark : --";
        
        if(obj.Remark != nil)
        {
            remarkLbl.text = "Remark : " + obj.Remark!;
        }
        
        dateLbl.text = AppUtility.getDateFromString(obj.RequestDate!, currentFormat: "yyyy-MM-dd HH:mm:ss", desiredFormat: "dd MMM yyyy, HH:mm:ss");
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func cancelBtnTapped(_ sender: Any) {
        self.callBackNotToReloadView!(false);
        self.dismiss(animated: true, completion: nil);
    }

}
