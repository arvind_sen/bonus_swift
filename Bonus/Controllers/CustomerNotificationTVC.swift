//
//  CustomerNotificationTVC.swift
//  Bonus
//
//  Created by Arvind Sen on 27/06/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class CustomerNotificationTVC: UITableViewController {
    var notificationDataArray : Array<CustomerNotification> = [CustomerNotification]();
    var flagToNoDataFound = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
        let defaults = UserDefaults.standard
        
        if(defaults.object(forKey: "pushNotificationData") != nil)
        {
            self.getANotificationDetailOnPushNotification()
        }
        else{
            self.getAllNotifications(offset: 0);
        }
    }
    
    func getANotificationDetailOnPushNotification(){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        let userId = user.ID;
        
        let pushData = defaults.object(forKey: "pushNotificationData") as! NSDictionary;
        let notificationId = pushData.value(forKey: "id") as? String;
        
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        let postDataDict : NSDictionary? = ["UserID": userId!,
                                            "Offset" :"0",
                                            "Limit": "1",
                                            "DeviceUniqueID": deviceUniqueId,
                                            "api_key": kWEB_SERVICES_API_KEY,
                                            "SortBy": "TransDate",
                                            "OrderBy": "DESC",
                                            "SearchFor": "",
                                            "NotificationTransID" : notificationId!];
        
        print("post data = ", postDataDict!)
        // Created postDataDictonary to post the data on server
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_USER_NOTIFICATION_LIST, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                // Removed push notification object from default data.
                let defaults = UserDefaults.standard
                if(defaults.object(forKey: "pushNotificationData") != nil)
                {
                    defaults.removeObject(forKey: "pushNotificationData");
                    defaults.synchronize();
                }

                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        let resourcePath = dataDict?.object(forKey: "ResourcePath") as! Dictionary<String, AnyObject>
                        
                        if(dictArray.count > 0)
                        {
                            let item = dictArray[0];
                            let obj = CustomerNotification(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>, resourcePath: resourcePath)
                                
                            self.displayNotificationDetailView(obj);
                        }
                        else{
                            self.getAllNotifications(offset: 0);
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as! String == "500"){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                        
                    }
                }
            }
            )
        }
    }

    func getAllNotifications(offset: Int) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        let userId = user.ID;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        let postDataDict : NSDictionary? = ["UserID": userId!,
                                            "Offset" :String(offset),
                                             "Limit": "10",
                                             "DeviceUniqueID": deviceUniqueId,
                                             "api_key": kWEB_SERVICES_API_KEY,
                                            "SortBy": "TransDate",
                                             "OrderBy": "DESC",
                                             "SearchFor": "",
                                            "NotificationTransID" : "0"];
        
        // Created postDataDictonary to post the data on server
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_USER_NOTIFICATION_LIST, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    let anyObj = dataDict?.object(forKey: "StatusCode")
                    let StatusCode = Int(("\(anyObj!)"))!;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && StatusCode == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        let resourcePath = dataDict?.object(forKey: "ResourcePath") as! Dictionary<String, AnyObject>
                        
                        if(offset == 0){
                            self.notificationDataArray.removeAll();
                        }
                        
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let obj = CustomerNotification(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>, resourcePath: resourcePath)
                                
                                self.notificationDataArray.append(obj);
                            }
                            //self.tableView.reloadData();
                            OperationQueue.main.addOperation {
                                self.flagToNoDataFound = false;
                                self.tableView.reloadData();
                            }
                        }
                        else
                        {
                            OperationQueue.main.addOperation {
                                self.flagToNoDataFound = true;
                                self.tableView.reloadData();
                            }
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && StatusCode == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                        
                    }
                }
            }
            )
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        var numOfSections: Int = 0
        if (self.notificationDataArray.count > 0)
        {
            //tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else if (self.flagToNoDataFound == true)
        {
            var oX = 0.0;
            var oY = 0.0;
            let widthX = tableView.frame.size.width;
            let heightY = tableView.frame.size.height;
            
            let emptyContainer = UIView.init(frame: CGRect(x: oX, y: oY, width: Double(widthX), height: Double(heightY)));
            emptyContainer.backgroundColor = K_EMPTY_TABLE_BG_COLOR;
            ///tblCell.contentView.addSubview(emptyContainer);
            
            let imgWidth = 100.0;
            let imgHeight = 100.0;
            oX = (Double(widthX) - imgWidth)/2;
            oY = (Double(heightY) - imgHeight)/2;
            
            let imageView = UIImageView.init(frame: CGRect(x: oX, y: oY, width: imgWidth, height: imgHeight));
            imageView.backgroundColor = UIColor.clear;
            imageView.contentMode = .scaleAspectFit;
            imageView.image = UIImage.init(named: "alert");
            emptyContainer.addSubview(imageView);
            
            let titleWidth = emptyContainer.frame.size.width - 20.0;
            let titleHeight = 40.0;
            let OY = imageView.frame.size.height + imageView.frame.origin.y + 10.0;
            
            let titleLbl = UILabel.init(frame: CGRect(x: 10.0, y: Double(OY), width: Double(titleWidth), height: titleHeight));
            titleLbl.textAlignment = .center;
            titleLbl.numberOfLines = 0;
            titleLbl.lineBreakMode = .byWordWrapping;
            titleLbl.text = "No notification has been received yet"
            titleLbl.textColor = UIColor.darkGray;
            emptyContainer.addSubview(titleLbl);
            //titleLbl.sizeToFit();
            
            tableView.backgroundView  = emptyContainer;
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.notificationDataArray.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        // Configure the cell...
        configureCell(tblCell: cell, indexPath: indexPath);
        return cell
    }
    
    
    func configureCell(tblCell: UITableViewCell, indexPath: IndexPath){
        
        let obj = self.notificationDataArray[indexPath.row];
        
        let logoImageV = tblCell.contentView.viewWithTag(625) as! UIImageView;
        let dateLbl = tblCell.contentView.viewWithTag(626) as! UILabel;
        let nameLbl = tblCell.contentView.viewWithTag(627) as! UILabel;
        let detailedLbl = tblCell.contentView.viewWithTag(628) as! UILabel;
        logoImageV.image = K_PLACE_HOLDER_IMAGE
        logoImageV.clipsToBounds = true;
        
        //let defaults: UserDefaults = UserDefaults.standard
        //let userType = defaults.value(forKey: "globalUserType") as! String;
        
        if(obj.PersonType == "bonusAdmin" && obj.AdminLogoPath != nil){
            let imageUrl = obj.AdminLogoPath;
            logoImageV.sd_setImage(with: URL(string:imageUrl!), placeholderImage: K_PLACE_HOLDER_IMAGE)
        }
        else{
            if(obj.IsImageVerified == "1" && obj.BusinessLogoImagePath != nil)
            {
                logoImageV.sd_setImage(with: URL(string: obj.BusinessLogoImagePath!), placeholderImage: K_PLACE_HOLDER_IMAGE)
            }
        }
        
        
        var notificatioDate = "";
        
        if(obj.CreatedDate != nil){
            notificatioDate = obj.CreatedDate!;
        }
        else{
            notificatioDate = obj.TransDate!;
        }
        
        nameLbl.text = obj.FirstName;
        
        let dates = AppUtility.getDateFromString(notificatioDate, currentFormat: "yyyy-MM-dd HH:mm:ss", desiredFormat: "dd MMM yyyy HH:mm:ss");
        
        dateLbl.text = dates;
        detailedLbl.text = (obj.Description)!;
        detailedLbl.sizeToFit();
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = self.notificationDataArray[indexPath.row];
        self.displayNotificationDetailView(obj);
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating ")
        
        if((scrollView.contentSize.height) < (scrollView.contentOffset.y + scrollView.frame.size.height)){
            
            let offset = self.notificationDataArray.count;
            self.getAllNotifications(offset: offset)
        }
    }

    func displayNotificationDetailView(_ objCustomerNotification : CustomerNotification){
    
        AppUtility.removedBackTitleAndChangeItsColor(VC: self, backTitle: "", backTintColor: UIColor.white);
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "NotificationDetailVC") as! NotificationDetailVC
        
        if(objCustomerNotification.FirstName != nil){
            objVC.businessName = objCustomerNotification.FirstName!;
        }
        
        if(objCustomerNotification.PersonType == "bonusAdmin"){
            let imageUrl = objCustomerNotification.AdminLogoPath;
            objVC.imageUrl = imageUrl!;
        }
        else{
            if(objCustomerNotification.IsImageVerified == "1")
            {
                objVC.imageUrl = objCustomerNotification.BusinessLogoImagePath!;
            }
        }
        var receivedDate = "";
        if(objCustomerNotification.CreatedDate != nil)
        {
            receivedDate = (objCustomerNotification.CreatedDate)!
        }
        else{
            receivedDate = (objCustomerNotification.TransDate)!
        }
        
        objVC.receivedOn = receivedDate;
        objVC.detailDescription = (objCustomerNotification.Description)!
        
        self.navigationController?.pushViewController(objVC, animated: true);
    }
    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
