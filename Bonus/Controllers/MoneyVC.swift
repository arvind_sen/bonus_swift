//
//  MoneyVC.swift
//  Bonus
//
//  Created by Arvind Sen on 07/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class MoneyVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var userMoneyTblView: UITableView!
    @IBOutlet weak var topEarnerContainerView: UIView!
    var objCustomerMoney: CustomerMoney? = nil;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = K_LISTING_VIEW_BG_COLOR;
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
        self.getUserMoneyDetail();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getUserMoneyDetail(){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID!;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["UserID": userId,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_GET_USER_MONEY_DETAIL, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    let status = (dataDict?.object(forKey: "Status"))! as? String
                    
                    if(status == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200)
                    {
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        
                        self.objCustomerMoney = CustomerMoney(data: (dictArray[0] as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>)
                        
                        self.addTopEarnerView();
                        
                        DispatchQueue.main.async {
                            self.userMoneyTblView.reloadData();
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                        
                    }
                }
                
            }
            )
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height = 22;
        if(indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 3){
            height = 27;
        }
        return CGFloat(height);
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        // Configure the cell...
        self.configureTableViewCell(cell, forRowAtIndexPath: indexPath);
        return cell
    }
    
    // This below method is setting different values for a row
    func configureTableViewCell(_ cell: UITableViewCell, forRowAtIndexPath:IndexPath){
        cell.backgroundColor = UIColor.clear;
        cell.accessoryType = .disclosureIndicator;
        
        let titleLbl = cell.contentView.viewWithTag(1335) as! UILabel;
        let amountLbl = cell.contentView.viewWithTag(1336) as! UILabel;
        let friendsOfFrinedsLbl = cell.contentView.viewWithTag(1337) as! UILabel;
        
        titleLbl.isHidden = false
        amountLbl.isHidden = false
        friendsOfFrinedsLbl.isHidden = true
        
        var currencyType = "";
        if (self.objCustomerMoney?.CurrencyAbrrevation != nil) {
            currencyType = (self.objCustomerMoney?.CurrencyAbrrevation)!
        }
        
        if(forRowAtIndexPath.row == 0 || forRowAtIndexPath.row == 1 || forRowAtIndexPath.row == 3)
        {
            var title = "";
            var amount = "0.00";
            titleLbl.font = UIFont.systemFont(ofSize: 12.0);
            if(forRowAtIndexPath.row == 0){
                title = "MANAGE MONEY";
                titleLbl.font = UIFont.boldSystemFont(ofSize: 12.0);
                amountLbl.isHidden = true;
            }
            else if(forRowAtIndexPath.row == 1){
                title = "TOTAL EARNED";
                
                if(self.objCustomerMoney?.TotalEarnedAmount != nil){
                    amount = String(format: "%@ %.2f", currencyType, Double((self.objCustomerMoney?.TotalEarnedAmount)!)!)
                }
            }
            else if(forRowAtIndexPath.row == 3){
                title = "TOTAL AVAILABLE";
                cell.accessoryType = .none;
                if(self.objCustomerMoney?.TotalBalance != nil){
                    amount = String(format: "%@ %.2f", currencyType, Double((self.objCustomerMoney?.TotalBalance)!)!)
                }
            }
            titleLbl.text = title;
            amountLbl.text = amount;
        }
        else if(forRowAtIndexPath.row == 2){
            cell.accessoryType = .none;
            cell.backgroundColor = UIColor.darkGray;
            titleLbl.isHidden = true
            amountLbl.isHidden = true
            friendsOfFrinedsLbl.isHidden = false
            
            var yourAmount = "0.0";
            var friendsAmount = "0.0";
            var friendsOfFriendsAmount = "0.0";
            
            if (self.objCustomerMoney?.Yourself != nil) {
                yourAmount = String(format: "%@ %.2f", currencyType, Double((self.objCustomerMoney?.Yourself)!)!)
            }
            
            if (self.objCustomerMoney?.Friend != nil) {
                friendsAmount = String(format: "%@ %.2f", currencyType, Double((self.objCustomerMoney?.Friend)!)!)
            }
            
            if (self.objCustomerMoney?.FriendsOfFriends != nil) {
                friendsOfFriendsAmount = String(format: "%@ %.2f", currencyType, Double((self.objCustomerMoney?.FriendsOfFriends)!)!)
            }
            
            friendsOfFrinedsLbl.text = ("You:\(yourAmount)   Friend:\(friendsAmount)   Friends of Friend:\(friendsOfFriendsAmount)");
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.row == 0){
            AppUtility.removedBackTitleAndChangeItsColor(VC: self.parent!, backTitle: "", backTintColor: UIColor.white);
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objManageMoneyVC = storyboard.instantiateViewController(withIdentifier: "ManageMoneyVC") as? ManageMoneyVC
            objManageMoneyVC?.objCustomerMoney = self.objCustomerMoney;
            self.navigationController?.pushViewController(objManageMoneyVC!, animated: true);
        }
        else if(indexPath.row == 1){
            AppUtility.removedBackTitleAndChangeItsColor(VC: self.parent!, backTitle: "", backTintColor: UIColor.white);
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objManageMoneyVC = storyboard.instantiateViewController(withIdentifier: "TotalEarningVC") as? TotalEarningVC
            objManageMoneyVC?.objCustomerMoney = self.objCustomerMoney;
            self.navigationController?.pushViewController(objManageMoneyVC!, animated: true);
        }
    }
    
    func addTopEarnerView()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objTopEarner = storyboard.instantiateViewController(withIdentifier: "TopEarnerVC") as? TopEarnerVC
        objTopEarner?.objCustomerMoney = self.objCustomerMoney;
        objTopEarner?.view.frame = CGRect(x: 0.0, y: 0.0, width: Double(self.topEarnerContainerView.frame.width), height: Double(self.topEarnerContainerView.frame.height));
        self.topEarnerContainerView.addSubview((objTopEarner?.view)!)
        self.addChildViewController((objTopEarner)!)
        objTopEarner?.didMove(toParentViewController: self);

        //self.addChildViewController(vc)
        //vc.view.frame = CGRect(x: 0, y: 0, width: self.container.frame.size.width, height: self.container.frame.size.height)
        //self.container.addSubview(vc.view)
        //vc.didMoveToParentViewController(self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
