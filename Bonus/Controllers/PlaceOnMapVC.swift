//
//  PlaceOnMapVC.swift
//  Bonus
//
//  Created by Arvind Sen on 14/10/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit
import MapKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class PlaceOnMapVC: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    var clLocationCoordinate: CLLocationCoordinate2D?
    var locationTitle: String?
    var locationSubTitle: String?
    var dealCategoryImage: String?
    var bonusPotLocationID: String?
    var bonusPotMaturityAmount: String?
    var branchID: String?
    
    @IBOutlet weak var containerViewLayout: NSLayoutConstraint!
    var isMapShown: Bool = false
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var closeBtnViewLayout: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.isMapShown = true;
        // Setup the different view constant constrains
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false
        
        if (self.isMapShown == true) {
            self.showLocationOnMap();
            self.isMapShown = false;
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touchesBegan");
        if let touch = touches.first {
            if touch.view == self.mapView {
                //self.hideInView()
            } else {
                self.cancelBtnTapped(touches as AnyObject);
            }
            
        }
    }
     */
    
    func showLocationOnMap(){
        
        let location = CLLocationCoordinate2DMake(clLocationCoordinate!.latitude, clLocationCoordinate!.longitude);
        let region = MKCoordinateRegionMakeWithDistance(location, 5000, 5000);
        self.mapView.setRegion(region, animated: true);
        
        self.mapView.removeAnnotations(self.mapView.annotations);
        
        
        let annotation = MyCustomAnnotation.init(title: locationTitle!, coordinate:location, subtitle:locationSubTitle!);
        
        if (bonusPotLocationID != nil && ((self.bonusPotLocationID?.characters.count) > 0 && self.bonusPotLocationID != "0")) {
            annotation.bonusPotLocationID = bonusPotLocationID;
        }
        else if (dealCategoryImage != nil) {
            annotation.dealCategroyImage = dealCategoryImage;
        }
        
        
        
        if (branchID != nil) {
           annotation.annotationNumber = NSInteger(branchID!)!;
        }
        
        self.mapView.addAnnotation(annotation);
        self.mapView.selectAnnotation(annotation, animated: true);
        
    }
    
    func mapView(_ mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        if (annotation .isKind(of: MKUserLocation.self)) {
            return nil
        }
        
        if (annotation.isKind(of: MyCustomAnnotation.self)) {
            var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: "CustomPinAnnotation")
            pinView = nil;
            pinView?.isUserInteractionEnabled = true;
            
            if (pinView == nil) {
                pinView = MKAnnotationView.init(annotation: annotation, reuseIdentifier: "CustomPinAnnotation")
            }
            else{
                pinView!.annotation = annotation;
            }
            
            pinView?.canShowCallout = true;
            
            let otherPin = UIImage(named: "OthersPin")
            pinView?.centerOffset = CGPoint(x: 0, y: -((otherPin?.size.height)!/2))
            pinView?.image = otherPin;
            
            let myAnnotation = annotation as! MyCustomAnnotation
            
            let manager = SDWebImageManager.shared()
            
            if(myAnnotation.dealCategroyImage != nil)
            {
            manager?.downloadImage(with: URL(string: myAnnotation.dealCategroyImage!), options: SDWebImageOptions.cacheMemoryOnly, progress: nil, completed: { (img, error, SDImageCacheType, bool, NSURL) in
                
                pinView!.image = img; // Setting pin image when downloaded from the server
                if (myAnnotation.bonusPotLocationID != nil && Int(myAnnotation.bonusPotLocationID!)! > 0) {
                    pinView!.image = UIImage(named:"MapIconYellow");
                }
            })
            }
            
            return pinView;
        }
        
        return nil;
    }

    @IBAction func cancelBtnTapped(_ sender: AnyObject) {
        
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        //self.dismissViewControllerAnimated(true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 
    @IBAction func transparentViewTouched(_ sender: Any) {
        print("transparentViewTouched");
        self.cancelBtnTapped("transparentViewTouched" as AnyObject);
    }
     */
}
