//
//  SubClassOfSWRevealVC.swift
//  Bonus
//
//  Created by Arvind Sen on 30/08/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit

class SubClassOfSWRevealVC: SWRevealViewController {
    
    var selectedNavigationController: UINavigationController?
    override func viewDidLoad() {
        super.viewDidLoad()
        //AppUtility.navigationBarTextColor(self);
        
        
        self.rearViewRevealOverdraw = 0 // Set it for rearview
        // Do any additional setup after loading the view.
        self.extendsPointInsideHit = true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
