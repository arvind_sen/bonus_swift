//
//  SharingVC.swift
//  Bonus
//
//  Created by Arvind Sen on 03/06/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class SharingVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    var objBranchCoupon: BranchCoupon? = nil;
    var objCustomerCoupon: CustomerCoupon? = nil;
    
    @IBOutlet weak var ContainerViewHeightConstraint: NSLayoutConstraint!
    var popupVCDelegate : DismissPopupVCProtocol? = nil
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var optionTblView: UITableView!
    @IBOutlet weak var closeBtn: UIButton!
    var selectedIndexPath: NSIndexPath? = nil
    var rowToggleFlag = false;
    var enteredEmailId = UITextField();
    override func viewDidLoad() {
        super.viewDidLoad()

        super.viewDidLoad()
        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false;
        // Do any additional setup after loading the view.
        
        self.containerView.backgroundColor = BLUE_BG_COLOR;
        self.containerView.layer.cornerRadius = 7.0;
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.containerView.layer.borderWidth = 1.0;
        self.containerView.clipsToBounds = true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        self.popupVCDelegate?.dismissPopupViewController(self, responseObj: nil);
        // Removed it from superview controller
        self.view.removeFromSuperview()
        self.removeFromParentViewController();
        
    }

    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var rowHeight = 40.0;
        
        if(indexPath.row == 1 && self.rowToggleFlag == true){
            rowHeight = rowHeight + rowHeight * 2;
        }
        return CGFloat(rowHeight);
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        // Configure the cell...
        self.configureTableViewCell(cell, forRowAtIndexPath: indexPath);
        return cell
    }
    
    // This below method is setting different values for a row
    func configureTableViewCell(_ cell: UITableViewCell, forRowAtIndexPath:IndexPath){
        
        let rowTitle = cell.contentView.viewWithTag(3332) as! UILabel;
        let arrowImg = cell.contentView.viewWithTag(3232) as! UIImageView;
        
        rowTitle.font = UIFont.systemFont(ofSize: 16.0);
        arrowImg.isHidden = false;
        cell.backgroundColor = UIColor.clear;
        cell.contentView.backgroundColor = UIColor.clear;
        
        if(forRowAtIndexPath.row == 0){
            cell.backgroundColor = UIColor.lightGray
            cell.contentView.backgroundColor = UIColor.lightGray;
            arrowImg.isHidden = true;
            
            rowTitle.text = "Select Sharing Option";
            rowTitle.font = UIFont.boldSystemFont(ofSize: 16.0);
        }
        else if(forRowAtIndexPath.row == 1){
            rowTitle.text = "Custom Bonus Sharing";
            
            if(self.selectedIndexPath != nil && self.rowToggleFlag == true){
                
                
                let txtFHeight = 30.0;
                var oX = CGFloat(10.0);
                var oY = rowTitle.frame.origin.y + rowTitle.frame.size.height;
                let txtFWidth = cell.contentView.frame.size.width - 2*oX;
                
                self.enteredEmailId = UITextField.init(frame: CGRect(x: oX, y: oY, width: txtFWidth, height: CGFloat(txtFHeight)));
                self.enteredEmailId.placeholder = "Email Address";
                self.enteredEmailId.keyboardType = .emailAddress;
                self.enteredEmailId.borderStyle = .roundedRect;
                self.enteredEmailId.delegate = self;
                self.enteredEmailId.font = UIFont.systemFont(ofSize: 16.0);
                self.enteredEmailId.returnKeyType = .done;
                self.enteredEmailId.backgroundColor = UIColor.white;
                self.enteredEmailId.spellCheckingType = .no;
                self.enteredEmailId.textAlignment = .center;
                self.enteredEmailId.autocapitalizationType = .none;
                self.enteredEmailId.autocorrectionType = .no;
                cell.contentView.addSubview(self.enteredEmailId);

            
                let btnWidth = 50.0;
                let btnHeight = 25.0;
                oX = CGFloat((Double(cell.frame.size.width) - btnWidth)/2.0);
                oY = self.enteredEmailId.frame.size.height + self.enteredEmailId.frame.origin.y + 5.0;
                
                let btn = UIButton.init(frame: CGRect(x: Double(oX), y: Double(oY), width: btnWidth, height: btnHeight))
                btn.setTitle("Send", for: .normal)
                btn.addTarget(self, action: #selector(sendButtonTapped), for: .touchUpInside)
                cell.contentView.addSubview(btn);
                //btn.addTarget
                /*
                [btn setTitle:@"Send" forState:UIControlStateNormal];
                [btn addTarget:self action:@selector(sendButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:btn];
                
                CGFloat sepratorOY = rowHeight * 2.7 - 1;
                if (K_SCREEN_SIZE.height < 500) {
                    sepratorOY += 20;
                }
                 */
            }
           
        }
        else if(forRowAtIndexPath.row == 2){
            rowTitle.text = "Other Sharing options";
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndexPath = nil;
        
        if(indexPath.row == 1){
            self.selectedIndexPath = indexPath as NSIndexPath?;
            self.showHideRow(indexPath);
        }
        else if(indexPath.row == 2){
            if (self.rowToggleFlag == true) {
                self.showHideRow(indexPath);
            }
            //self.optionTblView.reloadData();
            self.sharingWithOtherOption();
        }
    }
    
    func showHideRow(_ indexPath: IndexPath) {
        if(self.rowToggleFlag == false){
            self.rowToggleFlag = true;
            self.ContainerViewHeightConstraint.constant =  self.ContainerViewHeightConstraint.constant + (40.0 * 2);
        }
        else{
            self.rowToggleFlag = false;
            self.ContainerViewHeightConstraint.constant =  self.ContainerViewHeightConstraint.constant - (40.0 * 2);
        }
        self.optionTblView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.bottom);
    }
 
    func sendButtonTapped() {
        
        
        let emailId : String = self.enteredEmailId.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        self.enteredEmailId.text = emailId;
        let isValidEmail = AppUtility.validateEmail(emailId);
        
        var message : String? = nil;
        
        if !isValidEmail {
            message = LOGIN_ALERT_MESSAGE_EMAIL_ID;
        }
        
        if message != nil {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
        }
        else
        {
            let defaults: UserDefaults = UserDefaults.standard
            let nsDataUser = defaults.object(forKey: "UserData") as! Data
            let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
            let userId = user.ID;
            let userName = user.FirstName;
               
            let deviceUniqueId = AppUtility.getDeviceNSUUID();
            
            var dealMasterId = "";
            var branchID = "";
            
            if(self.objCustomerCoupon != nil){
                dealMasterId = (self.objCustomerCoupon?.DealMasterID)! ;
                branchID = (self.objCustomerCoupon?.BranchID)! ;
            }
            else{
                dealMasterId = (self.objBranchCoupon?.DealMasterID)! ;
                branchID = (self.objBranchCoupon?.BranchID)! ;
            }
            
            // Created postDataDictonary to post the data on server
            let postDataDict : NSDictionary = ["DealID" : dealMasterId,
                                               "BranchID": branchID,
                                               "UserID": userId!,
                                               "UserName": userName!,
                                               "ShareEmail": emailId,
                                               "DeviceUniqueID": deviceUniqueId,
                                               "api_key": kWEB_SERVICES_API_KEY];
            
            print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_SHARE_COUPONS, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! NSNumber) == 200) {
                        
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                            
                        let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
        
                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
                                self.closeBtnTapped("");
                            }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        
                    }
                }
            }
            )
            }
        }
    }
    
    func sharingWithOtherOption() {
        
        var dealMasterId = "";
        var branchID = "";
        
        if(self.objCustomerCoupon != nil){
            dealMasterId = (self.objCustomerCoupon?.DealMasterID)! ;
            branchID = (self.objCustomerCoupon?.BranchID)! ;
        }
        else{
            dealMasterId = (self.objBranchCoupon?.DealMasterID)! ;
            branchID = (self.objBranchCoupon?.BranchID)! ;
        }
        
        let getUrlString = kWEB_SHARING_URL + "/" + dealMasterId + "/" + branchID
        
        print("postDataDict = %", getUrlString);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(getUrlString, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "status"))! as! String == "success") {
                        
                        let sharingUrl = (dataDict?.object(forKey: "url"))! as! String;
                        self.sharingWithURL(sharingUrl: sharingUrl);
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = dataDict?.object(forKey: "Message")! as! String;
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                        
                    }
                }
            }
            )
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    func sharingWithURL(sharingUrl: String){
    
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userName = user.FirstName;
        
        var businessName = "";
        
        var firstName = "";
        var typeOfCoupon = "";
        if(self.objCustomerCoupon != nil){
            firstName = (self.objCustomerCoupon?.FirstName)! ;
            typeOfCoupon = (self.objCustomerCoupon?.TypeOfCoupon)! ;
        }
        else{
            firstName = (self.objBranchCoupon?.FirstName)! ;
            typeOfCoupon = (self.objBranchCoupon?.TypeOfCoupon)! ;
        }
        
        if(firstName != ""){
            let str1 = String(htmlEncodedString: firstName);
            let str2 = String(htmlEncodedString: str1);
            businessName = str2;
        }
        
        let dealWithTitle = "Bonus : " + businessName + " : " + typeOfCoupon;
        let inviterName = userName! + " thought you might be interested in following Bonus Coupon.";
        let business = "Business : " + businessName;
        let couponType = "Coupon Type : " + typeOfCoupon
        
        
        let activityItem = ["Check out this Coupon", dealWithTitle, inviterName,  business, couponType, sharingUrl];
        
        let activityVC = UIActivityViewController.init(activityItems: activityItem, applicationActivities: nil);
        activityVC.setValue(dealWithTitle, forKey: "subject");
        activityVC.completionWithItemsHandler = { (activity, success, items, error) in
            
            print(success ? "SUCCESS!" : "FAILURE")
            self.closeBtnTapped("");
            
        }
        self.present(activityVC, animated: true, completion: nil);
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
