//
//  TopEarnerVC.swift
//  Bonus
//
//  Created by Arvind Sen on 11/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class TopEarnerVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tblView: UITableView!
    var topEarnersArray : Array<TopEarner> = [TopEarner]();
    var objCustomerMoney: CustomerMoney? = nil;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getTopEarnersDetails();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getTopEarnersDetails(){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID!;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["UserID": userId,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_GET_TOP_EARNERS, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("TopEarnersJSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    let status = (dataDict?.object(forKey: "Status"))! as? String
                    
                    if(status == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200)
                    {
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                       
                        self.topEarnersArray.removeAll();
                        
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let obj = TopEarner(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>)
                                self.topEarnersArray.append(obj);
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.tblView.reloadData();
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                    }
                }
            }
            )
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.topEarnersArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        // Configure the cell...
        configureCell(tblCell: cell, indexPath: indexPath);
        return cell
    }
    
    
    func configureCell(tblCell: UITableViewCell, indexPath: IndexPath){
        
        tblCell.backgroundColor = UIColor.clear
        let obj = self.topEarnersArray[indexPath.row];

        let progressView = tblCell.contentView.viewWithTag(1340);
        progressView?.frame = CGRect(x: 0.0, y: 0.0, width: 0.0, height: Double(tblCell.frame.size.height));
        progressView?.isHidden = false;
        let nameLbl = tblCell.contentView.viewWithTag(1341) as! UILabel;
        let amountLbl = tblCell.contentView.viewWithTag(1342) as! UILabel;
        
        let fistObject = self.topEarnersArray[0] as? TopEarner;
        var topEarner = fistObject?.AmountInBaseCurrency! ;
        let currentEarner = obj.AmountInBaseCurrency!;
        
        if(topEarner! == "0"){
            topEarner = "1";
        }
        
        let percenteValue = (Double(tblCell.frame.size.width) / Double(topEarner!)! * Double(currentEarner)!)
        UIView.animate(withDuration: 1.0) {
            
            progressView?.frame = CGRect(x: 0.0, y: 0.0, width: percenteValue, height: Double(tblCell.frame.size.height));
        }
        
        if(obj.NickName != nil && (obj.NickName?.characters.count)! > 0)
        {
            nameLbl.text = obj.NickName!;
        }
        else if(obj.FirstName != nil && (obj.FirstName?.characters.count)! > 0)
        {
            nameLbl.text = obj.FirstName!;
        }
        
        if(obj.AmountInBaseCurrency != nil){
            amountLbl.text = String(format: "%@ %.2f", (obj.sBaseCurrency!) , Double((obj.AmountInBaseCurrency)!)!)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat(27.0);
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let mainView = UIView();
        if(self.topEarnersArray.count > 0)
        {
            mainView.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 25.0);
            mainView.backgroundColor = UIColor.lightGray;
            
            let sepratorView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1.0))
            sepratorView.backgroundColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1);
            mainView.addSubview(sepratorView);
            
            //let objMoneyVC = self.parent as! MoneyVC
            let fistObject = self.topEarnersArray[0];
            var topEarner = fistObject.AmountInBaseCurrency! ;
            let currentEarner = self.objCustomerMoney?.TotalEarnedAmount!;
             let currencyAbbr = self.objCustomerMoney?.CurrencyAbrrevation!;
            
            if(topEarner == "0"){
                topEarner = "1";
            }
            
            let percenteValue = (Double(tableView.frame.size.width) / Double(topEarner)! * Double(currentEarner!)!)
            
            let progressView = UIView(frame: CGRect(x: 0, y: 1, width: 0.0, height: 25.0));
            progressView.backgroundColor = UIColor(red: 64/255, green: 171/255, blue: 57/255, alpha: 1);
            mainView.addSubview(progressView);
            
            UIView.animate(withDuration: 1.0) {
                progressView.frame = CGRect(x: 0.0, y: 1.0, width: percenteValue, height: Double(25.0));
            }
        
            let nameLblWidth = mainView.frame.size.width * 0.70;
            let nameLbl = UILabel(frame: CGRect(x: 5.0, y: 0, width: nameLblWidth, height: 25.0))
            nameLbl.textColor = UIColor.white;
            nameLbl.backgroundColor = .clear;
            nameLbl.font = UIFont.systemFont(ofSize: 12.0);
            nameLbl.text = "You";
            mainView.addSubview(nameLbl);
            
            let amountLblWidth = mainView.frame.size.width * 0.30 - 10.0;
            let oX = mainView.frame.size.width - amountLblWidth;
            let amountLbl = UILabel(frame: CGRect(x: oX, y: 0, width: amountLblWidth, height: 25.0))
            amountLbl.textColor = UIColor.white;
            amountLbl.textAlignment = .right;
            amountLbl.backgroundColor = .clear;
            amountLbl.font = UIFont.systemFont(ofSize: 12.0);
            
            amountLbl.text = String(format: "%@ %.2f", (currencyAbbr!) , Double((currentEarner)!)!)
            mainView.addSubview(amountLbl);
        }
        return mainView;
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
