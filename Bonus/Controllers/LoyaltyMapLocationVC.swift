//
//  LoyaltyMapLocationVC.swift
//  Bonus
//
//  Created by Arvind Sen on 30/09/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class LoyaltyMapLocationVC: UIViewController, MKMapViewDelegate, UISearchBarDelegate, CLLocationManagerDelegate {

    var revealVC : SWRevealViewController? = nil;
    var searchBar: UISearchBar!
    var listArray: NSMutableArray!
    var currentLocationCoordinate : CLLocationCoordinate2D? ;
    var selectedCategoriesFromLeftMenus : String?
    var selectedIndexPathFromLeftMenus : IndexPath?

    var isWebServiceCalled = false
    var leftMenusSelectedFor : SelectionFor!
    var selectedDict: NSDictionary!
    var lastOffset: String!
    var locationManager : CLLocationManager?
    var didSelectedRowObj: Loyalty!;
    var searchDataArray: NSMutableArray!
    var isMapViewRegionSet = false;
    
    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // This code is using to hid left menus
        if (self.revealViewController() != nil) {
            //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
        self.showTopBtns();
        self.showMapWithSpecifiedArea();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.showDataOnMapView();
    }
    
    func showTopBtns(){
        
        // Left toggle button configration
        self.navigationController?.navigationBar.barTintColor = TAB_BAR_BG_COLOR;
        self.navigationController?.isNavigationBarHidden = false;
        self.revealVC = self.revealViewController()
        self.navigationController?.navigationBar.addGestureRecognizer(self.revealVC!.panGestureRecognizer())
        
        let revealButtonItem = UIBarButtonItem();
        revealButtonItem.image = UIImage (named: "FilterIcon")
        revealButtonItem.style = UIBarButtonItemStyle.plain;
        revealButtonItem.target = self;
        revealButtonItem.action = #selector(LoyaltyMapLocationVC.revealToggle(_:));
        revealButtonItem.tintColor = UIColor.white;
        self.navigationItem.leftBarButtonItem = revealButtonItem;
        
        //Show search bar on top of the view controller
        
        let oY = CGFloat(5.0)
        let oX = CGFloat(50.0)
        
        let height = (self.navigationController?.navigationBar.frame.height)! - CGFloat(2 * oY);
        let width = (self.navigationController?.navigationBar.frame.width)! - CGFloat(2 * oX);
        //let height = CGFloat (44);
        //let width = self.view.frame.width;
        
        let frameRect = CGRect(x: 0.0, y: oY, width: width, height: height);
        self.searchBar = UISearchBar.init(frame: frameRect);
        self.searchBar.placeholder = "Search"
        self.searchBar.showsCancelButton = false;
        self.searchBar?.delegate = self;
        self.navigationItem.titleView = self.searchBar
        self.navigationItem.titleView?.frame = frameRect;
        //self.navigationItem.titleView?.backgroundColor = UIColor.redColor()
        
        // Right Top tab bar button configration
        var rightBtn = UIImage(named: "MenuIcon")
        rightBtn = rightBtn?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let barRightBtn = UIBarButtonItem(image: rightBtn, style: UIBarButtonItemStyle.plain, target: self, action:#selector(LoyaltyMapLocationVC.rightBtnTapped(_:)));
        self.navigationItem.rightBarButtonItem = barRightBtn;
    }
    
    
    // Method will display map view with specified area
    func showMapWithSpecifiedArea(){
        
        if(currentLocationCoordinate != nil){
            let location = CLLocationCoordinate2DMake((currentLocationCoordinate?.latitude)!, (currentLocationCoordinate?.longitude)!);
            let region = MKCoordinateRegionMakeWithDistance(location, 12500, 12500);
            // 12500 Meter = 12.5 KM
            self.mapView.setRegion(region, animated: true);
        }
        else{
            let location = CLLocationCoordinate2DMake(20.5937, 78.9629);
            let region = MKCoordinateRegionMakeWithDistance(location, 10000, 10000);
            // 12500 Meter = 12.5 KM
            self.mapView.setRegion(region, animated: true);
        }
        
    }
    
    // Method get call to get user current location lat, long
    func callLocationManager() {
        
        if (CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied) {
            
            self.locationManager = CLLocationManager.init();// locationManager need to define at the class variable then only its call delegate methods.
            self.locationManager!.delegate = self;
            
            if self.locationManager!.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
                self.locationManager!.requestWhenInUseAuthorization();
            }
            self.locationManager!.desiredAccuracy = kCLLocationAccuracyBest;
            self.locationManager!.startUpdatingLocation();
        }
        else{
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "To re-enable, please go to Settings and turn on Location Service for this app.", onView: self)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.locationManager!.stopUpdatingLocation();
        let recentLocation = locations.last;
        self.currentLocationCoordinate = recentLocation?.coordinate;
        
        let latString = String(format: "%f", (recentLocation?.coordinate.latitude)!);
        let longString = String(format: "%f", (recentLocation?.coordinate.longitude)!);
        
        let dict = NSDictionary.init(objects: [latString, longString], forKeys: ["Latitude" as NSCopying, "Longitude" as NSCopying]);
        
        self.loyaltyCheckInServiceCall(SelectionFor.isSelectedNearBy, withParameters: dict, offSetValue: "0");
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(" failed to get current location");
        
        self.locationManager!.stopUpdatingLocation();
        let dict = NSDictionary.init(objects: ["0.0", "0.0"], forKeys: ["Latitude" as NSCopying, "Longitude" as NSCopying]);
        self.loyaltyCheckInServiceCall(SelectionFor.isSelectedNearBy, withParameters: dict, offSetValue: "0");
    }
    
    func showDataOnMapView(){
        
        if (self.mapView.annotations.count > 0) {
            self.mapView.removeAnnotations(self.mapView.annotations);
        }
        
        var mutArray :Array = [MyCustomAnnotation]()
        var index = 0;
        
        for obj in self.listArray  {
            
            let loyalty = obj as! Loyalty;
            
            if (loyalty.Longitude != nil && loyalty.Latitude != nil)
            {
                let latitude = loyalty.Latitude;
                let longitude = loyalty.Longitude;
                let location = CLLocation.init(latitude: Double(latitude!)!, longitude: Double(longitude!)!)

//                if (loyalty.BonusPotLocationID != nil)
//                {
//                    annotation.bonusPotLocationID = loyalty.BonusPotLocationID;
//                    annotation.bonusPotCurrentBalance = loyalty.NumberOfBalanceFreeBee;
//                }

                let annotation = MyCustomAnnotation.init(title: loyalty.FirstName!, coordinate:location.coordinate, subtitle:loyalty.BranchDescription!);
                annotation.dealCategroyImage = loyalty.MapPinCategoryPicture;
                annotation.bonusPotLocationID = loyalty.BonusPotLocationID;
                annotation.indexNumber = index;
                annotation.annotationNumber = NSInteger(loyalty.BranchID!)!;
                mutArray.append(annotation);
                index += 1;
            }
        }
        
        if (mutArray.count > 0) {
            self.mapView.addAnnotations(mutArray);
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if (annotation .isKind(of: MKUserLocation.self)) {
            return nil
        }
        
        if (annotation.isKind(of: MyCustomAnnotation.self)) {
            var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: "CustomPinAnnotation")
            pinView = nil;
            pinView?.isUserInteractionEnabled = true;
            
            if (pinView == nil) {
                pinView = MKAnnotationView.init(annotation: annotation, reuseIdentifier: "CustomPinAnnotation")
            }
            else{
                pinView!.annotation = annotation;
            }
            
            pinView?.canShowCallout = true;
            
            let otherPin = UIImage(named: "OthersPin")
            pinView?.centerOffset = CGPoint(x: 0, y: -((otherPin?.size.height)!/2))
            pinView?.image = otherPin;
            
            let myAnnotation = annotation as! MyCustomAnnotation
            
            let manager = SDWebImageManager.shared()
            
            _ = manager?.downloadImage(with: URL(string: myAnnotation.dealCategroyImage!), options: SDWebImageOptions.cacheMemoryOnly, progress: nil, completed: { (img, error, SDImageCacheType, bool, NSURL) in
                
                pinView!.image = img; // Setting pin image when downloaded from the server
                if (myAnnotation.bonusPotLocationID != nil && myAnnotation.bonusPotLocationID?.characters.count > 0) {
                    pinView!.image = UIImage(named:"MapIconYellow");
                }
            })
            
            // Because this is an iOS app, add the detail disclosure button to display details about the annotation in another view.
            let arrowImg = UIImage(named: "Arrow")
            let arrowFram = CGRect(x: 0, y: 0, width: 40.0, height: 40.0);
            let arrowBtn = UIButton.init(type: .custom);
            arrowBtn.frame = arrowFram;
            arrowBtn.setImage(arrowImg, for: UIControlState())
            arrowBtn.backgroundColor = UIColor.clear;
            pinView?.rightCalloutAccessoryView = arrowBtn;
            
            return pinView;
        }
        
        return nil;
    }
    
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        if (isMapViewRegionSet == true) {
            //isMapViewRegionSet = false;
        }
        else{
            
            print("--- lat = \(mapView.centerCoordinate.latitude), long = \(mapView.centerCoordinate.longitude)");
            self.currentLocationCoordinate = mapView.centerCoordinate;
            
            let latString = String(format: "%f", (mapView.centerCoordinate.latitude));
            let longString = String(format: "%f", (mapView.centerCoordinate.longitude));
            
            let dict = NSDictionary.init(objects: [latString, longString], forKeys: ["Latitude" as NSCopying, "Longitude" as NSCopying]);
            
            self.loyaltyCheckInServiceCall(leftMenusSelectedFor, withParameters: dict, offSetValue: "0");
            isMapViewRegionSet = true;
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        let myCustAnn = view.annotation as! MyCustomAnnotation;
        
        let obj = self.listArray[myCustAnn.indexNumber!] as! Loyalty;
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "LoyaltyDetailVC") as! LoyaltyDetailVC
        objVC.loyaltyObj = obj;
        self.navigationController?.pushViewController(objVC, animated: true);
    }

    
    func revealToggle(_ obj : AnyObject?){
        
        self.searchBar.resignFirstResponder();
        self.revealVC!.revealToggle(nil)
    }
    
    func callLoyaltyTVCFromLeftMenu(_ indexPath: IndexPath, categoriesIds: String = "") -> Void {
        
        print("indexPath = %@", indexPath);
        
        if (indexPath.section == 0 && indexPath.row == 0) {
            self.loyaltyCheckInServiceCall(SelectionFor.isSelectedFavorite, withParameters: nil)
        }
        else if (indexPath.section == 1 && indexPath.row == 0){
            self.callLocationManager();
        }
        else if (indexPath.section == 1 && indexPath.row == 1){
            self.loyaltyCheckInServiceCall(SelectionFor.isSelectedAToZ, withParameters: nil)
        }
        else if (indexPath.section == 2){
            
            let categoriesDict = NSDictionary.init(object: categoriesIds, forKey: "ID" as NSCopying);
            self.loyaltyCheckInServiceCall(SelectionFor.isSelectedProductGroup, withParameters: categoriesDict);
        }
    }
    
    
    func loyaltyCheckInServiceCall(_ selectedSection: SelectionFor, withParameters: NSDictionary?, offSetValue: String = "0") -> Void {
        
        self.leftMenusSelectedFor = selectedSection;
        self.selectedDict = withParameters;
        self.lastOffset = offSetValue;
        
        var searchFor = "";
        
        if self.searchBar.text?.characters.count > 0 {
            searchFor = self.searchBar.text!;
        }
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        let uniqueID = AppUtility.getDeviceNSUUID();
        let postDataDict : NSMutableDictionary = ["UserID" : user.ID!,
                                                  "FavoriteStatus" : "0",
                                                  "Latitude" : "0",
                                                  "Longitude" : "0",
                                                  "Limit" : K_TABLE_ROW_LIMITS,
                                                  "Offset" : offSetValue,
                                                  "SortBy" : "Distance",
                                                  "BusinessCategory" : 0,
                                                  "OrderBy" : "ASC",
                                                  "SearchFor" : searchFor,
                                                  "api_key" : kWEB_SERVICES_API_KEY,
                                                  "DeviceUniqueID" : uniqueID];
        
        if (self.currentLocationCoordinate != nil) {
            let latString = String(format: "%f", self.currentLocationCoordinate!.latitude);
            let longString = String(format: "%f", self.currentLocationCoordinate!.longitude);
            
            postDataDict.setObject(latString, forKey: "Latitude" as NSCopying);
            postDataDict.setObject(longString, forKey: "Longitude" as NSCopying);
        }
        
        switch selectedSection {
        case .isSelectedFavorite:
            postDataDict.setObject("1", forKey: "FavoriteStatus" as NSCopying)
            break;
        case .isSelectedNearBy:
            postDataDict.setObject("ASC", forKey: "OrderBy" as NSCopying)
            break;
        case .isSelectedAToZ:
            postDataDict.setObject("BranchDescription", forKey: "SortBy" as NSCopying)
            postDataDict.setObject("ASC", forKey: "OrderBy" as NSCopying)
            break;
        default:
            postDataDict.setObject(withParameters!.object(forKey: "ID")! as! String, forKey: "BusinessCategory" as NSCopying)
        }
        
        postDataDict.setObject("1", forKey: "IsShowOnMap" as NSCopying);
        postDataDict.setObject(user.PersonType!, forKey: "PersonType" as NSCopying);
        
        self.callLoyaltyService(postDataDict);
    }
    
    
    func callLoyaltyService(_ postData: NSDictionary){
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_NEAREST_PUNCH_CARD_BUSINESS_LIST, method: HTTPMethod.post, parameters: postData as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        let resourcePath = dataDict?.object(forKey: "ResourcePath") as! Dictionary<String, AnyObject>
                        
                        self.listArray = NSMutableArray();
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let item = Loyalty(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>, resourcePaths: resourcePath)
                                
                                self.listArray?.add(item);
                            }
                        }
                        else
                        {
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            
                            self.navigationController?.view.makeToast(message, duration: 2.0, position: .center)
                            //AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        }
                        
                        DispatchQueue.main.async {
                            self.showDataOnMapView();
                            self.isMapViewRegionSet = false;
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        
                    }
                }
                }
            )
        }
    }
    
    // Method will call to open map view on current view controller
    func rightBtnTapped(_ sender: AnyObject){
        
        //print( "controllers = %@", self.navigationController?.viewControllers)
        
        let loyaltyVC = self.navigationController?.viewControllers.first as! LoyaltyTVC;
        loyaltyVC.loyaltyListArray = self.listArray;
        loyaltyVC.isMapViewComeBack = true;
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = "flip"
        transition.subtype = kCATransitionFromLeft
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popViewController(animated: true);
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.loyaltyCheckInServiceCall(self.leftMenusSelectedFor, withParameters: self.selectedDict);
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        //self.searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        // searchBar.setShowsCancelButton(false, animated: true)
        //self.searchDataWithEnrty(nil);
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
