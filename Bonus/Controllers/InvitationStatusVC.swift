//
//  InvitationStatusVC.swift
//  Bonus
//
//  Created by Arvind Sen on 22/06/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit

class InvitationStatusVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    var dataArray: Array<Any>? = nil;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false
        // Do any additional setup after loading the view.
        
        self.containerView.layer.cornerRadius = 5.0;
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.containerView.layer.borderWidth = 1.0;
        self.containerView.clipsToBounds = true;
        
        self.tblView.register(UITableViewCell.classForKeyedArchiver(), forCellReuseIdentifier: "reuseIdentifier")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataArray!.count;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0;
        let dict = self.dataArray?[section] as! Dictionary<String, Any>;
        let array = dict["emails"] as? Array<Any>;
        rows = (array?.count)!;
        
        return rows;
    }
    
    func configure(_ cell: UITableViewCell, forRowAtIndexPath indexPath: IndexPath) {
        //cell.textLabel!.text = "Row \(indexPath.row)"
        
        let dict = self.dataArray?[indexPath.section] as! Dictionary<String, Any>;
        let array = dict["emails"] as? Array<Any>;
        cell.textLabel?.text = array?[indexPath.row] as? String;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let reuseCellId : String = "reuseIdentifier"
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseCellId)
        
        configure(cell, forRowAtIndexPath: indexPath)
        
        return cell;
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0;
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let lblView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: Double(tableView.frame.size.width), height: 40.0));
        lblView.backgroundColor = UIColor.lightGray;
        
        let lbl = UILabel(frame: CGRect(x: 10.0, y: 0.0, width: Double(lblView.frame.size.width) - (2 * 10.0), height: 40.0))
        
        let dict = self.dataArray?[section] as! Dictionary<String, Any>;
        let headerTitle = dict["headerTitle"] as? String;
        lbl.text = headerTitle;
        lbl.textColor = UIColor.white;
        lbl.backgroundColor = UIColor.clear;
        lblView.addSubview(lbl);
        
        return lblView;
    }
    
    @IBAction func closeBtnTapped(_ sender: AnyObject) {
        // Removed it from superview controller
        self.view.removeFromSuperview()
        self.removeFromParentViewController();
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
