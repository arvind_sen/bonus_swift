//
//  BranchCouponsTVC.swift
//  Bonus
//
//  Created by Arvind Sen on 19/01/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
//import CoreLocation
import Alamofire
import CoreLocation

class BranchCouponsTVC: UITableViewController, UISearchBarDelegate {

    var objPlaceOnMapVC : PlaceOnMapVC? = nil;
    var activeBusinessObj : ActiveBusiness? = nil;
    var searchBar : UISearchBar!
    var branchCouponsArray : Array<BranchCoupon> = [BranchCoupon]()
    var originalBranchCouponsArray : Array<BranchCoupon> = [BranchCoupon]()
    var locationCoordinate: CLLocationCoordinate2D? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.tableView.backgroundColor = K_LISTING_VIEW_BG_COLOR;
        self.showTopBtns();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
        
        if(self.activeBusinessObj != nil){
            self.callBranchSpecificDeals(0);
        }
    }

    func setBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
        btnLeftMenu.setImage(UIImage(named: "ArrowBackBtn"), for: UIControlState())
        btnLeftMenu.addTarget(self, action: #selector(self.onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: -10, y: 0, width: 25, height: 35)
        //btnLeftMenu.backgroundColor = UIColor.red;
        btnLeftMenu.contentHorizontalAlignment = .left;
        btnLeftMenu.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, -20);
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func onClcikBack(){
        self.hideChildView();
        self.navigationController?.popViewController(animated: true);
    }
    
    func showTopBtns(){
        
        self.setBackBarButtonCustom();
        let oY = CGFloat(5.0)
        let oX = CGFloat(0.0)
        
        let height = (self.navigationController?.navigationBar.frame.height)! - CGFloat(2 * oY);
        let width = (self.navigationController?.navigationBar.frame.width)! - CGFloat(2 * oX);
        self.searchBar = UISearchBar.init(frame: CGRect(x: -20.0, y: oY, width: width, height: height));
        self.searchBar.placeholder = "Search"
        self.searchBar.showsCancelButton = false;
        self.searchBar?.delegate = self;
        self.navigationItem.titleView = self.searchBar
        self.navigationItem.titleView?.clipsToBounds = false;
    }
    
    func callBranchSpecificDeals(_ offsetValue : Int){
        
        let defaults: UserDefaults = UserDefaults.standard
        var userId = ""
        var personType = "";
        
        let globalUserType = defaults.object(forKey: "globalUserType") as! String;
        
        if(globalUserType == USER_TYPE_VISITOR){
            userId = "0";
            personType = USER_TYPE_VISITOR;
        }
        else{
            
            let nsDataUser = defaults.object(forKey: "UserData") as! Data
            let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
            userId = user.ID!
            personType = user.PersonType!;
        }


        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        var latitude = "0";
        var longitude = "0";
        if(self.locationCoordinate != nil){
            
           latitude = ("\((self.locationCoordinate?.latitude)!)");
            longitude = ("\((self.locationCoordinate?.longitude)!)");
        }
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSMutableDictionary = ["UserID": userId,
                                           "PersonType": personType,
                                           "BranchID": (self.activeBusinessObj?.BranchID)!,
                                           "DealId": "0",
                                           "Latitude": latitude,
                                           "Longitude": longitude,
                                           "BusinessCategory": "0",
                                           "Limit": K_TABLE_ROW_LIMITS,
                                           "Offset": offsetValue,
                                           "OrderBy": "ASC",
                                           "SortBy": "Distance",
                                           "DeviceUniqueID": deviceUniqueId,
        "api_key": kWEB_SERVICES_API_KEY];
        
        
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicatorOnView(VC: self);
            Alamofire.request(kSERVICE_BRANCH_DEALS, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                OperationQueue.main.addOperation {
                    CustomProgressView.hideProgressIndicatorOnView(VC: self);
                }
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        let resourcePath = dataDict?.object(forKey: "ResourcePath") as! Dictionary<String, AnyObject>
                        
                        if(offsetValue == 0){
                            self.branchCouponsArray.removeAll()
                        }
                        
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let obj = BranchCoupon(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>, resourcePaths: resourcePath)
                                
                                self.branchCouponsArray.append(obj);
                            }
                            
                            self.originalBranchCouponsArray = self.branchCouponsArray;
                            self.tableView.reloadData();
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                }
            }
            )
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.branchCouponsArray.count;
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        // Configure the cell...
        
        configureTableViewCell(cell, forRowAtIndexPath: indexPath);
        
        return cell
    }
    
    // This below method is setting different values for a row
    func configureTableViewCell(_ cell: UITableViewCell, forRowAtIndexPath:IndexPath){
        let obj = branchCouponsArray[forRowAtIndexPath.row];
        
        let containerView = cell.contentView.viewWithTag(99);
        let businessImage = containerView?.viewWithTag(100) as! UIImageView;
        let nameLbl = containerView?.viewWithTag(102) as! UILabel;
        let areaLbl = containerView?.viewWithTag(104) as! UILabel;
        let couponTypeLbl = containerView?.viewWithTag(115) as! UILabel;
        let categoryImage = containerView?.viewWithTag(106) as! UIImageView;
        let mapPinBtn = containerView?.viewWithTag(108) as! UIButton;
        let distanceLbl = containerView?.viewWithTag(110) as! UILabel;
        
        if(obj.IsBusinessImageVerified != nil && Int(obj.IsBusinessImageVerified!) == 1 && obj.BusinessLogoImagePath != nil){
            businessImage.sd_setImage(with: URL(string: obj.BusinessLogoImagePath!), placeholderImage: K_PLACE_HOLDER_IMAGE)
        }else{
            businessImage.image = K_PLACE_HOLDER_IMAGE;
        }
        
        if (obj.FirstName != nil) {
            nameLbl.text = obj.FirstName!;
        }
        else if(obj.BranchDescription != nil){
            nameLbl.text = obj.BranchDescription!;
        }
        
        if(obj.BranchDescription != nil){
            areaLbl.text = obj.BranchDescription!;
        }
        if(obj.TypeOfCoupon != nil){
            couponTypeLbl.text = obj.TypeOfCoupon!;
        }
        
        if(obj.FilledCategoryPicture != nil){
            categoryImage.sd_setImage(with: URL(string: obj.FilledCategoryPicture!), placeholderImage: K_YELLOW_CIRCLE_SMALL_IMAGE)
        }
        
        // Set distance value
        distanceLbl.text = String(format: "%.02f KM", Float(obj.Distance!)!);
        
        mapPinBtn.setImage(K_BONUS_MAP_PIN_IMAGE, for: UIControlState())
        // Set map/pot/faviourite icons
        if(obj.BonusPotLocationID != nil && Int(obj.BonusPotLocationID!)! > 0){
            mapPinBtn.setImage(K_BONUS_POT_IMAGE, for: UIControlState())
        }else if(obj.FavoriteStatus != nil && Int(obj.FavoriteStatus!)! > 0){
            mapPinBtn.setImage(K_FAVIROUTE_IMAGE, for: UIControlState())
        }
        
        for index in 651...655 {
            let starImg = containerView?.viewWithTag(index) as? UIImageView;
            let ratingValue = index - 650;
            if (obj.MaxRating != nil && ratingValue <= Int(obj.MaxRating!)!) {
                starImg?.image = UIImage(named: "starfull")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
            }
            else{
                starImg?.image = UIImage(named: "starempty")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AppUtility.removedBackTitleAndChangeItsColor(VC: self, backTitle: "", backTintColor: UIColor.white);
        let obj = branchCouponsArray[indexPath.row];
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "CouponDetailVC") as! CouponDetailVC
        objVC.objBranchCoupon = obj;
        objVC.dealMasterId = obj.DealMasterID;
        self.navigationController?.pushViewController(objVC, animated: true);
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!;
        cell.accessoryType = UITableViewCellAccessoryType.none;
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        //self.searchBar.setShowsCancelButton(true, animated: true)
        self.hideChildView();
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filteredBranchDeals(searchString: searchText);
    }
    
    func filteredBranchDeals(searchString: String) {
        
        if(searchString == ""){
            self.branchCouponsArray.removeAll();
            self.branchCouponsArray = self.originalBranchCouponsArray;
            self.tableView.reloadData();
        }
        else{
            self.branchCouponsArray = self.originalBranchCouponsArray.filter { branchCoupon in
                
                return ((branchCoupon.TypeOfCoupon?.lowercased().contains(searchString.lowercased()))! || (branchCoupon.Address?.lowercased().contains(searchString.lowercased()))! || (branchCoupon.Description?.lowercased().contains(searchString.lowercased()))!)
            }
            self.tableView.reloadData()
        }
    }
    
    // Method will get call when user tap on a map pin on different screens
    @IBAction func placeOnMapBtnTapped(_ sender: AnyObject) {
        
        let btn = sender as! UIButton;
        let pointInTable = btn.convert(btn.bounds.origin, to: self.view)
        let indexPath = self.tableView.indexPathForRow(at: pointInTable);
        
        let objLoyalty = self.branchCouponsArray[(indexPath?.row)!];
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        objPlaceOnMapVC = storyboard.instantiateViewController(withIdentifier: "PlaceOnMapVC") as! PlaceOnMapVC
        
        // Initiate values with controller objects
        self.objPlaceOnMapVC?.clLocationCoordinate = CLLocationCoordinate2DMake(Double(objLoyalty.Latitude!)!, Double(objLoyalty.Longitude!)!);
        self.objPlaceOnMapVC?.locationTitle = objLoyalty.FirstName;
        self.objPlaceOnMapVC?.locationSubTitle = objLoyalty.BranchDescription
        self.objPlaceOnMapVC?.dealCategoryImage = objLoyalty.MapPinCategoryPicture;
        self.objPlaceOnMapVC?.bonusPotLocationID = objLoyalty.BonusPotLocationID;
        self.objPlaceOnMapVC?.branchID = objLoyalty.BranchID;
        
        self.addChildViewController(self.objPlaceOnMapVC!)
        self.objPlaceOnMapVC?.view.frame = self.view.bounds;
        self.tableView.addSubview((self.objPlaceOnMapVC?.view)!)
        self.objPlaceOnMapVC?.didMove(toParentViewController: self);
    }
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.hideChildView();
    }
    
    func hideChildView(){
        if(self.objPlaceOnMapVC != nil){
            self.objPlaceOnMapVC?.willMove(toParentViewController: nil)
            self.objPlaceOnMapVC?.view.removeFromSuperview()
            self.objPlaceOnMapVC?.removeFromParentViewController()
        }
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating ")
        
        if((scrollView.contentSize.height + 20.0) < (scrollView.contentOffset.y + scrollView.frame.size.height)){
            
            let offset = self.branchCouponsArray.count;
            // Call more places for the same coordinates
            self.callBranchSpecificDeals(offset)
        }
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
