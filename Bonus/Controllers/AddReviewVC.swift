//
//  AddReviewVC.swift
//  Bonus
//
//  Created by Arvind Sen on 17/06/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class AddReviewVC: UIViewController, UITextViewDelegate {

    var dealMasterId: String? = nil;
    
    var popupVCDelegate : DismissPopupVCProtocol? = nil
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var totalCharLbl: UILabel!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var crossBtn: UIButton!
    @IBOutlet weak var starContainerView: UIView!
    var starRating = 0;
    var callBackPreviousVC: ((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false;
        // Do any additional setup after loading the view.
        self.title = "Customers Review";
        self.containerView.layer.cornerRadius = 7.0;
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.containerView.layer.borderWidth = 1.0;
        self.containerView.clipsToBounds = true;
        
        //self.containerView.backgroundColor = BLUE_BG_COLOR;
        self.txtView.layer.cornerRadius = 8.0;
        self.txtView.layer.borderColor = UIColor.lightGray.cgColor;
        self.txtView.layer.borderWidth = 1.0;
        
        self.crossBtn.alpha = 0;
        self.configureInputAccesseryViewForInputFields();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func configureInputAccesseryViewForInputFields(){
        //Configured in viewDidLoad()
        var accessoryDoneButton: UIBarButtonItem!
        let accessoryToolBar = UIToolbar(frame: CGRect(x : 0, y : 0, width : UIScreen.main.bounds.width, height : 44))
        accessoryDoneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.accessoryViewDoneBtnPressed(_:)))
        let accessroySpace = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil);
        accessoryDoneButton.tintColor = UIColor.darkGray;
        accessoryToolBar.items = [accessroySpace, accessoryDoneButton]
        
        self.txtView.inputAccessoryView = accessoryToolBar
    }
    
    func accessoryViewDoneBtnPressed(_ sender: UIButton){
        self.txtView.resignFirstResponder();
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if (textView.text.characters.count < 255) {
            self.totalCharLbl.text = ("\((textView.text.characters.count + 1))/255");
        }
        else{
            if (textView.text.characters.count == 0) { }
            else{
                textView.resignFirstResponder();
                return false;
            }
        }
        // For any other character return TRUE so that the text gets added to the view
        return true;
    }
    
    @IBAction func starBtnTapped(_ sender: Any) {
        
        let tappedStarBtn = sender as! UIButton;
        
        let starEmptyImg = UIImage(named: "StarFullEmpty")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
        let starFullImg = UIImage(named: "StarFullYellow")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
        
        for index in 11...15 {
            
            let untappedStarBtn = self.starContainerView.viewWithTag(index) as! UIButton;
            if(index <= tappedStarBtn.tag){
                untappedStarBtn.setImage(starFullImg, for: UIControlState.normal);
                starRating = tappedStarBtn.tag - 10;
                self.crossBtn.alpha = 1;
            }
            else{
                untappedStarBtn.setImage(starEmptyImg, for: UIControlState.normal);
            }
        }
    }
    
    @IBAction func crossStarsBtnTapped(_ sender: Any) {
        
        let starEmptyImg = UIImage(named: "StarFullEmpty")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
        
        for index in 11...15 {
            
            let untappedStarBtn = self.starContainerView.viewWithTag(index) as! UIButton;
            untappedStarBtn.setImage(starEmptyImg, for: UIControlState.normal);
            starRating = 0;
            self.crossBtn.alpha = 0;
        }
    }
    
    @IBAction func commentBtnTapped(_ sender: Any) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID;
        let personType = user.PersonType;
        
        let comment : String = self.txtView.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        self.txtView.text = comment;
        
        //self.starRating
        
        var message : String? = nil;
        
        if (self.starRating == 0 && comment.characters.count <= 0) {
            message = "Please enter rate OR review.";
        }
        
        if message != nil {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
        }
        else {
            let deviceUniqueId = AppUtility.getDeviceNSUUID();
            
            // Created postDataDictonary to post the data on server
            let postDataDict : NSDictionary = ["DealMasterID" : self.dealMasterId!,
                                               "ID": "0",
                                               "UserID": userId!,
                                               "Rating" : ("\(self.starRating)"),
                                               "PersonType": personType!,
                                               "Review": comment,
                                               "DeviceUniqueID": deviceUniqueId,
                                               "api_key": kWEB_SERVICES_API_KEY];
            
            print("postDataDict = %", postDataDict);
            
            if !AppUtility.isNetworkAvailable(){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
            }
            else{
                
                CustomProgressView.showProgressIndicator();
                Alamofire.request(kSERVICE_SAVE_DEAL_REVIEW, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                    
                    CustomProgressView.hideProgressIndicator();
                    
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                    print("value =  \(datastring)");
                    
                    if let JSON = response.result.value
                    {
                        print("JSON: \(JSON)")
                        let dataDict = JSON as? NSDictionary;
                        
                        if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String) == 200) {
                            
                            let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                            
                            let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
                                
                                self.callBackPreviousVC!("Success");
                                // Removed it from superview controller
                                self.navigationController?.popViewController(animated: true);
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                            
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                        }
                        else
                        {
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                            
                        }
                    }
                }
                )
            }
        }
    }
    
    @IBAction func cancelBtnTapped(_ sender: Any) {
        
        let comment : String = self.txtView.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        if (comment.characters.count <= 0) {
            self.removeFromParentVC();
        }
        else{
            let alert = UIAlertController(title: nil, message: "Do you really want to cancel this comment?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                    self.removeFromParentVC();
                }))
            
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler:nil));
            self.present(alert, animated: true, completion: nil);
        }
    }
    
    func removeFromParentVC(){
        //self.popupVCDelegate?.dismissPopupViewController(self, responseObj: nil);
//        self.view.removeFromSuperview()
//        self.removeFromParentViewController();
        self.navigationController?.popViewController(animated: true);
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
