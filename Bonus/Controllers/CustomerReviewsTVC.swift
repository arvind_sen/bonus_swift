//
//  CustomerReviewsTVC.swift
//  Bonus
//
//  Created by Arvind Sen on 17/06/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class CustomerReviewsTVC: UITableViewController {

    var customerReviewsArray : Array<CustomerReview> = [CustomerReview]()
    var objBranchCoupon: BranchCoupon? = nil;
    var objCustomerCoupon: CustomerCoupon? = nil;
    var flagToNoDataFound = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.title = "Customers Review";
        
        let defaults: UserDefaults = UserDefaults.standard
        let userType = defaults.object(forKey: "globalUserType") as! String
        
        if(userType == USER_TYPE_BUSINESS || userType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE){
             
        }
        else{
            self.showTopBtn();
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
        self.getAllCustomerReviews(offset: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showTopBtn(){
        // Right Top tab bar button configration
        let addBtn = UIButton.init(type: .contactAdd);
        addBtn.addTarget(self, action: #selector(self.topAddBtnTapped), for: UIControlEvents.touchUpInside);
        let barRightBtn = UIBarButtonItem(customView: addBtn);
        self.navigationItem.rightBarButtonItem = barRightBtn;
    }
    
    func logoutFromSession() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.logoutFromTheSessionForAllUsers();
    }
    
    func showAlertForVisitor(){
        
        let alert = UIAlertController(title: nil, message: "Please login as customer to continue.", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            self.logoutFromSession();
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil));
        self.present(alert, animated: true, completion: nil)
    }
    
    func topAddBtnTapped(){
        
        let defaults: UserDefaults = UserDefaults.standard
        let userType = defaults.object(forKey: "globalUserType") as! String
        
        if(userType == USER_TYPE_VISITOR){
            self.showAlertForVisitor()
        }
        else
        {
            var dealMasterId = "";
            if(self.objCustomerCoupon != nil){
                dealMasterId = (self.objCustomerCoupon?.DealMasterID)! ;
            }
            else{
                dealMasterId = (self.objBranchCoupon?.DealMasterID)! ;
            }
            
            AppUtility.removedBackTitleAndChangeItsColor(VC: self, backTitle: "", backTintColor: UIColor.white);
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "AddReviewVC") as! AddReviewVC
            objVC.dealMasterId = dealMasterId;
            objVC.callBackPreviousVC = { returnStatus in
                
                if (returnStatus == "Success") {
                    self.getAllCustomerReviews(offset: 0)
                }
            }
            
            self.navigationController?.pushViewController(objVC, animated: true);
        }
    }
    
    func getAllCustomerReviews(offset: Int)
    {
        let defaults: UserDefaults = UserDefaults.standard
        let userType = defaults.object(forKey: "globalUserType") as! String
        var userId = "0";
        
        if(defaults.object(forKey: "UserData") != nil){
            let nsDataUser = defaults.object(forKey: "UserData") as! Data
            let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
            userId = user.ID!;
        }

        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        var dealMasterId = "";
        if(self.objCustomerCoupon != nil){
            dealMasterId = (self.objCustomerCoupon?.DealMasterID)! ;
        }
        else{
            dealMasterId = (self.objBranchCoupon?.DealMasterID)! ;
        }
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["DealMasterID" : dealMasterId,
                                           "UserID": userId,
                                           "PersonType": userType,
                                           "Limit": "10",
                                           "Offset" : offset,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        print("postDataDict = %", postDataDict);

        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_DEAL_REVIEW, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200)
                    {
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        let resourcePath = dataDict?.object(forKey: "ResourcePath") as! Dictionary<String, AnyObject>
                        
                        if(offset == 0){
                            self.customerReviewsArray.removeAll()
                        }
                        
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let obj = CustomerReview(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>, resourcePaths: resourcePath)
                                
                                self.customerReviewsArray.append(obj)
                            }
                        }
                        
                        DispatchQueue.main.async {
                            if(self.customerReviewsArray.count > 0){
                                self.flagToNoDataFound = false;
                            }
                            else{
                                self.flagToNoDataFound = true;
                            }
                            self.tableView.reloadData();
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                            
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else{
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                    }
                }
            }
            )
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        var numOfSections: Int = 0
        if (self.customerReviewsArray.count > 0)
        {
            //tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else if (self.flagToNoDataFound == true)
        {
            var oX = 0.0;
            var oY = 0.0;
            let widthX = tableView.frame.size.width;
            let heightY = tableView.frame.size.height;
            
            let emptyContainer = UIView.init(frame: CGRect(x: oX, y: oY, width: Double(widthX), height: Double(heightY)));
            emptyContainer.backgroundColor = K_EMPTY_TABLE_BG_COLOR;
            ///tblCell.contentView.addSubview(emptyContainer);
            
            let imgWidth = 100.0;
            let imgHeight = 100.0;
            oX = (Double(widthX) - imgWidth)/2;
            oY = (Double(heightY) - imgHeight)/2;
            
            let imageView = UIImageView.init(frame: CGRect(x: oX, y: oY, width: imgWidth, height: imgHeight));
            imageView.backgroundColor = UIColor.clear;
            imageView.contentMode = .scaleAspectFit;
            imageView.image = UIImage.init(named: "alert");
            emptyContainer.addSubview(imageView);
            
            let titleWidth = emptyContainer.frame.size.width - 20.0;
            let titleHeight = 40.0;
            let OY = imageView.frame.size.height + imageView.frame.origin.y + 10.0;
            
            let titleLbl = UILabel.init(frame: CGRect(x: 10.0, y: Double(OY), width: Double(titleWidth), height: titleHeight));
            titleLbl.textAlignment = .center;
            titleLbl.numberOfLines = 0;
            titleLbl.lineBreakMode = .byWordWrapping;
            titleLbl.text = "No customers review yet."
            titleLbl.textColor = UIColor.darkGray;
            emptyContainer.addSubview(titleLbl);
            //titleLbl.sizeToFit();
            
            tableView.backgroundView  = emptyContainer;
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.customerReviewsArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as UITableViewCell
        cell.backgroundColor = UIColor.clear
        // Configure the cell...
        self.configureTableViewCell(cell, forRowAtIndexPath: indexPath);
        return cell
    }
    
    // This below method is setting different values for a row
    func configureTableViewCell(_ cell: UITableViewCell, forRowAtIndexPath:IndexPath){
        
        let obj = self.customerReviewsArray[forRowAtIndexPath.row];
        
        //let containerView = cell.contentView.viewWithTag(99);
        let userImageView = cell.contentView.viewWithTag(134) as! UIImageView;
        let nameLbl = cell.contentView.viewWithTag(135) as! UILabel;
        nameLbl.text = "";
        let commentLbl = cell.contentView.viewWithTag(136) as! UILabel;
        commentLbl.text = ""
        
        if(obj.IsImageVerified != nil && Int(obj.IsImageVerified!) == 1 && obj.BusinessLogoImagePath != nil){
            userImageView.sd_setImage(with: URL(string: obj.BusinessLogoImagePath!), placeholderImage: K_PLACE_HOLDER_IMAGE)
        }else{
            userImageView.image = K_PLACE_HOLDER_IMAGE;
        }
        
        if(obj.NickName != nil && (obj.NickName?.characters.count)! > 0){
            //nameLbl.text = String(htmlEncodedString: obj.BranchDescription!);
            nameLbl.text = obj.NickName!;
        }
        else if (obj.FirstName != nil && (obj.FirstName?.characters.count)! > 0) {
            let string = obj.FirstName!;
            //nameLbl.text = String(htmlEncodedString: string);
            nameLbl.text = string;
        }
        nameLbl.sizeToFit();
        
        
        if (obj.Review != nil) {
            var string = obj.Review!;
            //nameLbl.text = String(htmlEncodedString: string);
            commentLbl.text = string;
            //string = "lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets lets";
            
            print("length = \(string.characters.count)");
            
            if(string.characters.count > 110) {
                
                commentLbl.isUserInteractionEnabled = true;
                let moreText = "...MORE";
                let index = string.index(string.startIndex, offsetBy: 110)
                
                let breakedString = string.substring(to: index)
                let newString = breakedString + "" + moreText;
                
                commentLbl.text = newString;
                let range = (newString as NSString).range(of: moreText)
                let attributedString = NSMutableAttributedString.init(string: newString);
                
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightText, range: range)
                attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 10.0), range: range)
                commentLbl.attributedText = attributedString;
                
                let singleTap = UITapGestureRecognizer.init(target: self, action: #selector(self.moreOptionTapped(_:)))
                singleTap.numberOfTapsRequired = 1;
                singleTap.numberOfTouchesRequired = 1;
                commentLbl.addGestureRecognizer(singleTap);
                //self.commetLbl.sizeToFit();
            }
            
        }
        commentLbl.sizeToFit();
        
        
        for index in 251...255 {
            let starContainerView = cell.contentView.viewWithTag(219)! as UIView;
            let starImg = starContainerView.viewWithTag(index) as? UIImageView;
            let rating = Int(obj.Rating!)!;
            
            let ratingValue = index - 250;
            if (ratingValue <= rating) {
                starImg?.image = UIImage(named: "starfull")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
            }
            else{
                starImg?.image = UIImage(named: "starempty")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
            }
        }
 
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating ")
        
        if((scrollView.contentSize.height + 20.0) < (scrollView.contentOffset.y + scrollView.frame.size.height)){
            
            
            let offset = self.customerReviewsArray.count;
            
            // Call more places for the same coordinates
            self.getAllCustomerReviews(offset: offset);
        }
    }
    
    func moreOptionTapped(_ tapGesture: UITapGestureRecognizer){
        
        //let btn = (UITapGestureRecognizer *)sender;
        let tappedView = tapGesture.view;
        let pointInTable = tappedView?.convert((tappedView?.bounds.origin)!, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: pointInTable!);
        
        let obj = self.customerReviewsArray[(indexPath?.row)!];
        
        let string = String(htmlEncodedString: (obj.Review!));
        let string1 = String(htmlEncodedString: string);
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "MoreDescriptionVC") as! MoreDescriptionVC
        objVC.popUptitle = "Review";
        objVC.descriptionTxt = string1;
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        self.addChildViewController(objVC)
        objVC.didMove(toParentViewController: self);
    }
    /*
    override func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        
        let obj = self.branchesListArray![indexPath.row] as! Branch;
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        var flagToEdit = false;
        if (user.PersonType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE) {
            if(obj.BranchEmployeeUserID == user.ID){
                flagToEdit = true;
            }
            else{
                AppUtility.showAlertWithTitleAndMessage("Sorry! you can not access this branch detail.", alertMessage: nil, onView: self);
            }
        }
        else{
            flagToEdit = true;
        }
        
        if(flagToEdit == true){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "AddEditABranchVC") as! AddEditABranchVC
            objVC.branchObjToEdit = obj;
            objVC.isHavingMainBranch = self.isHavingMainBranch;
            self.navigationController?.pushViewController(objVC, animated: true);
        }
 
    }
 */
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
