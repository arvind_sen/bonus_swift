//
//  PayFortPaymentVC.swift
//  Bonus
//
//  Created by Arvind Sen on 16/09/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import StartSDK
import Alamofire

let PayFortApiKey = "test_open_k_b9d1362e3734cfb90bf9"

class PayFortPaymentVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    var amountToPay: String = "0.0";
    var amountCurrency: String = "AED";
    @IBOutlet weak var amountToPayLbl: UILabel!
    
    @IBOutlet weak var payWithCardBtn: UIButton!
    @IBOutlet weak var securePaymentViewContainer: UIView!
    @IBOutlet weak var cardHolderNameTxtF: UITextField!
    @IBOutlet weak var cardNumberTxtF: UITextField!
    @IBOutlet weak var expiryMonthTxtF: UITextField!
    @IBOutlet weak var expiryYearTxtF: UITextField!
    @IBOutlet weak var cvcTxtF: UITextField!
    var focusedControl : UITextField?
    
    var callBackOnPreviousView: ((Bool)->Void)? ;
    override func viewDidLoad() {
        super.viewDidLoad()

        self.amountToPayLbl.text = self.amountToPay + " " + self.amountCurrency;
        // Do any additional setup after loading the view.
        self.securePaymentViewContainer.isHidden = true;
        self.securePaymentViewContainer.alpha = 0;
        self.configureInputAccesseryViewForInputFields();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureInputAccesseryViewForInputFields(){
        //Configured in viewDidLoad()
        var accessoryDoneButton: UIBarButtonItem!
        let accessoryToolBar = UIToolbar(frame: CGRect(x : 0, y : 0, width : UIScreen.main.bounds.width, height : 44))
        accessoryDoneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.accessoryViewDoneBtnPressed(_:)))
        let accessroySpace = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil);
        accessoryDoneButton.tintColor = UIColor.darkGray;
        accessoryToolBar.items = [accessroySpace, accessoryDoneButton]
        
        self.cardNumberTxtF.inputAccessoryView = accessoryToolBar
        self.expiryMonthTxtF.inputAccessoryView = accessoryToolBar
        self.expiryYearTxtF.inputAccessoryView = accessoryToolBar
    }
    
    func accessoryViewDoneBtnPressed(_ sender: UIButton){
        self.cardNumberTxtF.resignFirstResponder();
        self.expiryMonthTxtF.resignFirstResponder();
        self.expiryYearTxtF.resignFirstResponder();
    }
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        self.callBackOnPreviousView!(true);
        self.dismiss(animated: true, completion: nil);
    }

    @IBAction func payWithCardBtnTapped(_ sender: Any) {
        
        let btn = sender as! UIButton;
        
        UIView.animate(withDuration: 0.3) { 
            self.securePaymentViewContainer.isHidden = false;
            self.securePaymentViewContainer.alpha = 1;
            btn.backgroundColor = UIColor.lightGray;
            btn.isEnabled = false
        }
    }
    
    @IBAction func payBtnTapped(_ sender: Any) {
        
        let name : String = self.cardHolderNameTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let cardNumber : String = self.cardNumberTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let expiryMonth : String = self.expiryMonthTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let expiryYear : String = self.expiryYearTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let cvcNumber : String = self.cvcTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        self.cardHolderNameTxtF.text = name;
        self.cardNumberTxtF.text = cardNumber;
        self.expiryMonthTxtF.text = expiryMonth;
        self.expiryYearTxtF.text = expiryYear;
        self.cvcTxtF.text = cvcNumber;
        
        let exYear = Int("20\(expiryYear)")!
        
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        
        let lowBound = (exYear == year) ? month : 1;
        
        //_expirationMonth >= lowBound && _expirationMonth <= 12
        var message : String? = nil;
        
        if (name.characters.count <= 0) {
            message = "Please enter card holder name";
        }
        else if ((cardNumber.characters.count <= 0) || (cardNumber.characters.count <= 12 && cardNumber.characters.count >= 19)) {
            message = "Please enter valid card number";
        }
        else if (((expiryMonth.characters.count <= 0 || expiryMonth.characters.count > 2)) || (Int(expiryMonth)! < lowBound || Int(expiryMonth)! > 12)) {
            message = "Please enter valid card expiry month";
        }
        else if (expiryYear.characters.count <= 0) {
            message = "Please enter valid card expiry year";
        }
        else if ((cvcNumber.characters.count <= 0) || (cvcNumber.characters.count <= 3 && cvcNumber.characters.count >= 4)) {
            message = "Please enter valid CVC number";
        }
        
        if message != nil {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
        }
        else {
            
            let centsToPay = Int(self.amountToPay)! // 1 USD
            
            var errorString : String? = nil;
            var card : StartCard? = nil;
            
            CustomProgressView.showProgressIndicatorOnView(VC: self);
            
            do {
                let cardholder = name
                let number = cardNumber
                let cvc = cvcNumber
                let month = Int(expiryMonth)!
                let year = Int("20\(expiryYear)")!
                
                card = try StartCard(cardholder: cardholder, number: number, cvc: cvc, expirationMonth: month, expirationYear: year)
            }
            catch let error as NSError {
                if let errors = error.userInfo[StartCardErrorKeyValues] as? [String] {
                    errorString = "The following fields are invalid:"
                    errors.forEach { errorString = "\(errorString ?? "")\n\($0)" }
                }
                else {
                    errorString = "Unknown error occured"
                }
            }
            catch {
                errorString = "Unknown error occured"
            }

            CustomProgressView.hideProgressIndicatorOnView(VC: self);
                
            //let card = try! StartCard(cardholder: name, number: cardNumber, cvc: cvcNumber, expirationMonth: Int(expiryMonth)!, expirationYear: Int(expiryYear)!)
            if(errorString == nil)
            {
                CustomProgressView.showProgressIndicatorOnView(VC: self);
                
                let start = Start(apiKey: PayFortApiKey);
                start.createToken(for: card!, amount: centsToPay, currency: self.amountCurrency, successBlock: { token in
                    CustomProgressView.hideProgressIndicatorOnView(VC: self);
                    // Use token.tokenId when performing payments through API
                    self.callPayFortFromNativeServer(token.tokenId, centsToPay, self.amountCurrency);
                    print(token.tokenId);
                }, errorBlock: { error in
                    CustomProgressView.hideProgressIndicatorOnView(VC: self);
                    // Process error
                    
                    //let errorUserInfo : NSDictionary? = ((error as Any) as! NSError).userInfo["error"] as? NSDictionary
                    var alertMsg = "";
                    
                    if let dict = (error as? NSError)?.userInfo {
                        print(dict)
                        
                        for (_, value) in (dict as NSDictionary) {
                            
                            //print(key)
                            //print(value)
                            
                            let dict = self.convertToDictionary(text: value as! String);
                            //print(dict);
                            
                            let errors = dict?["error"] as? NSDictionary;
                            let extras = errors?["extras"] as? NSDictionary;
                            let numbers = extras?["number"] as? NSArray;
                            
                            if((numbers?.count)! > 0)
                            {
                                for values in numbers! {
                                    
                                    let str = values as! String;
                                    if(alertMsg.characters.count == 0){
                                        alertMsg = str;
                                    }
                                    else
                                    {
                                        alertMsg = alertMsg + "\n" + str;
                                    }
                                }
                            }
                        }
                    }
                    
                    AppUtility.showAlertWithTitleAndMessage("Error occured", alertMessage: alertMsg, onView: self)
                    //print(error);
                    
                }, cancel: {
                    CustomProgressView.hideProgressIndicatorOnView(VC: self);
                    // User cancelled payment verification
                    AppUtility.showAlertWithTitleAndMessage(nil, alertMessage:"Payment verification cancelled", onView: self)
                })
            }
            else
            {
                AppUtility.showAlertWithTitleAndMessage("Error occured", alertMessage: "\(errorString!)", onView: self);
            }
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func callPayFortFromNativeServer(_ tokenId: String, _ amount: Int, _ currency: String){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userEmail = user.EmailID!;
        let userId = user.ID!;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        //Post params - startToken, startEmail, amount, currencyCode
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["UserID": userId,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key" : kWEB_SERVICES_API_KEY,
                                           "startToken": tokenId,
                                           "startEmail": userEmail,
                                           "amount": "\(amount)",
                                           "currencyCode": currency
                                          ];
        
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_PAYFORT_CHARGE, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    let status = (dataDict?.object(forKey: "status"))! as? String
                    
                    if(status == "success")
                    {
                        //let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        let message = (dataDict?.object(forKey: "message"))! as! String;
                        let alert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
                            
                            self.dismiss(animated: true, completion: nil);
                        }))

                        self.present(alert, animated: true, completion: nil)
                    }
                    else if(status == "fail" || status == "Fail"){
                        
                        let message = (dataDict?.object(forKey: "message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "message"))! as! String) as NSString;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        
                    }
                }
            }
            )
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.focusedControl = textField;
    }
    // MARK:- TextField delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.focusedControl = textField;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (self.expiryMonthTxtF == textField && (self.expiryMonthTxtF.text?.characters.count)! < 2) {
           
        }
        else if (self.expiryYearTxtF == textField && (self.expiryYearTxtF.text?.characters.count)! < 2) {
            
        }
        else if (self.cvcTxtF == textField && (self.cvcTxtF.text?.characters.count)! < 4) {
            
        }
        else{
            if (self.expiryMonthTxtF == textField && self.expiryMonthTxtF.text?.characters.count == 0) { }
            else if (self.expiryYearTxtF == textField && self.expiryYearTxtF.text?.characters.count == 0) { }
            else if (self.cvcTxtF == textField && self.cvcTxtF.text?.characters.count == 0) { }
            else if ((self.expiryMonthTxtF == textField || self.expiryYearTxtF == textField || self.cvcTxtF == textField) && string != ""){
                return false;
            }
        }
        return true;
    }
}
