//
//  RedeemCouponsVC.swift
//  Bonus
//
//  Created by Arvind Sen on 16/06/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class RedeemCouponsVC: UIViewController {

    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var businessPinContainer: UIView!
    
    @IBOutlet weak var customerPinContainer: UIView!
    @IBOutlet weak var customerPin1: UITextField!
    @IBOutlet weak var customerPin2: UITextField!
    @IBOutlet weak var customerPin3: UITextField!
    @IBOutlet weak var customerPin4: UITextField!
    
    @IBOutlet weak var numberOfCouponTxtF: UITextField!
    var numberOfCouponsToRedeem: String!
    
    @IBOutlet weak var popupHeaderTitle: UILabel!
    @IBOutlet weak var containerViewLayout: NSLayoutConstraint!
    
    @IBOutlet weak var maxCouponsLbl: UILabel!
    @IBOutlet weak var businessPin1: UITextField!
    @IBOutlet weak var businessPin2: UITextField!
    @IBOutlet weak var businessPin3: UITextField!
    @IBOutlet weak var businessPin4: UITextField!
    
    var focusedControl : UITextField?
    var popupVCDelegate : DismissPopupVCProtocol? = nil
    var objBranchCoupon : BranchCoupon!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        super.viewDidLoad()
        view.backgroundColor = K_COLOR_TRANSPARNT
        view.isOpaque = false;
        // Do any additional setup after loading the view.
        
        self.containerView.backgroundColor = BLUE_BG_COLOR;
        self.containerView.layer.cornerRadius = 7.0;
        self.containerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.containerView.layer.borderWidth = 1.0;
        self.containerView.clipsToBounds = true;
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.customerPin1.becomeFirstResponder();
        self.maxCouponsLbl.text = "Max Coupon to Redeem : " + self.numberOfCouponsToRedeem;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func redemedCouponMinusPlusButtonTapped(_ sender: AnyObject) {
        
        let btn = sender as! UIButton;
        var totalCoupons = Int(self.numberOfCouponTxtF.text!)!
        let maxCouponsRedeem = Int(self.numberOfCouponsToRedeem!)!
        
        if (btn.tag == 67) {
            totalCoupons = totalCoupons + 1;
            if (totalCoupons <= maxCouponsRedeem) {
                self.numberOfCouponTxtF.text = String(totalCoupons);
            }
            else{
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: String(format: "You have a maximum of %d coupons which can be redeemed", (totalCoupons - 1)), onView: self)
            }
        }
        else if (btn.tag == 66){
            totalCoupons = totalCoupons - 1;
            if (totalCoupons >= 1) {
                self.numberOfCouponTxtF.text = String(format: "%d", totalCoupons)
            }
        }
    }
    
    
    @IBAction func customerContainerRightArrowBtnTapped(_ sender: AnyObject) {
        
        self.customerPin1.resignFirstResponder();
        self.customerPin2.resignFirstResponder();
        self.customerPin3.resignFirstResponder();
        self.customerPin4   .resignFirstResponder();
        
        var message : String? = nil;
        
        if (((self.customerPin1.text?.characters.count)! <= 0) || ((self.customerPin2.text?.characters.count)! <= 0) || ((self.customerPin3.text?.characters.count)! <= 0) || ((self.customerPin4.text?.characters.count)! <= 0)){
            message = "Please enter pin number correctly";
        }
        
        if message != nil {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
        }
        else {
            
            UIView.animate(withDuration: ANIMATION_DURATION_VIEW_SLIDING, animations: {
                self.businessPinContainer.isHidden = false;
                self.businessPinContainer.alpha = 1;
                
                self.containerView.backgroundColor = UIColor.lightGray;
                self.popupHeaderTitle.text = "Enter Business Pin"
                self.containerViewLayout.constant = self.containerView.frame.size.height - 50;
                self.customerPinContainer.isHidden = true;
                self.customerPinContainer.alpha = 0;
                
            }, completion: nil);
        }
    }
    
    
    @IBAction func businessContainerArrowBtnTapped(_ sender: AnyObject) {
        
        var message : String? = nil;
        
        if (((self.businessPin1.text?.characters.count)! <= 0) || ((self.businessPin2.text?.characters.count)! <= 0) || ((self.businessPin3.text?.characters.count)! <= 0) || ((self.businessPin4.text?.characters.count)! <= 0)){
            message = "Please enter pin number correctly";
        }
        
        if message != nil {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
        }
        else {
            
            self.businessPin1.resignFirstResponder();
            self.businessPin2.resignFirstResponder();
            self.businessPin3.resignFirstResponder();
            self.businessPin4.resignFirstResponder();
            
            self.callServiceToRedeemCoupon();
        }
    }
    
    // Method will call to allot a free bie
    func callServiceToRedeemCoupon(){
        
        let pin1 : String = self.customerPin1.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let pin2 : String = self.customerPin2.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let pin3 : String = self.customerPin3.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let pin4 : String = self.customerPin4.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        let combineCustomerPin = pin1 + "" + pin2 + "" + pin3 + "" + pin4;
        
        let pin21 : String = self.businessPin1.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let pin22 : String = self.businessPin2.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let pin23 : String = self.businessPin3.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let pin24 : String = self.businessPin4.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        let combineBusinessPin = pin21 + "" + pin22 + "" + pin23 + "" + pin24;
        
        let dealId = self.objBranchCoupon.DealMasterID;
        let branchId = self.objBranchCoupon.BranchID;
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID;
        let userType = user.PersonType;
        let buyQuantity = self.numberOfCouponTxtF.text!;
        
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        let invoiceAmount = "0";
        let receiptNumber = "0";
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["ID" : dealId!,
                                           "RedemptionPin" : combineCustomerPin,
                                           "UserID" : userId!,
                                           "CustomerBusinessPin" : combineBusinessPin,
                                           "BranchID" : branchId!,
                                           "CouponQuantity" : buyQuantity,
                                           "PersonType" : userType!,
                                           "InvoiceAmount" : invoiceAmount,
                                           "ReceiptNumber" : receiptNumber,
                                           "DeviceUniqueID" : deviceUniqueId,
                                           "api_key" : kWEB_SERVICES_API_KEY];
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_REDEEM_COUPONS, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictionary = dataDict?.object(forKey: "Response") as! Dictionary<String, AnyObject>
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        
                        if(message != ""){
                            
                            let alert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                self.popupVCDelegate?.dismissPopupViewController(self, responseObj:dictionary as AnyObject?);
                                
                                // Removed it from superview controller
                                self.view.removeFromSuperview()
                                self.removeFromParentViewController()
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        self.businessPin1.text = "";
                        self.businessPin2.text = "";
                        self.businessPin3.text = "";
                        self.businessPin4.text = "";
                        
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                            self.popupVCDelegate?.dismissPopupViewController(self, responseObj:nil);
                            
                            // Removed it from superview controller
                            self.view.removeFromSuperview()
                            self.removeFromParentViewController()
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
            }
            )
        }
    }
    
    @IBAction func closeBtnTapped(_ sender: AnyObject?) {
        self.popupVCDelegate?.dismissPopupViewController(self, responseObj: nil);
        // Removed it from superview controller
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    // MARK:- TextField delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if(self.customerPinContainer.alpha == 1){
            
            if (textField == self.customerPin1 && (textField.text?.characters.count)! >= 1 && string.isEmpty){
                
            }
            else if ((textField == self.customerPin1) && (textField.text?.characters.count == 1 && !string.isEmpty)){
                
                if ((self.customerPin2.text?.characters.count)! < 1) {
                    self.customerPin2.becomeFirstResponder();
                }
                else{
                    self.customerPin2.resignFirstResponder();
                }
            }
            
            if (textField == self.customerPin2 && (textField.text?.characters.count)! >= 1 && string.isEmpty){
                
            }
            else if ((textField == self.customerPin2) && (textField.text?.characters.count == 1 && !string.isEmpty)){
                
                if ((self.customerPin3.text?.characters.count)! < 1) {
                    self.customerPin3.becomeFirstResponder();
                }
                else{
                    self.customerPin2.resignFirstResponder();
                }
            }
            
            if (textField == self.customerPin3 && (textField.text?.characters.count)! >= 1 && string.isEmpty){
                
            }
            else if ((textField == self.customerPin3) && (textField.text?.characters.count == 1 && !string.isEmpty)){
                
                if ((self.customerPin4.text?.characters.count)! < 1) {
                    self.customerPin4.becomeFirstResponder();
                }
                else{
                    self.customerPin3.resignFirstResponder();
                }
            }
            
            if (textField == self.customerPin4 && (textField.text?.characters.count)! >= 1 && string.isEmpty){
                
            }
            else if ((textField == self.customerPin4) && (textField.text?.characters.count == 1 && !string.isEmpty)){
                
                self.customerPin4.resignFirstResponder();
            }
        }
        else if(self.businessPinContainer.alpha == 1) {
            
            if (textField == self.businessPin1 && (textField.text?.characters.count)! >= 1 && string.isEmpty){
                
            }
            else if ((textField == self.businessPin1) && (textField.text?.characters.count == 1 && !string.isEmpty)){
                
                if ((self.businessPin2.text?.characters.count)! < 1) {
                    self.businessPin2.becomeFirstResponder();
                }
                else{
                    self.businessPin2.resignFirstResponder();
                }
            }
            
            if (textField == self.businessPin2 && (textField.text?.characters.count)! >= 1 && string.isEmpty){
                
            }
            else if ((textField == self.businessPin2) && (textField.text?.characters.count == 1 && !string.isEmpty)){
                
                if ((self.businessPin3.text?.characters.count)! < 1) {
                    self.businessPin3.becomeFirstResponder();
                }
                else{
                    self.businessPin2.resignFirstResponder();
                }
            }
            
            if (textField == self.businessPin3 && (textField.text?.characters.count)! >= 1 && string.isEmpty){
                
            }
            else if ((textField == self.businessPin3) && (textField.text?.characters.count == 1 && !string.isEmpty)){
                
                if ((self.businessPin4.text?.characters.count)! < 1) {
                    self.businessPin4.becomeFirstResponder();
                }
                else{
                    self.businessPin3.resignFirstResponder();
                }
            }
            
            if (textField == self.businessPin4 && (textField.text?.characters.count)! >= 1 && string.isEmpty){
                
            }
            else if ((textField == self.businessPin4) && (textField.text?.characters.count == 1 && !string.isEmpty)){
                
                self.businessPin4.resignFirstResponder();
            }
        }
        return true;
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        self.focusedControl = textField;
        
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(self.customerPinContainer.alpha == 1){
            
            if (textField == self.customerPin1)
            {
                self.customerPin2.becomeFirstResponder();
            }
            else if (textField == self.customerPin2)
            {
                self.customerPin3.becomeFirstResponder();
            }
            else if (textField == self.customerPin3)
            {
                self.customerPin4.becomeFirstResponder();
            }
            else if (textField == self.customerPin4)
            {
                customerPin4.resignFirstResponder();
            }
        }
        else
        {
            if (textField == self.businessPin1)
            {
                self.businessPin2.becomeFirstResponder();
            }
            else if (textField == self.businessPin2)
            {
                self.businessPin3.becomeFirstResponder();
            }
            else if (textField == self.businessPin3)
            {
                self.businessPin4.becomeFirstResponder();
            }
            else if (textField == self.businessPin4)
            {
                businessPin4.resignFirstResponder();
            }
        }
        return true;
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
