//
//  AddNotificationVC.swift
//  Bonus
//
//  Created by Arvind Sen on 26/06/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class AddNotificationVC: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var userPicture: UIImageView!
    @IBOutlet weak var txtView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Add Notification"
        
        self.userPicture.layer.cornerRadius = 7;
        self.userPicture.layer.borderWidth = 1;
        self.userPicture.clipsToBounds = true;
        self.userPicture.layer.borderColor = UIColor.lightGray.cgColor;
        
        self.txtView.layer.borderColor = UIColor.lightGray.cgColor;
        self.txtView.layer.borderWidth = 1.0;
        self.txtView.layer.cornerRadius = 8;
        
        self.showTopBtnAndUserDetail()
        self.configureInputAccesseryViewForInputFields();
        
        // Do things before loading the VC
        NotificationCenter.default.addObserver(self, selector: #selector(AddNotificationVC.keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddNotificationVC.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    // MARK:- Keyboard Appearance delegate
    func keyboardWasShown(_ aNotification: Notification){
        
        let info:NSDictionary =  aNotification.userInfo! as NSDictionary;
        //let kbSize = (info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue.size;
        var kbSize = (info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue.size;
        if(kbSize.height <= 0.0){
            kbSize = (info.object(forKey: UIKeyboardFrameEndUserInfoKey) as AnyObject).cgRectValue.size;
        }
        
        let contentInsets = UIEdgeInsetsMake(0, 0.0, kbSize.height, 0.0);
        scrollView.contentInset = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        var aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!aRect.contains(self.txtView!.frame.origin) ) {
            let scrollPoint = CGPoint(x: 0.0, y: txtView!.frame.origin.y-kbSize.height);
            self.scrollView.setContentOffset(scrollPoint, animated: true);
        }
    }
    
    func keyboardWillBeHidden(_ aNotification:Notification){
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
            self.scrollView.contentInset = contentInsets;
            //self.scrollView.contentInset = self.scrollViewInitialEdgeInsets;
            //self.scrollView.scrollIndicatorInsets = contentInsets;
            // [scrollView setContentOffset:CGPointZero animated:YES];
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    func showTopBtnAndUserDetail(){
        
        // Right Top tab bar button configration
        let rightBtn = UIBarButtonItem(title: "Send", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.topSendBtnTapped));
        self.navigationItem.rightBarButtonItem = rightBtn;
        
        
        let marqueeLbl = self.view.viewWithTag(159) as? MarqueeLabel;
        marqueeLbl?.isHidden = true;
        marqueeLbl?.backgroundColor = UIColor(colorLiteralRed: 0.0, green: 0.0, blue: 0.0, alpha: 0.7);
        self.userPicture.image = K_PLACE_HOLDER_IMAGE
        
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        if(user.IsImageVerified != nil && Int(user.IsImageVerified!)! == 1){
            let imageUrl = user.BusinessLogoImagePath;
            self.userPicture.sd_setImage(with: URL(string:imageUrl!), placeholderImage: K_PLACE_HOLDER_IMAGE)
        }
        else{
            marqueeLbl?.isHidden = false;
            marqueeLbl?.text = "Image verification pending";
            if(user.IsImageVerified != nil && Int(user.IsImageVerified!)! == 2){
                marqueeLbl?.text = "Image has been disapproved";
            }
            self.userPicture.sd_setImage(with: URL(string: user.BusinessLogoImagePath!), placeholderImage: K_PLACE_HOLDER_IMAGE)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureInputAccesseryViewForInputFields(){
        //Configured in viewDidLoad()
        var accessoryDoneButton: UIBarButtonItem!
        let accessoryToolBar = UIToolbar(frame: CGRect(x : 0, y : 0, width : UIScreen.main.bounds.width, height : 44))
        accessoryDoneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.accessoryViewDoneBtnPressed(_:)))
        let accessroySpace = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil);
        accessoryDoneButton.tintColor = UIColor.darkGray;
        accessoryToolBar.items = [accessroySpace, accessoryDoneButton]
        
        self.txtView.inputAccessoryView = accessoryToolBar
    }
    
    func accessoryViewDoneBtnPressed(_ sender: UIButton){
        self.txtView.resignFirstResponder();
    }
    
    func topSendBtnTapped(){
        
        //"Please enter notification description"
        let descrip : String = self.txtView.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        self.txtView.text = descrip;
        
        var message : String? = nil;
        
        if (descrip.characters.count <= 0) {
            message = "Please enter notification description";
        }
        
        if message != nil {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
        }
        else
        {
            let defaults: UserDefaults = UserDefaults.standard
            let nsDataUser = defaults.object(forKey: "UserData") as! Data
            let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
            let userId = user.ID;
            
            let deviceUniqueId = AppUtility.getDeviceNSUUID();
            
            // Created postDataDictonary to post the data on server
            let postDataDict : NSDictionary = ["BusinessCategory" : (user.BusinessCategory)!, "BusinessUserID": ("\(userId!)"), "Description" : descrip, "DeviceUniqueID": deviceUniqueId, "api_key": kWEB_SERVICES_API_KEY];
            
            print("postDataDict = %", postDataDict);
            
            if !AppUtility.isNetworkAvailable(){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
            }
            else{
                
                CustomProgressView.showProgressIndicatorOnView(VC: self);
                Alamofire.request(kSERVICE_SEND_BUSINESS_NOTIFICATION, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                    
                    CustomProgressView.hideProgressIndicatorOnView(VC: self);
                    
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                    print("value =  \(datastring)");
                    
                    if let JSON = response.result.value
                    {
                        print("JSON: \(JSON)")
                        let dataDict = JSON as? NSDictionary;
                        
                        if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String) == 200) {
                            
                            DispatchQueue.main.async {
                                _ = self.navigationController?.popViewController(animated: true);
                            }
                        }
                        else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                            
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                        }
                        else
                        {
                            let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                            
                        }
                    }
                    else{
                         _ = self.navigationController?.popViewController(animated: true);
                    }
                }
                )
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
