//
//  BusinessAccountVC.swift
//  Bonus
//
//  Created by Arvind Sen on 17/10/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit
import Alamofire
import MessageUI
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}




class BusinessAccountVC: UIViewController, DismissPopupVCProtocol, UITextFieldDelegate, UITextViewDelegate, CategoryVCDelegate, MFMailComposeViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, DismissSelfViewControllerDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var topBtnContainer: UIView!
    @IBOutlet weak var accountBtn: UIButton!
    @IBOutlet weak var branchesBtn: UIButton!
    @IBOutlet weak var loyaltyBtn: UIButton!
    
    @IBOutlet weak internal var emailTxtFContainerView: UIView!
    
    @IBOutlet weak internal var pwdTxtFContainerView: UIView!
    
    //Controls on view    
    @IBOutlet weak var invitedByLbl: UILabel!
    @IBOutlet weak var emailIdTxtF: UITextField!
    @IBOutlet weak var passwordTxtF: UITextField!
    @IBOutlet weak var userImgBtn: UIButton!
    @IBOutlet weak var descriptionTxtV: UITextView!
    @IBOutlet weak var firstNameTxtF: UITextField!
    @IBOutlet weak var urlTxtF: UITextField!
    @IBOutlet weak var businessCategoryTxtF: UITextField!
    @IBOutlet weak var countryNameTxtF: UITextField!
    @IBOutlet weak var punchCardBtn: UIButton!
    @IBOutlet weak var statusBtn: UIButton!
    var imageCropperView: UIView!
    var cropper : BFCropInterface!
    var croppedImageToUpload: UIImage!
    
    var categoryListArray: NSArray?
    
    var selectedPencil: UIButton!;
    var isPunchCardLoyalty = false;
    var focusedControl: AnyObject!
    var selectedCategoryID : String?
    var displayOriginalImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Added keypad notifications
        self.navigationController?.navigationBar.barTintColor = TAB_BAR_BG_COLOR;
        //self.navigationController?.navigationBar.barTintColor = UIColor.red;
        
        self.setUpTopHeaderBtn();
        self.setUpViewControlsAndDetails();
        //self.setUpPencilBtns();
        
        self.configureInputAccesseryViewForInputFields();
        
        self.emailTxtFContainerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.emailTxtFContainerView.layer.borderWidth = 0.5;
        self.emailTxtFContainerView.clipsToBounds = true;
        self.emailTxtFContainerView.layer.cornerRadius = 5.0;
        
        self.pwdTxtFContainerView.layer.borderColor = UIColor.lightGray.cgColor;
        self.pwdTxtFContainerView.layer.borderWidth = 0.5;
        self.pwdTxtFContainerView.clipsToBounds = true;
        self.pwdTxtFContainerView.layer.cornerRadius = 5.0;
    }
    
    func keyboardWasShown(_ aNotification: Notification){
        
        if (self.focusedControl != nil) {
            let info:NSDictionary = aNotification.userInfo! as NSDictionary;
            //let kbSize = (info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue.size;
            var kbSize = (info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue.size;
            if(kbSize.height <= 0.0){
                kbSize = (info.object(forKey: UIKeyboardFrameEndUserInfoKey) as AnyObject).cgRectValue.size;
            }
            
            let contentInsets = UIEdgeInsetsMake(0, 0.0, kbSize.height, 0.0);
            scrollView.contentInset = contentInsets;
            
            // If active text field is hidden by keyboard, scroll it so it's visible
            // Your application might not need or want this behavior.
            var aRect = self.view.frame;
            aRect.size.height -= kbSize.height;
            if (!aRect.contains(self.focusedControl!.frame.origin) ) {
                let scrollPoint = CGPoint(x: 0.0, y: focusedControl!.frame.origin.y-kbSize.height);
                self.scrollView.setContentOffset(scrollPoint, animated: true);
            }
        }
    }
    
    func keyboardWillBeHidden(_ aNotification:Notification){
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
            self.scrollView.contentInset = contentInsets;
            self.scrollView.scrollIndicatorInsets = contentInsets;
            // [scrollView setContentOffset:CGPointZero animated:YES];
        }) 
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(BusinessAccountVC.keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BusinessAccountVC.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width, height: 1000.0)
        OperationQueue.main.addOperation {
           // self.setUpTopHeaderBtn();
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureInputAccesseryViewForInputFields(){
        //Configured in viewDidLoad()
        var accessoryDoneButton: UIBarButtonItem!
        let accessoryToolBar = UIToolbar(frame: CGRect(x : 0, y : 0, width : UIScreen.main.bounds.width, height : 44))
        accessoryDoneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.accessoryViewDoneBtnPressed(_:)))
        let accessroySpace = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil);
        accessoryDoneButton.tintColor = UIColor.darkGray;
        accessoryToolBar.items = [accessroySpace, accessoryDoneButton]
        
        self.descriptionTxtV.inputAccessoryView = accessoryToolBar
    }
    
    func accessoryViewDoneBtnPressed(_ sender: UIButton){
        
        self.descriptionTxtV.resignFirstResponder();
    }
    
    func setUpPencilBtns(){
        
        let emailIdBtn = UIButton.init(type: .custom);
        emailIdBtn.frame = CGRect(x: 30, y: 0, width: 30, height: self.passwordTxtF.frame.size.height);
        emailIdBtn.setImage(UIImage(named: "Pencilgry"), for: UIControlState());
        emailIdBtn.titleLabel?.font = UIFont.systemFont(ofSize: 11.0);
        emailIdBtn.setTitleColor(UIColor.darkGray, for: UIControlState());
        emailIdBtn.addTarget(self, action: #selector(BusinessAccountVC.pencilBtnTapped), for: .touchUpInside);
        emailIdBtn.tag = 40;
        self.emailIdTxtF.rightViewMode = UITextFieldViewMode.always;
        self.emailIdTxtF.rightView = emailIdBtn;
        
        let passwordBtn = UIButton.init(type: .custom);
        passwordBtn.frame = CGRect(x: 30, y: 0, width: 30, height: self.passwordTxtF.frame.size.height);
        passwordBtn.setImage(UIImage(named: "Pencilgry"), for: UIControlState());
        passwordBtn.titleLabel?.font = UIFont.systemFont(ofSize: 11.0);
        passwordBtn.setTitleColor(UIColor.darkGray, for: UIControlState());
        passwordBtn.addTarget(self, action:#selector(BusinessAccountVC.pencilBtnTapped(_:)), for: .touchUpInside);
        passwordBtn.tag = 50;
        self.passwordTxtF.rightViewMode = UITextFieldViewMode.always;
        self.passwordTxtF.rightView = passwordBtn;
    }
    
    /*
    func pencilBtnTapped(_ sender: AnyObject){
        
        self.selectedPencil = sender as! UIButton;
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "SecurityAlertVC") as! SecurityAlertVC
        objVC.popupVCDelegate = self;
        objVC.view.frame = self.view.bounds;
        self.view.addSubview(objVC.view)
        self.addChildViewController(objVC)
        objVC.didMove(toParentViewController: self);
    }
    */
    func dismissPopupViewController(_ vc: UIViewController, responseObj: AnyObject?) {
        self.dismiss(animated: true, completion: nil);
        
        // If child view return response then only it will excecute
        if (responseObj != nil) {
            
            // If view controller will PunchesVC then excecute it
            if (vc.isKind(of: SecurityAlertVC.self)) {
                //let vcObj = responseObj as! SecurityAlertVC;
            
                if (responseObj != nil) {
                    
                    if (self.selectedPencil.tag == 40) {
                        self.emailIdTxtF.isUserInteractionEnabled = true;
                        //self.emailIdTxtF.text = "";
                        self.emailIdTxtF.becomeFirstResponder();
                    }
                    else if (self.selectedPencil.tag == 50) {
                        self.passwordTxtF.isUserInteractionEnabled = true;
                        self.passwordTxtF.text = "";
                    self.passwordTxtF.becomeFirstResponder();
                    }
                }
            }
        }
    }
    
    // Method get call to setup
    func setUpTopHeaderBtn() {
        
        let height = self.navigationController?.navigationBar.frame.size.height;
        self.topBtnContainer.frame = CGRect(x: 0, y: 0, width: (self.navigationController?.view.frame.width)!, height: height!);
        print("print----",self.topBtnContainer)
        self.topBtnContainer.backgroundColor = UIColor.clear;
        self.navigationController?.navigationBar.isUserInteractionEnabled = true;
        self.accountBtn.setImage(UIImage(named: "YellowAccountinfo"), for: UIControlState())
    }
    
    // Method will call to set extranal values for the controls
    func setUpViewControlsAndDetails(){
        
        self.userImgBtn.layer.cornerRadius = 7.0;
        //self.userImgBtn.layer.borderWidth = 1
        self.userImgBtn.clipsToBounds = true;
        
        self.descriptionTxtV.layer.cornerRadius = 7.0;
        self.descriptionTxtV.clipsToBounds = true;
        self.descriptionTxtV.layer.borderWidth = 1;
        self.descriptionTxtV.layer.borderColor = GRAY_CUSTOM_BAR_AND_SEPRATOR_BG_COLOR.cgColor;
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        //let userId = user.ID;
        //let personType = user.PersonType;

        self.emailIdTxtF.text = user.EmailID;
        self.passwordTxtF.text = user.Password;
        self.emailIdTxtF.isUserInteractionEnabled = false;
        self.passwordTxtF.isUserInteractionEnabled = false;
        
        let imageUrl = user.BusinessLogoImagePath;
        
        if(imageUrl != nil){
            
            if (imageUrl?.range(of: ".png") == nil) {
                self.userImgBtn.setImage(K_PLACE_HOLDER_IMAGE, for:UIControlState());
            }
            else
            {
                if(Int(user.IsImageVerified!)! == 1){
                    self.userImgBtn.sd_setImage(with: URL(string:imageUrl!), for: UIControlState(), placeholderImage: K_PLACE_HOLDER_IMAGE)
                }
                else{
                    var imgMsg = "Image verification pending";
                    if(Int(user.IsImageVerified!)! == 2){
                        imgMsg = "Image has been disapproved";
                    }
                     self.userImgBtn.sd_setImage(with: URL(string:imageUrl!), for: UIControlState(), placeholderImage: K_PLACE_HOLDER_IMAGE)
                    //self.userImgBtn.sd_setImage(with: URL(string:imageUrl!), for: UIControlState());
                    
                    if (self.userImgBtn.viewWithTag(3098) != nil){
                        let lbl = self.userImgBtn.viewWithTag(3098) as! UILabel;
                        lbl.removeFromSuperview();
                    }
                  
                    let imgMarqueeLbl = MarqueeLabel.init(frame: CGRect(x: 2.0, y: self.userImgBtn.frame.size.height - 15, width: (self.userImgBtn.frame.size.width - 2*3), height: 15));
                    imgMarqueeLbl?.text = imgMsg;
                    imgMarqueeLbl?.marqueeType = .MLContinuous;
                    imgMarqueeLbl?.scrollDuration = 15.0;
                    imgMarqueeLbl?.leadingBuffer = 10.0;
                    imgMarqueeLbl?.trailingBuffer = 5.0;
                    imgMarqueeLbl?.tag = 3098;
                    imgMarqueeLbl?.textColor = UIColor.red;
                    imgMarqueeLbl?.font = UIFont.boldSystemFont(ofSize: 12.0);
                    imgMarqueeLbl?.backgroundColor = UIColor(red: 10/255.0, green: 10/255.0, blue: 10/255.0, alpha: 0.8);
                    self.userImgBtn.addSubview(imgMarqueeLbl!);
                }
            }
        }
        
        if (user.AboutUs != nil) {
            self.descriptionTxtV.text = user.AboutUs
        }
        
        if (user.FirstName != nil) {
            self.firstNameTxtF.text = user.FirstName
        }
        
        if (user.BusinessCategory != nil) {
            self.selectedCategoryID = user.BusinessCategory
            
            let arrayList = BonusLocalDBHandler.getAllCategories();
            for category in arrayList {
                let cat = category as! Category;
                if (cat.ID == self.selectedCategoryID) {
                    self.businessCategoryTxtF.text = cat.CategoryName;
                    break;
                }
            }
        }
        
        if (user.CountryName != nil) {
            self.countryNameTxtF.text = user.CountryName;
            //self.countryNameTxtF.disabledBackground =
        }
        
        if (user.Url != nil) {
            self.urlTxtF.text = user.Url;
        }
        
        if (user.ReferalUserFirstName != nil) {
            self.invitedByLbl.text = "Invited By : \(user.ReferalUserFirstName!)";
        }
        
        self.punchCardBtn.setImage(K_RIGHT_YELLOW_EMPTY, for: UIControlState());
        self.isPunchCardLoyalty = false;
        if (user.IsPunchCardLoyality != nil && Int(user.IsPunchCardLoyality!)! == 1)
        {
            self.isPunchCardLoyalty = true;
            self.punchCardBtn.setImage(K_RIGHT_YELLOW, for: UIControlState());
        }
        
        self.setActiveDeactiveBtnStatus((user.IsActive!));
        // Is business created coupons then not able to update name and category
        if (user.IsCouponCount != nil && Int(user.IsCouponCount!)! >= 1) {
            self.firstNameTxtF.isUserInteractionEnabled = false;
            self.businessCategoryTxtF.isUserInteractionEnabled = false;
        }
        
        if(user.PersonType != nil && user.PersonType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE){
            self.emailIdTxtF.isUserInteractionEnabled = false;
            self.passwordTxtF.isUserInteractionEnabled = false;
            self.userImgBtn.isUserInteractionEnabled = false;
            self.descriptionTxtV.isUserInteractionEnabled = false;
            self.businessCategoryTxtF.isUserInteractionEnabled = false;
            self.firstNameTxtF.isUserInteractionEnabled = false;
            self.urlTxtF.isUserInteractionEnabled = false;
            self.loyaltyBtn.isUserInteractionEnabled = false;
        }
    }
    
    

    @IBAction func topBtnTapped(_ sender: AnyObject) {
        
        let btn = sender as! UIButton;
        
        let viewControllers = self.childViewControllers;
        
        // First check if having childViewControllers
        if (viewControllers.count > 0) {
            for viCont in viewControllers {
                let controller = viCont as UIViewController;
                controller.view.removeFromSuperview();
                controller.removeFromParentViewController();
            }
        }
        
        if(btn == self.accountBtn){
            self.accountBtn.setImage(UIImage(named: "YellowAccountinfo"), for: UIControlState())
            self.branchesBtn.setImage(UIImage(named: "Branches"), for: UIControlState())
            self.loyaltyBtn.setImage(UIImage(named: "LoyaltyWTxt"), for: UIControlState())
            
            self.setUpViewControlsAndDetails();
        }
        else if(btn == self.branchesBtn){
            
            if(self.checkForMandatoryInformation() == true)
            {
                self.accountBtn.setImage(UIImage(named: "Accountinfo"), for: UIControlState())
                self.branchesBtn.setImage(UIImage(named: "YellowBranches"), for: UIControlState())
                self.loyaltyBtn.setImage(UIImage(named: "LoyaltyWTxt"), for: UIControlState())
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyboard.instantiateViewController(withIdentifier: "BranchesVC") as! BranchesVC
                
                objVC.view.frame = self.view.bounds;
                self.view.addSubview(objVC.view)
                self.addChildViewController(objVC)
                objVC.didMove(toParentViewController: self);
                objVC.delegate = self;
            }
        }
        else if(btn == self.loyaltyBtn){
            self.accountBtn.setImage(UIImage(named: "Accountinfo"), for: UIControlState())
            self.branchesBtn.setImage(UIImage(named: "Branches"), for: UIControlState())
            self.loyaltyBtn.setImage(UIImage(named: "LoyaltyYellowWTxt"), for: UIControlState())
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "BusinessLoyaltyVC") as! BusinessLoyaltyVC
            
            objVC.view.frame = self.view.bounds;
            self.view.addSubview(objVC.view)
            self.addChildViewController(objVC)
            objVC.didMove(toParentViewController: self);
            objVC.delegate = self;
        }
    }
    
    func checkForMandatoryInformation()->Bool{
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        if (user.BusinessCategory != nil) {
            return true;
        }
        else{
            let alert = UIAlertController(title: nil, message: "Please update mandatory account information.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
        return false;
    }
    
    @IBAction func sendFeedBackBtnTapped(_ sender: AnyObject) {
        
        let mailComposer = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = self;
        mailComposer.setToRecipients([K_CUSTOMER_CARE_EMAIL_ID]);
        mailComposer.setSubject("Bonus : Feedback")
        self.navigationController?.present(mailComposer, animated: true, completion: nil);
    }
    
    // MARK :- Mail composer delegate handler
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        // Notifies users about errors associated with the interface
        var message: String?;
        switch (result)
        {
        case MFMailComposeResult.cancelled:
           print("Result: canceled");
            break;
        case MFMailComposeResult.saved:
            print("Result: saved");
            message = "Mail saved successfully";
            break;
        case MFMailComposeResult.sent:
            print("Result: sent");
            message = "Mail sent successfully";
            break;
        case MFMailComposeResult.failed:
            print("Result: failed");
            break;
        default:
            print("Result: not sent");
            message = "Mail could not send";
            break;
        }
        
        controller.dismiss(animated: true, completion: nil);
        
        if (message != nil) {
            AppUtility.showAlertWithTitleAndMessage("Mail Status", alertMessage: message!, onView: self);
        }
    }
    
    
    @IBAction func helpBtnTapped(_ sender: AnyObject) {
        
        let businessImagesArray = ["Business_Screen_0_0", "Business_Screen_1_0", "Business_Screen_2", "Business_Screen_3", "Business_Screen_4", "Business_Screen_5", "Business_Screen_6", "Business_Screen_7"];
        let slidingScrollView = HelpScrollView(frame: UIScreen.main.bounds);
        slidingScrollView.callBackOnInitiatedController = {status in
            
        }
        slidingScrollView.showSlidingWindow(pictureArray: businessImagesArray);
    }
    
    
    @IBAction func termsOfUseBtnTapped(_ sender: AnyObject) {
        UIApplication.shared.openURL(URL(string: kTERMS_OF_USE)!)
    }
    
    @IBAction func statusBtnTapped(_ sender: AnyObject) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        if(user.PersonType != nil && user.PersonType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE) {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "You are not the authorized user to deactivate account!" as String, onView: self)
        }
        else{
            
            if (user.IsActiveCoupanCount != nil && Int(user.IsActiveCoupanCount!)! > 0) {
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Business cannot be deactivated as some coupons are still active." as String, onView: self)
            }
            else
            {
                var isActivate = false;
                var alertMessage = "Once you deactivated your account, your all coupons will not listed in coupons list at the customer end";
                if(user.IsActive != nil && Int(user.IsActive!)! == 0) {
                    isActivate = true;
                    alertMessage = "After activation of your account, your all active coupons will be listed in coupons list";
                }
                
                let alert = UIAlertController(title: nil, message: alertMessage as String, preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    
                    if(isActivate == true){
                        self.activateDeactiveUserAccount(1);
                    }
                    else{
                        self.activateDeactiveUserAccount(0);
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func activateDeactiveUserAccount(_ currentStatus : Int){
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        let userId = user.ID;
        let personType = user.PersonType;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["UserID": userId!,
                                           "PersonType": personType!,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            var serviceName = kSERVICE_ACTIVATE_A_BUSINESS_ACCOUNT;
            if(currentStatus == 0) {
                serviceName = kSERVICE_DEACTIVATE_A_BUSINESS_ACCOUNT;
            }
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(serviceName, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {

                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        
                        // Made changes in current session data
                        let defaults: UserDefaults = UserDefaults.standard
                        let nsDataUser = defaults.object(forKey: "UserData") as! Data
                        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
                        
                        user.IsActive = "1";
                        if(currentStatus == 0) {
                            user.IsActive = "0";
                        }

                        let encodedObject = NSKeyedArchiver.archivedData(withRootObject: user) as Data
                        defaults.set(encodedObject, forKey: "UserData")
                        defaults.synchronize();
                        
                        self.setActiveDeactiveBtnStatus("\(currentStatus)");
 
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                }
                }
            )
        }
    }
    
    func setActiveDeactiveBtnStatus(_ currentStatus: String){
    
        let statusImgRed =  UIImage.init(named: "Redbutton")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let statusImgGreen =  UIImage.init(named: "Greenbutton")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        if(currentStatus == "0") {
            self.statusBtn.setTitle("Activate Account", for: UIControlState());
            self.statusBtn.setBackgroundImage(statusImgGreen, for: UIControlState());
        }
        else{
            self.statusBtn.setTitle("De-activate Account", for: UIControlState());
            self.statusBtn.setBackgroundImage(statusImgRed, for: UIControlState());
        }
    }
    
    @IBAction func logoutBtnTapped(_ sender: AnyObject) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID;
        let personType = user.PersonType;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();

        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["UserID": userId!,
                                           "PersonType": personType!,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicator();
            Alamofire.request(kSERVICE_LOGOUT, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicator();
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.logoutFromTheSessionForAllUsers();
                        
                        /*
                        if(message != ""){
                            
                            let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.Alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
                                
                                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                            appDelegate.logoutFromTheSessionForAllUsers();
                                //AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                                
                            }))
                            self.presentViewController(alert, animated: true, completion: nil)
                        }
                        */
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                }
                }
            )
        }
    }
    
    
    
    @IBAction func punchCardBtnTapped(_ sender: AnyObject) {
        if(self.isPunchCardLoyalty == false){
            self.isPunchCardLoyalty = true;
             self.punchCardBtn.setImage(K_RIGHT_YELLOW, for: UIControlState());
        }
        else{
            self.isPunchCardLoyalty = false;
            self.punchCardBtn.setImage(K_RIGHT_YELLOW_EMPTY, for: UIControlState());
        }
    }
    
    
    @IBAction func userImgVTapped(_ sender: AnyObject) {
        
        let alertController = UIAlertController(title: "Choose Media Type", message: "", preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            //println("you have pressed the Cancel button");
        }
        alertController.addAction(cancelAction)
        
        let photosAction = UIAlertAction(title: "Photos", style: .default) { (action:UIAlertAction!) in
            //println("you have pressed OK button");
            self.imagePickerControllerCallWithSource("Photos")
        }
        alertController.addAction(photosAction)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action:UIAlertAction!) in
            //println("you have pressed OK button");
            self.imagePickerControllerCallWithSource("Camera")
        }
        alertController.addAction(cameraAction)
        
        self.present(alertController, animated: true, completion:nil)
    }
    
    func imagePickerControllerCallWithSource(_ source: String?){
        
        let imagPick = UIImagePickerController()
        //imagPick.allowsEditing = true
        imagPick.delegate = self
        if (source == "Photos"){
            imagPick.sourceType = .photoLibrary
        }
        else{
            imagPick.sourceType = .camera
        }
        self.present(imagPick, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        self.dismiss(animated: true, completion: nil)
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.showCropperOnView(pickedImage);
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showCropperOnView(_ croppingImage : UIImage){
    
        let oY : CGFloat = 64.0; //status and top navigation bar height
        let heights = self.view.frame.size.height - oY - 44.0

        let cropperFrameView = CGRect(x: self.view.frame.origin.x, y: CGFloat(oY), width: self.view.frame.size.width, height: heights);
        //let cropperFrameView = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height);
        
        if (self.imageCropperView != nil) {
            self.imageCropperView = nil;
        }
        
        self.imageCropperView = UIView.init(frame: cropperFrameView)
        self.imageCropperView.backgroundColor = UIColor.black;
        self.view.addSubview(self.imageCropperView);
        
        let maxWidth = self.imageCropperView.frame.size.width;
        let maxHeight = maxWidth;
        var scaledImage = AppUtility.imageWithImage(croppingImage, scaledToMaxWidth: maxWidth, scaledToMaxHeight: maxHeight);
        
        var imgWidth = scaledImage.size.width;
        var imgHeight = scaledImage.size.height;
        var imgoX = (cropperFrameView.size.width - imgWidth)/2;
        var imgoY = (cropperFrameView.size.height - imgHeight)/2;
        
        if (croppingImage.size.height > croppingImage.size.width && croppingImage.size.height > (2 * self.imageCropperView.frame.size.height)) {
            //maxHeight = self.imageCropperView.frame.size.height;
            //maxWidth = maxHeight * 3/2;
            imgWidth = cropperFrameView.size.width;
            imgHeight = cropperFrameView.size.height * 0.85;
            imgoX = (cropperFrameView.size.width - imgWidth)/2;
            imgoY = 20;
            scaledImage = croppingImage;
        }
        
        self.displayOriginalImageView = UIImageView.init(frame: CGRect(x: imgoX, y: imgoY, width: imgWidth, height: imgHeight))
        self.displayOriginalImageView.isUserInteractionEnabled = true;
        self.displayOriginalImageView.image = scaledImage;
        self.displayOriginalImageView.contentMode = UIViewContentMode.scaleAspectFit;
        self.imageCropperView.addSubview(self.displayOriginalImageView);
        
        let cancelWidth: CGFloat = 70;
        let cancelHeight: CGFloat = 30;
        let canceloX = (cropperFrameView.size.width/2 - CGFloat(cancelWidth))/2;
        let canceloY = imgoY + imgHeight + 10;
        
        let cropCloseBtn = UIButton(type: UIButtonType.custom)
        cropCloseBtn.frame = CGRect(x: canceloX, y: canceloY, width: cancelWidth, height: cancelHeight);
        cropCloseBtn.setTitle("Cancel", for:UIControlState());
        cropCloseBtn.setTitleColor(UIColor.white, for: UIControlState());
        cropCloseBtn.addTarget(self, action: #selector(BusinessAccountVC.cropperCloseBtnTapped), for: UIControlEvents.touchUpInside);
        self.imageCropperView.addSubview(cropCloseBtn);
        
        let cropBtnWidth = cancelWidth;
        let cropBtnHeight = cancelHeight;
        let cropBtnoX = cropperFrameView.size.width/2 + (cropperFrameView.size.width/2 - cancelWidth)/2;
        let cropBtnoY = canceloY;
        
        let cropBtn = UIButton(type: UIButtonType.custom)
        cropBtn.frame = CGRect(x: cropBtnoX, y: cropBtnoY, width: cropBtnWidth, height: cropBtnHeight);
        cropBtn.setTitle("Crop", for:UIControlState());
        cropBtn.setTitleColor(UIColor.white, for: UIControlState());
        cropBtn.addTarget(self, action: #selector(BusinessAccountVC.cropperCropBtnTapped), for: UIControlEvents.touchUpInside);
        self.imageCropperView.addSubview(cropBtn);
        
        // allocate crop interface with frame and image being cropped
        self.cropper = BFCropInterface.init(frame: self.displayOriginalImageView.bounds, andImage: scaledImage, nodeRadius: 8, cropType: kCroppingTypeSquare);
        self.cropper.isStaticCopWindow = true;
        
        // this is the default color even if you don't set it
        self.cropper.shadowColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.60);
        self.cropper.borderColor = UIColor.white;
        self.cropper.borderWidth = 1.5;
        self.cropper.showNodes = true;
        
        var croperWidth = self.displayOriginalImageView.frame.size.width;
        var croperHeight = self.displayOriginalImageView.frame.size.height;
        
        if (croperWidth < croperHeight) {
            croperHeight = croperWidth;
        }
        else if (croperHeight < croperWidth){
            croperWidth = croperHeight;
        }
        
        let positionX = (self.displayOriginalImageView.frame.size.width - croperWidth)/2;
        let positionY = (croperWidth - croperHeight)/2;
        self.cropper.setCropViewPosition(positionX, y: positionY, width: croperWidth, height: croperHeight);
        // add interface to superview. here we are covering the main image view.
        self.displayOriginalImageView.addSubview(self.cropper);
    }
    
    func cropperCloseBtnTapped(){
        self.imageCropperView.removeFromSuperview();
        self.imageCropperView = nil;
        self.cropper.removeFromSuperview();
        self.cropper = nil;
    }
    
    func cropperCropBtnTapped(){
        // crop image
        let croppedImage = self.cropper.getCroppedImage()
        self.cropperCloseBtnTapped();
        
        let maxWidth = self.view.frame.size.width;
        let maxHeight = maxWidth;
        print("--- %@", croppedImage);
        
        let scaledImage = AppUtility.imageWithImage(croppedImage!, scaledToMaxWidth: maxWidth, scaledToMaxHeight: maxHeight);
        
        self.userImgBtn.setImage(scaledImage, for: UIControlState());
        self.croppedImageToUpload = scaledImage;
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder();
        return true;
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.focusedControl = textView;
        return true;
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.focusedControl = textField;
    }
    // MARK:- TextField delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.focusedControl = textField;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == self.passwordTxtF && (self.passwordTxtF.text?.characters.count > 15 && string.characters.count > 0)) {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Password length can not be more then 15 charactors", onView: nil);
        }
        
        return true;
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        self.focusedControl = textField;
        var returnValue = true
        
        if(textField == self.businessCategoryTxtF)
        {
            returnValue = false;
            self.businessCategoryTxtF.resignFirstResponder();
            self.categoryListArray = nil;
            self.categoryListArray = BonusLocalDBHandler.getAllCategories();
            if (self.categoryListArray!.count > 0) {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyboard.instantiateViewController(withIdentifier: "CategoriesVC") as! CategoriesVC
                objVC.delegate = self;
                //objVC.dataArray = self.categoryListArray as! NSMutableArray;
                objVC.view.frame = self.view.bounds;
                self.view.addSubview(objVC.view)
                self.addChildViewController(objVC)
                objVC.didMove(toParentViewController: self);
                objVC.showTableView(self.categoryListArray as! NSMutableArray);
            }
        }
        return returnValue;
    }

    // MARK: - CategoryVC delegate method
    func dismissCategoryVC(_ vc: UIViewController, selectedObj: AnyObject?) {
        
        if (selectedObj != nil) {
            
            let categoryObj = selectedObj as! Category;
            self.businessCategoryTxtF.text = categoryObj.CategoryName;
            selectedCategoryID = categoryObj.ID;
        }
        
    }
    
    @IBAction func saveBtnTapped(_ sender: AnyObject) {
        // Disable user interection
        self.emailIdTxtF.isUserInteractionEnabled = false;
        self.passwordTxtF.isUserInteractionEnabled = false;
        
        // Resign from responder
        self.descriptionTxtV.resignFirstResponder();
        self.firstNameTxtF.resignFirstResponder();
        self.urlTxtF.resignFirstResponder();
        
        let emailId : String = self.emailIdTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let password : String = self.passwordTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        var url : String = self.urlTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        var businessName : String = self.firstNameTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        var aboutUs : String = self.descriptionTxtV.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        var category = self.selectedCategoryID;
        
        let isValidEmail = AppUtility.validateEmail(emailId);
        
        var message : String? = nil;
        
        if !isValidEmail {
            message = LOGIN_ALERT_MESSAGE_EMAIL_ID;
        }
        else if password.isEmpty {
            message = "Please enter a valid password";
        }
        else if (password.characters.count < 6){
            message = "Please enter a Password containing at least 6 and max 15 characters";
        }
        else if (businessName.characters.count <= 0){
            message = "Please enter a valid business name";
        }
        else if (category == nil){
            message = "Please select business category.";
        }
        
        if message != nil {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
        }
        else{
            
            let deviceToken = AppUtility.getDeviceToken();
            let deviceUniqueId = AppUtility.getDeviceNSUUID();
            
            var pictureDataString = "";
            // Profile picture uploading
            if (self.croppedImageToUpload != nil)
            {
                let pictureData = UIImagePNGRepresentation(self.croppedImageToUpload);
                pictureDataString = (pictureData?.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))!;
            }
            
            if (aboutUs.characters.count <= 0) {
                aboutUs = "";
            }
            
            if (businessName.characters.count <= 0)
            {
                businessName = "";
            }
            
            if (url.characters.count <= 0)
            {
                url = "";
            }
            
            if (category!.characters.count <= 0)
            {
                category = "";
            }
            
            var loyalty = "0";
            if(self.isPunchCardLoyalty == true)
            {
                loyalty = "1"
            }
            
            let defaults: UserDefaults = UserDefaults.standard
            let nsDataUser = defaults.object(forKey: "UserData") as! Data
            let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
            
            // Created postDataDictonary to post the data on server
            let postDataDict : NSDictionary = ["ID" : user.ID!,
                                               "EmailID" : emailId,
                                               "Password" : password,
                                               "Name": businessName,
                                               "Url" : url,
                                               "BusinessCategory" : category!,
                                               "IsPunchCardLoyality":loyalty,
                                               "DeviceUniqueID" : deviceUniqueId,
                                               "PersonType" : USER_TYPE_BUSINESS,
                                               "BusinessLogoImagePath" : pictureDataString,
                                               "AboutUs" : aboutUs,
                                               "api_key" : kWEB_SERVICES_API_KEY
                                                ];
            
            print("postData = %@", postDataDict);
            
            if !AppUtility.isNetworkAvailable(){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
            }
            else{
                
                CustomProgressView.showProgressIndicatorOnView(VC: self);
                Alamofire.request(kSERVICE_SAVE_BUSINESS_INFO, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                    
                    CustomProgressView.hideProgressIndicatorOnView(VC: self);
                    
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                    print("value =  \(datastring)");
                    
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                        let dataDict = JSON as? NSDictionary;
                        
                        if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                            
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
                            
                            let user = User(data: dataDict?.object(forKey: "Response") as! Dictionary<String, AnyObject>)
                            
                            self.setUserDataInDefaultSession(user);
                        }
                        else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                            
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                        }
                        else if((dataDict?.object(forKey: "Status"))! as! String == "Pending" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 700) {
                            
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            
                            let alert = UIAlertController(title: message, message: nil, preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
                                self.logoutBtnTapped("" as AnyObject);
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        else
                        {
                            let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        }
                    }
                    }
                )
            }
        }
    }
    
    func setUserDataInDefaultSession(_ userObj: User){
        
        let encodedObject = NSKeyedArchiver.archivedData(withRootObject: userObj) as Data
        let defaults = UserDefaults.standard
        defaults.set(encodedObject, forKey: "UserData")
        defaults.synchronize()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK:- Dismiss child view Controllers
    func dismissSelfViewControllerFromSuperViewController(_ vc: UIViewController, responseObj: AnyObject?) {
        
        
    }

    @IBAction func pencilBtnTapped(_ sender: Any) {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;

        if(user.PersonType != nil && user.PersonType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE) {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "You are not authorized user to edit login detail" as String, onView: self)
        }
        else
        {
            self.selectedPencil = sender as! UIButton;
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "SecurityAlertVC") as! SecurityAlertVC
            objVC.popupVCDelegate = self;
            objVC.view.frame = self.view.bounds;
            self.view.addSubview(objVC.view)
            self.addChildViewController(objVC)
            objVC.didMove(toParentViewController: self);
        }
        
    }

}
