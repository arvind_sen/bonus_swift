//
//  CouponsTVC.swift
//  Bonus
//
//  Created by Arvind Sen on 12/01/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

extension String {
    init(htmlEncodedString: String) {
        self.init()
        guard let encodedData = htmlEncodedString.data(using: .utf8) else {
            self = htmlEncodedString
            return
        }
        
        let attributedOptions: [String : Any] = [
            NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
            NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue
        ]
        
        do {
            let attributedString = try NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
            self = attributedString.string
        } catch {
            print("Error: \(error)")
            self = htmlEncodedString
        }
    }
}

class CouponsTVC: UITableViewController, UISearchBarDelegate, CLLocationManagerDelegate {
    
    var objPlaceOnMapVC : PlaceOnMapVC? = nil;
    var revealVC : SWRevealViewController? = nil;
    var searchBar: UISearchBar!
    var isWebServiceCalled = false
    var leftMenusSelectedFor : SelectionFor!
    var selectedDict: NSDictionary!
    var lastOffset: String!
    var selectedCategoriesFromLeftMenus : String?
    var selectedIndexPathFromLeftMenus : IndexPath?
    
    var didSelectedRowObj: Loyalty!;
    
    var businessListArray: NSMutableArray!
    var searchDataArray: NSMutableArray!
    var locationManager : CLLocationManager?
    var userCurrentLocationCoordinate : CLLocationCoordinate2D? ;
    var isMapViewComeBack: Bool = false;
    
    var isVisitorLookingFirst = true;
    var flagToNoDataFound = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        // This code is using to hid left menus
        if (self.revealViewController() != nil) {
            //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        self.tableView.backgroundColor = K_LISTING_VIEW_BG_COLOR;
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.showTopBtns();
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if #available(iOS 11.0, *) {
            //self.tableView.contentInsetAdjustmentBehavior = .never
            //self.tableView.contentInset = UIEdgeInsetsMake(0.0, 5.0, 0.0, 5.0)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
        
        let defaults = UserDefaults.standard
    
        // If got notification for the deal type then this condition will execute
        if(defaults.object(forKey: "pushNotificationData") != nil)
        {
            self.performActionOnPushNotification();
        }
        else
        {
            let defaults: UserDefaults = UserDefaults.standard
            let globalUserType = defaults.object(forKey: "globalUserType") as! String;
            
            if(globalUserType == USER_TYPE_VISITOR && self.isVisitorLookingFirst == true){
                self.isVisitorLookingFirst = false;
                let customerImagesArray = ["Customer_Screen_0_0", "Customer_Screen_1_0", "Customer_Screen_1_5", "Customer_Screen_2", "Customer_Screen_3", "Customer_Screen_4", "Customer_Screen_5", "Customer_Screen_6", "Customer_Screen_7", "Customer_Screen_8", "Customer_Screen_8_1", "Customer_Screen_9", "Customer_Screen_10", "Customer_Screen_11"];
                let slidingScrollView = HelpScrollView(frame: UIScreen.main.bounds);
                slidingScrollView.callBackOnInitiatedController = { status in
                    if(status == true){
                        self.viewWillAppear(true);
                    }
                }
                slidingScrollView.showSlidingWindow(pictureArray: customerImagesArray);
            }
            else
            {
                // This condition will excecute every time except once if visitor visiting coupons screen first time
                if (isMapViewComeBack == true && self.businessListArray != nil) {
                    self.showTableView();
                }
                else
                {
                    let objVC = self.navigationController?.presentedViewController;
                    
                    if (objVC != nil && objVC!.isKind(of: PlaceOnMapVC.self)) {
                        // <#code#>
                    }
                    else{
                        self.callLocationManager();
                    }
                }
            }
        }
    }

    func performActionOnPushNotification(){
    
        AppUtility.removedBackTitleAndChangeItsColor(VC: self, backTitle: "", backTintColor: UIColor.white);
        
        let defaults = UserDefaults.standard
        let pushData = defaults.object(forKey: "pushNotificationData") as! NSDictionary;
        let dealId = pushData.value(forKey: "id") as? String;
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "CouponDetailVC") as! CouponDetailVC
        //objVC.objBranchCoupon = obj;
        objVC.dealMasterId = dealId;
        self.navigationController?.pushViewController(objVC, animated: true);
        
        // Removed push notification object from default data.
        if(defaults.object(forKey: "pushNotificationData") != nil)
        {
            defaults.removeObject(forKey: "pushNotificationData");
            defaults.synchronize();
        }
    }
    
    func showTopBtns(){
        
        // Left toggle button configration
        self.navigationController?.navigationBar.barTintColor = TAB_BAR_BG_COLOR;
        self.navigationController?.isNavigationBarHidden = false;
        self.revealVC = self.revealViewController()
        //self.navigationController?.navigationBar.addGestureRecognizer(self.revealVC!.panGestureRecognizer())
        
        let revealButtonItem = UIBarButtonItem();
        revealButtonItem.image = UIImage (named: "FilterIcon")
        revealButtonItem.style = UIBarButtonItemStyle.plain;
        revealButtonItem.target = self;
        revealButtonItem.action = #selector(CouponsTVC.revealToggle(_:));
        revealButtonItem.tintColor = UIColor.white;
        self.navigationItem.leftBarButtonItem = revealButtonItem;
        
        
        //Show search bar on top of the view controller
        
        let oY = CGFloat(5.0)
        let oX = CGFloat(50.0)
        
        let height = (self.navigationController?.navigationBar.frame.height)! - CGFloat(2 * oY);
        let width = (self.navigationController?.navigationBar.frame.width)! - CGFloat(2 * oX);
        self.searchBar = UISearchBar.init(frame: CGRect(x: 0.0, y: oY, width: width, height: height));
        self.searchBar.placeholder = "Search"
        self.searchBar.showsCancelButton = false;
        self.searchBar?.delegate = self;
        self.navigationItem.titleView = self.searchBar
        
        // Right Top tab bar button configration
        var rightBtn = UIImage(named: "MapWhitePin")
        rightBtn = rightBtn?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let barRightBtn = UIBarButtonItem(image: rightBtn, style: UIBarButtonItemStyle.plain, target: self, action:#selector(CouponsTVC.rightBtnTapped(_:)));
        self.navigationItem.rightBarButtonItem = barRightBtn;
    }
    
    
    func revealToggle(_ obj : AnyObject?){
        self.hideChildView();
        self.searchBar.resignFirstResponder();
        self.revealVC!.revealToggle(nil)
    }
    
    // Method will call to open map view on current view controller
    func rightBtnTapped(_ sender: AnyObject){
        
        self.hideChildView();
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "CouponsMapLocationVC") as! CouponsMapLocationVC
        objVC.currentLocationCoordinate = userCurrentLocationCoordinate;
        objVC.leftMenusSelectedFor = self.leftMenusSelectedFor; //Passing same left menu selection data
        objVC.listArray = self.businessListArray;
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = "flip"
        transition.subtype = kCATransitionFromLeft
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    func showTableView(){
        
        if self.searchDataArray != nil{
            self.searchDataArray.removeAllObjects()
        }
        
        self.searchDataArray = NSMutableArray();
        self.searchDataArray.addObjects(from: self.businessListArray as [AnyObject])
        
        self.tableView.reloadData();
    }
    
    // Method get call to get user current location lat, long
    func callLocationManager() {
        
        if (CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied) {
            
            self.locationManager = CLLocationManager.init();// locationManager need to define at the class variable then only its call delegate methods.
            self.locationManager!.delegate = self;
            
            if self.locationManager!.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
                self.locationManager!.requestWhenInUseAuthorization();
            }
            self.locationManager!.desiredAccuracy = kCLLocationAccuracyBest;
            self.locationManager!.startUpdatingLocation();
        }
        else{
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "To re-enable, please go to Settings and turn on Location Service for this app.", onView: self)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.locationManager!.stopUpdatingLocation();
        let recentLocation = locations.last;
        self.userCurrentLocationCoordinate = recentLocation?.coordinate;
        
        let latString = String(format: "%f", (recentLocation?.coordinate.latitude)!);
        let longString = String(format: "%f", (recentLocation?.coordinate.longitude)!);
        
        let dict = NSDictionary.init(objects: [latString, longString], forKeys: ["Latitude" as NSCopying, "Longitude" as NSCopying]);
        
        
        
        self.callActiveBusinessWebService(SelectionFor.isSelectedNearBy, withParameters: dict, offSetValue: "0");
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(" failed to get current location");
        
        self.locationManager!.stopUpdatingLocation();
        
        let dict = NSDictionary.init(objects: ["0.0", "0.0"], forKeys: ["Latitude" as NSCopying, "Longitude" as NSCopying]);
        
        self.callActiveBusinessWebService(SelectionFor.isSelectedNearBy, withParameters: dict, offSetValue: "0");
    }
    
    func callCouponsTVCFromLeftMenu(_ indexPath: IndexPath, categoriesIds: String = "") -> Void {
        
        print("indexPath = %@", indexPath);
        
        if (indexPath.section == 0 && indexPath.row == 0) {
            self.callActiveBusinessWebService(SelectionFor.isSelectedFavorite, withParameters: nil)
        }
        else if (indexPath.section == 1 && indexPath.row == 0){
            self.callLocationManager();
        }
        else if (indexPath.section == 1 && indexPath.row == 1){
            self.callActiveBusinessWebService(SelectionFor.isSelectedAToZ, withParameters: nil)
        }
        else if (indexPath.section == 2){
            
            let categoriesDict = NSDictionary.init(object: categoriesIds, forKey: "ID" as NSCopying);
            self.callActiveBusinessWebService(SelectionFor.isSelectedProductGroup, withParameters: categoriesDict);
        }
    }
    
    func callActiveBusinessWebService(_ selectedSection: SelectionFor, withParameters: NSDictionary?, offSetValue: String = "0") -> Void {
        
        self.leftMenusSelectedFor = selectedSection;
        self.selectedDict = withParameters;
        self.lastOffset = offSetValue;
        
        var searchFor = "";
        
        if ((self.searchBar.text?.characters.count)! > 0) {
            searchFor = self.searchBar.text!;
        }
        
        var userId = ""
        var userType = "";
        let defaults: UserDefaults = UserDefaults.standard
        
        let globalUserType = defaults.object(forKey: "globalUserType") as! String;
        
        if(globalUserType == USER_TYPE_VISITOR){
            userId = "0";
            userType = USER_TYPE_VISITOR;
        }
        else{
            
            let nsDataUser = defaults.object(forKey: "UserData") as! Data
            let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
            
            userId = user.ID!
            userType = user.PersonType!;
        }
        
        let uniqueID = AppUtility.getDeviceNSUUID();
        let postDataDict : NSMutableDictionary = ["UserID" : userId,
                                                  "PersonType" : userType,
                                                  "BusinessCategory" : "0",
                                                  "FavoriteStatus" : "0",
                                                  "AllSelected" : "1",
                                                  "DealId" : "0",
                                                  "Latitude" : "0",
                                                  "Longitude" : "0",
                                                  "Limit" : "10",
                                                  "Offset" : offSetValue,
                                                  "SortBy" : "Distance",
                                                  "OrderBy" : "ASC",
                                                  "SearchFor" : searchFor,
                                                  "api_key" : kWEB_SERVICES_API_KEY,
                                                  "DeviceUniqueID" : uniqueID];
        
        if (self.userCurrentLocationCoordinate != nil) {
            let latString = String(format: "%f", self.userCurrentLocationCoordinate!.latitude);
            let longString = String(format: "%f", self.userCurrentLocationCoordinate!.longitude);
            
            postDataDict.setObject(latString, forKey: "Latitude" as NSCopying);
            postDataDict.setObject(longString, forKey: "Longitude" as NSCopying);
        }
        
        switch selectedSection {
        case .isSelectedFavorite:
            postDataDict.setObject("1", forKey: "FavoriteStatus" as NSCopying)
            break;
        case .isSelectedNearBy:
            postDataDict.setObject("ASC", forKey: "OrderBy" as NSCopying)
            break;
        case .isSelectedAToZ:
            postDataDict.setObject("BranchDescription", forKey: "SortBy" as NSCopying)
            postDataDict.setObject("ASC", forKey: "OrderBy" as NSCopying)
            break;
        default:
            postDataDict.setObject(withParameters!.object(forKey: "ID")! as! String, forKey: "BusinessCategory" as NSCopying)
        }
        
        postDataDict.setObject("0", forKey: "IsShowOnMap" as NSCopying);
        //postDataDict.setObject(user.PersonType!, forKey: "PersonType" as NSCopying);
        
        self.callActiveBusinesses(postDataDict);
    }
    
    func callActiveBusinesses(_ postData: NSDictionary){
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicatorOnView(VC: self);
            Alamofire.request(kSERVICE_BUSINESS_LIST, method: HTTPMethod.post, parameters: postData as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicatorOnView(VC: self);
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        let resourcePath = dataDict?.object(forKey: "ResourcePath") as! Dictionary<String, AnyObject>
                        
                        if(Int(postData.object(forKey: "Offset") as! String)! == 0){
                            self.businessListArray = nil;
                        }
                        
                        if(self.businessListArray == nil){
                            self.businessListArray = NSMutableArray();
                        }
                        
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let item = ActiveBusiness(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>, resourcePaths: resourcePath)
                                
                                self.businessListArray?.add(item);
                            }
                            
                            OperationQueue.main.addOperation {
                                self.flagToNoDataFound = false;
                                self.tableView.reloadData();
                            }
                        }
                        else
                        {
                            OperationQueue.main.addOperation {
                                self.flagToNoDataFound = true;
                                self.tableView.reloadData()
                            }
                            var message = (dataDict?.object(forKey: "Message"))! as! String;
                            
                            if (self.leftMenusSelectedFor == SelectionFor.isSelectedFavorite) {
                                message = "You do not have any Favourites yet...";
                            }
                            /*
                            else{
                                [AppUtility showAlertMessage:@"No data found for selected item"];
                            }
                            */
                            self.navigationController?.view.makeToast(message, duration: 2.0, position: .center)
                        }
                        
                    DispatchQueue.main.async {
                        self.showTableView();
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                        
                    }
                }
            }
            )
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        var numOfSections: Int = 0
        if (self.searchDataArray != nil && (self.searchDataArray?.count)! > 0)
        {
            //tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else if (self.flagToNoDataFound == true)
        {
            var oX = 0.0;
            var oY = 0.0;
            let widthX = tableView.frame.size.width;
            let heightY = tableView.frame.size.height;
            
            let emptyContainer = UIView.init(frame: CGRect(x: oX, y: oY, width: Double(widthX), height: Double(heightY)));
            emptyContainer.backgroundColor = K_EMPTY_TABLE_BG_COLOR;
            ///tblCell.contentView.addSubview(emptyContainer);
            
            let imgWidth = 100.0;
            let imgHeight = 100.0;
            oX = (Double(widthX) - imgWidth)/2;
            oY = (Double(heightY) - imgHeight)/2;
            
            let imageView = UIImageView.init(frame: CGRect(x: oX, y: oY, width: imgWidth, height: imgHeight));
            imageView.backgroundColor = UIColor.clear;
            imageView.contentMode = .scaleAspectFit;
            imageView.image = UIImage.init(named: "alert");
            emptyContainer.addSubview(imageView);
            
            let titleWidth = emptyContainer.frame.size.width - 20.0;
            let titleHeight = 40.0;
            let OY = imageView.frame.size.height + imageView.frame.origin.y + 10.0;
            
            let titleLbl = UILabel.init(frame: CGRect(x: 10.0, y: Double(OY), width: Double(titleWidth), height: titleHeight));
            titleLbl.textAlignment = .center;
            titleLbl.numberOfLines = 0;
            titleLbl.lineBreakMode = .byWordWrapping;
            titleLbl.text = "No coupons added yet"
            titleLbl.textColor = UIColor.darkGray;
            emptyContainer.addSubview(titleLbl);
            //titleLbl.sizeToFit();
            
            tableView.backgroundView  = emptyContainer;
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        var returnValue = 0;
        
        if self.searchDataArray != nil {
            returnValue = self.searchDataArray.count;
        }
        return returnValue;
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let cl = tableView.cellForRow(at: indexPath);
//        
//        if(cl != nil && (cl?.contentView.subviews.count)! > 0)
//        {
//            for obj in (cl?.contentView.subviews)! {
//                obj.removeFromSuperview();
//            }
//        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        // Configure the cell...
        self.configureTableViewCell(cell, forRowAtIndexPath: indexPath);
        
        return cell
    }
    

    // This below method is setting different values for a row
    func configureTableViewCell(_ cell: UITableViewCell, forRowAtIndexPath:IndexPath){
        
        let obj = searchDataArray[forRowAtIndexPath.row] as! ActiveBusiness;
        
        let containerView = cell.contentView.viewWithTag(99);
        let businessImage = containerView?.viewWithTag(1305) as! UIImageView;
        let nameLbl = containerView?.viewWithTag(1306) as! UILabel;
        nameLbl.text = "";
        let areaLbl = containerView?.viewWithTag(1307) as! UILabel;
        areaLbl.text = ""
        let categoryImage = containerView?.viewWithTag(1308) as! UIImageView;
        let mapPinBtn = containerView?.viewWithTag(1309) as! UIButton;
        let distanceLbl = containerView?.viewWithTag(1310) as! UILabel;
        distanceLbl.text = "";
        
        if(obj.IsImageVerified != nil && Int(obj.IsImageVerified!) == 1 && obj.BusinessLogoImagePath != nil){
            businessImage.sd_setImage(with: URL(string: obj.BusinessLogoImagePath!), placeholderImage: K_PLACE_HOLDER_IMAGE)
        }else{
            businessImage.image = K_PLACE_HOLDER_IMAGE;
        }
        
        if (obj.FirstName != nil) {
            let string = obj.FirstName!;
            //nameLbl.text = String(htmlEncodedString: string);
            nameLbl.text = string;
        }
        else if(obj.BranchDescription != nil){
            //nameLbl.text = String(htmlEncodedString: obj.BranchDescription!);
            nameLbl.text = obj.BranchDescription!;
        }
        //nameLbl.sizeToFit();
        
        if(obj.BranchDescription != nil){
            areaLbl.text = obj.BranchDescription!;
        }
        
        if(obj.FilledCategoryPicture != nil){
             categoryImage.sd_setImage(with: URL(string: obj.FilledCategoryPicture!), placeholderImage: K_YELLOW_CIRCLE_SMALL_IMAGE)
        }
    
        // Set distance value
        distanceLbl.text = String(format: "%.02f KM", Float(obj.Distance!)!);
        
        mapPinBtn.setImage(K_BONUS_MAP_PIN_IMAGE, for: UIControlState())
        // Set map/pot/faviourite icons
        if(obj.BonusPotLocationID != nil && Int(obj.BonusPotLocationID!)! > 0){
            mapPinBtn.setImage(K_BONUS_POT_IMAGE, for: UIControlState())
        }else if(obj.FavoriteStatus != nil && Int(obj.FavoriteStatus!)! > 0){
            mapPinBtn.setImage(K_FAVIROUTE_IMAGE, for: UIControlState())
        }
        
        for index in 51...55 {
            let starImg = containerView?.viewWithTag(index) as? UIImageView;
            let ratingValue = index - 50;
            if (obj.MaxRating != nil && ratingValue <= Int(obj.MaxRating!)!) {
                starImg?.image = UIImage(named: "starfull")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
            }
            else{
                starImg?.image = UIImage(named: "starempty")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AppUtility.removedBackTitleAndChangeItsColor(VC: self, backTitle: "", backTintColor: UIColor.white);
        let obj = searchDataArray[indexPath.row] as! ActiveBusiness;
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "BranchCouponsTVC") as! BranchCouponsTVC
        objVC.activeBusinessObj = obj;
        objVC.locationCoordinate = self.userCurrentLocationCoordinate;
        self.navigationController?.pushViewController(objVC, animated: true);
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!;
        cell.accessoryType = UITableViewCellAccessoryType.none;
    }
    
    // Method will get call when user tap on a map pin on different screens
    @IBAction func placeOnMapBtnTapped(_ sender: AnyObject) {
        
        let btn = sender as! UIButton;
        let pointInTable = btn.convert(btn.bounds.origin, to: self.view)
        let indexPath = self.tableView.indexPathForRow(at: pointInTable);
        
        let objLoyalty = self.businessListArray.object(at: (indexPath?.row)!) as! ActiveBusiness;
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        objPlaceOnMapVC = storyboard.instantiateViewController(withIdentifier: "PlaceOnMapVC") as! PlaceOnMapVC
        
        // Initiate values with controller objects
        objPlaceOnMapVC?.clLocationCoordinate = CLLocationCoordinate2DMake(Double(objLoyalty.Latitude!)!, Double(objLoyalty.Longitude!)!);
        objPlaceOnMapVC?.locationTitle = objLoyalty.FirstName;
        objPlaceOnMapVC?.locationSubTitle = objLoyalty.BranchDescription
        objPlaceOnMapVC?.dealCategoryImage = objLoyalty.MapPinCategoryPicture;
        objPlaceOnMapVC?.bonusPotLocationID = objLoyalty.BonusPotLocationID;
        objPlaceOnMapVC?.branchID = objLoyalty.BranchID;
        
        self.addChildViewController(objPlaceOnMapVC!)
        
        objPlaceOnMapVC?.view.frame = self.view.bounds;
        self.view.addSubview((objPlaceOnMapVC?.view)!)
        objPlaceOnMapVC?.didMove(toParentViewController: self);
        //self.view.isUserInteractionEnabled = false;
    }
    
    func searchDataWithEnrty(_ str: String?){
        
        if str != nil { //If data array empty
            
            let servicePredicate = NSPredicate(format: "SELF.FirstName CONTAINS[cd] %@", str!)
            
            let result = self.businessListArray.filtered(using: servicePredicate)
            self.searchDataArray.removeAllObjects()
            self.searchDataArray.addObjects(from: result)
        }
        else{
            self.searchDataArray.removeAllObjects()
            self.searchDataArray.addObjects(from: self.businessListArray as [AnyObject])
        }
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.callActiveBusinessWebService(self.leftMenusSelectedFor, withParameters: self.selectedDict);
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        //self.searchBar.setShowsCancelButton(true, animated: true)
        self.hideChildView();
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        // searchBar.setShowsCancelButton(false, animated: true)
        //self.searchDataWithEnrty(nil);
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.hideChildView();
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating ")
        
        if((scrollView.contentSize.height + 20.0) < (scrollView.contentOffset.y + scrollView.frame.size.height)){
            
            var offset = 0;
            
            if (self.businessListArray != nil && self.businessListArray.count > 0){
                offset = self.businessListArray.count;
            }
            // Call more places for the same coordinates
            if(self.selectedDict != nil){
                self.callActiveBusinessWebService(self.leftMenusSelectedFor, withParameters: self.selectedDict, offSetValue: String(offset))
            }
            
        }
    }
    
//    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
//        for view in self.view.subviews {
//            if view.isUserInteractionEnabled, view.point(inside: self.convert(point, to: view), with: event) {
//                return true
//            }
//        }
//        
//        return false
//    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func hideChildView(){
        
        for vc in self.childViewControllers{
            
            vc.view.removeFromSuperview()
            vc.removeFromParentViewController()
            vc.willMove(toParentViewController: nil)
        }
        /*
        if(self.objPlaceOnMapVC != nil){
            self.objPlaceOnMapVC?.willMove(toParentViewController: nil)
            self.objPlaceOnMapVC?.view.removeFromSuperview()
            self.objPlaceOnMapVC?.removeFromParentViewController()
        }
         */
    }
}
