//
//  ManageMoneyVC.swift
//  Bonus
//
//  Created by Arvind Sen on 12/07/17.
//  Copyright © 2017 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

class ManageMoneyVC: UIViewController, UITableViewDataSource, UITableViewDelegate, CCAvenueWebViewDelegate, UITextFieldDelegate {
    
    var objCustomerMoney: CustomerMoney? = nil;
    
    @IBOutlet weak var transactionTblView: UITableView!
    @IBOutlet weak var totalCreditAmountLbl: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewContainerView: UIView!
    @IBOutlet weak var enterAmountTxtF: UITextField!
    @IBOutlet weak var amountCurrencyCode: UILabel!

    @IBOutlet weak var withdrawalAmountTxtF: UITextField!
    @IBOutlet weak var withdrawalAmountCurrencyCode: UILabel!
    
    var transactionHistoryArray : Array<TransactionHistory> = [TransactionHistory]();
    var objAvailableCredit : CustomerAvailableCredit? = nil;
    var ccAvenuePaymentReturnData: NSDictionary? = nil;
    
    var isNeedToReloadViewOnAppear = true;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        AppUtility.navigationColorAndItsTextColor(VC: self);
        self.title = "Credit"
        
        /*
         let postDataDict : NSDictionary = ["UserID": "4",
         "Amount": "50",
         "CurrencyAbrrevation": "INR",
         "TransactionType": "ccavenue",
         "OrderID": "AsdafdfbFSFds",
         "TrackingID": "jfdsldslffdsfdklf",
         "TransactionID": "XYZ",
         "PaymentId": "",
         "DeviceUniqueID": AppUtility.getDeviceNSUUID(),
         "api_key": kWEB_SERVICES_API_KEY];
         
         self.uploadLoadedAmountInWallet(postDataDict);
         */
        self.configureInputAccesseryViewForInputFields();
    }

    func configureInputAccesseryViewForInputFields(){
        //Configured in viewDidLoad()
        var accessoryDoneButton: UIBarButtonItem!
        let accessoryToolBar = UIToolbar(frame: CGRect(x : 0, y : 0, width : UIScreen.main.bounds.width, height : 44))
        accessoryDoneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.accessoryViewDoneBtnPressed(_:)))
        let accessroySpace = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil);
        accessoryDoneButton.tintColor = UIColor.darkGray;
        accessoryToolBar.items = [accessroySpace, accessoryDoneButton]
        
        self.withdrawalAmountTxtF.inputAccessoryView = accessoryToolBar
    }
    
    func accessoryViewDoneBtnPressed(_ sender: UIButton){
        self.withdrawalAmountTxtF.resignFirstResponder();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
        
        if(self.isNeedToReloadViewOnAppear == true)
        {
            self.getUserAvailableCredit();
        }
        else{
            self.isNeedToReloadViewOnAppear = true;
        }
        self.hideChildView();
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.scrollView.contentSize = CGSize(width: Double(self.view.frame.size.width), height: 600.0);
    }
    
    func getUserAvailableCredit(){
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID!;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["UserID": userId,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicatorOnView(VC: self);
            Alamofire.request(kSERVICE_USER_AVAILABLE_CREDIT, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicatorOnView(VC: self);
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    let status = (dataDict?.object(forKey: "Status"))! as? String
                    
                    if(status == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200)
                    {
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        
                        self.objAvailableCredit = CustomerAvailableCredit(data: (dictArray[0] as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>)
                    
                        DispatchQueue.main.async {
                            self.totalCreditAmountLbl.text = String(format: "%@ %.2f", (self.objAvailableCredit?.CurrencyAbrrevation!)! , Double((self.objAvailableCredit?.TotalBalance)!)!)
                            self.amountCurrencyCode.text = self.objAvailableCredit?.CurrencyAbrrevation;
                            self.withdrawalAmountCurrencyCode.text = self.objAvailableCredit?.CurrencyAbrrevation;
                        }
                        
                        self.getUserAllTransaction();
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                        
                    }
                }
            }
            )
        }
    }
    
    func getUserAllTransaction() {
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        let userId = user.ID!;
        
        
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
       
        var months = ("\(month)");
        if(month < 10)
        {
            months = ("0\(month)");
        }
        
        var days = ("\(day)");
        if(day < 10)
        {
            days = ("0\(day)");
        }
        var userCreatedDate = ("\(year - 1)-\(months)-\(days) 00:00:00");
        
        if(user.CreatedDate != nil)
        {
            userCreatedDate = user.CreatedDate!;
        }
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = "yyyy-MM-dd";
        let toDate = dateFormatter.string(from: Date())
        
        let fromDate = AppUtility.getDateFromString(userCreatedDate, currentFormat: "yyyy-MM-dd HH:mm:ss", desiredFormat: "yyyy-MM-dd");
        
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["UserID": userId,
                                           "DateUpto": toDate,
                                           "DateFrom" : fromDate,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicatorOnView(VC: self);
            Alamofire.request(kSERVICE_GET_WALLET_TRANSECTION, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicatorOnView(VC: self);
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("TransctionJSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    let status = (dataDict?.object(forKey: "Status"))! as? String
                    
                    if(status == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200)
                    {
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        
                        self.transactionHistoryArray.removeAll();
                        
                        if(dictArray.count > 0)
                        {
                            for item in dictArray {
                                let obj = TransactionHistory(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>)
                                self.transactionHistoryArray.append(obj);
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.transactionTblView.reloadData();
                        }
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                    }
                }
            }
            )
        }
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.transactionHistoryArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        // Configure the cell...
        configureCell(tblCell: cell, indexPath: indexPath);
        return cell
    }
    
    
    func configureCell(tblCell: UITableViewCell, indexPath: IndexPath){
        
        tblCell.backgroundColor = UIColor.clear
        let obj = self.transactionHistoryArray[indexPath.row];

        let dateAndDescription = tblCell.contentView.viewWithTag(1386) as! UILabel;
        let amountLbl = tblCell.contentView.viewWithTag(1387) as! UILabel;
        
        var desc = "";
        let transDate = AppUtility.getDateFromString(obj.TransactionDate!, currentFormat: "yyyy-MM-dd HH:mm:ss", desiredFormat: "dd MMM yyyy")
        
        if(obj.TransactionType != nil && obj.TransactionType! == "Withdraw"){
            desc = obj.TransactionType! ;
        }
        else{
            desc = obj.Narration!;
            desc = desc.replacingOccurrences(of: "1 coupons", with: "1 coupon")
            
            if(desc == ""){
                desc = obj.DrCrFlag!;
            }
            else
            {
                let descNString = desc as NSString
                let loc = descNString.range(of: ": ").location
                if(loc > 0){
                    let array = descNString.components(separatedBy: ": ");
                    desc = array[1];
                }
            }
        }
        
        dateAndDescription.text = String(format: "%@        %@", transDate, desc)
        amountLbl.text = String(format: "%@ %.2f", (obj.CurrencyAbrrevation!) , Double((obj.Amount)!)!)
    }
    
    @IBAction func loadBtnTapped(_ sender: Any) {
        
        self.enterAmountTxtF.resignFirstResponder();
        let enteredAmount = self.enterAmountTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let currencyCode = self.amountCurrencyCode.text;
        
        self.enterAmountTxtF.text = enteredAmount;
        
        if(enteredAmount.characters.count < 1 || Double(enteredAmount)! <= 0){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Please enter amount to load credit", onView: self);
        }
        else {
            let defaults: UserDefaults = UserDefaults.standard
            let nsDataUser = defaults.object(forKey: "UserData") as! Data
            let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
            
            if(user.CurrencyCode != nil && user.CurrencyCode! == "INR"){

                let objCCAvenue = CCAvenueWebViewController()
                objCCAvenue.amount = enteredAmount;
                //objCCAvenue.amount = "1";
                objCCAvenue.currency = currencyCode;
                objCCAvenue.ccAvenueDelegate = self;
                self.present(objCCAvenue, animated: true, completion: nil);
            }
            else{
                let objCCAvenue = CCAvenueWebViewController()
                objCCAvenue.amount = enteredAmount;
                //objCCAvenue.amount = "1";
                objCCAvenue.currency = currencyCode;
                objCCAvenue.ccAvenueDelegate = self;
                self.present(objCCAvenue, animated: true, completion: nil);
                
                //AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Load amount feature is in progress..!", onView: self);
                //self.callPayFortsApi(amount: enteredAmount, currency: currencyCode!);
            }
        }
    }
    
    
    func remove(_ objCCAvenue: CCAvenueWebViewController!, withResponse returnData: [AnyHashable : Any]!) {
        
        self.dismiss(animated: true, completion: nil);
        
        if(returnData != nil)
        {
            print("ccavenueData = %@", returnData);
            if(returnData.count > 0)
            {
                let statusTitle = returnData["order_status"] as! String;
                if(statusTitle == "Success")
                {
                    
                    let defaults: UserDefaults = UserDefaults.standard
                    let nsDataUser = defaults.object(forKey: "UserData") as! Data
                    let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
                    let userId = user.ID!;
                    
                    self.ccAvenuePaymentReturnData = returnData as NSDictionary?;
                    
                    let bankRefNo = self.ccAvenuePaymentReturnData?.value(forKey: "bank_ref_no") as! String;
                    let trackingId = self.ccAvenuePaymentReturnData?.value(forKey: "tracking_id") as! String;
                    let orderId = self.ccAvenuePaymentReturnData?.value(forKey: "order_id") as! String;
                    let planAmount = self.ccAvenuePaymentReturnData?.value(forKey: "amount") as! String;
                    let currencyCode = user.CurrencyCode!;
                    
                    let deviceUniqueId = AppUtility.getDeviceNSUUID();
                    // Created postDataDictonary to post the data on server
                    let postDataDict : NSDictionary = ["UserID": userId,
                                                       "Amount": planAmount,
                                                       "CurrencyAbrrevation": currencyCode,
                                                       "TransactionType": "ccavenue",
                                                       "OrderID": orderId,
                                                       "TrackingID": trackingId,
                                                       "TransactionID": bankRefNo,
                                                       "PaymentId": "",
                                                       "DeviceUniqueID": deviceUniqueId,
                                                       "api_key": kWEB_SERVICES_API_KEY];
                    
                    self.uploadLoadedAmountInWallet(postDataDict);
                }
            }
        }
    }
    
    func callPayFortsApi(amount: String, currency: String){
        
        AppUtility.removedBackTitleAndChangeItsColor(VC: self.parent!, backTitle: "", backTintColor: UIColor.white);
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "PayFortPaymentVC") as? PayFortPaymentVC
        objVC?.amountToPay = amount;
        objVC?.amountCurrency = currency;
        objVC?.callBackOnPreviousView = { flag in
            
            self.isNeedToReloadViewOnAppear = false;
        }
        self.present(objVC!, animated: true, completion: nil);
    }
    
    func uploadLoadedAmountInWallet(_ postDataDict: NSDictionary)
    {
        print("postDataDict = %", postDataDict);
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicatorOnView(VC: self);
            Alamofire.request(kSERVICE_USER_LOAD_CREDIT, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                CustomProgressView.hideProgressIndicatorOnView(VC: self);
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    let status = (dataDict?.object(forKey: "Status"))! as? String
                    
                    if(Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 100)
                    {
                        // Refunded code will come here
                    }
                    else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                        
                        let message = (dataDict?.object(forKey: "Message"))! as! String;
                        AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                    }
                    else
                    {
                        let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                        AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message as String, onView: self)
                    }
                }
            }
            )
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (self.enterAmountTxtF == textField) {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "AmountSelectionVC") as! AmountSelectionVC
            
            objVC.amountCallBack = { amount in
                self.enterAmountTxtF.text = amount;
            }
            objVC.view.frame = self.view.bounds;
            self.view.addSubview(objVC.view)
            self.addChildViewController(objVC)
            objVC.didMove(toParentViewController: self);
            
            return false;
        }
        return true;
    }
    @IBAction func withdrawalBtnTapped(_ sender: Any) {

        self.withdrawalAmountTxtF.resignFirstResponder();
        let withdrawalAmount = self.withdrawalAmountTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let currencyCode = self.withdrawalAmountCurrencyCode.text;
        
        self.withdrawalAmountTxtF.text = withdrawalAmount;
        
        let mininumWithDrawValue: Double = 5.0
        
        var message: String? = nil;
        
        if(withdrawalAmount.characters.count < 1 || Double(withdrawalAmount)! <= 0){
            message = "Please enter valid withdrawal amount."
        }
        else if(Double((self.objAvailableCredit?.TotalBalance!)!)! <= 0.0 && Double(withdrawalAmount)! > 0.0){
             message = "You does not have sufficient balance to withdrawal"
        }
        else if(Double(withdrawalAmount)! > Double((self.objAvailableCredit?.TotalBalance!)!)!){
            message = String(format: "You can not withdrawal amount more than  %@ %.2f", currencyCode!, Double((self.objAvailableCredit?.TotalBalance!)!)!)
        }
        else if(Double(withdrawalAmount)! < mininumWithDrawValue){
            message = String(format: "You can not withdrawal less than %@ %.2f", currencyCode!, mininumWithDrawValue)
        }
        
        if (message != nil) {
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self);
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyboard.instantiateViewController(withIdentifier: "BankAccountInfoVC") as! BankAccountInfoVC
            objVC.withdrawalAmount = withdrawalAmount;
            objVC.currencyCode = currencyCode;
            objVC.callBackNotToReloadView = { flagToRelaodView in
                self.isNeedToReloadViewOnAppear = flagToRelaodView;
            }
            self.present(objVC, animated: true, completion: nil);
        }
    }
    
    @IBAction func withdrawalHistoryBtnTapped(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyboard.instantiateViewController(withIdentifier: "WithdrawalHistoryVC") as! WithdrawalHistoryVC
        objVC.callBackNotToReloadView = { flagToRelaodView in
            self.isNeedToReloadViewOnAppear = flagToRelaodView;
        }
        self.present(objVC, animated: true, completion: nil);

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func hideChildView(){
        
        for vc in self.childViewControllers{
            vc.view.removeFromSuperview()
            vc.removeFromParentViewController()
            vc.willMove(toParentViewController: nil)
        }
    }

}
