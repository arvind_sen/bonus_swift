//
//  AddEditCouponVC.swift
//  Bonus
//
//  Created by Arvind Sen on 29/12/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit
import Alamofire

enum TypeOfCoupons{
    case kPercentOffTypeCoupon
    case kBuyGetFreeCoupon
}

class AddEditCouponVC: UIViewController, CustomPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var isTypeOFCoupon: TypeOfCoupons = .kBuyGetFreeCoupon;
    @IBOutlet weak var topHereLbl: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var activeInactiveView: UIView!
    
    @IBOutlet weak var switchBtn: UISwitch!
    @IBOutlet weak var activeInactiveStatusLbl: UILabel!
    
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var businessImgV: UIImageView!
    @IBOutlet weak var couponImgV: UIImageView!
    @IBOutlet weak var checkMarkOne: UIButton!
    @IBOutlet weak var getTxtF: UITextField!
    @IBOutlet weak var freeTxtF: UITextField!
    @IBOutlet weak var checkMarkTwo: UIButton!
    @IBOutlet weak var offTxtF: UITextField!
    @IBOutlet weak var couponDetailTxtV: UITextView!
    @IBOutlet weak var numberOfCouponsTxtF: UITextField!
    @IBOutlet weak var perCustomerCouponsTxtF: UITextField!
    @IBOutlet weak var offerStartDateTxtF: UITextField!
    @IBOutlet weak var offerEndDateTxtF: UITextField!
    @IBOutlet weak var redemptionExpiryDateTxtF: UITextField!
    @IBOutlet weak var branchesTVContainerV: UIView!
    @IBOutlet weak var selectBranchesBtn: UIButton!
    
    var branchesListArray: NSMutableArray!
    var focusedControl: Any!
    @IBOutlet weak var branchesTableView: UITableView!
    @IBOutlet weak var branchTVContainerHeight: NSLayoutConstraint!
    
    var branchTVContainerDefaultHeight: CGFloat = 0.0;
    var isBranchesOpen = false;
    
    var scrollViewInitialEdgeInsets: UIEdgeInsets!
    var scrollViewInitialContentOffset: CGPoint!
    
    var myCouponToEdit : MyCoupon? = nil;
    var isAllBranchesSelected = false;
    var customPickerView : CustomPickerView? = nil;
    
    var imageCropperView: UIView!
    var cropper : BFCropInterface!
    var croppedImageToUpload: UIImage!
    var displayOriginalImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        self.navigationController?.navigationBar.barTintColor = TAB_BAR_BG_COLOR;
        self.navigationController?.isNavigationBarHidden = false;
        
        self.title = "Add Coupon";
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white];
        self.addSaveButton();
        self.getAllBranchDetail(0);
        self.configureInputAccesseryViewForInputFields();
        
        // Set business image and name
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        self.businessImgV.sd_setImage(with: URL(string: (user.BusinessLogoImagePath!)), placeholderImage: K_PLACE_HOLDER_IMAGE)
        self.businessName.text = user.FirstName;
        self.couponImgV.image = K_DEAL_PLACE_HOLDER_IMAGE
        
        
        self.branchesTableView.separatorColor = GRAY_CUSTOM_BAR_AND_SEPRATOR_BG_COLOR
        self.branchesTableView.separatorStyle = .none;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // Do things before loading the VC
        NotificationCenter.default.addObserver(self, selector: #selector(BusinessAccountVC.keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BusinessAccountVC.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if (myCouponToEdit != nil) {
            topHereLbl.isHidden = true;
            self.setDisplayDataToEdit()
        }
        else{
            topHereLbl.isHidden = true;
        }
    }
    // MARK:- Keyboard Appearance delegate
    func keyboardWasShown(_ aNotification: Notification){
        
        let info:NSDictionary =  aNotification.userInfo! as NSDictionary;
        //let kbSize = (info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue.size;
        var kbSize = (info.object(forKey: UIKeyboardFrameBeginUserInfoKey) as AnyObject).cgRectValue.size;
        if(kbSize.height <= 0.0){
            kbSize = (info.object(forKey: UIKeyboardFrameEndUserInfoKey) as AnyObject).cgRectValue.size;
        }
        
        let contentInsets = UIEdgeInsetsMake(0, 0.0, kbSize.height, 0.0);
        scrollView.contentInset = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        var aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!aRect.contains((self.focusedControl! as AnyObject).frame.origin) ) {
            let scrollPoint = CGPoint(x: 0.0, y: (focusedControl! as AnyObject).frame.origin.y-kbSize.height);
            self.scrollView.setContentOffset(scrollPoint, animated: true);
        }
    }
    
    func keyboardWillBeHidden(_ aNotification:Notification){
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            //let contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
            //self.scrollView.contentInset = contentInsets;
            self.scrollView.contentInset = self.scrollViewInitialEdgeInsets;
            //self.scrollView.scrollIndicatorInsets = contentInsets;
            // [scrollView setContentOffset:CGPointZero animated:YES];
        }) 
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true);
        //let contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width, height: 1200);
        scrollViewInitialEdgeInsets = self.scrollView.contentInset;
        self.scrollViewInitialContentOffset = self.scrollView.contentOffset;
    }
    
    func configureInputAccesseryViewForInputFields(){
        //Configured in viewDidLoad()
        var accessoryDoneButton: UIBarButtonItem!
        let accessoryToolBar = UIToolbar(frame: CGRect(x : 0, y : 0, width : UIScreen.main.bounds.width, height : 44))
        accessoryDoneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.accessoryViewDoneBtnPressed(_:)))
        let accessroySpace = UIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil);
        accessoryDoneButton.tintColor = UIColor.darkGray;
        accessoryToolBar.items = [accessroySpace, accessoryDoneButton]
        
        self.getTxtF.inputAccessoryView = accessoryToolBar
        self.freeTxtF.inputAccessoryView = accessoryToolBar
        self.offTxtF.inputAccessoryView = accessoryToolBar;
        
        self.numberOfCouponsTxtF.inputAccessoryView = accessoryToolBar
        self.perCustomerCouponsTxtF.inputAccessoryView = accessoryToolBar
        self.couponDetailTxtV.inputAccessoryView = accessoryToolBar;
    }
    
    func accessoryViewDoneBtnPressed(_ sender: UIButton){
        
        self.getTxtF.resignFirstResponder();
        self.freeTxtF.resignFirstResponder();
        self.offTxtF.resignFirstResponder();

        self.numberOfCouponsTxtF.resignFirstResponder();
        self.perCustomerCouponsTxtF.resignFirstResponder();
        self.couponDetailTxtV.resignFirstResponder();
    }
    
    func addSaveButton(){
        
        let saveBtn = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(AddEditCouponVC.addEditBtnTapped));
        
        saveBtn.tintColor = UIColor.white;
        self.navigationItem.rightBarButtonItem = saveBtn;
        
        self.branchesTVContainerV.layer.cornerRadius = 5.0;
        self.branchesTVContainerV.layer.borderColor = UIColor.lightGray.cgColor;
        self.branchesTVContainerV.layer.borderWidth = 1.0;
        
        self.couponDetailTxtV.layer.cornerRadius = 5.0;
        self.couponDetailTxtV.layer.borderColor = UIColor.lightGray.cgColor;
        self.couponDetailTxtV.layer.borderWidth = 1.0;
        
        branchTVContainerDefaultHeight = self.branchTVContainerHeight.constant;
        self.branchTVContainerHeight.constant = self.selectBranchesBtn.frame.size.height;
    }
    
    func addEditBtnTapped() {
        
        var activeBtnFlag = "0";
        if (self.switchBtn.isOn == true) {
            activeBtnFlag = "1";
        }
        
        // Resign from responder
        
        let buyFirstTxt : String = self.getTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let buySecondTxt : String = self.freeTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        
        let offPercentTxt : String = self.offTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let couponsDescription : String = self.couponDetailTxtV.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let totalNumberOfCoupon : String = self.numberOfCouponsTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let couponsPerCustomer : String = self.perCustomerCouponsTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        var offerStartDate : String = self.offerStartDateTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        var offerEndDate : String = self.offerEndDateTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        var redemptionExpiryDate : String = self.redemptionExpiryDateTxtF.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);

        // Check for atleast one branch selected or not
        var branchFlag = false;
        for item in self.branchesListArray {
            let branch = item as! Branch
            if (branch.isSelected == 1) {
                branchFlag = true;
                break;
            }
        }

        var message: String? = nil;
        var dateComparisionMessage: String? = nil;
        
        if ((isTypeOFCoupon == TypeOfCoupons.kPercentOffTypeCoupon) && ((offPercentTxt.characters.count) <= 0)) {
            message = "Please enter % value for selected type of coupon";
        }
        else if ((isTypeOFCoupon == TypeOfCoupons.kBuyGetFreeCoupon) && ((buyFirstTxt.characters.count) <= 0) && ((buySecondTxt.characters.count) <= 0)) {
            message = "Please enter values for selected type of coupon";
        }
        else if (couponsDescription.characters.count <= 0)
        {
            message = "Please enter a Coupon Description, i.e. Buy one DESSERT and get another DESSERT of equal or lower value FREE of Charge.";
        }
        else if (totalNumberOfCoupon.characters.count <= 0)
        {
            message = "Please enter valid number";
        }
        else if (Int(totalNumberOfCoupon)! <= 0)
        {
            message = "Please enter valid number";
        }
        else if (couponsPerCustomer.characters.count <= 0)
        {
            message = "Plese enter valid number of coupons each customer will be allowed to buy.";
        }
        else if (Int(couponsPerCustomer)! <= 0)
        {
            message = "Number of Coupons for each Customer will always greater than 0";
        }
        else if(Int(totalNumberOfCoupon)! < Int(couponsPerCustomer)!)
        {
            message = "Please ensure that the number of coupons per customer does not exceed the total number of available coupons.";
        }
        else if (offerStartDate.characters.count <= 0)
        {
            message = "Please select a valid Offer Start Date. Make sure Offer Start Date is smaller than or equal to Offer End Date and greater than or equal to Current Date. OR Offer end date cannot be less than its start date.";
        }
        else if (offerEndDate.characters.count <= 0)
        {
            message = "Please select a valid Offer End Date. Make sure Offer End Date is smaller than or equal to Redemption Expiry Date and greater than or equal to Offer Start Date.";
        }
        else if (redemptionExpiryDate.characters.count <= 0)
        {
            message = "Please select a valid Redemption Expiry Date. Make sure RedemptionExpiry Date is greater than or equal to Offer End Date.";
        }
        else if (!branchFlag)
        {
            message = "Please select at least one Branch.";
        }
        
        if(message == nil)
        {
            let dateFormatter = DateFormatter();
            dateFormatter.dateFormat = kDATE_FORMAT;
            
            let startDate: Date = dateFormatter.date(from: offerStartDate)!
            let endDate: Date = dateFormatter.date(from: offerEndDate)!
            let redeemptionDate: Date = dateFormatter.date(from: redemptionExpiryDate)!
            
            let strDate = dateFormatter.string(from: Date())
            let currentDate = dateFormatter.date(from: strDate);
            
            
            // This condition will execute when a business trying to edit a coupon
            if (myCouponToEdit != nil)
            {
                let redeExpDate = AppUtility.getDateFromString(redemptionExpiryDate, currentFormat: kDATE_FORMAT, desiredFormat: kSTANDARD_DATE_FORMAT);
                let starDate = AppUtility.getDateFromString(offerStartDate, currentFormat: kDATE_FORMAT, desiredFormat: kSTANDARD_DATE_FORMAT);
                let enDate = AppUtility.getDateFromString(offerEndDate, currentFormat: kDATE_FORMAT, desiredFormat: kSTANDARD_DATE_FORMAT);
                
                let previousPromoDate = AppUtility.getDateFromString((myCouponToEdit?.PromotionEndDate!)!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kSTANDARD_DATE_FORMAT);
                
                let previousStartDate = AppUtility.getDateFromString((myCouponToEdit?.StartDate!)!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kSTANDARD_DATE_FORMAT);
                
                let previousEndDate = AppUtility.getDateFromString((myCouponToEdit?.EndDate!)!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kSTANDARD_DATE_FORMAT);
                
                // Coupons redemption date should not able to decrease.
                var isRedemptionDateEarlier = false;
                if (redeExpDate.compare(previousPromoDate) == .orderedAscending) { // Redemption date reduced
                    print("redeExpDate is earlier than previousPromoDate");
                    isRedemptionDateEarlier = true;
                }
                
                if (Int((myCouponToEdit?.TotalActiveCoupan!)!)! > 0 && isRedemptionDateEarlier == true) {
                    dateComparisionMessage = "Date cannot be reduced, you still have some coupons active.";
                }
                
                // Coupons offer start date should not able to increase.
                var isStartDateEarlier = false;
                if (previousStartDate.compare(starDate) == .orderedAscending) {
                    print("previousStartDate is earlier than starDate");
                    isStartDateEarlier = true;
                }
                
                if (Int((myCouponToEdit?.TotalActiveCoupan!)!)! > 0 && isStartDateEarlier == true) {
                    dateComparisionMessage = "Date cannot be reduced, you still have some coupons active.";
                }
                
                // Coupons offer end date should not able to decrease.
                var isEndDateEarlier = false;
                if (enDate.compare(previousEndDate) == .orderedAscending) {
                    print("enDate date is earlier than previousEndDate");
                    isEndDateEarlier = true;
                }
                
                if (Int((myCouponToEdit?.TotalActiveCoupan!)!)! > 0 && isEndDateEarlier == true) {
                    dateComparisionMessage = "Date cannot be reduced, you still have some coupons active.";
                }
                
                if (endDate.compare(redeemptionDate) == .orderedDescending)
                {
                    dateComparisionMessage = "Coupons redemption date can not be less than it's end date";
                }
        
                if (Int((myCouponToEdit?.TotalActiveCoupan!)!)! > 0 && (Int(totalNumberOfCoupon)! < Int((myCouponToEdit?.TotalNoOfDeal!)!)!)) {
                    dateComparisionMessage = "Total number of coupons can not be reduce, you still have some coupons active.";
                }

                if (Int((myCouponToEdit?.TotalActiveCoupan!)!)! > 0 && (Int(couponsPerCustomer)! < Int((myCouponToEdit?.DealPerCustomer!)!)!)) {
                    dateComparisionMessage = "Coupons per customer can not be reduce, you still have some coupons active.";
                }
            }
            else
            {
                if (startDate.compare(endDate) == .orderedDescending)
                {
                    dateComparisionMessage = "Offer end date can not be less than it's start date";
                }
                if (currentDate?.compare(endDate) == .orderedDescending)
                {
                    dateComparisionMessage = "Offer end date can not be less than from current date";
                }
                if (endDate.compare(redeemptionDate) == .orderedDescending)
                {
                    dateComparisionMessage = "Coupons redeemption date can not be less than it's end date";
                }
            }
            
            let remainingDaysString = AppUtility.daysSinceDateFrom(startDate, dateTo: redeemptionDate, currentDateFormat: kDATE_FORMAT);
            
            if (Int(remainingDaysString)! > 180) { // 180 Days are the validation on coupon redeemption.
                dateComparisionMessage = "Please make sure Redemption Expiry Date is not greater than 180 days.";
            }
            
            message = dateComparisionMessage;
        }
    
        if (message != nil)
        {
            AppUtility.showAlertWithTitleAndMessage(message, alertMessage: nil, onView: self);
        }
        else
        {
            var typeOfCoupon: String? = nil;
            
            if (isTypeOFCoupon == TypeOfCoupons.kBuyGetFreeCoupon) {
                typeOfCoupon = ("Buy \(buyFirstTxt) Get \(buySecondTxt) Free")
            }
            else {
                typeOfCoupon = ("\(offPercentTxt)% Off");
            }
            
            offerStartDate = AppUtility.getDateFromString(offerStartDate, currentFormat: kDATE_FORMAT, desiredFormat: kSTANDARD_DATE_FORMAT);
            offerStartDate = ("\(offerStartDate) 00:00:00")
                
            offerEndDate = AppUtility.getDateFromString(offerEndDate, currentFormat: kDATE_FORMAT, desiredFormat: kSTANDARD_DATE_FORMAT);
            offerEndDate = ("\(offerEndDate) 23:59:59")
            
            redemptionExpiryDate = AppUtility.getDateFromString(redemptionExpiryDate, currentFormat: kDATE_FORMAT, desiredFormat: kSTANDARD_DATE_FORMAT);
            redemptionExpiryDate = ("\(redemptionExpiryDate) 23:59:59")
            
            var pictureDataString = "";
            // Profile picture uploading
            if (self.croppedImageToUpload != nil)
            {
                let pictureData = UIImagePNGRepresentation(self.croppedImageToUpload);
                pictureDataString = (pictureData?.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))!;
            }
            
            var nsmutableString = "";
            
            for item in self.branchesListArray {
                let branch = item as! Branch;
                
                if (branch.isSelected == 1) {
                    if (nsmutableString.characters.count == 0) {
                        nsmutableString = ("\(branch.ID!)")
                    }
                    else{
                        nsmutableString = ("\(nsmutableString),\(branch.ID!)")
                    }
                }
            }
        
            var branchSpecific = "1";
            if (self.isAllBranchesSelected == true) {
                branchSpecific = "0";
            }
            let deviceUniqueId = AppUtility.getDeviceNSUUID();
            var branchToEdit = "0";
            if (self.myCouponToEdit != nil) {
                branchToEdit = (self.myCouponToEdit?.DealMasterID)!;
            }
            
            let defaults: UserDefaults = UserDefaults.standard
            let nsDataUser = defaults.object(forKey: "UserData") as! Data
            let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;

            // Created postDataDictonary to post the data on server
            let postDataDict : NSDictionary = ["IsActive" : activeBtnFlag,
                                               "TypeOfCoupon" : typeOfCoupon!,
                                               "TotalNoOfDeal" : totalNumberOfCoupon,
                                               "DealPerCustomer": couponsPerCustomer,
                                               "StartDate" : offerStartDate,
                                               "EndDate" : offerEndDate,
                                               "RedemptionEndDate":redemptionExpiryDate,
                                               "Description" : couponsDescription,
                                               "DealImage1" : pictureDataString,
                                               "BranchSpecific" : branchSpecific,
                                               "Branches" : nsmutableString,
                                               "UserID" : user.ID!, // Branch Id
                                                "CategoryID" : user.BusinessCategory!,
                                                "ID" : branchToEdit,
                                                "api_key" : kWEB_SERVICES_API_KEY,
                                                "DeviceUniqueID" : deviceUniqueId
            ];
            
            print("postData = %@", postDataDict);
            
            if !AppUtility.isNetworkAvailable(){
                AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
            }
            else{
                
                CustomProgressView.showProgressIndicatorOnView(VC: self);
                Alamofire.request(kSERVICE_SAVE_COUPON_DETAILS_FOR_A_BUSINESS, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                    
                    CustomProgressView.hideProgressIndicatorOnView(VC: self);
                    
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                    print("value =  \(datastring)");
                    
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                        let dataDict = JSON as? NSDictionary;
                        
                        if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                            
                            let message = ((dataDict?.object(forKey: "Message"))! as! String) as NSString;
                            
                            let alert = UIAlertController(title: nil, message: message as String, preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                            
                                if(self.myCouponToEdit != nil) {
                                    let VCNumber = ((self.navigationController?.viewControllers.count)! - 3);
                                    let vc = self.navigationController?.viewControllers[VCNumber]
                                    _ = self.navigationController?.popToViewController(vc!, animated: true);
                                }
                                else{
                                    _ = self.navigationController?.popViewController(animated: true);
                                }
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else if((dataDict?.object(forKey: "Status"))! as! String == "Fail" && (dataDict?.object(forKey: "StatusCode"))! as? Int == 500){
                            
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            AppUtility.userLogoutForCurrentSession(alertMessage: message, VC: self);
                        }
                        else
                        {
                            let message = (dataDict?.object(forKey: "Message"))! as! String;
                            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: message, onView: self)
                        }
                    }
                }
                )
            }
        }
    }
    
    func getAllBranchDetail(_ offset: Int)
    {
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        var userId = user.ID;
        
        if(user.PersonType != nil && user.PersonType == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE) {
            userId = user.BusinessUserID;
        }
        
        let personType = user.PersonType;
        let deviceUniqueId = AppUtility.getDeviceNSUUID();
        
        // Created postDataDictonary to post the data on server
        let postDataDict : NSDictionary = ["UserID": userId!,
                                           "PersonType": personType!,
                                           "DeviceUniqueID": deviceUniqueId,
                                           "api_key": kWEB_SERVICES_API_KEY];
        
        
        if !AppUtility.isNetworkAvailable(){
            AppUtility.showAlertWithTitleAndMessage(nil, alertMessage: "Internet not available", onView: self)
        }
        else{
            
            CustomProgressView.showProgressIndicatorOnView(VC: self);
            
            Alamofire.request(kSERVICE_BRANCH_LIST_OF_A_BUSINESS, method: HTTPMethod.post, parameters: postDataDict as? [String : Any], encoding: JSONEncoding.default, headers: ["Accept": "application/json"]).responseJSON(completionHandler: { (response) in
                
                CustomProgressView.hideProgressIndicatorOnView(VC: self);
                
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue);
                print("value =  \(datastring)");
                
                if let JSON = response.result.value
                {
                    print("JSON: \(JSON)")
                    let dataDict = JSON as? NSDictionary;
                    
                    if((dataDict?.object(forKey: "Status"))! as! String == "Success" && Int((dataDict?.object(forKey: "StatusCode"))! as! String)! == 200) {
                        
                        let dictArray = dataDict?.object(forKey: "Response") as! NSArray
                        
                        
                        if(offset == 0){
                            self.branchesListArray = nil;
                            self.branchesListArray = NSMutableArray();
                        }
                        
                        if(dictArray.count > 0)
                        {
                            var counter = 0;
                            var selectedBranches: [Branch]? = nil;
                            if(self.myCouponToEdit != nil)
                            {
                                selectedBranches = self.myCouponToEdit?.BranchesList;
                            }

                            for item in dictArray {
                                let branch = Branch(data: (item as AnyObject).object(forKey: "output") as! Dictionary<String, AnyObject>)
                                
                                if(self.myCouponToEdit != nil)
                                {
                                    for item in selectedBranches! {
                                        let branc = item as Branch;
                                        if(branc.ID == branch.ID){
                                            branch.isSelected = 1;
                                            counter = counter + 1;
                                            break;
                                        }
                                    }
                                }
                                self.branchesListArray.add(branch);
                            }
                            
                            if(self.myCouponToEdit != nil && (counter == self.branchesListArray.count)){
                                self.isAllBranchesSelected = true;
                            }
                            
                            self.branchesTableView.reloadData();
                        }
                    }
                }
                }
            )
        }
    }

    // MARK: - Table view data source
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // #warning Incomplete implementation, return the number of rows
        var returnValue = 0;
        
        if self.branchesListArray != nil {
            returnValue = self.branchesListArray!.count + 1;
        }
        return returnValue;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reusableCell", for: indexPath) as UITableViewCell
        cell.backgroundColor = UIColor.clear
        
        // Configure the cell...
        self.configureTableViewCell(cell, forRowAtIndexPath: indexPath);
        return cell
    }
    
    // This below method is setting different values for a row
    func configureTableViewCell(_ cell: UITableViewCell, forRowAtIndexPath:IndexPath){
        
        cell.backgroundColor = UIColor.clear;
        
        
        let imageView = cell.contentView.viewWithTag(15) as! UIImageView;
        imageView.image = K_RIGHT_YELLOW_EMPTY;
        let allLbl = cell.contentView.viewWithTag(10) as! UILabel;
        let nameLbl = cell.contentView.viewWithTag(20) as! UILabel;
        let areaLbl = cell.contentView.viewWithTag(25) as! UILabel;
        allLbl.isHidden = true;
        areaLbl.isHidden = false;
        nameLbl.isHidden = false;
        
        if (forRowAtIndexPath.row == 0) {

            allLbl.isHidden = false;
            nameLbl.isHidden = true;
            areaLbl.isHidden = true;
            
            if (isAllBranchesSelected == true) {
                imageView.image = K_RIGHT_YELLOW;
            }

        }
        else{
            let obj = branchesListArray[forRowAtIndexPath.row - 1] as! Branch;
            
            if (obj.isSelected == 1) {
                imageView.image = K_RIGHT_YELLOW;
            }
            nameLbl.text = obj.BranchDescription;
            areaLbl.text = obj.AreaName;
            
        }
//        cell.imageView?.image = K_RIGHT_YELLOW_EMPTY;
//        cell.textLabel?.text = obj.BranchDescription;
//        cell.textLabel?.font = UIFont.systemFontOfSize(12.0);
        
        let seprator = UIView(frame: CGRect(x: 0.0, y: (cell.frame.height - 1.0), width: cell.frame.width, height: 1.0))
        seprator.backgroundColor = K_EMPTY_TABLE_BG_COLOR;
        cell.addSubview(seprator);
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        
        if (indexPath.row == 0) {
            
            if (isAllBranchesSelected == false) {
                isAllBranchesSelected = true;
            }
            else{
                isAllBranchesSelected = false
            }
            
            for item in self.branchesListArray {
                let branch = item as! Branch;
                if (isAllBranchesSelected == true) {
                    branch.isSelected = 1;
                }
                else{
                    branch.isSelected = 0;
                }
            }
        }
        else{
            let obj = self.branchesListArray![indexPath.row - 1] as! Branch;
            if (obj.isSelected == 0) {
                obj.isSelected = 1;
            }
            else{
                obj.isSelected = 0;
            }
            
            var totalSelected = 0;
            for item in self.branchesListArray {
                let branch = item as! Branch;
                if (branch.isSelected == 1) {
                    totalSelected = totalSelected + 1;
                }
            }
            
            if (totalSelected == self.branchesListArray.count) {
                isAllBranchesSelected = true;
            }
            else{
                isAllBranchesSelected = false;
            }
        }
        self.branchesTableView.reloadData();
    }
    
    @IBAction func selectBranchBtnTapped(_ sender: AnyObject) {
        
        if(isBranchesOpen == false){
            isBranchesOpen = true;
            
            UIView.animate(withDuration: 0.3, animations: {
                self.branchTVContainerHeight.constant = self.branchTVContainerDefaultHeight;
                self.selectBranchesBtn.setImage(UIImage(named:"DownArrow"), for: UIControlState());
                self.branchesTVContainerV.frame = CGRect(x: self.branchesTVContainerV.frame.origin.x, y: self.branchesTVContainerV.frame.origin.y, width: self.branchesTVContainerV.frame.size.width, height: self.branchTVContainerHeight.constant)
                }, completion: nil)
        }
        else{
            isBranchesOpen = false;
            UIView.animate(withDuration: 0.3, animations: {
                self.selectBranchesBtn.setImage(UIImage(named:"RightArrow"), for: UIControlState());
                self.branchTVContainerHeight.constant = self.selectBranchesBtn.frame.size.height;
                self.branchesTVContainerV.frame = CGRect(x: self.branchesTVContainerV.frame.origin.x, y: self.branchesTVContainerV.frame.origin.y, width: self.branchesTVContainerV.frame.size.width, height: self.selectBranchesBtn.frame.size.height)
                }, completion: nil)
        }
    }
    
    @IBAction func switchBtnTapped(_ sender: AnyObject) {
        
        let switchBtn = sender as! UISwitch;
        
        if (self.myCouponToEdit != nil && Int((self.myCouponToEdit?.TotalActiveCoupan)!)! > 0) {
            
            let alert = UIAlertController(title: nil, message: "Coupon cannot be deactivated as some issued coupons are not yet redeemed." as String, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                
                self.switchBtn.setOn(true, animated: true);
            }))
            self.present(alert, animated: true, completion: nil)
           
            return;
        }
        
        self.switchButtonChanged(switchBtn: switchBtn);
    }

    func switchButtonChanged(switchBtn: UISwitch){
        
        if(switchBtn.isOn == true){
            self.activeInactiveView.backgroundColor = COLOR_GREEN_BG;
            self.activeInactiveStatusLbl.text = "Active"
        }
        else{
            self.activeInactiveView.backgroundColor = LIGHT_GRAY_COLOR_BG_COLOR;
            self.activeInactiveStatusLbl.text = "Inactive"
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder();
        return true;
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.focusedControl = textView;
        return true;
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.focusedControl = textField;
    }
    // MARK:- TextField delegates
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.focusedControl = textField;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        
        var returnValue = true
        
        if (self.offerStartDateTxtF == textField) {
            returnValue = false;
            if(self.focusedControl != nil)
            {
                (self.focusedControl as AnyObject).resignFirstResponder();
            }
            textField.resignFirstResponder();
            self.displayDatePickerView(tagValue: 700);
        }
        else if (self.offerEndDateTxtF == textField) {
            returnValue = false;
            if(self.focusedControl != nil)
            {
                (self.focusedControl as AnyObject).resignFirstResponder();
            }
            textField.resignFirstResponder();
            self.displayDatePickerView(tagValue: 800);
        }
        else if (self.redemptionExpiryDateTxtF == textField) {
            returnValue = false;
            if(self.focusedControl != nil)
            {
                (self.focusedControl as AnyObject).resignFirstResponder();
            }
            textField.resignFirstResponder();
            self.displayDatePickerView(tagValue: 900);
        }
        self.focusedControl = textField;
        
        return returnValue;
    }
    
    @IBAction func checkMarkBtnTapped(_ sender: AnyObject) {
        
        let btn = sender as! UIButton;
        
        if(btn == checkMarkOne)
        {
            isTypeOFCoupon = .kBuyGetFreeCoupon;
            self.checkMarkOne.setImage(K_RIGHT_YELLOW, for: UIControlState())
            self.getTxtF.isUserInteractionEnabled = true;
            self.freeTxtF.isUserInteractionEnabled = true;
            
            self.checkMarkTwo.setImage(K_RIGHT_YELLOW_EMPTY, for: UIControlState())
            self.offTxtF.isUserInteractionEnabled = false;
            self.offTxtF.text = "";
        }
        else if(btn == checkMarkTwo)
        {
            isTypeOFCoupon = .kPercentOffTypeCoupon;
            self.checkMarkOne.setImage(K_RIGHT_YELLOW_EMPTY, for: UIControlState())
            self.getTxtF.isUserInteractionEnabled = false;
            self.freeTxtF.isUserInteractionEnabled = false;
            self.getTxtF.text = "";
            self.freeTxtF.text = "";
            self.checkMarkTwo.setImage(K_RIGHT_YELLOW, for: UIControlState())
            self.offTxtF.isUserInteractionEnabled = true;
            
        }
    }
    
    // Custom UI Date picker.
    func displayDatePickerView(tagValue: Int)
    {
        if(self.customPickerView == nil)
        {
            self.customPickerView = CustomPickerView(frame: (self.view.window?.frame)!);
            self.view.window?.addSubview(self.customPickerView!);
            self.customPickerView?.pickerViewDelegate = self;
            self.customPickerView?.tag = tagValue;
            self.customPickerView?.showDatePickerView("MinToday");
            
            // Set today default date
            let picker = self.customPickerView?.viewWithTag(108) as! UIDatePicker;
            
            if ((tagValue == 700 && self.offerStartDateTxtF.text?.characters.count == 0) || (tagValue == 800 && self.offerEndDateTxtF.text?.characters.count == 0) || (tagValue == 900 && self.offerEndDateTxtF.text?.characters.count == 0)) {
                self.customPickerView?.datePickerChanged(picker);
            }
            /*
            else if(myCouponToEdit != nil){
                if (tagValue == 700 && (self.offerStartDateTxtF.text?.characters.count)! != 0) {
                    self.customPickerView?.setPreviousEnteredDate(dateString: self.offerStartDateTxtF.text);
                }
                else if (tagValue == 800 && (self.offerEndDateTxtF.text?.characters.count)! != 0) {
                    self.customPickerView?.setPreviousEnteredDate(dateString: self.offerEndDateTxtF.text);
                }
                else if (tagValue == 900 && (self.redemptionExpiryDateTxtF.text?.characters.count)! != 0) {
                    self.customPickerView?.setPreviousEnteredDate(dateString: self.redemptionExpiryDateTxtF.text);
                }
            }
            */
        }
    }
    
    func datePickerViewValueChanged(dateStr: String, pickerView: UIView){
        
        if(pickerView.tag == 700){
            self.offerStartDateTxtF.text = dateStr;
        }
        else if(pickerView.tag == 800){
            self.offerEndDateTxtF.text = dateStr;
        }
        else if(pickerView.tag == 900){
            self.redemptionExpiryDateTxtF.text = dateStr;
        }
    }
    
    func removeCustomPickerView(buttonTitle: String, pickerView: UIView) {
        self.customPickerView = nil;
        
        if (self.myCouponToEdit != nil) {
            
            var dateComparisionMessage: String? = nil;
            
            let offerStartDate = self.offerStartDateTxtF.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            let offerEndDate = self.offerEndDateTxtF.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            let redemptionExpiryDate = self.redemptionExpiryDateTxtF.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
            
            let redeExpDate = AppUtility.getDateFromString(redemptionExpiryDate!, currentFormat: kDATE_FORMAT, desiredFormat: kSTANDARD_DATE_FORMAT);
            let starDate = AppUtility.getDateFromString(offerStartDate!, currentFormat: kDATE_FORMAT, desiredFormat: kSTANDARD_DATE_FORMAT);
            let enDate = AppUtility.getDateFromString(offerEndDate!, currentFormat: kDATE_FORMAT, desiredFormat: kSTANDARD_DATE_FORMAT);
           
            let previousPromoDate = AppUtility.getDateFromString((myCouponToEdit?.PromotionEndDate!)!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kSTANDARD_DATE_FORMAT);
            
            let previousStartDate = AppUtility.getDateFromString((myCouponToEdit?.StartDate!)!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kSTANDARD_DATE_FORMAT);
            
            let previousEndDate = AppUtility.getDateFromString((myCouponToEdit?.EndDate!)!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kSTANDARD_DATE_FORMAT);
            
            
            let keepPromoDate = AppUtility.getDateFromString((myCouponToEdit?.PromotionEndDate!)!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kDATE_FORMAT);
            
            let keepStartDate = AppUtility.getDateFromString((myCouponToEdit?.StartDate!)!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kDATE_FORMAT);
            
            let keepEndDate = AppUtility.getDateFromString((myCouponToEdit?.EndDate!)!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kDATE_FORMAT);
            
            
            // Coupons redemption date should not able to decrease.
            var isRedemptionDateEarlier = false;
            if (redeExpDate.compare(previousPromoDate) == .orderedAscending) {
                print("redeExpDate is earlier than previousPromoDate");
                isRedemptionDateEarlier = true;
            }
            
            if (Int((myCouponToEdit?.TotalActiveCoupan!)!)! > 0 && isRedemptionDateEarlier == true) {
                dateComparisionMessage = "Date can not be reduced, you still have some coupons active.";
                self.redemptionExpiryDateTxtF.text = keepPromoDate;
            }
            
            // Coupons offer start date should not able to increase.
            var isStartDateEarlier = false;
            if (previousStartDate.compare(starDate) == .orderedAscending) {
                print("previousStartDate is earlier than starDate");
                isStartDateEarlier = true;
            }
            
            if (Int((myCouponToEdit?.TotalActiveCoupan!)!)! > 0 && isStartDateEarlier == true) {
                dateComparisionMessage = "Date can not be reduced, you still have some coupons active.";
                self.offerStartDateTxtF.text = keepStartDate;
            }
            
            // Coupons offer end date should not able to decrease.
            var isEndDateEarlier = false;
            if (enDate.compare(previousEndDate) == .orderedAscending) {
                print("enDate date is earlier than previousEndDate");
                isEndDateEarlier = true;
            }
            
            if (Int((myCouponToEdit?.TotalActiveCoupan!)!)! > 0 && isEndDateEarlier == true) {
                dateComparisionMessage = "Date can not be reduced, you still have some coupons active.";
                self.offerEndDateTxtF.text = keepEndDate;
            }
            
            if (dateComparisionMessage != nil) {
                AppUtility.showAlertWithTitleAndMessage(dateComparisionMessage!, alertMessage: nil, onView: self)
            }
        }
    }
    
    @IBAction func couponImageTapped(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Choose Media Type", message: "", preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            //println("you have pressed the Cancel button");
        }
        alertController.addAction(cancelAction)
        let photosAction = UIAlertAction(title: "Photos", style: .default) { (action:UIAlertAction!) in
            //println("you have pressed OK button");
            self.imagePickerControllerCallWithSource("Photos")
        }
        alertController.addAction(photosAction)
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action:UIAlertAction!) in
            self.imagePickerControllerCallWithSource("Camera")
        }
        alertController.addAction(cameraAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    func imagePickerControllerCallWithSource(_ source: String?){
        
        let imagPick = UIImagePickerController()
        imagPick.allowsEditing = true
        imagPick.delegate = self
        if (source == "Photos"){
            imagPick.sourceType = .photoLibrary
        }
        else{
            imagPick.sourceType = .camera
        }
        self.present(imagPick, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        self.dismiss(animated: true, completion: nil)
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.showCropperOnView(pickedImage);
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showCropperOnView(_ croppingImage : UIImage){
        
        let oY : CGFloat = 64.0; //status and top navigation bar height
        let heights = self.view.frame.size.height - oY - 44.0
        
        let cropperFrameView = CGRect(x: self.view.frame.origin.x, y: CGFloat(oY), width: self.view.frame.size.width, height: heights);
        //let cropperFrameView = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height);
        
        if (self.imageCropperView != nil) {
            self.imageCropperView = nil;
        }
        
        self.imageCropperView = UIView.init(frame: cropperFrameView)
        self.imageCropperView.backgroundColor = UIColor.black;
        self.view.addSubview(self.imageCropperView);
        
        
        let maxWidth = self.imageCropperView.frame.size.width;
        let maxHeight = maxWidth * 2/3;
        var scaledImage = AppUtility.imageWithImage(croppingImage, scaledToMaxWidth: maxWidth, scaledToMaxHeight: maxHeight)
        var imgWidth = scaledImage.size.width;
        var imgHeight = scaledImage.size.height;
        var imgoX = (cropperFrameView.size.width - imgWidth)/2;
        var imgoY = (cropperFrameView.size.height - imgHeight)/2;
        
        if (croppingImage.size.height > croppingImage.size.width && croppingImage.size.height > (2 * self.imageCropperView.frame.size.height)) {

            imgWidth = cropperFrameView.size.width;
            imgHeight = cropperFrameView.size.height * 0.85;
            imgoX = (cropperFrameView.size.width - imgWidth)/2;
            imgoY = 20;
            scaledImage = croppingImage;
        }
        
        self.displayOriginalImageView = UIImageView.init(frame: CGRect(x: imgoX, y: imgoY, width: imgWidth, height: imgHeight))
        self.displayOriginalImageView.isUserInteractionEnabled = true;
        self.displayOriginalImageView.image = scaledImage;
        self.displayOriginalImageView.contentMode = UIViewContentMode.scaleAspectFit;
        self.imageCropperView.addSubview(self.displayOriginalImageView);
        
        let cancelWidth: CGFloat = 70;
        let cancelHeight: CGFloat = 30;
        let canceloX = (cropperFrameView.size.width/2 - CGFloat(cancelWidth))/2;
        let canceloY = imgoY + imgHeight + 10;
        
        let cropCloseBtn = UIButton(type: UIButtonType.custom)
        cropCloseBtn.frame = CGRect(x: canceloX, y: canceloY, width: cancelWidth, height: cancelHeight);
        cropCloseBtn.setTitle("Cancel", for:UIControlState());
        cropCloseBtn.setTitleColor(UIColor.white, for: UIControlState());
        cropCloseBtn.addTarget(self, action: #selector(BusinessAccountVC.cropperCloseBtnTapped), for: UIControlEvents.touchUpInside);
        self.imageCropperView.addSubview(cropCloseBtn);
        
        let cropBtnWidth = cancelWidth;
        let cropBtnHeight = cancelHeight;
        let cropBtnoX = cropperFrameView.size.width/2 + (cropperFrameView.size.width/2 - cancelWidth)/2;
        let cropBtnoY = canceloY;
        
        let cropBtn = UIButton(type: UIButtonType.custom)
        cropBtn.frame = CGRect(x: cropBtnoX, y: cropBtnoY, width: cropBtnWidth, height: cropBtnHeight);
        cropBtn.setTitle("Crop", for:UIControlState());
        cropBtn.setTitleColor(UIColor.white, for: UIControlState());
        cropBtn.addTarget(self, action: #selector(BusinessAccountVC.cropperCropBtnTapped), for: UIControlEvents.touchUpInside);
        self.imageCropperView.addSubview(cropBtn);
        
        // allocate crop interface with frame and image being cropped
        self.cropper = BFCropInterface.init(frame: self.displayOriginalImageView.bounds, andImage: scaledImage, nodeRadius: 8, cropType: kCroppingTypeRactangle);
        self.cropper.isStaticCopWindow = true;
        
        // this is the default color even if you don't set it
        self.cropper.shadowColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.60);
        self.cropper.borderColor = UIColor.white;
        self.cropper.borderWidth = 1.5;
        self.cropper.showNodes = true;
        
        let croperWidth = self.displayOriginalImageView.frame.size.width;
        let croperHeight = croperWidth * 2/3;
        
        let positionX = (self.displayOriginalImageView.frame.size.width - croperWidth)/2;
        let positionY = (croperWidth - croperHeight)/2;
        self.cropper.setCropViewPosition(positionX, y: positionY, width: croperWidth, height: croperHeight);
        // add interface to superview. here we are covering the main image view.
        self.displayOriginalImageView.addSubview(self.cropper);
    }
    
    func cropperCloseBtnTapped(){
        self.imageCropperView.removeFromSuperview();
        self.imageCropperView = nil;
        self.cropper.removeFromSuperview();
        self.cropper = nil;
    }
    
    func cropperCropBtnTapped(){
        // crop image
        self.topHereLbl.isHidden = true;
        let croppedImage = self.cropper.getCroppedImage()
        self.cropperCloseBtnTapped();
        
        ///self.cropper.removeFromSuperview();
        //self.cropper = nil;
        
        
        let maxWidth = self.view.frame.size.width;
        var maxHeight = maxWidth * 2/3;
        // print("--- %@", croppedImage ?? UIImage);
        if (maxHeight > 500) {
            maxHeight = 500;
        }
        
        var scaledImage = AppUtility.imageWithImage(croppedImage!, scaledToMaxWidth: maxWidth, scaledToMaxHeight: maxHeight);
        print("Final ScalledImage --- %@", scaledImage);
        
        if(scaledImage.size.height < maxHeight) // Fixing again ratios
        {
            let newWidth = maxWidth;
            let newHight = maxHeight;
            scaledImage = AppUtility.imageWithImage(croppedImage!, scaledToMaxWidth: newWidth, scaledToMaxHeight: newHight);
        }
        
        couponImgV.image = scaledImage;
        couponImgV.contentMode = UIViewContentMode.scaleAspectFit;
        self.croppedImageToUpload = scaledImage;
        self.imageCropperView = nil;
    }
    
    
    //MARK:- Method use when edit coupon
    func setDisplayDataToEdit(){
        
        self.title = "Edit Coupon";
        
        if(Int((self.myCouponToEdit?.IsActive!)!)! == 1){
            self.switchBtn.isOn = true;
            self.switchButtonChanged(switchBtn: self.switchBtn);
        }

        self.couponImgV.sd_setImage(with: URL(string: (self.myCouponToEdit?.DealImage1!)!), placeholderImage: K_DEAL_PLACE_HOLDER_IMAGE);
        
        self.businessImgV.sd_setImage(with: URL(string: (self.myCouponToEdit?.BusinessLogoImagePath!)!), placeholderImage: K_PLACE_HOLDER_IMAGE)
        
        let defaults: UserDefaults = UserDefaults.standard
        let nsDataUser = defaults.object(forKey: "UserData") as! Data
        let user = NSKeyedUnarchiver.unarchiveObject(with: nsDataUser) as! User;
        
        self.businessName.text = user.FirstName;
        
        if(self.myCouponToEdit?.TypeOfCoupon != nil)
        {
            let typeOfCoupon = self.myCouponToEdit?.TypeOfCoupon;
            let stringArray = typeOfCoupon?.components(separatedBy: " ")
            
            if((stringArray?.count)! <= 3){
                var offPercent = stringArray?[0]
                offPercent = offPercent?.replacingOccurrences(of: "%", with: "");
                self.offTxtF.text = offPercent!;
                self.checkMarkBtnTapped(self.checkMarkTwo)
            }
            else{
                self.getTxtF.text = stringArray?[1];
                self.freeTxtF.text = stringArray?[3];
                self.checkMarkBtnTapped(self.checkMarkOne)
            }
        }
        
        self.couponDetailTxtV.text = self.myCouponToEdit?.Description?.removingPercentEncoding
        
        self.numberOfCouponsTxtF.text = self.myCouponToEdit?.TotalNoOfDeal!;
        self.perCustomerCouponsTxtF.text = self.myCouponToEdit?.DealPerCustomer!;
        
        let startDate = AppUtility.getDateFromString((self.myCouponToEdit?.StartDate)!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kDATE_FORMAT);
        self.offerStartDateTxtF.text = startDate;
        
        let endDate = AppUtility.getDateFromString((self.myCouponToEdit?.EndDate)!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kDATE_FORMAT);
        self.offerEndDateTxtF.text = endDate;
        
        let redeemedDate = AppUtility.getDateFromString((self.myCouponToEdit?.PromotionEndDate)!, currentFormat: "dd/MM/yyyy HH:mm:ss a", desiredFormat: kDATE_FORMAT);
        self.redemptionExpiryDateTxtF.text = redeemedDate;
        
        self.isBranchesOpen = false;
        self.selectBranchBtnTapped(self.selectBranchesBtn);
        
    }
}
