//
//  AppDelegate.swift
//  Bonus
//
//  Created by Arvind Sen on 22/08/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

import UserNotifications
import UserNotificationsUI
import AudioToolbox

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // Registering app for the push notifications
        //let defaults = NSUserDefaults.standardUserDefaults();
        // PUSH NOTIFICATION
        
        // Textfileds cursor color
        UITextField.appearance().tintColor = ICONS_TINT_COLOR_YELLOW;
        UITextView.appearance().tintColor = ICONS_TINT_COLOR_YELLOW;
        
        let defaults = UserDefaults.standard
        
        if(defaults.object(forKey: "UserData") != nil) {
            self.setUpForCustomerAndVisitorTabs()
        }
        else
        {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let objMainViewController = mainStoryboard.instantiateViewController(withIdentifier: "LaunchController")
            self.window?.rootViewController = objMainViewController
            self.window?.makeKeyAndVisible()
        }
        
        
        let deviceToken = AppUtility.getDeviceToken();
        
        if (deviceToken?.characters.count <= 0) {
            print("There is no deviceToken saved yet.")
            
            
             if #available(iOS 10, *) {
                
                //Notifications get posted to the function (delegate):  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: () -> Void)"
                
                
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                    
                    guard error == nil else {
                        //Display Error.. Handle Error.. etc..
                        return
                    }
                    
                    if granted {
                        //Do stuff here..
                        
                        //Register for RemoteNotifications. Your Remote Notifications can display alerts now :)
                        DispatchQueue.main.async {
                            application.registerForRemoteNotifications()
                        }
                    }
                    else {
                        //Handle user denying permissions..
                    }
                }
                
                //Register for remote notifications.. If permission above is NOT granted, all notifications are delivered silently to AppDelegate.
                application.registerForRemoteNotifications()
            }
            else {
                let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                application.registerUserNotificationSettings(settings)
                application.registerForRemoteNotifications()
            }
        }
        
        return true
    }

    
    /* Application methods to get device token */
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("Got token data! %@",(deviceToken.description))
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print(deviceTokenString)
        
        AppUtility.saveDeviceToken(deviceTokenString);
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("i am not available in simulator \(error)")
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        //print("Notification received %@", userInfo);
        application.applicationIconBadgeNumber = 0;

        let defaults = UserDefaults.standard;
        defaults.setValue(userInfo["aps"], forKey: "pushNotificationData");
        defaults.synchronize();

        print(defaults.value(forKey: "pushNotificationData")!);
        
        if(application.applicationState == .active){
            
            let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                // Your code with delay
                if #available(iOS 10.0, *) {
                    let localNotification = UNUserNotificationCenter.current();
                    
                    let apsDict = userInfo["aps"] as! NSDictionary;
                    let alertDict = apsDict["alert"] as! NSDictionary;
                    let bodyMessage = alertDict.object(forKey: "body") as! String;
                    let titleMessage = alertDict.object(forKey: "title") as! String;
                    
                    let localContent = UNMutableNotificationContent()
                    localContent.body = bodyMessage;
                    localContent.title = titleMessage;
                    localContent.userInfo = userInfo;
                    localContent.sound = UNNotificationSound.default()
                    
                     let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1.0, repeats: false)
                    let request = UNNotificationRequest(identifier: UUID().uuidString, content: localContent, trigger: trigger)
                    localNotification.add(request)
                    
                    var soundID = SystemSoundID()
                    var soundFile = Bundle.main.path(forResource: "coin_flip", ofType: "wav")!
                    AudioServicesCreateSystemSoundID((URL(fileURLWithPath: soundFile) as! CFURL), &soundID)
                    AudioServicesPlaySystemSound(soundID)
                    
                    
                    JCNotificationCenter.shared().presenter = JCNotificationBannerPresenterIOS7Style();
                    JCNotificationCenter.enqueueNotification(withTitle: titleMessage, message: bodyMessage, tapHandler: {
                        print("Received tap on notification banner!");
                        self.executionOfPushNotification();
                    })
                    
                }
            }
            
        }
        else if(application.applicationState == .background || application.applicationState == .inactive){
            self.executionOfPushNotification();
        }
    }
    
    func executionOfPushNotification(){
        
        let defaults = UserDefaults.standard;
        
        if(defaults.object(forKey: "UserData") != nil){
            if(defaults.object(forKey: "pushNotificationData") != nil){
                let pushData = defaults.object(forKey: "pushNotificationData") as! NSDictionary;
                
                let notificationType = pushData.value(forKey: "type") as? String;
                
                if(notificationType == "deal"){
                    self.setUpForCustomerAndVisitorTabs()
                }
                else if(notificationType == "notification"){
                    self.setUpForCustomerAndVisitorTabs()
                }
            }
        }
        //defaults.setValue(userInfo["aps"], forKey: "pushNotificationData");
        //defaults.synchronize();
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func setUpForCustomerAndVisitorTabs(){
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let swRevealVC = storyboard.instantiateViewController(withIdentifier: "SWRevealVC") as! SubClassOfSWRevealVC
        let leftRearViewController = storyboard.instantiateViewController(withIdentifier: "CategroriesLeftMenuTVC");
        
        var newFrontController = UITabBarController();
        let defaults = UserDefaults.standard
    
        
        let globalUser = defaults.object(forKey: "globalUserType") as! String;
        
        if (globalUser == USER_TYPE_CUSTOMER || globalUser == USER_TYPE_VISITOR) {
            newFrontController = storyboard.instantiateViewController(withIdentifier: "CustomerAndVisitorTBC") as! CustomerAndVisitorTBC
        }
        else if (globalUser == USER_TYPE_BUSINESS || globalUser == USER_TYPE_BUSINESS_BRANCH_EMPLOYEE){
            newFrontController = storyboard.instantiateViewController(withIdentifier: "BusinessAndEmployeeTBC") as! BusinessAndEmployeeTBC
        }
        
        swRevealVC.setFront(newFrontController, animated: true);
        
        swRevealVC.setRear(leftRearViewController, animated: true);
        OperationQueue.main.addOperation {
            self.window?.rootViewController = swRevealVC;
            self.window?.makeKeyAndVisible()
            
            // If user get push notification // this will execute only for customer
            if(defaults.object(forKey: "pushNotificationData") != nil){
                let pushData = defaults.object(forKey: "pushNotificationData") as! NSDictionary;
                
                let notificationType = pushData.value(forKey: "type") as? String;
                
                if(notificationType == "deal"){
                    newFrontController.selectedIndex = 4;
                }
                else if(notificationType == "notification"){
                    newFrontController.selectedIndex = 0;
                }
            }
        }
    }
    
    func logoutFromTheSessionForAllUsers(){
        
        // Clear notification center all the notification
        UIApplication.shared.applicationIconBadgeNumber = 0;
        //UNUserNotificationCenter.removeAllPendingNotificationRequests()
        UIApplication.shared.cancelAllLocalNotifications();
        
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "rememberUserEmailId");
        defaults.removeObject(forKey: "rememberUserPassword");
        defaults.removeObject(forKey: "globalUserType");
        defaults.removeObject(forKey: "UserData");
        defaults.synchronize();

        //[self.navigationController popToRootViewControllerAnimated:YES];
        //Override point for customization after application launch.
        //NSLog(@"--- %@", self.navigationController.childViewControllers);
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navController = storyboard.instantiateViewController(withIdentifier: "LaunchController") as! UINavigationController
        
        self.window!.rootViewController = navController;
    }
    
    /*
    // MARK:- Custom Functions
    func moveToSetUpMainController(){
     
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let swRevealVC = storyboard.instantiateViewControllerWithIdentifier("SWRevealVC") as! SWRevealViewController
        let leftRearViewController = storyboard.instantiateViewControllerWithIdentifier("LeftMenuTVC");
        
        var newFrontController = UIViewController();
        
        var frontViewController = UINavigationController();
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        // If user logged in first time
        if (defaults.objectForKey("isUserLoggedFirstTime") == nil) {
            defaults.setObject("Y", forKey: "isUserLoggedFirstTime") // After first login set this variable
            newFrontController = storyboard.instantiateViewControllerWithIdentifier("UserDetailVC")
        }
        else{
            newFrontController = storyboard.instantiateViewControllerWithIdentifier("DashBoardVC") as! DashBoardVC
        }
        
        frontViewController = UINavigationController.init(rootViewController: newFrontController)
        
        swRevealVC.setFrontViewController(frontViewController, animated: true);
        
        swRevealVC.setRearViewController(leftRearViewController, animated: true);
        NSOperationQueue.mainQueue().addOperationWithBlock {
            self.window?.rootViewController = swRevealVC;
        }
        
    }
 */
}

