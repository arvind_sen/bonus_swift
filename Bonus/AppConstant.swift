//
//  AppConstant.swift
//  Bonus
//
//  Created by Arvind Sen on 24/08/16.
//  Copyright © 2016 Bhumati. All rights reserved.
//

import UIKit

// App level constant
let K_CUSTOMER_CARE_EMAIL_ID = "customercare@bonusapp.net"
let K_DATABASE_FILE = "BonusDB.sqlite"
let kWEB_SERVICES_API_KEY = "BT4oqNhWcuDysWQmRm@TpkKPH4bIGff" // THIS IS REQUIRED TO POST WITH EVERY API CALL

// Development web service url
//let WEB_SERVICE_BASE_URL = "http://dev.bonusapp.net/api/"
//let kWEB_SHARING_URL = "http://dev.bonusapp.net/lists/get_deal_url"
//let kTERMS_OF_USE = "http://dev.bonusapp.net/pages/terms_of_use"

// Live web service url
let WEB_SERVICE_BASE_URL = "http://www.bonusapp.net/api/"
let kWEB_SHARING_URL = "http://www.bonusapp.net/lists/get_deal_url"
let kTERMS_OF_USE = "http://www.bonusapp.net/pages/terms_of_use"

// Azure web service url
//let WEB_SERVICE_BASE_URL = "http://test-new-upload.azurewebsites.net/api/"
//let kWEB_SHARING_URL = "http://test-new-upload.azurewebsites.net/lists/get_deal_url"
//let kTERMS_OF_USE = "http://test-new-upload.azurewebsites.net/pages/terms_of_use"


let kSERVICE_COUNTRY_LIST = WEB_SERVICE_BASE_URL+"lists/getAllCountries"
let kSERVICE_CATEGORIES = WEB_SERVICE_BASE_URL+"lists/getAllCategories"
let kSERVICE_SELECT_TIME_ZONE_LIST = WEB_SERVICE_BASE_URL+"lists/selectTimeZoneList"
let kSERVICE_INVITES_LIST = WEB_SERVICE_BASE_URL+"lists/getInvitesList"
let kSERVICE_LOGOUT_PREVIOUS_SESSION = WEB_SERVICE_BASE_URL+"users/logoutPreviousSession"
let kSERVICE_USER_RESEND_ACTIVATION_LINK = WEB_SERVICE_BASE_URL+"users/UserResendActivationLink"
let kSERVICE_LOGIN = WEB_SERVICE_BASE_URL+"users/UserLogin"
let kSERVICE_REGISTRATION = WEB_SERVICE_BASE_URL+"users/UserRegistration"
let kSERVICE_FORGOT_PASSWORD = WEB_SERVICE_BASE_URL+"users/UserForgotPassword"
let kSERVICE_SAVE_BUSINESS_INFO =  WEB_SERVICE_BASE_URL+"businesses/SaveBusinessInfo"
let kSERVICE_BRANCH_LIST_OF_A_BUSINESS = WEB_SERVICE_BASE_URL+"branches/BranchListOfABusiness"
let kSERVICE_SAVE_BRANCH_FOR_A_BUSINESS = WEB_SERVICE_BASE_URL+"branches/SaveBranchForABusiness"
let kSERVICE_DELETE_BRANCH_FOR_A_BUSINESS = WEB_SERVICE_BASE_URL+"branches/DeleteBranchForABusiness"
let kSERVICE_MY_COUPONS_LIST_OF_A_BUSINESS = WEB_SERVICE_BASE_URL+"deals/MyCouponsListOfABusiness"
let kSERVICE_SAVE_COUPON_DETAILS_FOR_A_BUSINESS = WEB_SERVICE_BASE_URL+"deals/SaveCouponDetailsForABusiness"
let kSERVICE_DELETE_A_COUPON_DETAIL = WEB_SERVICE_BASE_URL+"deals/DeleteCouponDetailsForABusiness"
let kSERVICE_BUSINESS_LIST = WEB_SERVICE_BASE_URL+"businesses/BusinessList"
let kSERVICE_BRANCH_DEALS = WEB_SERVICE_BASE_URL+"deals/BranchDeals"
let kSERVICE_DEAL_REVIEW =  WEB_SERVICE_BASE_URL+"deals/DealReview"
let kSERVICE_SAVE_USER_PERSONALINFO =  WEB_SERVICE_BASE_URL+"customers/SaveUserPersonalInfo"
let kSERVICE_SAVE_DEAL_REVIEW = WEB_SERVICE_BASE_URL+"deals/SaveDealReview"
let kSERVICE_USER_AVAILABLE_CREDIT = WEB_SERVICE_BASE_URL+"customers/UserAvilableCredit"
let kSERVICE_BUY_COUPONS = WEB_SERVICE_BASE_URL+"deals/BuyCoupon"
let  kSERVICE_REDEEM_COUPONS = WEB_SERVICE_BASE_URL+"deals/RedeemCoupon"
let kSERVICE_FAVOURITE_A_DEAL = WEB_SERVICE_BASE_URL+"deals/FavoriteADeal"
let kSERVICE_GIFT_COUPON_TO_FRIEND = WEB_SERVICE_BASE_URL+"deals/GiftCouponToFriend"
let kSERVICE_CUSTOMER_COUPONS = WEB_SERVICE_BASE_URL+"deals/CustomerCoupons"
let kSERVICE_INVITATION_TEMPLATE = WEB_SERVICE_BASE_URL+"users/InviteHtmlTemplate"
let kSERVICE_GET_INVITES_LIST_NOT_ACCEPTED = WEB_SERVICE_BASE_URL+"lists/getInvitesListNotAccepted"
let kSERVICE_DEACTIVATE_A_BUSINESS_ACCOUNT = WEB_SERVICE_BASE_URL+"businesses/DeactivateABusinessAccount"
let kSERVICE_ACTIVATE_A_BUSINESS_ACCOUNT = WEB_SERVICE_BASE_URL+"businesses/ActivateBusinessAccount"
let kSERVICE_USER_INVITATIONS = WEB_SERVICE_BASE_URL+"users/UserInvitations"
let kSERVICE_MULTI_USERS_INVITATIONS = WEB_SERVICE_BASE_URL+"users/MultiUserInvitations"
let kSERVICE_RESEND_USER_INVITATIONS = WEB_SERVICE_BASE_URL+"users/ResendUserInvitations"
let kSERVICE_HTML_CONTENT = WEB_SERVICE_BASE_URL+"users/StaticHtmlContent"
let kSERVICE_SELECT_LOYALTY = WEB_SERVICE_BASE_URL+"businesses/SelectLoyalty"
let kSERVICE_ADD_LOYALTY = WEB_SERVICE_BASE_URL+"businesses/AddLoyalty"
let kSERVICE_NEAREST_PUNCH_CARD_BUSINESS_LIST = WEB_SERVICE_BASE_URL+"customers/NearestPunchCardBusinessList"
let kSERVICE_ALLOT_A_PUNCH_CARD = WEB_SERVICE_BASE_URL+"customers/AllotAPunchCardToACustomer"
let kSERVICE_ALLOT_LOYALTY_FREE_BIES_TO_CUSTOMER = WEB_SERVICE_BASE_URL+"customers/AllotLoyaltyFeeBiesToACustomer"
let kSERVICE_SEND_BUSINESS_NOTIFICATION = WEB_SERVICE_BASE_URL+"businesses/SendBusinessNotificationToCustomer"
let kSERVICE_BUSINESS_USER_NOTIFICATION_LIST = WEB_SERVICE_BASE_URL+"customers/BusinessNotificationsList"
let kSERVICE_USER_LOAD_CREDIT = WEB_SERVICE_BASE_URL+"customers/UserLoadCredit"
let kSERVICE_GET_DEAL_CONVERTED_RATE = WEB_SERVICE_BASE_URL+"customers/GetDealPriceConveratedRate"
let kSERVICE_GET_TOP_EARNERS =  WEB_SERVICE_BASE_URL+"customers/TopEarners"
let kSERVICE_VERIFY_PAYPAL_PAYMENT =  WEB_SERVICE_BASE_URL+"customers/VerifyPaypalPayment"

let kSERVICE_LOGOUT = WEB_SERVICE_BASE_URL+"users/Logout"
let kSERVICE_GET_WALLET_TRANSECTION = WEB_SERVICE_BASE_URL+"customers/getWalletTransection"
let kSERVICE_SELECT_USER_PUNCH_CARD_DETAILS = WEB_SERVICE_BASE_URL+"customers/selectUserPunchCardDetails"
let kSERVICE_USER_TOTAL_EARNING_DETAIL = WEB_SERVICE_BASE_URL+"customers/userTotalEarningDetails"
let kSERVICE_USER_NOTIFICATION_LIST = WEB_SERVICE_BASE_URL+"customers/UserNotificationsList"
let kSERVICE_GET_USER_MONEY_DETAIL = WEB_SERVICE_BASE_URL+"customers/GetUserMoneyDetail"

let kSERVICE_LOYALTY_CHECK_IN_BUSINESSES = WEB_SERVICE_BASE_URL+"loyaltyCheckInBusinesses"
let kSERVICE_SEND_FEEDBACK = WEB_SERVICE_BASE_URL+"users/SendFeedback"

let kSERVICE_SHARE_COUPONS = WEB_SERVICE_BASE_URL+"deals/ShareCoupons"
let kSERVICE_WITHDRAW_WALLET_AMOUNT = WEB_SERVICE_BASE_URL+"customers/WithdrawWalletAmount"
let kSERVICE_GET_USER_BANK_DETAIL = WEB_SERVICE_BASE_URL+"customers/GetUserBankDetail"
let kSERVICE_USER_SAVE_WITHDRWAL_REQUEST = WEB_SERVICE_BASE_URL+"customers/UserSaveWithdrawlRequest"
let kSERVICE_GET_USER_WITHDRWAL_REQUEST_LIST = WEB_SERVICE_BASE_URL+"customers/GetUserWithdrawlRequestList"

let kSERVICE_PAYFORT_CHARGE = WEB_SERVICE_BASE_URL+"payforts/charge"



// Images name using in app.
let K_RIGHT_YELLOW_EMPTY = UIImage.init(named: "RightYellowEmpty")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
let K_RIGHT_YELLOW = UIImage.init(named: "RightYellow")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
let K_GRAY_RIGHT_EMPTY = UIImage.init(named: "GrayRightEmpty")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal);

// Images name using in app.
let kIMAGE_ACTIVE_COUPONS_WHITE = UIImage.init(named: "ActiveCouponsWhiteWTxt")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
let kIMAGE_ACTIVE_COUPONS_YELLOW = UIImage.init(named: "ActiveCouponsYellowWTxt")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
let kIMAGE_MONEY_WHITE = UIImage.init(named: "MoneyWhiteWTxt")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
let kIMAGE_MONEY_YELLOW = UIImage.init(named: "MoneyYellowWTxt")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
let kIMAGE_COUPONS_HISTORY_WHITE = UIImage.init(named: "CouponsHistoryWhiteWTxt")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
let kIMAGE_COUPONS_HISTORY_YELLOW = UIImage.init(named: "CouponsHistoryYellowWTxt")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
let kIMAGE_EXPIRED_COUPON = UIImage.init(named: "Expired")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
let kIMAGE_RIGHT_CHECK_MARK = UIImage.init(named: "RightWhiteCircleBig")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)

//let K_RIGHT_YELLOW : UIImage = UIImage(named: "RightYellow")!
//let K_RIGHT_YELLOW_EMPTY : UIImage = UIImage(named: "RightYellowEmpty")!

let kDATE_FORMAT = "dd MMM yyyy"
let kSTANDARD_DATE_FORMAT = "yyyy-MM-dd"

// Constant key
let K_STR_DEVICE_TOKEN = "DeviceToken"
let kKEY_PASSWORD_CONCAT_STRING = "bonus@123"
let USER_TYPE_VISITOR = "visitor"
let USER_TYPE_BUSINESS = "bUser"
let USER_TYPE_CUSTOMER = "customer"
let USER_TYPE_BUSINESS_BRANCH_EMPLOYEE = "branchEmployee"
let USER_TYPE_BONUS_ADMIN = "bonusAdmin"


// Different Messages
let LOGIN_ALERT_MESSAGE_EMAIL_ID = "Please enter a valid Email address"
let LOGIN_ALERT_MESSAGE_EMPTY_PASSWORD = "Please enter a valid password"
let PLEASE_ENTER_ATLEAST_SIX_CHARACTORS = "Please enter a Password containing at least 6 and max 15 characters"
let kMESSAGE_EXPLAIN_YOUR_PUNCH_CARD_POLICY = "Explain your punch card policy..."
let AFTER_DEACTIVATE_BUSINESS_ACCOUNT_MESSAGE = "Sorry..!You cannot perform this action, your account is in deactivate state."

// Constant colors
let K_COLOR_TRANSPARNT = UIColor.init(red: 10/255.0, green: 10/255.0, blue: 10/255.0, alpha: 0.5);
let TAB_BAR_BG_COLOR = UIColor.init(red: 73/255.0, green: 73/255.0, blue: 73/255.0, alpha: 1);
let ICONS_TINT_COLOR_YELLOW = UIColor.init(red: 227/255.0, green: 168/255.0, blue: 26/255.0, alpha: 1);
let ICONS_TINT_COLOR_WHITE = UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1);
let TEXT_FIELD_BORDER_COLOR = UIColor.init(red: 224/255.0, green: 224/255.0, blue: 224/255.0, alpha: 1);
let K_LISTING_VIEW_BG_COLOR  = UIColor.init(red: 220/255.0, green: 220/255.0, blue: 220/255.0, alpha: 1)
let BLUE_BG_COLOR  = UIColor.init(red:38/255.0, green: 169/255.0, blue: 224/255.0, alpha: 1)

let GRAY_CUSTOM_BAR_AND_SEPRATOR_BG_COLOR = UIColor.init(red: 154/255.0, green: 154/255.0, blue: 154/255.0, alpha: 1)
let COLOR_GREEN_BG = UIColor(colorLiteralRed: 71/255, green: 186/255, blue: 51/255, alpha: 1);
let LIGHT_GRAY_COLOR_BG_COLOR = UIColor(colorLiteralRed: 190/255, green: 190/255, blue: 190/255, alpha: 1);

let YELLOW_BG_COLOR = UIColor.init(red: 244/255.0, green: 200/255.0, blue: 17/255.0, alpha: 1)


let K_EMPTY_TABLE_BG_COLOR = UIColor.init(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1)

//[UIColor colorWithRed:244/255.0f green:200/255.0f blue:17/255.0f alpha:1]

let K_PLACE_HOLDER_IMAGE = UIImage(named: "EmptyPicture")
let K_DEAL_PLACE_HOLDER_IMAGE = UIImage(named: "CouponImagePlaceHolder")
let K_BONUS_MAP_PIN_IMAGE = UIImage(named: "MapPin")
let K_BONUS_POT_IMAGE = UIImage(named: "MapIconYellow")
let K_FAVIROUTE_IMAGE = UIImage(named: "FavoriteMapPin")
let K_YELLOW_CIRCLE_SMALL_IMAGE = UIImage(named: "YellowCircleSmall");


let K_TABLE_ROW_LIMITS = "10"
let ANIMATION_DURATION_VIEW_SLIDING = 0.4
let CURRENT_DEVICE_SIZE = UIScreen.main.bounds.size

protocol DismissPopupVCProtocol: class {
    func dismissPopupViewController(_ vc:UIViewController, responseObj: AnyObject?);
}

protocol DismissSelfViewControllerDelegate{
    func dismissSelfViewControllerFromSuperViewController(_ vc:UIViewController, responseObj: AnyObject?);
}

enum SelectionFor {
    case isSelectedFavorite, isSelectedNearBy, isSelectedAToZ,isSelectedProductGroup
}

class AppConstant: NSObject {

}
